# Microservicios externos para interconectividad

## **Indice**

- [Microservicios](#microservicios)
  - [Ver Desarrolladores](#ver-desarrolladores)
  - [Ver Juegos](#ver-juegos)
  - [Ver Juegos (Específicos)](#ver-juegos-específicos)
  - [Compra](#compra)
  - [Devolucion](#devolución)

# Microservicios

## Ver Desarrolladores

Este microservicio permite a otros distribuidores de videojuegos obtener los desarrolladores que trabajan con los juegos que distribuye Obtic Games.

### **Ruta:**

http://obtic-shared-api.fiusapp.com/developers/


### **Método:**

GET

### **Formato de entrada:**

Ninguna entrada

### **Formato de salida:**

JSON

### **Códigos de respuesta:**

| Código | Definición |
| - | - |
| HTTP 200 | Respuesta exitosa del servidor |
| HTTP 404 | Hubo un error en la consulta y no se encontraron desarrolladores |

### **Atributos de salida:**

| Atributo | Tipo | Definición |
| - | - | - |
| success | boolean | Estado de la respuesta del servidor a la consulta |
| data | array | Listado de objetos (desarrolladores) que se encuentran registrados dentro de Obtic Games |

### **Ejemplos de salida:**

Respuesta exitosa

```
{
    "success": true,
    "data": [
        {
            "developerId": 1,
            "name": "Developer 1",
            "image": "http://obtic.fiusapp.com/imagedev1.jpg"
        },
        {
            "developerId": 2,
            "name": "Developer 2",
            "image": "http://obtic.fiusapp.com/imagedev2.jpg"
        }
    ]
}
```

Respuesta fallida

```
{
    "success": false,
    "data": []
}
```

<br>

<br>

---

## Ver Juegos

Este microservicio permite a otros distribuidores de videojuegos obtener los juegos que distribuye Obtic Games.

### **Ruta:**

http://obtic-shared-api.fiusapp.com/games/


### **Método:**

GET

### **Formato de entrada:**

Headers

### **Atributos de entrada (headers):**

| Atributo | Tipo | Definición |
| - | - | - |
| ip | string | Dirección IP del usuario que consulta los juegos |

### **Formato de salida:**

JSON

### **Códigos de respuesta:**

| Código | Definición |
| - | - |
| HTTP 200 | Respuesta exitosa del servidor |
| HTTP 404 | Hubo un error en la consulta y no se encontraron juegos |

### **Atributos de salida:**

| Atributo | Tipo | Definición |
| - | - | - |
| success | boolean | Estado de la respuesta del servidor a la consulta |
| data | array | Listado de objetos (juegos) que se encuentran registrados dentro de Obtic Games |

### **Ejemplos de salida:**

Respuesta exitosa

```
{
    "success": true,
    "data": [
        {
            "gameId": 1,
            "name": "Game 1",
            "image": "http://obtic.fiusapp.com/game1",
            "description": "Description of game 1",
            "releaseDate": "26/10/2022",
            "restrictionAge": "E",
            "price": 200,
            "discount": 30,
            "group": 1,
            "developer": [
                {
                    "developerId": 1,
                    "name": "Developer 1",
                    "image": "http://obtic.fiusapp.com/dev1.jpg"
                }
            ],
            "category": ["Accion", "Multijugador", "Crimen"]
        }
    ]
}
```

Respuesta fallida

```
{
    "success": false,
    "data": []
}
```

<br>

<br>

---

## Ver Juegos (Específicos)

Este microservicio permite a otros distribuidores de videojuegos obtener los juegos que distribuye Obtic Games de forma específica con una lista.

### **Ruta:**

http://obtic-shared-api.fiusapp.com/games/


### **Método:**

POST

### **Formato de entrada:**

Headers, JSON

### **Atributos de entrada (headers):**

| Atributo | Tipo | Definición |
| - | - | - |
| ip | string | Dirección IP del usuario que consulta los juegos |

### **Atributos de entrada (body):**

| Atributo | Tipo | Definición |
| - | - | - |
| games | array | Lista de identificadores para los juegos que se necesitan obtener |

### **Ejemplo de entrada:**

Body

```
{
    "games": [1]
}
```

### **Formato de salida:**

JSON

### **Códigos de respuesta:**

| Código | Definición |
| - | - |
| HTTP 200 | Respuesta exitosa del servidor |
| HTTP 404 | Hubo un error en la consulta y no se encontraron juegos |

### **Atributos de salida:**

| Atributo | Tipo | Definición |
| - | - | - |
| success | boolean | Estado de la respuesta del servidor a la consulta |
| data | array | Listado de objetos (juegos) que se encuentran registrados dentro de Obtic Games |

### **Ejemplos de salida:**

Respuesta exitosa

```
{
    "success": true,
    "data": [
        {
            "gameId": 1,
            "name": "Game 1",
            "image": "http://obtic.fiusapp.com/game1",
            "description": "Description of game 1",
            "releaseDate": "26/10/2022",
            "restrictionAge": "E",
            "price": 200,
            "discount": 30,
            "group": 1,
            "developer": [
                {
                    "developerId": 1,
                    "name": "Developer 1",
                    "image": "http://obtic.fiusapp.com/dev1.jpg"
                }
            ],
            "category": ["Accion", "Multijugador", "Crimen"]
        }
    ]
}
```

Respuesta fallida

```
{
    "success": false,
    "data": []
}
```

<br>

<br>

---

## Compra

Este microservicio permite a otros distribuidores de videojuegos comprar los juegos que distribuye Obtic Games.

### **Ruta:**

http://obtic-shared-api.fiusapp.com/shopping/


### **Método:**

POST

### **Formato de entrada:**

Authorization, JSON

### **Atributos de entrada (authorization):**

| Atributo | Tipo | Definición |
| - | - | - |
| Bearer token | string | Token del usuario con un payload con información sobre el mismo |

### **Atributos de entrada (body):**

| Atributo | Tipo | Definición |
| - | - | - |
| games | array | Lista de identificadores para los juegos que se necesitan comprar |

### **Ejemplos de entrada:**

Authorization

```
Bearer token:
"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjoxLCJlbWFpbCI6IjMzNzA5ODEzMTA5MjFAaW5nZW5pZXJpYS51c2FjLmVkdS5ndCIsInVzZXJuYW1lIjoiZmVybmFuZHYiLCJpZF91c2VyX3R5cGUiOjIsImlhdCI6MTY2NjkzMjY3MSwiZXhwIjoxNjY2OTM5ODcxfQ.X20Zd4SmtxgHgwRRfdtxzmAejeoKVT7SfjGRc5Y6Ejg"

Payload del token:
{
    "group": 1,
    "userId": 1,
    "email": "user1@gmail.com",
    "name": "User 1",
    "region": "Guatemala",
    "ip": "190.56.117.222"
}
```

Body

```
{
    "games": [1, 2, 3]
}
```

### **Formato de salida:**

JSON

### **Códigos de respuesta:**

| Código | Definición |
| - | - |
| HTTP 200 | Respuesta exitosa del servidor |
| HTTP 404 | Hubo un error en la consulta y no se pudieron comprar los juegos |

### **Atributos de salida:**

| Atributo | Tipo | Definición |
| - | - | - |
| success | boolean | Estado de la respuesta del servidor a la consulta |
| data | array | En caso de error, se proveerá este atributo para colocar la lista de mensajes de error para cada juego que se intentó comprar |

### **Ejemplos de salida:**

Respuesta exitosa

```
{
    "success": true
}
```

Respuesta fallida

```
{
    "success": false,
    "data": [
        {
            "message": "El juego no está disponible para la región"
        },
        {
            "message": "El juego no está disponible para la región"
        },
        {
            "message": "El juego no está disponible para la región"
        }
    ]
}
```

<br>

<br>

---

## Devolución

Este microservicio permite a otros distribuidores de videojuegos devolver los juegos que distribuye Obtic Games.

### **Ruta:**

http://obtic-shared-api.fiusapp.com/devolution/


### **Método:**

POST

### **Formato de entrada:**

Authorization, JSON

### **Atributos de entrada (authorization):**

| Atributo | Tipo | Definición |
| - | - | - |
| Bearer token | string | Token del usuario con un payload con información sobre el mismo |

### **Atributos de entrada (body):**

| Atributo | Tipo | Definición |
| - | - | - |
| game | integer | Identificador del juego que se desea devolver |
| message | string | Razón por la cual se devuelve el juego |

### **Ejemplos de entrada:**

Authorization

```
Bearer token:
"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjoxLCJlbWFpbCI6IjMzNzA5ODEzMTA5MjFAaW5nZW5pZXJpYS51c2FjLmVkdS5ndCIsInVzZXJuYW1lIjoiZmVybmFuZHYiLCJpZF91c2VyX3R5cGUiOjIsImlhdCI6MTY2NjkzMjY3MSwiZXhwIjoxNjY2OTM5ODcxfQ.X20Zd4SmtxgHgwRRfdtxzmAejeoKVT7SfjGRc5Y6Ejg"

Payload del token:
{
    "group": 1,
    "userId": 1,
    "email": "user1@gmail.com",
    "name": "User 1",
    "region": "Guatemala",
    "ip": "190.56.117.222"
}
```

Body

```
{
    "game": 1,
    "message": "Es demasiado costoso"
}
```

### **Formato de salida:**

JSON

### **Códigos de respuesta:**

| Código | Definición |
| - | - |
| HTTP 200 | Respuesta exitosa del servidor |
| HTTP 404 | Hubo un error en la consulta y no se pudo devolver el juego |

### **Atributos de salida:**

| Atributo | Tipo | Definición |
| - | - | - |
| success | boolean | Estado de la respuesta del servidor a la consulta |
| message | string | En caso de error, se proveerá este atributo para colocar el mensaje de error que devuelve el servidor |

### **Ejemplos de salida:**

Respuesta exitosa

```
{
    "success": true
}
```

Respuesta fallida

```
{
    "status": false,
    "message": "El juego no se encuentra disponible en la región"
}
```
