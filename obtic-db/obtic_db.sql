CREATE TABLE user_type(
    id_user_type INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    type_name VARCHAR(20)
);

CREATE TABLE "user"(
    id_user INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    username VARCHAR(25) UNIQUE,
    email VARCHAR(130) UNIQUE,
    name VARCHAR(25) NOT NULL,
    last_name VARCHAR(25) NOT NULL,
    password VARCHAR(64) NOT NULL,
    profile_image TEXT,
    registration_date TIMESTAMP DEFAULT NOW(),
    active BOOLEAN DEFAULT true,
    confirmed BOOLEAN DEFAULT false,
    id_user_type INT,
    FOREIGN KEY (id_user_type) REFERENCES user_type(id_user_type)
);

CREATE TABLE confirmation(
    id_confirmation INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    code VARCHAR(6) NOT NULL,
    generated_date TIMESTAMP DEFAULT NOW(),
    confirmed_date TIMESTAMP,
    id_user INT,
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE shopping_cart(
    id_shopping_cart INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT NOT NULL,
    active BOOLEAN DEFAULT true,
    opening_date TIMESTAMP DEFAULT NOW(),
    closing_date TIMESTAMP NULL,
    total DECIMAL(8, 2) NULL,
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE detail_external_shopping_cart(
    id_detail_external_shopping_cart INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_shopping_cart INT NOT NULL,
    final_purchase_price DECIMAL(8, 2),
    "group" INT,
    id_product INT,
    added_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (id_shopping_cart) REFERENCES shopping_cart(id_shopping_cart)
);

CREATE TABLE external_library(
    id_external_library INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT NOT NULL,
    "group" INT,
    id_product INT,
    added_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE developer(
    id_developer INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    developer_name VARCHAR(50),
    registration_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE category(
    id_category INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    category_name VARCHAR(50),
    registration_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE product(
    id_product INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    product_name VARCHAR(100) NOT NULL,
    description TEXT,
    short_description VARCHAR(150),
    release_date TIMESTAMP,
    registration_date TIMESTAMP DEFAULT NOW(),
    active BOOLEAN DEFAULT true,
    desktop_image TEXT,
    mobile_image TEXT,
    logo TEXT,
    age_restriction VARCHAR(5),
    is_game BOOLEAN NOT NULL,
    parent_game INT
);

ALTER TABLE
    product
ADD
    CONSTRAINT parent_game_fk FOREIGN KEY (parent_game) REFERENCES product(id_product);

CREATE TABLE external_sale(
    id_external_sale INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_product INT NOT NULL,
    "group" INT,
    id_external_user INT,
    sale_price DECIMAL(8, 2),
    sale_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY(id_product) REFERENCES product(id_product)
);

CREATE TABLE product_image(
    id_product_image INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_product INT NOT NULL,
    url_image TEXT,
    is_image BOOLEAN DEFAULT true,
    FOREIGN KEY(id_product) REFERENCES product(id_product)
);

CREATE TABLE developer_product(
    id_developer INT,
    id_product INT,
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY(id_developer) REFERENCES developer(id_developer)
);

CREATE TABLE category_game(
    id_category INT,
    id_product INT,
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY(id_category) REFERENCES category(id_category)
);

CREATE TABLE region(
    id_region INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    region VARCHAR(100)
);

CREATE TABLE region_restriction(
    id_region INT,
    id_product INT,
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY(id_region) REFERENCES region(id_region)
);

CREATE TABLE price(
    id_price INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    active BOOLEAN DEFAULT true,
    price DECIMAL(8, 2),
    discount DECIMAL(8, 2),
    id_product INT NOT NULL,
    by_region BOOLEAN,
    FOREIGN KEY(id_product) REFERENCES product(id_product)
);

CREATE TABLE price_region(
    id_region INT,
    id_price INT,
    FOREIGN KEY(id_price) REFERENCES price(id_price),
    FOREIGN KEY(id_region) REFERENCES region(id_region)
);

CREATE TABLE wish_list(
    id_wish_list INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT NOT NULL,
    id_product INT NOT NULL,
    added_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE library(
    id_library INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT NOT NULL,
    id_product INT NOT NULL,
    added_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE detail_shopping_cart(
    id_detail_shopping_cart INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_shopping_cart INT NOT NULL,
    id_product INT NOT NULL,
    added_date TIMESTAMP DEFAULT NOW(),
    final_price DECIMAL(8, 2) NULL,
    FOREIGN KEY(id_product) REFERENCES product(id_product),
    FOREIGN KEY (id_shopping_cart) REFERENCES shopping_cart(id_shopping_cart)
);

CREATE TABLE score(
    id_product INT NOT NULL,
    id_user INT NOT NULL,
    score SMALLINT,
    FOREIGN KEY (id_product) REFERENCES product(id_product),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user),
    UNIQUE(id_product, id_user)
)

CREATE TABLE recommended(
    id_recommended INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_product INT NOT NULL,
    id_user INT NOT NULL,
    FOREIGN KEY (id_product) REFERENCES product(id_product),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user),
    UNIQUE(id_product, id_user)
)

CREATE TABLE resolution_type(
    id_resolution_type INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name_type VARCHAR(20)
);

INSERT INTO resolution_type(name_type) VALUES
    ('Baja permanente'),
    ('Baja temporal'),
    ('Advertencia'),
    ('Sin efecto');

CREATE TABLE report(
    id_report INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    reporting_user INT NOT NULL,
    reported_user INT NOT NULL,
    reason TEXT,
    attended BOOLEAN DEFAULT false,
    resolution INT NULL,
    resolution_detail TEXT,
    creation_date TIMESTAMP DEFAULT NOW(),
    resolution_date TIMESTAMP NULL,
    FOREIGN KEY (reporting_user) REFERENCES "user"(id_user),
    FOREIGN KEY (reported_user) REFERENCES "user"(id_user),
    FOREIGN KEY (resolution) REFERENCES resolution_type(id_resolution_type)
);

CREATE TABLE credit_card(
    id_credit_card INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    alias VARCHAR(30),
    card_number VARCHAR(64) NOT NULL,
    cvv VARCHAR(64) NOT NULL,
    is_favorite BOOLEAN DEFAULT false,
    id_user INT NOT NULL,
    expiration_date VARCHAR(5) NOT NULL,
    last_digits VARCHAR(4) NOT NULL,
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

ALTER TABLE "user" ADD COLUMN suspension_date DATE DEFAULT NULL;

ALTER TABLE "user" ADD COLUMN wallet DECIMAL(10,2) DEFAULT 0;

CREATE TABLE movement_type(
    id_movement_type INT PRIMARY KEY,
    description_movement_type VARCHAR(100),
    is_credit BOOLEAN
);

INSERT INTO 
    movement_type
VALUES 
    (1, 'Venta', true),
    (2, 'Devolución', false),
    (3, 'Venta de juego externo', true),
    (4, 'Devolución de juego externo', false),
    (5, 'Venta desde plataforma externa', true),
    (6, 'Devolución desde plataforma externa', false),
    (7, 'Devolución de la plataforma de origen del juego', true),
    (8, 'Pago a la plataforma de origen del juego', false);

CREATE TABLE balance(
    id_balance INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT,
    id_group INT NULL,
    id_game INT,
    id_movement_type INT,
    is_credit boolean,
    amount DECIMAL(8, 2),
    external_platform INT NULL,
    movement_date TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (id_movement_type) REFERENCES movement_type(id_movement_type)
);

CREATE TABLE log_wallet_reason(
    id_log_wallet_reason INT PRIMARY KEY,
    description_log_wallet_reason VARCHAR(20),
    is_credit BOOLEAN
);

INSERT INTO 
    log_wallet_reason
VALUES
    (1, 'Compra de productos', false),
    (2, 'Devolución por producto', true);

CREATE TABLE log_wallet(
    id_log_wallet INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    amount DECIMAL(10, 2),
    is_credit BOOLEAN,
    id_log_wallet_reason INT,
    id_user INT,
    FOREIGN KEY (id_log_wallet_reason) REFERENCES log_wallet_reason(id_log_wallet_reason),
    FOREIGN KEY (id_user) REFERENCES "user"(id_user)
);

CREATE TABLE purchase_log(
    id_purchase_log INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT,
    id_group INT NULL,
    id_game INT,
    purchase_date TIMESTAMP DEFAULT NOW(),
    external_platform INT NULL,
    holding BOOLEAN DEFAULT TRUE
);

CREATE TABLE devolution_log(
    id_devolution_log INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id_user INT,
    id_group INT NULL,
    id_game INT,
    devolution_date TIMESTAMP DEFAULT NOW(),
    external_platform INT NULL,
    reason VARCHAR(150)
);

alter table purchase_log add column devolution_reason varchar(200) default null;
alter table purchase_log add column devolution_date date default null;

alter table detail_external_shopping_cart add column game_name VARCHAR(300) default null;
alter table detail_external_shopping_cart add column game_image TEXT default null;

alter table external_library add column game_name VARCHAR(300) default null;
alter table external_library add column game_image TEXT default null;