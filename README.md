## [**Registros de Bitácora**](./Bitacora.md)

## [**Comunicación de servicios para terceros**](./Interconectividad.md)

## **Indice**

- [Planteamiento Inicial](#planteamiento-inicial)
  - [Problema Inicial](#problema-inicial)
  - [Análisis](#análisis)
- [Solución](#solución)
  - [Casos de uso de Alto Nivel:](#casos-de-uso-de-alto-nivel)
  - [Diagrama de Arquitectura:](#diagrama-de-arquitectura)
  - [Herramientas de trabajo](#herramientas-de-trabajo)
    - [Herramienta de gestión de proyectos](#herramienta-de-gestión-de-proyectos)
    - [Plataforma de Servicios en la Nube](#plataforma-de-servicios-en-la-nube)
    - [Sistema de Manejo de Base de Datos (DBMS)](#sistema-de-manejo-de-base-de-datos-dbms)
    - [Frameworks para Desarrollo](#frameworks-para-desarrollo)
  - [Comunicación entre servicios](#comunicación-entre-servicios)
    - [Descripción de microservicios](#descripción-de-microservicios)
    - [Tipo de comunicación entre microservicios](#tipo-de-comunicación-entre-microservicios)
    - [Herramientas para la comunicación](#herramientas-para-la-comunicación)
    - [Diagrama de infraestructura](#diagrama-de-infraestructura)
  - [Base de datos](#base-de-datos)
    - [Manejo de usuarios](#manejo-de-usuarios)
    - [Manejo de juegos](#manejo-de-juegos)
    - [Logica de carrito](#logica-de-carrito)
  - [Lista de Microservicios](#lista-de-microservicios)
    - [Usuario](#usuario)
      - [**Registro**](#registro)
      - [**Generar OTP**](#generar-otp)
      - [**Confirm OTP**](#confirm-otp)
      - [**Inicio de sesión**](#inicio-de-sesión)
    - [Catálogo de juegos](#catálogo-de-juegos)
      - [**Ver Catalogo**](#ver-catalogo)
    - [Juegos](#juegos)
      - [**Creación de Juegos**](#creación-de-juegos)
    - [Compras](#compras)
      - [**Agregar a carrito**](#agregar-a-carrito)
      - [**Agregar externo a carrito**](#agregar-externo-a-carrito)
      - [**Eliminar elemento de carrito**](#eliminar-elemento-de-carrito)
      - [**Obtener carrito**](#obtener-carrito)
      - [**Comprar carrito**](#comprar-carrito)
    - [Lista de deseos](#lista-de-deseos)
      - [**Agregar**](#agregar)
      - [**Eliminar**](#eliminar)
      - [**Obtener**](#obtener)
    - [Contenido descargable](#contenido-descargable)
      - [**Crear DLC**](#crear-dlc)
      - [**Obtener DLCs**](#obtener-dlcs)
    - [Biblioteca de juegos](#biblioteca-de-juegos)
      - [**Ver juegos comprados**](#ver-juegos-comprados)
    - [Funciones administrativas](#funciones-administrativas)
      - [**Dar de baja usuario**](#dar-de-baja-usuario)
      - [**Crear descuentos**](#crear-descuentos)
  - [Versionamiento](#versionamiento)
  - [Documentación de los pipelines](#documentación-de-los-pipelines)
  - [Diagrama de Actividades](#diagrama-de-actividades)
    - [Regitro de Usuarios](#regitro-de-usuarios)
    - [Inicio de Sesión](#inicio-de-sesic3b3n-1)
    - [Agregar a lista de deseos](#agregar-a-lista-de-deseos)
    - [Agregar tarjeta](#agregar-tarjeta)
    - [Agregar al carrito](#agregar-al-carrito)
    - [Remover del carrito](#remover-del-carrito)
  - [Bitácora de Tareas](#bitácora-de-tareas)
  - [Metodología Ágil](#metodología-ágil)
  - [Descripción de la seguridad de la aplicación](#descripción-de-la-seguridad-de-la-aplicación)
  - [Comunicación de los servicios disponibles para todos los grupos](#comunicación-de-los-servicios-disponibles-para-todos-los-grupos)

# Planteamiento Inicial

## Problema Inicial

El problema que se debe resolver es la implementación completa de una plataforma de distribución digital de videojuegos de mediana escala, dentro de esta se deben brindar facilidades para los usuarios finales como el registro, la compra en categorías, contenido descargable o DLCs, bibliotecas, favoritos, catálogos, descuentos, lista de deseos, control de usuarios, interconectividad entre plataformas y un diseño atractivo que sea de interés de cara al usuario final que consuma el producto. Además de usuarios finales también se deben implementar algunas funcionalidades para usuarios administradores los cuales podrán tener el control total de la plataforma, la gestión de las ofertas y descuentos, la gestión de usuarios entre otras funcionalidades que se soliciten en un futuro.

## Análisis

La plataforma solicitada como tal se puede implementar a través de un modelo de tres capas si fuera una implementación sencilla, pero en este caso al tener diferentes funcionalidades dentro de la plataforma con una arquitectura monolítica si en algún momento se llega a saturar o dar de baja algún servicio, toda la implementación falla y los usuarios finales perderían el acceso total a los servicios de toda la plataforma, por este motivo una arquitectura básica monolítica no es suficiente para esta implementación, lo que nos lleva a el planteamiento de una arquitectura orientada en servicios (SOA), lo cual solventaría el problema de la interdependencia entre las funcionalidades que se necesitan, sin embargo, hay funcionalidades que sí dependen de un servicio de autenticación, por lo que una arquitectura SOA sería difícil implementarla completamente bajo las necesidades que se tienen en la plataforma.

Los microservicios son una herramienta de desarrollo de software moderno el cual se basa en la arquitectura orientada a servicios, sin embargo, su mayor diferencia es su alta compatibilidad con diferente software y su especialización para cada una de las funcionalidades solicitadas además de esto se pueden implementar modelos de mayor tamaño debido a que los microservicios son más granulares que una implementación SOA, por lo que es escalable con mayor facilidad y su diseño de solución encaja perfectamente con el problema que se necesita solventar.

Además de la arquitectura a utilizar también se necesita saber el lugar donde se gestionará en general la plataforma, la infraestructura que se utilizará. Hoy en día es muy frecuente el uso de plataformas en la nube debido a que es una manera más económica y eficiente de mantener los servicios de manera fácil, rápida y segura, por lo que la primera opción de infraestructura es justamente en la nube, más adelante en este documento se detallará la solución de manera más detallada, pero indiscutiblemente la mejor solución para una plataforma de consumo bajo demanda es una implementación en la nube.

# Solución

Para implementar esta plataforma en primer lugar se deben entender los flujos que se tendrán para luego desglosar a partir de ellos las funcionalidades necesarias para cada uno de los microservicios que se implementarán, para un mejor entendimiento de estas funcionalidades a un alto nivel se pueden considerar la información siguiente.

## Casos de uso de Alto Nivel:

|                      |                                                                                                                                                                                                                                                                                 |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                                                                                                                                                                                             |
| **Caso de uso:**     | Registro de usuario.                                                                                                                                                                                                                                                            |
| **Autores:**         | Usuario, Sistema.                                                                                                                                                                                                                                                               |
| **Precondiciones:**  | Ninguna.                                                                                                                                                                                                                                                                        |
| **Descripción:**     | El sistema deberá permitir que el usuario ingrese y envíe sus datos de forma segura, con esto se deberán almacenar los datos para que cuando el usuario ingrese nuevamente su sesión pueda ser validada enviando un correo de confirmación para validar la cuenta en 2 minutos. |
| **Postcondiciones:** | Los datos del usuario son guardados en base de datos de forma segura, o se necesita reenviar el correo electrónico de validación.                                                                                                                                               |
| **Tipo:**            | Primario                                                                                                                                                                                                                                                                        |

|                      |                                                                                                                                                                                                              |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **Versión:**         | 1.0                                                                                                                                                                                                          |
| **Caso de uso:**     | Inicio de sesión de usuario.                                                                                                                                                                                 |
| **Autores:**         | Usuario, Sistema.                                                                                                                                                                                            |
| **Precondiciones:**  | El usuario debe estar registrado con todos sus datos en el sistema.                                                                                                                                          |
| **Descripción:**     | El sistema deberá validar que los datos para el inicio de sesión del usuario coincidan con los datos almacenados para entrar en el sistema, si no solo podrá intentarse un máximo de 3 veces cada 5 minutos. |
| **Postcondiciones:** | El usuario puede entrar exitosamente dentro del sistema, o puede ingresar nuevamente sus datos para hacer otro intento.                                                                                      |
| **Tipo:**            | Primario                                                                                                                                                                                                     |

|                      |                                                                                                                                        |
| -------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                                                    |
| **Caso de uso:**     | Visualización de catálogo de juegos                                                                                                    |
| **Autores:**         | Usuario, Sistema.                                                                                                                      |
| **Precondiciones:**  | Ninguna.                                                                                                                               |
| **Descripción:**     | El sistema mostrará los datos de juegos, desarrolladores, etc. a cualquier usuario, pero no podrá hacer otro proceso sin autenticarse. |
| **Postcondiciones:** | El usuario puede ver el listado de juegos en la plataforma.                                                                            |
| **Tipo:**            | Primario                                                                                                                               |

|                      |                                                                                                                                                                                                         |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                                                                                                                     |
| **Caso de uso:**     | Selección de categorías                                                                                                                                                                                 |
| **Autores:**         | Usuario, Sistema.                                                                                                                                                                                       |
| **Precondiciones:**  | Ninguna.                                                                                                                                                                                                |
| **Descripción:**     | El sistema mostrará el catálogo el cual podrá ser filtrado por las categorías: más vendidos, género, desarrollador, recomendaciones y calificaciones. Si quiere hacer otro proceso deberá autenticarse. |
| **Postcondiciones:** | El usuario puede visualizar todos los juegos y filtrarlos por categorías, pero para otro proceso, debe autenticarse                                                                                     |
| **Tipo:**            | Primario                                                                                                                                                                                                |

|                      |                                                                                                                                                                                                   |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                                                                                                               |
| **Caso de uso:**     | Ingreso de juegos.                                                                                                                                                                                |
| **Autores:**         | Administrador, Sistema.                                                                                                                                                                           |
| **Precondiciones:**  | El administrador debe estar autenticado en el sistema como tal.                                                                                                                                   |
| **Descripción:**     | El administrador podrá ingresar los datos de un nuevo juego al sistema, además de indicar su clasificación por edad, desarrolladores, fecha de lanzamiento y restricción de descargas por región. |
| **Postcondiciones:** | El juego queda almacenado y publicado bajo todas las condiciones dadas.                                                                                                                           |
| **Tipo:**            | Primario                                                                                                                                                                                          |

|                      |                                                                                                                                             |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                                                         |
| **Caso de uso:**     | Compra de juegos                                                                                                                            |
| **Autores:**         | Usuario, Sistema.                                                                                                                           |
| **Precondiciones:**  | El usuario debe estar autenticado.                                                                                                          |
| **Descripción:**     | El usuario podrá agregar los juegos a un carrito de compras, y luego procederá a cancelarlo o realizar la compra con sus datos registrados. |
| **Postcondiciones:** | El usuario tiene acceso a los archivos del juego para poder adquirirlo y jugarlo, o cancela su pedido y regresa al menú principal           |
| **Tipo:**            | Primario                                                                                                                                    |

|                      |                                                                                          |
| -------------------- | ---------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                      |
| **Caso de uso:**     | Realización de promociones.                                                              |
| **Autores:**         | Administrador, Sistema.                                                                  |
| **Precondiciones:**  | El administrador debe estar autenticado en el sistema como tal.                          |
| **Descripción:**     | El administrador podrá agregar al sistema promociones a un juego por temporada y región. |
| **Postcondiciones:** | El juego se actualiza con la promoción con las condiciones dadas.                        |
| **Tipo:**            | Primario                                                                                 |

|                      |                                                                                                                                      |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| **Versión:**         | 1.0                                                                                                                                  |
| **Caso de uso:**     | Gestión de la lista de deseos                                                                                                        |
| **Autores:**         | Usuario, Sistema.                                                                                                                    |
| **Precondiciones:**  | El usuario debe estar autenticado.                                                                                                   |
| **Descripción:**     | El sistema permitirá al usuario que agregue diferentes juegos a una lista de deseos que no comprará actualmente pero son de interés. |
| **Postcondiciones:** | El sistema guarda las preferencias de juego de cada usuario.                                                                         |
| **Tipo:**            | Primario                                                                                                                             |

|                      |                                                                                                                |
| -------------------- | -------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                            |
| **Caso de uso:**     | Gestión de descargas DLC                                                                                       |
| **Autores:**         | Usuario, Sistema.                                                                                              |
| **Precondiciones:**  | El usuario debe estar autenticado.                                                                             |
| **Descripción:**     | El sistema permitirá al usuario que descargue contenidos de los juegos únicamente si el juego ha sido lanzado. |
| **Postcondiciones:** | La descarga se ejecuta exitosamente.                                                                           |
| **Tipo:**            | Primario                                                                                                       |

|                      |                                                                                                               |
| -------------------- | ------------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                           |
| **Caso de uso:**     | Gestión de biblioteca                                                                                         |
| **Autores:**         | Usuario, Sistema.                                                                                             |
| **Precondiciones:**  | El usuario debe estar autenticado.                                                                            |
| **Descripción:**     | El sistema permitirá al usuario ver las diferentes compras que se han realizado desde el inicio de la cuenta. |
| **Postcondiciones:** | El sistema guarda las compras de juegos de cada usuario.                                                      |
| **Tipo:**            | Primario                                                                                                      |

|                      |                                                                                                         |
| -------------------- | ------------------------------------------------------------------------------------------------------- |
| **Versión:**         | 1.0                                                                                                     |
| **Caso de uso:**     | Control de usuarios                                                                                     |
| **Autores:**         | Administrador, Sistema.                                                                                 |
| **Precondiciones:**  | El administrador debe estar autenticado en el sistema como tal.                                         |
| **Descripción:**     | El administrador podrá ver todos los usuarios que se crean y dar de baja usuarios que se han reportado. |
| **Postcondiciones:** | El sistema actualiza la información que el administrador gestione.                                      |
| **Tipo:**            | Primario                                                                                                |

Por lo que las funcionalidades a nivel general que se deben realizar se tienen descritas en cada uno de estos casos de uso para su posterior implementación en microservicios, ya sea completos o subdividiendo funcionalidades para aumentar la granularidad del proyecto.

## Diagrama de Arquitectura:

Debido a que se utilizará una arquitectura de microservicios por los motivos que se describieron en el análisis, la distribución que se le dará a los componentes es de suma importancia, el listado de microservicios que se implementará se detallará más adelante en el documento, pero su distribución dentro de una infraestructura y la división que se realizará para los casos de uso listados anteriormente puede entenderse con un diagrama de arquitectura.

![Arquitectura](./imgs/arquitectura.png)

En este diagrama de arquitectura se encuentra la distribución general de microservicios dentro de un entorno en la nube como se aclaró anteriormente para optimizar los recursos físicos que se necesitan para levantar la plataforma, dentro de este diagrama se pueden encontrar específicamente los siguientes elementos:

- 8 servicios generales a partir de los cuales se originan cada uno de los microservcios que se listarán posteriormente en este documento, los cuales satisfacen cada una de las funcionalidades solicitadas por el interesado.

- La implementación de la base de datos en el servicio Cloud SQL dentro de Google Cloud Platform, donde se estará implementando la base de datos en PostgreSQL como se detallara más adelante y teendrá conexión directa con cada uno de los microservicios programados a través de una conexión con VPC.

- Un servicio de balanceo de carga implementado con Nginx el cual comunica directamente cada uno de los microservicios para que puedan ser atendidos desde un mismo host, además de poder intercambiar información si en algún momento se necesitara.

- Un orquestador de contenedores para levantar toda esta arquitectura y a través de la cual se podrá acceder desde el cliente a todo el backend implementado en microservicios, luego se detallará más profundamente la infraestructura que se utilizará para la comunicación entre microservicios dentro del servicio de Kubernetes GKE.

- Un cliente el cual se encuentra ligado al backend en microservicios, siendo este el único recurso que puede acceder a la infraestructura de backend e interactuar con cada uno de los microservicios implementados, esto a través de algunas reglas de firewall implementadas en nube.

- Por último se tienen los hosts de plataformas de terceros los cuales están representados como servicios ajenos a los cuales se debe conectar el cliente y mediante el cual se gestionarán las integraciones con plataformas de otros desarrolladores en el futuro.

Con esto queda completamente definida la arquitectura de microservicios a utilizar además de la estructura interna que se tendrá y gestionará al momento de su implementación, este formato se seguira a lo largo de todo el desarrollo y se tendrá en cuenta posibles cambios para su mejora en el futuro.

## Herramientas de trabajo

Las herramientas que se utilizarán para el desarrollo de esta implementación esta definido y justificado en las descripciones que se comentarán más adelante.

### Herramienta de gestión de proyectos

En el mercado se tienen varias opciones para las herramientas que se pueden elegir para gestionar un proyecto, dentro de las más populares o comercialmente utilizadas se encuentran:

- **Trello:** Herramienta fácil y rápida para la generación de actividades y reportes, sin embargo, no posee variedad de gráficos que a veces son necesarios como el diagrama de Grantt, o la especificación de tareas en concreto.
- **JIRA:** La plataforma estándar para muchas empresas donde se pueden implementar varias metodologías ágiles como SCRUM y Kanban, además es fácil el seguimiento de errores y generación de tareas y reportes. Además de su integración con otras plataformas de desarrollo colaborativo como GitLab, Slack, etc.
- **Monday:** Una plataforma atractiva donde se pueden importar y personalizar plantillas, integrar documentos y se pueden programar notificaciones para su versión de móvil.

Con las tecnologías de integración continua y despliegue continuo (CI/CD) que se utilizará (GitLab) y las herramientas de comunicación entre integrantes del grupo, la mejor opción para gestionar el proyecto es JIRA, debido a que no carece de gráficos en sus reportes, posee integraciones con tecnologías colaborativas que se utilizarán no solo con GitLab, y además su interfaz es lo suficientemente intuitiva como para poder gestionar rápidamente las tareas y procesos de un proyecto en un tiempo determinado, además de que es compatible con la metodología ágil que se utilizará como se describirá más adelante en este documento. Por tanto, la elección de plataforma es clara, se utilizará **Atlassian JIRA**.

### Plataforma de Servicios en la Nube

Dentro de los servicios de nube disponibles para el despliegue de los componentes del proyecto se tienen tres candidatos fuertemente demandados:

- Amazon Web Services (AWS)
- Google Cloud Platform (GCP)
- Microsoft Azure

Cada una de las nubes ofrece diferentes beneficios con respecto a diferentes implementaciones, en este caso debido a la estructura definida en el diagrama mostrado la mejor forma de implementar es a través de un orquestador de contenedores, esto se debe combinar en conjunto con los proveedores de nube anteriormente mencionados, por lo que las posibles opciones como orquestadores de contenedores son los siguientes:

- Kubernetes
- Openshift
- Docker Swarm

Para los alcances de este proyecto una implementación en Openshift no sería viable, debido a que este tipo de implementaciones se trabajan a nivel empresarial pagando la licencia a Red Hat, por lo que no es viable en el corto y mediano plazo, luego con Docker Swarm no tienen implementaciones tan sencillas en las plataformas de nube que se mencionaron, por lo que la sinergia entre tecnologías a pesar de no ser imposible, lo vuelve una carga en trabajo y tiempo por lo que no es eficiente, por lo que la mejor opción es la utilización de Kubernetes como orquestador de contenedores, además de eso se debe comparar este orquestador en cada una de las plataformas que se describieron debido a que cada una de ellas tiene sus ventajas y desventajas por lo que se evaluarán a continuación.

- **Amazon Elastic Kubernetes Service (EKS):** Es la implementación que amazon tiene para la gestión del orquestador en su entorno en la nube, sin embargo este servicio es considerablemente nuevo en comparación de los competidores, a pesar de tener una fuerte demanda por empresas y sistemas implementados en EKS.
- **Azure Kubernetes (AKS):** Para la administración de contenedores, Azure comenzó como una plataforma independiente del orquestador, diferente a como se tiene hoy en día, por lo que implementó la integración con Kubernetes hasta finales del 2017 por lo que su demanda no es muy elevada.
- **Google Kubernetes Engine (GKE):** Este fue el primer servicio de Kubernetes disponible en el mercado, por lo que es ideal para escalar implementar y administrar aplicaciones de manera segura, además que el propio Kubernetes fue creado por propios ingenieros de Google, las integraciones en un futuro con GKE serán mejores que en el resto de plataformas.

Con la descripción general de cada una de estas integraciones de Kubernetes con cada una de las plataformas seleccionadas de alta demanda, la mejor con diferencia es **Google Kuberneetes Engine GKE**, esto debido a que son pioneros en el desarrollo de este tipo de tecnologías, además de que tienen mayor propensión a generar avances, actualizaciones y soporte en general a este servicio por tal razón, se utilizará GKE como infraestructura para el despliegue de los microservicios de la plataforma a través de otras tecnologías que también se detallarán más adelante.

### Sistema de Manejo de Base de Datos (DBMS)

El manejo de los datos es sumamente importante para plataformas de alto consumo como es el caso, se debe asegurar por completo la integridad de los datos y combinarlo con una buena eficiencia al momento de almacenar y consultar datos. En este caso como se trata de un sistema colaborativo entre plataformas, lo más recomendable sería utilizar una base de datos relacional, debido a que evitaría la redundancia de los datos cuando se realice una implementación a gran escala, dentro de las opciones posibles más comerciables se encuentran las siguientes:

- Microsoft SQL Server
- PostgreSQL
- Oracle

Para la gestión empresarial a gran escala lo más recomendable sería Oracle debido al gran centro de soporte que maneja, y la robustez de su sistema sin embargo, una implementación empresarial en Oracle tiene un costo considerablemente alto, por lo que no es una opción a corto y mediano plazo, y entre las dos opciones restantes, la opción de Microsoft SQL Server podría ser una opción, pero su vinculación a Transact-SQL (T-SQL) lo hace especializado al momento de implementar, por lo que se necesita a profesionales dedicados a la materia para una correcta implementación en este DBMS además de que también está asociado a una empresa, por lo que su implementación a corto y mediano plazo se ve comprometida debido al lucro que se generará desde la plataforma, por último se tiene a PostgreSQL, el cual es conocido como la elección empresarial ideal debido a que es de código abierto y no depende de una empresa, además cuenta con una potencia considerable, una funcionalidad amplia y flexibilidad comercial lo cual arrasa con las otras dos opciones, la mejor opción para una empresa con las características que se definieron la opción ideal es usar este DBMS.

Por tanto, la decisión del DBMS a utilizar será PostgreSQL, esto combinado con la arquitectura en la nube que se implementará, puede ser montado a través de servicios como **Cloud SQL** el cual está dedicado explícitamente para bases de datos relacionales como PostgreSQL como es el caso, en el se podrán desplegar las bases de datos que sean necesarias sin necesidad de hacer una gestión manual de estas instancias. Además de esto el servicio posee una versión de prueba que se puede utilizar para implementaciones iniciales.

### Frameworks para Desarrollo

Una de las caras más importantes de una compañía que distribuye una plataforma de videojuegos como es el caso, es el desarrollo de una interfaz gráfica amigable para el usuario, que sea intuitiva para no complicar los procesos a los usuarios finales que interactúen con el sistema, y que además sea atractivo a la vista, ya sea con una buena elección de colores, animaciones, fuentes, tamaños, sombras, bordes, márgenes y muchos otros componentes gráficos que hacen que un usuario final tenga una buena experiencia al momento de utilizar la plataforma.

Además de la experiencia con la interfaz gráfica, también se debe asegurar un buen funcionamiento de cara a la lógica detrás de la aplicación, en este caso los marcos de trabajo para backend también forman parte fundamental de los microservicios a implementar en la nube, estos deben ser compatibles con microservicios debido que es la arquitectura que se va a utilizar, además de ser robusto y de preferencia de código abierto para poder utilizar con propósitos comerciales para una implementación a gran escala.

Existe una gran variedad de marcos de trabajo para la rápida implementación de interfaces gráficas, estos son demandados en el mercado considerablemente por grandes empresas hoy en día, dentro de los frameworks más utilizados en empresas de consumo masivo se encuentran los siguientes:

- Frontend:

  - React JS
  - Angular
  - Laravel

- Backend:
  - Node JS
  - Django
  - SpringBoot

Para frontend, los frameworks son bastante parecidos de cara al resultado final, la plataforma realizada puede ser la misma basada en los diferentes marcos de trabajo, por lo que la estructura interna es lo que determinará la elección de la plataforma, React es una de las librerías más utilizadas y demandadas actualmente, sin embargo, no está especialmente diseñado para gestionar módulos (que en este caso representarían los microservicios) por lo que queda descartado al igual que Laravel, por lo que la elección indicada para la implementación de la interfaz gráfica es Angular. De parte de backend cualquier tecnología es posible, sin embargo por la facilidad de integración y gestión de paquetes que tiene Node JS será el que se utilizará en esta implementación.

Por lo que el proyecto se usará la combinación de **Angular y microservicios en Node JS.**

## Comunicación entre servicios

### Descripción de microservicios

Se describirá brevemente cada uno de los microservicios, para dar una idea general del por qué existe cada uno, para una descripción más en detalle se debe consultar el área de microservicios.

- **Registro:** Esta funcionalidad fue elegida para proveer la posibilidad de ingresar al sistema, por medio de un registro, requiriendo información básica.

- **Generación de OTP:** Esta funcionalidad provee la posibilidad de generar una one-time password, para previa verificación y autorización de un nuevo usuario.

- **Confirmación de OTP:** Esta funcionalidad provee la posibilidad de validar el OTP ya creado, para garantizar la propiedad del correo.

- **Inicio de sesión:** Esta funcionalidad provee la posibilidad al usuario de ingresar al sistema con sus credenciales previamente creadas.

- **Visualización de catálogo:** Esta funcionalidad provee a los usuarios el poder visualizar todos los juegos que la plataforma le ofrece.

- **Agregar al carrito:** Por medio de esta funcionalidad se agregara un juego al carrito activo del usuario, en caso no exista un carrito activo en ese momento, el mismo será creado de manera automatica.

- **Agregar externo a carrito:** Por medio de esta funcionalidad se agregara un juego externo al carrito activo del usuario, en caso no exista un carrito activo en ese momento, el mismo será creado de manera automatica.

- **Eliminar elemento del carrito:** Por medio de esta funcionalidad se eliminara un objeto del carrito actualmente activo para el usuario.

- **Obtener carrito:** Por medio de esta funcionalidad se obtendra con detalle los elemntos agregados al carrito, junto a sus precios y el total para el carrito.

- **Comprar carrito:** Por medio de esta funcionalidad sera posible comprar todos los elementos que se encuentran agregados en el carrito actual, ademas el carrito sera cerrado automaticamente despues de la transacción de compra.

- **Agregar a la lista de deseos:** Esta funcionalidad provee a los usuarios el poder agregar juegos a que le hayan llamado su atención a un listado, sin embargo los mismos no se compraran de inmediato, por lo cual los precios pueden variar.

- **Eliminar de la lista de deseos:** Esta funcionalidad provee a los usuarios el poder eliminar juegos de la lista de deseos.

- **Obtener lista de deseos:** Esta funcionalidad provee a los usuarios el poder visualizar su lista de deseos.

- **Crear DLC:** Esta funcionalidad provee al administrador la posibilidad del registro de nuevos DLCs para los juegos existentes.

- **Obtener DLCs:** Esta funcionalidad provee al usuario la posibilidad de conocer acerca del contenido descargable para cada juego.

- **Dar de baja a un usuario:** Por medio de esta funcionalidad se da de baja a los usuarios que cometieron alguna falta grave a las normas de la plataforma.

- **Crear descuentos:** Por medio de esta funcionalidad el adminsitrador puede crear descuentos por fecha y región para un juego en especifico.

- **Ver desarrolladores de juegos:** Esta funcionalidad fue elegida para visualizar los desarrolladores en general de todos los juegos entre distintas plataformas.

- **Ver juegos:** Esta funcionalidad fue elegida para visualizar todos los juegos entre distintas plataformas.

- **Compra de juegos:** Esta funcionalidad fue elegida para la compra de los juegos entre distintas plataformas.

### Tipo de comunicación entre microservicios

Normalmente para una aplicación que está basada en microservicios las counicaciones entre ellos se basan en una cobinación de estilos, dentro de los estilos que se encuentran normalmente están los siguientes:

- **Protocolos síncronos:** Son las formas de comunicación entre servidores más común para las APIs web debido a que un claro ejemplo de este tipo de comunicación son los protocolos HTTP/HTTPS.

- **Protocolos asíncronos:** Normalmente se utilizan protocolos de mensajería para la counicación asíncrona de microservicios.

Dentro de la implementación de microservicios normalmente la comunicación no es una preocupación importante a la hora de compilar los microservicios, ni tampoco al integrarlos entre sí, el tipo de comunicación que se utiliza no es relevante para el buen funcionamiento de los mismos, lo que sí importa es poder integrar los microservicios de forma asíncrona manteniendo su independencia, por lo que la correcta implementación de los tipos de comunicación es indispensable para que no se generen excepciones inesperadas durante un proceso de comunicación.

Otro enfoque para la definición de tipo de comunicación entre microservicios es en base a criterios y no tipos de respuesta, en este caso los criterios que se manejan normalmente son los siguientes:

- **Por clase de protocolo** que se mencionaban anteriormente como síncrono o asíncrono.

- **Por número de receptores** que pueden ser uno o varios receptores a los que se debe servir.

El primer criterio ya se describió anteriormente, y el segundo depende de la implementación y como las solicitudes han de ser procesadas en un servicio, normalmente se utiliza el criterio de varios receptores debido a que el receptor único no tiene muchas utilidades en un proyecto para una plataforma de consumo masivo. Teniendo en cuenta todos estos tipos y criterios para la elección de un tipo de comuncación entre microservicios se puede seleccionar una herramienta para la comunicación que se implementará en este desarrollo en específico más adelante.

### Herramientas para la comunicación

Las herramientas para la comunicación que se utilizarán en este proyecto basándose en el análisis previo de los tipos de comunicación será a traves del protocolo síncrono HTTP/HTTPS y de receptor múltiple debido al tipo de plataforma que se necesita realizar, para el desarrollo de esta infraestructura se necesitarán dos tipos de servicio principales dentro del cluster de Kubernetes donde se albergarán los microservicios:

- **Servicio load balancer local** el cual a través de nombres enrutará cada uno de los microservicios dentro de un servicio los cuales podrán interactuar únicamente de manera local dentro del clúster y no tendrán una salida directa al exterior, la única manera de acceder a este servicio será a través del nombre en la ruta y el receptor deberá estar dentro del mismo clúster que el servicio.

- **Servicio load balancer global** en el cual se definirá el nombre de la API, y tendrá asociada una dirección IP la cual se podrá consultar de manera externa para su consumo desde múltiples receptores, este load balancer es el que se encargará de redirigir las peticiones a cada uno de los servicios implementados con el enfoque anterior haciendo posible una red de microservicios que funcione a través de redireccionamiento HTTP/HTTPS de receptor múltiple.

Cabe resalta que estas herramientas se implementarán gracias a un balanceador de carga realizado en base a Nginx, el cual es un servidor web proxy de alto rendimiento de código abierto el cual se usa normalmente dentro de Google Kubernetes Engine (GKE) para el desarrollo de proxies y balanceadores de carga a nivel empresarial.

### Diagrama de infraestructura

Para describir los elementos físicos en nube que se requerirán en este proyecto estos recursos se eligieron con base a el análisis realizado anteriormente, y será la infraestructura en la que se realicen los despliegues a través de integración y despliegue continuo más adelante en la etapa de desarrollo.

![Infraestructura](./imgs/infra.png)
![Infraestructura2](./imgs/infra2.png)

Dentro de la infraestructura lo único que vendrá de fuera serán las peticiones del usuario a través del cliente, luego una vez la petición es procesada y redireccionada al clúster de Kubernetes se tiene acceso al API como tal, la cual está dividida en microservicios que serán distribuidos a través de los controladores de recursos, los cuales dependiendo del endpoint que se esté consumiendo en ese momento.

Después de redirigir la petición a los nodos dentro del clúster, cada uno de los pods de usuario tendrá los contenedores correspondientes para ejecutar la funcionalidad asignada a esa ruta, esto puede significar una conexión a la base de datos o conexiones entre diferentes microservicios para validación de información o autenticación.

Por último las implementaciones de servicios adicionales de Google Cloud Platform como la implementación de discos físicos para la base de datos, los balanceadores de carga necesarios para redirigir las peticiones a un destino en específico y la gestión de la Virtual Private Cloud para que se pueda interconectar toda la infraestructura entre sí y garantizar la seguridad de la implementación en una capa.

## Base de datos

![ER de la aplicación](./imgs/er_SA_Proyecto.png)

Como se puede observar en el diagrama, se busca tener una base de datos normalizada, para facilitar y hacer mas rapida la transaccionalidad de la misma, con tablas catalogo que apoyen y demarquen de mejor manera la misma.

A grandes rasgos podemos observar 3 secciones principales en el diagrama, **manejo de usuarios, manejo de juegos y logica de carrito**. (Estas secciones no son explicitas, se hace esta división para explicar de mejor manera las tablas)

### Cambios por nuevos requerimientos

Debido a los nuevos requerimeintos la base de dato sufrio leves modificaciones, entre las cuales podemos mencionar:

- DLC y juegos se convirtieron en una sola entidad para un manejo mas simple y directo de los mismos
- Tablas de recomendaciones y punteos

### Manejo de usuarios

En esta seccion se encuentran todas las tablas relacionadas al usuario, desde la concepción misma hasta las posesiones que este tendra tendra en la aplciación, podemos encontrar las siguientes tablas:

- **Usuario:** Resguarda la información de cada usuario que se registre en la plataforma.

- **Tipo usuario:** Alberga los tipos de usuarios existentes en la plataforma, se realizo una tabla pensando en un posible crecimiento.

- **Biblioteca:** En esta entidad se relacionan los juegos que ha comprado cada usuario.

- **Lista de deseo:** En esta entidad se relaciona el id de los juegos deseados con el id del usaurio que los desea.

- **Biblioteca externa:** Guarda la información de los juegos externos adquiridos, agregando el grupo del cual se consumio y el id que tiene el juego en la base de dicho grupo.

### Manejo de juegos

Integra todas las tablas que manejan la información de los juegos, desde la información mas general hasta precios y restricciones por región, las tablas son las siguientes:

- **Juego:** Cada fila de esta entidad representa un juego en la aplicación en el se guarda su información basica.

- **Desarrollador y desarrollador_juego:** La primera es la tabla catalogo de todos los desarrolladores que se incluyen en la aplicación, mientras la segunda es la tabla que rompe la relación muchos a muchos entre la tabla desarrollador y juego.

- **Categoria y categoria_juego:** Es exactamente igual que el caso anterior, pero encocado a las categorias de los juegos.

- **Contenido descargable:** Alberga la información del contenido disponible para diferentes juegos, cuidando la fecha de lanzamiento de los mismos.

- **Region:** Catalogo de todas las regiones disponibles para la aplciación.

- **Restriccion region:** Esta tabla asocia los juegos con las regiones en las que estaran restringidos.

- **Precio:** Guarda el detalle de precios y descuentos disponibles puede ser especificado solo por fechas, o especificar las regiones en las que estara disponible.

- **Precio_region:** Detalle de las regiones en las que es valida una tupla de la tabla precio.

### Logica de carrito

Estas son las tablas con mayor tranasaccionalidad en la aplciación, como se indica por medio de ellas se realizara toda la logica y manejo de un carrito, desde su concepción hasta su compra.

- **Carrito:** Para cada usuario solo puede existir una instancia activa en esta tabla, el cual es el carrito actual, independiente de la fecha de su apertura este seguira vivo hasta que el usuario lo cancele, el total sera ingresado hasta que la compra sea efectiva.

- **Detalle_carrito:** Relaciona la instancia de carrito activa con los juegos agregados a la misma, el precio final se agrega hasta que la compra se hace efectiva.

- **Detalle_carrito_externo:** Sigue la misma logica del detalle de carrito normal, pero guardando información del grupo y id externo del juego.

## Lista de Microservicios

### Usuario

#### **Registro**

Esta funcionalidad fue elegida para proveer la
posibilidad de ingresar al sistema, por medio de un registro, requiriendo información
básica.

| <!-- -->                 | <!-- -->                                                                                                               |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                    |
| **Prioridad:**           | Alta                                                                                                                   |
| **Módulo:**              | Usuario                                                                                                                |
| **Nombre:**              | Microservicio de Registro                                                                                              |
| **Historia de usuario:** | Como usuario necesito la posibilidad de registrarme en el sistema para poder realizar compras dentro de la plataforma. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/auth/register
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo  | Tipo   | Descripción                                        |
| :-------- | :----- | :------------------------------------------------- |
| username  | string | Nombre de usuario que se usará para la cuenta.     |
| email     | string | Email del nuevo usuario.                           |
| names     | string | Los nombres del nuevo usuario.                     |
| lastNames | string | Los apellidos del nuevo usuario.                   |
| password  | string | Contraseña a utilizar para el usuario a registrar. |
| image     | string | Base64 de la imagen del usuario.                   |
| region    | string | Código con la región del usuario.                  |
| userType  | string | Tipo de usuario.                                   |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                                                                             |
| :------- | :------ | :---------------------------------------------------------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                                                                 |
| content  | object  | Datos del usuario registrado, con su identificador único, pensado para ser almacenado en localstorage dentro de la app. |
| message  | string  | En un response exitoso no se usa este campo.                                                                            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **username**: El campo es requerido en el request body y debe ser único.
- **email**: El campo es requerido en el request body y obligatoriamente debe ser un correo.
- **names**: El campo es requerido en el request body.
- **lastNames**: El campo es requerido en el request body.
- **password**: El campo es requerido en el request body.
- **region**: El campo es requerido en el request body.
- **userType**: El campo es requerido en el request body.

`Ejemplos`

> Entrada

```
{
    "username": "fernando",
    "email": "fernando@gmail.com",
    "names": "Fernando José",
    "lastNames": "Vásquez Castillo",
    "password": "123",
    "region": "GT",
    "userType": 1
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "userID": 6,
        "username": "fernando",
        "email": "fernando@gmail.com",
        "names": "Fernando José",
        "lastNames": "Vásquez Castillo",
        "password": "123",
        "region": "GT",
        "userType": 1
    },
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "username is required",
            "param": "username",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "error: value too long for type character varying(13)"
}
```

#### **Generar OTP**

Esta funcionalidad provee la posibilidad de generar una one-time password, para previa verificación y autorización de un nuevo usuario.

| <!-- -->                 | <!-- -->                                                                                                                                                |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                                     |
| **Prioridad:**           | Alta                                                                                                                                                    |
| **Módulo:**              | Usuario                                                                                                                                                 |
| **Nombre:**              | Microservicio de Generación de OTP.                                                                                                                     |
| **Historia de usuario:** | Como administrador de la plataforma deseo garantizar la autenticidad de las personas que navegan por ella, por lo cual deseo una validación por correo. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/auth/createOTP
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                               |
| :------- | :----- | :---------------------------------------- |
| email    | string | Email a donde se realizará la validación. |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Mensaje de confirmación.                                |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **email**: El campo es requerido en el request body y obligatoriamente debe ser un correo de un usuario activo.

`Ejemplos`

> Entrada

```
{
    "email": "fernando@gmail.com"
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": "SUCCESS",
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "email is required",
            "param": "email",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "There is not a valid user with this email"
}
```

#### **Confirm OTP**

Esta funcionalidad provee la posibilidad de validar el OTP ya creado, para garantizar la propiedad del correo.

| <!-- -->                 | <!-- -->                                                                                                                                        |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                             |
| **Prioridad:**           | Alta                                                                                                                                            |
| **Módulo:**              | Usuario                                                                                                                                         |
| **Nombre:**              | Microservicio de Confirmación de OTP.                                                                                                           |
| **Historia de usuario:** | Como usuario que necesita completar el proceso de registro, necesito confirmar mi OTP previamente creado y así concluír existosamente el flujo. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/auth/createOTP
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                |
| :------- | :----- | :------------------------- |
| otp      | string | OTP recibido en el correo. |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Mensaje de confirmación.                                |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **otp**: El campo es requerido en el request body.

`Ejemplos`

> Entrada

```
{
    "otp": "549846"
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": "SUCCESS",
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "otp is required",
            "param": "otp",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "Invalid/Expired OTP"
}
```

#### **Inicio de sesión**

Esta funcionalidad provee la posibilidad al usuario de ingresar al sistema con sus credenciales previamente creadas.

| <!-- -->                 | <!-- -->                                                                                                                                      |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                           |
| **Prioridad:**           | Alta                                                                                                                                          |
| **Módulo:**              | Usuario                                                                                                                                       |
| **Nombre:**              | Microservicio de Inicio de sesión.                                                                                                            |
| **Historia de usuario:** | Como usuario que necesita utilizar el sistema de una manera ya real, utilizando las funcionalidades disponibles, deseo poder ingresar a este. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/auth/login
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                                      |
| :------- | :----- | :----------------------------------------------- |
| email    | string | Correo del usuario que desea iniciar sesión.     |
| password | string | Contraseña del usuario que desea iniciar sesión. |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Tokens necesarios para la aplicación.                   |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **email**: El campo es requerido en el request body y debe ser un correo válido de una cuenta existente.
- **password**: El campo es requerido en el request body.

`Ejemplos`

> Entrada

```
{
    "email": "fernando@gmail.com",
    "password": "123"
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
         "access_token": "eyJraWQ.....rm8EA4osYg",
         "expires_in": 3600,
         "refresh_token": "i6mapTIAVSp2oJkgUnCACKKfZxt_H5MBLiqcybBBd04"
    },
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "email is required",
            "param": "email",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "The account has not been validated"
}
```

### Catálogo de juegos

#### **Ver Catalogo**

Esta funcionalidad provee a los usuarios el poder visualizar todos los juegos que la plataforma le ofrece

| <!-- -->                 | <!-- -->                                                                                                                                        |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                             |
| **Prioridad:**           | Alta                                                                                                                                            |
| **Módulo:**              | Catalogo                                                                                                                                        |
| **Nombre:**              | Microservicio de Visualizar el catalogo de juegos                                                                                               |
| **Historia de usuario:** | Como usuario necesito la posibilidad de visualizar los juegos existentes en la plataforma, ademas de poder filtrarlos por distintas categorias. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/client/catalog
>
> **Método:** GET
>
> **Formato de entrada:** JSON

| Atributo   | Tipo    | Descripción                         |
| :--------- | :------ | :---------------------------------- |
| id_catalog | numeric | Tipo de catalogo a consultar:       |
|            |         | 0. Todos                            |
|            |         | 1. Juegos más vendidos              |
|            |         | 2. Genero                           |
|            |         | 3. Desarrollador                    |
|            |         | 4. Recomendaciones.                 |
|            |         | 5. Calificaciones                   |
|            |         | 6. Busqueda por texto               |
|            |         | 7. Busqueda por id de juego         |
|            |         | 8. Que incluyan desktop_image       |
|            |         | 9. Ofertas                          |
| filter     | string  | Filtro que se agrega a la categoría |

El parametro filter varia su signnificado segun el id_catalog.
| id_catalog | filter |
| :--------- | :------ |
| 0 | Agrupación de la consulta; 0. Sin agrupar, 1. Agrupado por categorias, 2. Agrupado por desarrolladores |
| 1 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos mas vendidos. |
| 2 | Id del genero a obtener |
| 3 | Id del desarrollador a obtener |
| 4 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos más recomendados. |
| 5 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos mejor valorados. |
| 6 | Texto a buscar en el titulo del juego |
| 7 | Id del juego que deseas obtener |
| 8 | Se debe enviar un 0 |
| 9 | Se debe enviar un 0 |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                              |
| :------- | :------ | :----------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                  |
| content  | object  | Devuelve la informacion de los juegos que cumplen con el filtro aplicado |
| message  | string  | En un response exitoso no se usa este campo.                             |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **id_catalog**: El campo es requerido en el request body.
- **filter**: El campo es opcional en el request body y unicamente aplica para los catalogos 2 al 5.

`Ejemplos`

> Entrada

```
{
    "id_catalog": "2",
    "filter": "Aventura",
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "data": [
			{
				"gameId": 1,
				"name": "",
				"image": "",
				"releaseDate": "DD/MM/YYYY",
				"restrictionAge": "T",
				"price": 0,
				"discount": 0,
				"group": 1,
				"developer": [],
				"category": [],
				"region": []
			},
			{
				"gameId": 2,
				"name": "",
				"image": "",
				"releaseDate": "DD/MM/YYYY",
				"restrictionAge": "T",
				"price": 0,
				"discount": 0,
				"group": 1,
				"developer": [],
				"category": [],
				"region": []
			}
	    ]
    },
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "id_catalog is required",
            "param": "id_catalog",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "error: value too long for type character varying(13)"
}
```

### Juegos

#### Creación de Juegos

Este servicio ofrece la funcionalidad de agregar juegos al catálogo registrando toda la información requerida; tomando en cuenta la clasificación de edad, desarrolladores, fecha de lanzamiento y restriccion de descarga por región, entre otros datos importantes.

| **ID**                   | 002                                                                                                                                                                                                                         |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Prioridad:**           | Alta                                                                                                                                                                                                                        |
| **Módulo:**              | Juegos                                                                                                                                                                                                                      |
| **Nombre:**              | Creación de Juegos                                                                                                                                                                                                          |
| **Historia de usuario:** | Como usuario se desea agregar un nuevos juegos al catálogo agregando todos los datos necesarios para que los usuarios puedan descargar dicho juego respetando las restricciones para dicho juego, tales como región o edad. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/games
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo       | Tipo     | Descripción                                                                                                                     |
| -------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------- |
| gameId         | int      | ID que identifica a un juego                                                                                                    |
| name           | string   | Título del juegos a registrar                                                                                                   |
| image          | string   | URL de imagen del juego                                                                                                         |
| releaseDate    | string   | Fecha de lanzamiento del juego                                                                                                  |
| restrictionAge | string   | Restriccion de edad, posibles valores: "EC", "E" , "E 10+", "T", "M", "AO" (ver apartado 1)                                     |
| price          | float    | Cantidad monetaria para el precio del juego.                                                                                    |
| discount       | float    | Cantidad monetaria de descuento para el juego.                                                                                  |
| group          | int      | Numero de grupo al cual pertenece el juego                                                                                      |
| developer      | [string] | Arreglo que contiene cadenas de texto con los nombre de los desarrolladores                                                     |
| category       | [string] | Arreglo que contiene cadenas de texto con las categorias asignadas al juego                                                     |
| region         | [string] | Arreglo que contiene cadenas de texto con las regiones en donde estara disponible el juego o bien para descuentos y promociones |

**Apartado 1: Posibles valores de clasificación de edad**

- **EC (Early Childhood – Primera infancia):** Juegos orientados para niños entre los 3 y 5 años, su temática es principalmente educativa.
- **E (Everyone – todos):** Su contenido está dirigido para todo público. Los juegos contienen un poco de animación, violencia fantástica, así como el uso de insultos suaves.
- **E 10+ (Everyone 10 and up – Todos, mayores de 10 años):** Estos juegos contienen animaciones, fantasía, violencia e insultos leves.
- **T (Teen – adolescentes):** Su contenido está dirigido a jóvenes de 13 años en adelante, y se caracteriza por tener violencia, sugerencias sexuales, humor crudo, sangre, juegos de azar y uso de un lenguaje fuerte.
- **M (Mature 17+ - Edad madura):** Estos videojuegos están restringidos para los menores de 17 años, ya que su contenido se caracteriza por mostrar explícitamente violencia, sangre, insultos y temas sexuales.
- **AO (Adults only 18+ - Solo para adultos):** Estos videojuegos son sólo para mayores de 18 años, su contenido tiene escenas prologadas de violencia, desnudez y temas sexuales.

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Mensaje de confirmación.                                |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **Campos Obligatorios:** Se deben introducir estos campos de forma obligatoria en el body request con el tipo estipulado, de los contrario se generará un error.

  - **gameId**
  - **name**
  - **image**
  - **releaseDate**
  - **restrictionAge**
  - **price**
  - **group**
  - **developer**
  - **category**
  - **region**

- **Se debe verificar que no hay otros juego con el mismo titulo al momento de registrar uno nuevo.**

`Ejemplos`

> Entrada

```json
{
  "gameId": 1,
  "name": "Assasins Creed",
  "image": "https://imagelURL",
  "releaseDate": "DD/MM/YYYY",
  "restrictionAge": "T",
  "price": 20.9,
  "discount": 1.9,
  "group": 1,
  "developer": ["Dev1"],
  "category": ["EC"],
  "region": ["Guatemala"]
}
```

> Respuesta **exitosa**

```json
{
  "status": true,
  "content": {
    "gameId": 1,
    "name": "Assasins Creed",
    "image": "https://imagelURL",
    "releaseDate": "DD/MM/YYYY",
    "restrictionAge": "T",
    "price": 20.9,
    "discount": 1.9,
    "group": 1,
    "developer": ["Dev1"],
    "category": ["EC"],
    "region": ["Guatemala"]
  },
  "message": "Juego agregado al catalogo con exito"
}
```

> Respuesta **fallida 400**

```json
{
  "status": false,
  "content": {},
  "message": [
    {
      "msg": "The following params are required: gameId",
      "param": "gameId",
      "location": "body"
    }
  ]
}
```

> Respuesta **fallida 500**

```json
{
  "status": false,
  "content": {},
  "message": "Invalid data type for params: gameId"
}
```

### Compras

#### **Agregar a carrito**

Por medio de esta funcionalidad se agregara un juego al carrito activo del usuario, en caso no exista un carrito activo en ese momento, el mismo
sera creado de manera automatica.

| <!-- -->                 | <!-- -->                                                                                             |
| ------------------------ | ---------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                  |
| **Prioridad:**           | Alta                                                                                                 |
| **Módulo:**              | Compras                                                                                              |
| **Nombre:**              | Microservicio de agregar a carrito                                                                   |
| **Historia de usuario:** | El usuario necesita agregar a su carrito los juegos que desea comprar en una transacción especifica. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/cart/add/:gameId
>
> **Método:** PUT
>
> **Formato de entrada:** Params

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                                                                           |
| :------- | :------ | :-------------------------------------------------------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                                                               |
| content  | object  | Regresa el número de objetos actualmente en el carrito con la intención de poder mostrar esto en el icono de carrito. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

- **gameId**: El campo es requerido, debe ser un campo numerico y un id existente.

`Ejemplos`

> Entrada

```
/apis/cart/add/25
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "itemsInCart": 2
    }
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
        {
            "msg": "The game does not exist",
            "param": "gameId",
            "location": "params"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

#### **Agregar externo a carrito**

Por medio de esta funcionalidad se agregara un juego externo al carrito activo del usuario, en caso no exista un carrito activo en ese momento, el mismo
sera creado de manera automatica.

| <!-- -->                 | <!-- -->                                                                                                                           |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                |
| **Prioridad:**           | Alta                                                                                                                               |
| **Módulo:**              | Compras                                                                                                                            |
| **Nombre:**              | Microservicio de agregar externo a carrito                                                                                         |
| **Historia de usuario:** | El usuario necesita agregar a su carrito los juegos que desea comprar, aun sean de consumo externo, en una transacción especifica. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/cart/addExternal
>
> **Método:** PUT
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                                        |
| :------- | :----- | :------------------------------------------------- |
| group    | number | Numero del grupo al que pertenece el juego         |
| gameId   | number | Id del juego en la base de datos del grupo externo |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                                                                           |
| :------- | :------ | :-------------------------------------------------------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                                                               |
| content  | object  | Regresa el número de objetos actualmente en el carrito con la intención de poder mostrar esto en el icono de carrito. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

- **group**: El campo es requerido, debe ser un campo numerico y debe ser un grupo al que actualmente estemos conectados.
- **group**: El campo es requerido, debe ser un campo numerico y debe existir en la base de datos del grupo que estemos consumiendo.

`Ejemplos`

> Entrada

```
{
    "group": 5,
    "gameId": 25
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "itemsInCart": 2
    }
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
        {
            "msg": "The game does not exist",
            "param": "gameId",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

#### **Eliminar elemento de carrito**

Por medio de esta funcionalidad se eliminara un objeto del carrito actualmente activo para el usuario.

| <!-- -->                 | <!-- -->                                                                                               |
| ------------------------ | ------------------------------------------------------------------------------------------------------ |
| **ID**                   | 001                                                                                                    |
| **Prioridad:**           | Alta                                                                                                   |
| **Módulo:**              | Compras                                                                                                |
| **Nombre:**              | Microservicio para eleminar juegos del carrito                                                         |
| **Historia de usuario:** | El usuario necesita tener la posibilidad de eliminar de su carrito los juegos que ya no desea adquirir |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/cart/removeElement
>
> **Método:** DELETE
>
> **Formato de entrada:** JSON

| Atributo      | Tipo    | Descripción                                                                       |
| :------------ | :------ | :-------------------------------------------------------------------------------- |
| idCartElement | number  | Id del elemento del carrito                                                       |
| isExternal    | boolean | Indica si el juego a eliminar es de nuestra base de datos o es de consumo externo |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                                                                           |
| :------- | :------ | :-------------------------------------------------------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                                                               |
| content  | object  | Regresa el número de objetos actualmente en el carrito con la intención de poder mostrar esto en el icono de carrito. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

- **idCartElement**: El campo es requerido, debe ser un campo numerico, debe existir en la tabla correspondiente y debe pertenencer al carrito del usuario.
- **isExternal**: El campo es requerido y debe ser un campo booleano.

`Ejemplos`

> Entrada

```
{
    "idCartElement": 125,
    "isExternal": true
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "itemsInCart": 2
    }
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
        {
            "msg": "The item does not belong to the user's cart",
            "param": "idCartElelemnt",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

#### **Obtener carrito**

Por medio de esta funcionalidad se obtendra con detalle los elemntos agregados al carrito, junto a sus precios y el total para el carrito.

| <!-- -->                 | <!-- -->                                                                                                           |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------ |
| **ID**                   | 001                                                                                                                |
| **Prioridad:**           | Alta                                                                                                               |
| **Módulo:**              | Compras                                                                                                            |
| **Nombre:**              | Microservicio para obtener detalles del carrito                                                                    |
| **Historia de usuario:** | El usuario necesita poder visualizar los elementos agregados a su carrito, asi como el precio de estos y el total. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/cart
>
> **Método:** GET

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                                                                  |
| :------- | :------ | :----------------------------------------------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                                                      |
| content  | object  | Regresa un objeto que contiene datos del carrito como el total de compra y los elementos agregados al mismo. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

`Ejemplos`

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "total": 900
        "discount": 100
        "granTotal": 800,
        "items": [
          {
            "idCartDetail": 25,
            "name": "CS: GO",
            "image": "www.alamacenamiento.gx/images/csgo.jpg",
            "classification": "E"
            "price": 450,
            "discount": 50,
            "finalPrice": 400,
            "isExternal": false
          },
          {
            "idCartDetail": 25,
            "name": "FIFA 22",
            "image": "www.alamacenamiento.gx/images/fifa22.jpg",
            "classification": "E"
            "price": 450,
            "discount": 50,
            "finalPrice": 400,
            "isExternal": true
          },
        ]
    }
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
        {
            "msg": "The element was not found in the external database",
            "param": "idGame",
            "location": "externalCart"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

#### **Comprar carrito**

Por medio de esta funcionalidad sera posible comprar todos los elementos que se encuentran agregados en el carrito actual, ademas el carrito sera cerrado
automaticamente despues de la transacción de compra.

| <!-- -->                 | <!-- -->                                                                           |
| ------------------------ | ---------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                |
| **Prioridad:**           | Alta                                                                               |
| **Módulo:**              | Compras                                                                            |
| **Nombre:**              | Microservicio para realizar la compra del carrito.                                 |
| **Historia de usuario:** | El usuario necesita hacer efectiva la compra de los elementos agregados al carrito |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/buyCart
>
> **Método:** PUT

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Regresa los detalles finales de la compra.              |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

`Ejemplos`

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "total": 900
        "discount": 100
        "granTotal": 800,
        "items": [
          {
            "idCartDetail": 25,
            "name": "CS: GO",
            "image": "www.alamacenamiento.gx/images/csgo.jpg",
            "classification": "E"
            "price": 450,
            "discount": 50,
            "finalPrice": 400,
            "isExternal": false
          },
          {
            "idCartDetail": 25,
            "name": "FIFA 22",
            "image": "www.alamacenamiento.gx/images/fifa22.jpg",
            "classification": "E"
            "price": 450,
            "discount": 50,
            "finalPrice": 400,
            "isExternal": true
          },
        ]
    }
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
        {
            "msg": "There is no active cart for the user",
            "param": "User",
            "location": "Cart"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

### Lista de deseos

#### **Agregar**

Esta funcionalidad provee a los usuarios el poder agregar juegos a que le hayan llamado su atención a un listado, sin embargo los mismos no se compraran de inmediato, por lo cual los precios pueden variar.

| <!-- -->                 | <!-- -->                                                                                                                                    |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                         |
| **Prioridad:**           | Alta                                                                                                                                        |
| **Módulo:**              | Lista de deseos                                                                                                                             |
| **Nombre:**              | Microservicio de Agregar a la Lista de Deseos                                                                                               |
| **Historia de usuario:** | Como usuario necesito la posibilidad de guardar en un listado aquellos juegos que me gusten y que posiblemente en un futuro deseo adquirir. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/wishList
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo  | Tipo   | Descripción                            |
| :-------- | :----- | :------------------------------------- |
| productId | string | Id del juego que se agregar a la lista |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                            |
| :------- | :------ | :--------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                |
| data     | object  | Devuelve la información del juego agregado para uso interno de la app. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                         |
| :------- | :------ | :---------------------------------- |
| status   | boolean | Identifica el fallo de la petición. |
| data     | object  | Describe el error con un código.    |

**Validaciones requeridas:**

- **productId**: El campo es requerido en el request body y debe ser el id de un juego existente.

`Ejemplos`

> Entrada

```
{
    "productId": 5
}
```

> Respuesta **exitosa**

```
{
    "success": true,
    "data": {
        "userId": 2,
        "productId": 4
    }
}
```

> Respuesta **fallida 400**

```
{
    "success": false,
    "data": {
        "error": "EWC-006"
    }
}
```

> Respuesta **fallida 500**

```
{
    "success": false,
    "data": {
        "error": "EWC-003"
    }
}
```

#### **Eliminar**

Esta funcionalidad provee a los usuarios el poder eliminar juegos de la lista de deseos.

| <!-- -->                 | <!-- -->                                                                         |
| ------------------------ | -------------------------------------------------------------------------------- |
| **ID**                   | 002                                                                              |
| **Prioridad:**           | Alta                                                                             |
| **Módulo:**              | Lista de deseos                                                                  |
| **Nombre:**              | Microservicio de Eliminar de la Lista de Deseos                                  |
| **Historia de usuario:** | Como usuario necesito la posibilidad de eliminar juegos de mi listado de deseos. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/wishList
>
> **Método:** DELETE
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                             |
| :------- | :----- | :-------------------------------------- |
| gameId   | string | Id del juego que se elimina de la lista |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                             |
| :------- | :------ | :---------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                 |
| data     | object  | Devuelve la información del juego eliminado para uso interno de la app. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| data     | object  | Devuelve el estado y el código de error.                |

**Validaciones requeridas:**

- **gameId**: El campo es requerido en el request body y debe ser el id de un juego existente en la lista de deseos.

`Ejemplos`

> Entrada

```
[URL]/wishList?gameId=4
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": {
        "id_game": 01
    },
    "message": "Juego eliminado de la lista de deseos"
}
```

> Respuesta **fallida 400**

```
{
    "success": false,
    "data": {
        "error": "EWD-001"
    }
}
```

> Respuesta **fallida 500**

```
{
    "success": false,
    "data": {
        "error": "EWD-003"
    }
}
```

#### **Obtener**

Esta funcionalidad provee a los usuarios el poder visualizar su lista de deseos.

| <!-- -->                 | <!-- -->                                                                                                                           |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 003                                                                                                                                |
| **Prioridad:**           | Alta                                                                                                                               |
| **Módulo:**              | Lista de deseos                                                                                                                    |
| **Nombre:**              | Microservicio de Obtener Lista de Deseos                                                                                           |
| **Historia de usuario:** | Como usuario necesito la posibilidad de visualizar mi listado de deseos, de tal forma que pueda revisar sus precios, nombres, etc. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/wishList
>
> **Método:** GET
>
> **Formato de entrada:** JSON

| Atributo | Tipo | Descripción |
| :------- | :--- | :---------- |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                      |
| :------- | :------ | :--------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.          |
| data     | object  | Devuelve la información del todos los juegos para uso de la app. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| data     | object  | Contiene el estado y el código del error.               |

**Validaciones requeridas:**

- **-**

`Ejemplos`

> Entrada

```
{
}
```

> Respuesta **exitosa**

```
{
    "success": true,
    "data": [
        {
            "id_product": 5,
            "product_name": "Call of Duty: Black Ops Cold War",
            "description": "Black Ops Cold War arroja a los fans a las profundidades de la frágil batalla geopolítica de la Guerra Fría del comienzo de los años 80.",
            "short_description": "es un videojuego de disparos en primera persona. Es la quinta entrega Black Ops y el decimoséptimo Call of Duty.",
            "release_date": "2020-11-13T06:00:00.000Z",
            "registration_date": "2022-09-27T22:32:28.109Z",
            "active": true,
            "desktop_image": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/zeus/home/hero/hero-key-art-zeus-desktop.jpg",
            "mobile_image": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/zeus/home/hero/hero-key-art-zeus-desktop.jpg",
            "logo": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/zeus/home/hero/hero-key-art-zeus-desktop.jpg",
            "age_restriction": "AO",
            "is_game": true,
            "parent_game": null,
            "developer": [
                {
                    "id_developer": 3,
                    "developer_name": "CAPCOM"
                },
                {
                    "id_developer": 6,
                    "developer_name": "MICROSOFT"
                }
            ],
            "category": [
                {
                    "id_category": 3,
                    "category_name": "Adventure"
                },
                {
                    "id_category": 4,
                    "category_name": "Fantasy"
                }
            ],
            "price": "500.00",
            "discount": "0.00",
            "end_date": "2022-11-25T06:00:00.000Z",
            "available": true
        },
        {
            "id_product": 2,
            "product_name": "Call of Duty: Vanguard",
            "description": "es un videojuego de acción first person shooter ambientado en la Segunda Guerra Mundial a cargo de Sledgehammer Games y Activision para PC, PlayStation 4, Xbox One, PlayStation 5 y Xbox Series. Domina en cada frente: lucha en combates aéreos sobre el Pacífico, lánzate sobre Francia, defiende Stalingrado con precisión de tirador y ábrete paso entre las fuerzas que avanzan por el norte de África. En este juego, los jugadores se sumergirán en los combates viscerales de la Segunda Guerra Mundial a una escala global sin precedentes.",
            "short_description": "Es un videojuego de disparos en primera persona desarrollado por.​ Es el decimoctavo título de la franquicia Call of Duty.",
            "release_date": "2021-10-05T06:00:00.000Z",
            "registration_date": "2022-09-27T21:58:50.053Z",
            "active": true,
            "desktop_image": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/vanguard/home/hero/vanguard-bg.jpg",
            "mobile_image": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/vanguard/home/hero/vanguard-bg.jpg",
            "logo": "https://www.activision.com/content/dam/atvi/callofduty/cod-touchui/vanguard/home/hero/vanguard-bg.jpg",
            "age_restriction": "AO",
            "is_game": true,
            "parent_game": null,
            "developer": [
                {
                    "id_developer": 1,
                    "developer_name": "SEGA"
                }
            ],
            "category": [
                {
                    "id_category": 1,
                    "category_name": "Action"
                }
            ],
            "price": "450.00",
            "discount": "0.00",
            "end_date": "2022-11-25T06:00:00.000Z",
            "available": true
        }
    ]
}
```

> Respuesta **fallida 400**

```
{
    "success": false,
    "data": {
        "error": "EWD-004"
    }
}
```

> Respuesta **fallida 500**

```
{
    "success": false,
    "data": {
        "error": "EWD-003"
    }
}
```

### Contenido descargable

#### **Crear DLC**

Esta funcionalidad provee al administrador la posibilidad del registro de nuevos DLCs para los juegos existentes.

| <!-- -->                 | <!-- -->                                                                                                                                                              |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | 001                                                                                                                                                                   |
| **Prioridad:**           | Alta                                                                                                                                                                  |
| **Módulo:**              | DLC                                                                                                                                                                   |
| **Nombre:**              | Microservicio de Creación de DLC.                                                                                                                                     |
| **Historia de usuario:** | Como administrador deseo poder gestionar el contenido adicional por juego, por lo cual es primordial que se me permita registrar nuevo contenido asociado a un juego. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/dlc/createDLC
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo    | Tipo   | Descripción                                          |
| :---------- | :----- | :--------------------------------------------------- |
| gameID      | string | Juego al que se encontrará asociado.                 |
| description | string | Detalle de lo que se trata el contenido descargable. |
| release     | string | Fecha en que lanzó o lanzará al público.             |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Confirmación.                                           |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **gameID**: El campo es requerido en el request body y tener referencia a un juego existente.
- **description**: El campo es requerido en el request body.
- **release**: El campo es requerido en el request body.

`Ejemplos`

> Entrada

```
{
    "gameID"; 1,
    "description": "...",
    "release": "22/05/2025
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "content": "SUCCESS",
    "message": ""
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "content": {},
    "message": [
        {
            "msg": "gameID is required",
            "param": "gameID",
            "location": "body"
        }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "There is not a game with this id"
}
```

#### **Obtener DLC**

Esta funcionalidad provee a los usuarios el poder visualizar todos los juegos que la plataforma le ofrece

| <!-- -->                 | <!-- -->                                                                                               |
| ------------------------ | ------------------------------------------------------------------------------------------------------ |
| **ID**                   | 001                                                                                                    |
| **Prioridad:**           | Alta                                                                                                   |
| **Módulo:**              | DLC                                                                                                    |
| **Nombre:**              | Microservicio de Obtención de DLC                                                                      |
| **Historia de usuario:** | Como usuario quiero conocer acerca del contenido descargable con el que cuenta un juego en específico. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/dlc/getDlcs
>
> **Método:** GET
>
> **Formato de entrada:** JSON

| Atributo   | Tipo    | Descripción                         |
| :--------- | :------ | :---------------------------------- |
| id_catalog | numeric | Tipo de catalogo a consultar:       |
|            |         | 0. Todos                            |
|            |         | 1. DLCs más vendidos                |
|            |         | 2. Genero                           |
|            |         | 3. Desarrollador                    |
|            |         | 4. Recomendaciones.                 |
|            |         | 5. Calificaciones                   |
|            |         | 6. Busqueda por texto               |
|            |         | 7. Busqueda por id del DLC          |
|            |         | 8. Que incluyan desktop_image       |
|            |         | 20. DLCs por id de juego            |
| filter     | string  | Filtro que se agrega a la categoría |

El parametro filter varia su significado segun el id_catalog.
| id_catalog | filter |
| :--------- | :------ |
| 0 | Agrupación de la consulta; 0. Sin agrupar, 1. Agrupado por categorias, 2. Agrupado por desarrolladores, 3. Agrupado por Juegos |
| 1 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos mas vendidos. |
| 2 | Id del genero a obtener |
| 3 | Id del desarrollador a obtener |
| 4 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos más recomendados. |
| 5 | El top que desee obtener, si ingresa un 5 obtendra los 5 juegos mejor valorados. |
| 6 | Texto a buscar en el titulo del juego |
| 7 | Id del DLC que se desee obtener |
| 8 | Se debe enviar un 0 |
| 20 | Se debe enviar el ID del juego del que se deseen obtener los DLCs |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                                            |
| :------- | :------ | :--------------------------------------------------------------------- |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición.                |
| content  | object  | Devuelve la informacion de los DLCs que cumplen con el filtro aplicado |
| message  | string  | En un response exitoso no se usa este campo.                           |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **id_catalog**: El campo es requerido en el request body.
- **filter**: El campo es opcional en el request body y unicamente aplica para los catalogos 2 al 5.

`Ejemplos`

> Entrada

```
{
    "id_catalog": "20",
    "filter": "1",
}
```

> Respuesta **exitosa**

```
{
    "success": true,
    "data": [
        {
            "id_product": 31,
            "product_name": "DLC: RAILGRADE",
            "description": "RAILGRADE es un simulador de gestión en el que debes utilizar el ferrocarril para transportar recursos y suministrar energía a la industria en un remoto mundo colonial. Como responsable de administrar el planeta, tendrás que trabajar sin descanso para restaurar la producción industrial tras un catastrófico colapso de las infraestructuras.",
            "short_description": null,
            "release_date": "2022-09-29T06:00:00.000Z",
            "registration_date": "2022-10-01T05:46:28.253Z",
            "active": true,
            "desktop_image": "https://cdn2.unrealengine.com/railgrade-launchtrailer-thumbnail-1920x1080-e642f88fb5de.jpg?h=720&resize=1&w=1280",
            "mobile_image": "https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_RAILGRADE_MinakataDynamics_S2_1200x1600-f40aa411ee439ddd990de6b9e12e4ba6?h=854&resize=1&w=640",
            "logo": "https://cdn2.unrealengine.com/egs-railgrade-carousel-logo-350x147-fc813818b08f.png",
            "age_restriction": "G",
            "is_game": false,
            "parent_game": 1,
            "developer": [
                {
                    "developerId": 1,
                    "name": "Rockstar Games",
                    "image": ""
                }
            ],
            "category": [
                "Simulación",
                "Estrategia"
            ],
            "images": [],
            "price": "66.47",
            "discount": "0.00",
            "end_date": "2023-01-01T01:57:40.000Z",
            "available": true
        },
        {
            "id_product": 32,
            "product_name": "DLC: RAILGRADE",
            "description": "RAILGRADE es un simulador de gestión en el que debes utilizar el ferrocarril para transportar recursos y suministrar energía a la industria en un remoto mundo colonial. Como responsable de administrar el planeta, tendrás que trabajar sin descanso para restaurar la producción industrial tras un catastrófico colapso de las infraestructuras.",
            "short_description": null,
            "release_date": "2022-09-29T06:00:00.000Z",
            "registration_date": "2022-10-01T05:46:28.253Z",
            "active": true,
            "desktop_image": "https://cdn2.unrealengine.com/egs-railgrade-minakatadynamics-g1c-00-1920x1080-b5e15bc976e3.jpg?h=720&resize=1&w=1280",
            "mobile_image": "https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_RAILGRADE_MinakataDynamics_S2_1200x1600-f40aa411ee439ddd990de6b9e12e4ba6?h=854&resize=1&w=640",
            "logo": "https://cdn2.unrealengine.com/egs-railgrade-carousel-logo-350x147-fc813818b08f.png",
            "age_restriction": "G",
            "is_game": false,
            "parent_game": 1,
            "developer": [
                {
                    "developerId": 1,
                    "name": "Rockstar Games",
                    "image": ""
                }
            ],
            "category": [
                "Simulación",
                "Estrategia"
            ],
            "images": [],
            "price": "135.88",
            "discount": "0.00",
            "end_date": "2022-12-31T19:24:51.000Z",
            "available": true
        },
        {
            "id_product": 33,
            "product_name": "DLC: RAILGRADE",
            "description": "RAILGRADE es un simulador de gestión en el que debes utilizar el ferrocarril para transportar recursos y suministrar energía a la industria en un remoto mundo colonial. Como responsable de administrar el planeta, tendrás que trabajar sin descanso para restaurar la producción industrial tras un catastrófico colapso de las infraestructuras.",
            "short_description": null,
            "release_date": "2022-09-29T06:00:00.000Z",
            "registration_date": "2022-10-01T05:46:28.253Z",
            "active": true,
            "desktop_image": "https://media-cdn.epicgames.com/c5b50574bd2d42e78741688dadf743f7/c5b50574bd2d42e78741688dadf743f7-00001-thumb.png?h=720&resize=1&w=1280",
            "mobile_image": "https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_RAILGRADE_MinakataDynamics_S2_1200x1600-f40aa411ee439ddd990de6b9e12e4ba6?h=854&resize=1&w=640",
            "logo": "https://cdn2.unrealengine.com/egs-railgrade-carousel-logo-350x147-fc813818b08f.png",
            "age_restriction": "G",
            "is_game": false,
            "parent_game": 1,
            "developer": [
                {
                    "developerId": 1,
                    "name": "Rockstar Games",
                    "image": ""
                }
            ],
            "category": [
                "Simulación",
                "Estrategia"
            ],
            "images": [],
            "price": "105.16",
            "discount": "0.00",
            "end_date": "2022-12-31T08:27:49.000Z",
            "available": true
        }
    ]
}
```

> Respuesta **fallida 400**

```
{
    "success": false,
    "data": {
        "error": "GD-005"
    }
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "content": {},
    "message": "error: value too long for type character varying(13)"
}
```

### Biblioteca de juegos

#### Ver juegos comprados

Este servicio ofrece la funcionalidad de ver la lista de juegos que han sido comprados por el usuario desde la apertura de su cuenta dentro de la plataforma.

| **ID**                   | 003                                                                                                                                                                                                                                                      |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Prioridad:**           | Alta                                                                                                                                                                                                                                                     |
| **Módulo:**              | Biblioteca de Juegos                                                                                                                                                                                                                                     |
| **Nombre:**              | Ver lista de Juegos Comprados                                                                                                                                                                                                                            |
| **Historia de usuario:** | Como usuario despues de hacer la compra de un juego se dirige a la seccion de biblioteca para ver todos los juegos que ha comprado hasta la fecha para poder ordenarlos por fecha de compra, fecha de lanzamiento, desarrollador, genero o calificacion. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/biblioteca/:username
>
> **Método:** GET
>
> **Formato de entrada:** Parametro

| **Parametro** | **Tipo** |                     **Descripción**                     |
| :-----------: | :------: | :-----------------------------------------------------: |
|   username    |  string  | Nombre de usuario con el cual el usuario fue registrado |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | Mensaje de confirmación.                                |
| message  | string  | En un response exitoso no se usa este campo.            |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| content  | object  | En un response no exitoso no se usa este campo.         |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **Se debe verificar que el nombre de usuario es correcto.**

`Ejemplos`

> Entrada

```
https://base_url/api/biblioteca/airton47
```

> Respuesta **exitosa**

```json
{
  "status": true,
  "content": [
    {
      "gameId": 1,
      "fechaCompra": "DD/MM/YYYY",
      "calificacion": "4.5",
      "name": "Assasins Creed",
      "image": "https://imagelURL",
      "releaseDate": "DD/MM/YYYY",
      "restrictionAge": "T",
      "price": 20.9,
      "discount": 1.9,
      "group": 1,
      "developer": ["Dev1"],
      "category": ["EC"],
      "region": ["Guatemala"]
    },
    {
      "gameId": 2,
      "fechaCompra": "DD/MM/YYYY",
      "calificacion": "4.5",
      "name": "Toy Story",
      "image": "https://imagelURL",
      "releaseDate": "DD/MM/YYYY",
      "restrictionAge": "T",
      "price": 20.9,
      "discount": 1.9,
      "group": 1,
      "developer": ["Dev1"],
      "category": ["EC"],
      "region": ["Petapa"]
    },
    {
      "gameId": 3,
      "fechaCompra": "DD/MM/YYYY",
      "calificacion": "4.5",
      "name": "Harry Potter",
      "image": "https://imagelURL",
      "releaseDate": "DD/MM/YYYY",
      "restrictionAge": "T",
      "price": 20.9,
      "discount": 1.9,
      "group": 1,
      "developer": ["Dev1"],
      "category": ["EC"],
      "region": ["Villa Hermosa"]
    }
  ],
  "message": "Se ha obtenido lista de juegos comprados con exito"
}
```

> Respuesta **fallida 400**

```json
{
  "status": false,
  "content": {},
  "message": [
    {
      "msg": "The following params in request are required: username",
      "param": "username",
      "location": "request"
    }
  ]
}
```

> Respuesta **fallida 500**

```json
{
  "status": false,
  "content": {},
  "message": "User could not be found with username provided"
}
```

### Funciones administrativas

#### **Dar de baja usuario**

Por medio de esta funcionalidad se da de baja a los usuarios que cometieron alguna falta grave a las normas de la plataforma.

| <!-- -->                 | <!-- -->                                                                                                                             |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------ |
| **ID**                   | 001                                                                                                                                  |
| **Prioridad:**           | Alta                                                                                                                                 |
| **Módulo:**              | Administrativo                                                                                                                       |
| **Nombre:**              | Microservicio dar de baja usaurios                                                                                                   |
| **Historia de usuario:** | El adminsitrador debe tener la posibilidad de dar de baja a los usuarios que incumplan con las normas de convivencia en la comunidad |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/administrator/unsubscribe
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo | Tipo   | Descripción                                                |
| :------- | :----- | :--------------------------------------------------------- |
| idUser   | number | Id del usuario a dar de baja                               |
| message  | string | Mensaje explicando el por que se le dio de baja al usuario |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | string  | Indica el exito de la petición.                         |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | string  | Describe el error.                                      |

**Validaciones requeridas:**

- **idUser**: El campo es requerido, debe ser un campo numerico y un id existente.
- **message**: El campo es requerido y debe ser un string.

`Ejemplos`

> Entrada

```
{
  "idUser": 45,
  "message": "Su cuenta sera dada de baja pues se detectaron inicios de sesión incongruentes en cuanto a la hora y ubicación de los mismos, pruebas suficientes para determinar que la cuenta esta siendo compartida"
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "message": "La desactivación y notificación se realizo de manera exitosa"
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": "No se encontro al usuario especificado"
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

#### **Crear descuentos**

Por medio de esta funcionalidad el adminsitrador puede crear descuentos por fecha y región para un juego en especifico.

| <!-- -->                 | <!-- -->                                                                                                     |
| ------------------------ | ------------------------------------------------------------------------------------------------------------ |
| **ID**                   | 001                                                                                                          |
| **Prioridad:**           | Alta                                                                                                         |
| **Módulo:**              | Administrativo                                                                                               |
| **Nombre:**              | Microservicio para descuentos                                                                                |
| **Historia de usuario:** | El adminsitrador debe ser capaz de crear descuentos por región y fecha para cualquier juego de la plataforma |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /apis/administrator/discount
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo  | Tipo           | Descripción                                                                                  |
| :-------- | :------------- | :------------------------------------------------------------------------------------------- |
| idGame    | number         | Id del juego al que se le aplicara el descuento                                              |
| price     | number         | Precio del juego                                                                             |
| discount  | number         | Descuento a aplicar en el juego                                                              |
| startDate | date           | Fecha de inicio del descuento                                                                |
| finalDate | date           | Fecha de fin del descuento                                                                   |
| region    | list[idRegion] | Lista de las regiones par las que estara disponible el descuento, puede ser una lista vacia. |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | string  | Indica el exito de la petición.                         |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Request inválido           |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| status   | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| message  | object  | Describe el error.                                      |

**Validaciones requeridas:**

- **idGame**: El campo es requerido, debe ser un campo numerico y un id existente.
- **price, discount**: Campos requeridos, deben ser positivos y discount no puede ser mayor a price.
- **startDate, finalDate**: Campos requeridos, deben ser fechas mayores o iguales a la actual, startDate no puede ser mayor a finalDate.
- **region**: Debe ser una lista, en caso de aplicar para todas las regiones, sera una lista vacia.

`Ejemplos`

> Entrada

```
{
  "idGame": 45,
  "price": 500,
  "discount": 200,
  "startDate": 20/09/2022,
  "finalDate": 20/10/2022,
  "region": []
}
```

> Respuesta **exitosa**

```
{
    "status": true,
    "message": "Se agrego el descuento de manera exitosa"
}
```

> Respuesta **fallida 400**

```
{
    "status": false,
    "message": [
      {
        "msg": "end date cannot be less than start date",
        "param": "finalDate",
        "location": "body"
      }
    ]
}
```

> Respuesta **fallida 500**

```
{
    "status": false,
    "message": "error: There was an error in the connection"
}
```

## Versionamiento

Para un proyecto colaborativo como este una herramienta de versionamiento es imprescindible, además de esto también se debe considerar que se necesita una herramienta que proporcione facilidades para gestionar una integración y despliegue continuos, debido a que esta misma herramienta se utilizará para realizar los despliegues cuando se realicen cambios sobre una rama principal del flujo, la herramienta por excelencia que hace todo esto de manera conjunta sin depender de otras implementaciones es GitLab.

GitLab es un sistema de control de versiones al igual que muchos otros pero que a su vez facilita y automatiza completamente el proceso de integración y despliegue de las aplicaciones con muchas opciones de configuración además de la disponibilidad que se tiene con diferentes plataformas en la nube, en este caso Google Cloud Platform, GitLab además provee un área dedicado a Pipelines en el cual se puede visualizar de manera gráfica cada uno de los despliegues que se realizan y el estado en el que concluyó la petición, si hubo algún error o si la operación fue exitosa.

Las integraciones con diferentes proveedores y plataformas que se pueden realizar con GitLab también son varias, se puede integrar con la herramienta para la gestión de proyectos seleccionada (JIRA) y también con plataformas de comunicación profesional como Slack, sin mencionar otras plataformas que aceleran el proceso de gestión de las versiones como GitKraken. Por esto y otras ventajas que tienen que ver con la experiencia de usuario al utilizar la herramienta se ha decidido utilizar como herramienta de versionamiento, despliegue e integración continua a GitLab.

**GitFlow:**

Para una mejor gestión del trabajo colaborativo, se implementará el modelo de GitFlow para gestionar las ramas dentro del repositorio e integrar los cambios de una forma uniforme y ordenada bajo las siguientes reglas que se deberán seguir para una estandarización en la documentación del proyecto:

- **Rama main** como rama base para la ejecución de pipelines y otras gestiones de despliegue dentro del proyecto.

- **Rama develop** como rama principal para la integración de los cambios entre diferentes desarrolladore, en esta rama es donde se realizan los merge request de todas las ramas de desarrollo que se generen.

- **Ramas de desarrollo** dependiendo del ticket que tenga asignado en JIRA será el nombre de cada una de las ramas y los cambios que se realicen dentro de estas.

Todo esto con un formato estándar para los merge requests se mantendrá estandarizado con el título de la incidencia creada en JIRA como título de merge request, una corta descripción de la totalidad de trabajo que se realizó en la rama determinada, esto para poder consultar y hacer seguimiento de algún error si llega a surgir una vez ya se encuentre implementado en la rama develop.

Cada uno de estos merge request deberá tener persona asignada, persona encargada de revisar los cambios y etiqueta la cual la identificará dentro de las diferentes catetgorías en las que se puede realizar un desarrollo, esto se puede ver posteriormente en la parte inferior al momento de crear un merge request para una revisión posterior de la documentación generada.

## Documentación de los pipelines

Los pipelines son grupos de procesos que normalmente se utilizan para automatizar la compilación, la construcción y el despliegue de código a las plataformas de pruebas o de producción correspondientemente, este tipo de procesos se realiza por parte de los encargados de las operaciones de desarrollo o profesionales DevOps, y agilizan en gran manera los esfuerzos para el desarrollo de un proyecto.

En este caso se ejecutará una secuencia de pipelines en un orden específico para el correcto despliegue de la aplicación, si uno de estos pipelines fallara, el proceso completo se detendría, por lo que es muy útil realizar las pruebas necesarias antes de realizar el pipeline que despliega la aplicación a la infraestructura pertinente, por lo que el flujo de pipelines resultante sería el siguiente:

1. **Auditoría de seguridad:** Este pipeline verifica que todas las dependencias que son necesarias dentro de la aplicación estén correctamente implementadas e instaladas y estas no generen nuevos errores actualmente ni en un futuro.

2. **Revisión de código:** Este pipeline hace que el linter que se haya configurado junto con el prettier hagan una revisión completa del código que se está subiendo, por lo que todo el nuevo código subido debe superar las pruebas que estos plugins dictaminen.

3. **Pruebas:** Realiza todas las pruebas programadas dentro de la aplicación, esto para garantizar que ninguna funcionalidad del proyecto fue alterada o dañada en el la nueva versión a desplegar.

4. **Generación de imagen:** Este pipeline realiza la imagen correspondiente de la implementación de la nueva versión, asignandole la tag de latest, una vez realizada la imagen se realiza un push al repositorio de contenedores DockerHub para su posterior despliegue.

5. **Despliegue:** Este pipeline ejecuta todos los comandos en la nube que son necesaros para realizar el despliegue de la aplicación desde el orquestador de contenedores basándose en la última versión de la imagen en Docker como es en este caso.

Con este proceso de pipelines se garantiza que el código que se implementa versión tras versión es un código funcional, limpio y que obedece a los altos estándares de calidad independientemente el lenguaje que se utilice para su programación, además de verificar que ninguno de los componentes o funcionalidades se vea alterado o dañado con el paso de las actualizaciones.

![Pipelines](./imgs/pipelines.jpeg)

## Diagrama de Actividades

Para un mejor entendimiento de las actividades de algunos de los servicios que se describieron se realizará un diagrama de actividades de servicios específicos que son de importancia dentro de la implementación. Este tipo de reportes normalmente se requieren en un futuro para las personas que desarrollan tanto el código como la infraestructura necesaria para la implementación de la solución, esto implementando todo lo que se mencionó anteriormente en este documento.

### Regitro de Usuarios

![Registro de Usuarios](./imgs/register.png)

### Inicio de Sesión

![Inicio de sesión](./imgs/login.png)

### Agregar a lista de deseos

![Agregar a lista de deseos](./imgs/favs.png)

### Agregar tarjeta

![Agregar tarjeta](./imgs/card.png)

### Agregar al carrito

![Agregar al carrito](./imgs/addCart.png)

### Remover del carrito

![Remover del carrito](./imgs/removeCart.png)

Siempre dentro de los actores se tendrán a estas tres entidades de manera estándar, esto debido a que en una vista general se tiene un modelo de tres capas con implementación interna orientada a microservicios, por lo que siempre los actores serán el usuario o administrador que realiza la acción manualmente, el sistema que atiende y realiza las funciones necesarias para que se ejecute correctamente el flujo descrito y la base de datos encargada de almacenar datos o validar datos específicos.

## Bitácora de Tareas

El control sobre las tareas que se deben realizar para el correcto desarrollo del proyecto junto con sus respectivas subtareas se planearon y definieron completamente en un tablero implementado en JIRA, como se definirá más adelante, en este se definieron las etapas que tendrán cada una de estas actividades, la descripción asociada a las mismas y el tiempo estimado que se tiene para solucionarlas. Para acceder al tablero utilizado para este proyecto se puede consultar la [Implementación de tablero en JIRA](https://ayd2-grupo11.atlassian.net/jira/software/projects/SAG6/boards/4).

Dentro de este tablero a cada uno de los integrantes se les definieron sus tareas correspondientemente, en este caso como es el primer sprint se trabajarán unicamente las documentaciones de los microservicios que se implementarán en un futuro, además de todo este documento que es necesario para la completa descripción de la solución y sus apartados detallados de manera específica.

El tablero principal está dividido en seis columnas diferentes las cuales tienen cada una una funcionalidad específica o indica algún estado en concreto en el que se encuentran cada una de las incidencias, estas columnas no siempre son secuenciales por lo que se detallará su orden y descripción brevemente:

- **TO DO:** Columna para todas las tareas o incidencias dentro de este sprint que no han sido inicializadas de momento, al principio del sprint todas las actividades se encuentran en este estado.

- **BLOCKED:** Columna para todas las tareas o incidencias dentro de este sprint que se encuentran en un bloqueo o que dependan de actividades que aún no han sido finalizadas por parte de otro desarrollador, o cualquier tipo de situación que impida su avance en un período de tiempo determinado.

- **IN PROGRESS:** Columna para todas las tareas o incidencias dentro de este sprint que ya han sido analizadas y están en proceso de realización, el tiempo que dura en este estado debe ser aproximado con los story points o SP asignados al ticket en la plataforma.

- **TESTING/QA:** Columna para todas las tareas o incidencias dentro de este sprint que han sido finalizadas, y necesitan una revisión final para estar listas para producción.

- **READY FOR PRODUCTION:** Columna para todas las tareas o incidencias dentro de este sprint que ya han sido probadas, y aprobadas por personal capacitado, y en este momento se encuentran estos cambios listos para ser enviados en el despliegue de la siguiente versión de la aplicación.

- **DONE:** Columna para todas las tareas o incidencias dentro de este sprint que han sido desplegados a la infraestructura en la nube y ya se encuentra disponible en la aplicación de producción, este estado es en el que finaliza el proceso.

El flujo de trabajo se puede visualizar, sin embargo hay algunas restricciones, el estado de BLOCKED puede ocurrir en cualquier momento del proceso y no tiene un orden específico, adicionalmente se tiene una condición la cual establece que para pasar al estado DONE se debe estar inicialmente en READY FOR PRODUCTION, no se pueden realizar cambios abruptos dentro del tablero por lo que el seguimiento al flujo es importante al momento de estar desarrollando una tarea dentro de este tablero ágil.

![Board](./imgs/board.jpeg)

Obteniendo la siguiente división:

![Board2](./imgs/board2.jpeg)

## Metodología Ágil

Para optimizar la distribución de tareas y la comunicación general entre los integrantes del grupo y los interesados en el proyecto se implementará una metodología ágil, esto con el fin de agregar funcionalidad al proyecto de forma iterativa, en diferentes lapsos de tiempo con diferentes objetivos cada uno, en este caso, las opciones que se tienen varias opciones disponibles que son altamente utilizados en empresas con alta demanda empresarial, entre ellas están las siguientes:

- Extreme Programming
- SCRUM
- Kanban

Dentro de estas posibilidades, debido a que este proyecto no está destinado para una empresa startup, Extreme Programming no es la metodología más adecuada para llevarlo a cabo, normalmente es recomendable para este tipo de empresas porque el enfoque que se le da al desarrollo es precisamente las relaciones personales a través de un trabajo en equipo para fomentar la comunicación entre sus integrantes, lo cual no es el objetivo principal de este proyecto.

Kanban podría ser una metodología ágil para mejorar la productividad y la eficiencia del equipo, sin embargo, al estar especializado en la elaboración de cuadros o diagramas en la repetición de las tareas, y no llevar un control riguroso del proceso que se está teniendo en un momento determinado, el tablero Kanban en sí es útil pero no es suficiente para lo que se necesita en esta implementación, por lo que sólo nos queda una única metodología a utilizar.

SCRUM es una metodología ágil que se cimienta en el concepto del desarrollo incremental, esto quiere decir que se secciona el trabajo en segmentos de menor complejidad, esto para lo que se desea implementar es perfecto debido a que los microservicios tienen exactamente el mismo objetivo, dividir la complejidad de un sistema en servicios y microservicios de menor carga para el desarrollo, por lo que esta implementación es la indicada desde este punto de vista, además debido a que el proyecto se debe entregar en fases, estas se pueden representar en base a las iteraciones de SCRUM llamadas Sprint, que duran aproximadamente el tiempo que se está dando para cada fase del proyecto, por último, la documentación que se genera de un proyecto SCRUM (las reuniones que se realizan para su planificación, revisión diaria, y de análisis retrospectivo, etc.) son de vital importancia para proponer mejoras en el avance del proyecto lo que proporciona mayor productividad, flexibilidad y competitividad al producto final que se desea realizar.

Por este análisis realizado, la metodología ágil que se utilizará en todo el proyecto será SCRUM.

**Metodología SCRUM**

Este marco de trabajo es un método para colaborar entre personas de un mismo equipo, o incluso trabajo colaborativo entre equipos, promueve el aprendizaje a través de la experiencia que se gana durante el desarrollo, además que ayuda a la autoorganización de los equipos mientras se aborda un problema, además de que una vez solventado, se reflexiona sobre los logros y fracasos que se obtuvieron para mejorar de forma continua e iterativa, aunque este tipo de pensamiento se puede aplicar para todo tipo de trabajo en equipo, se especializará en este documento las fases que se utilizarán para esta implementación en específico para no generar documentación innecesaria. Esta metodología posee varios conceptos o fases dentro de su enfoque estos son:

Sprints
Cuando se habla de Sprints, se hablan de períodos de breve tiempo en el cual se trabaja para completar una cantidad de trabajo definida al principio como se detallará más adelante, este período de tiempo es fijo y no puede ser alterado incluso bajo percances de retraso, todas las actividades que se realizan dentro de cada uno de los sprints se explicarán a continuación.

- **Sprint Planning:** Es una reunión en la que se definen completamente las tareas que se entregarán al final del sprint, además de cómo se va a lograr este cometido en el tiempo determinado que durará el Sprint.

- **Daily:** Es una reunión en la que cada uno de los integrantes de un equipo pequeño presenta sus avances que ha tenido hasta el momento, lo que trabajará en el futuro y si se ha encontrado con algún problema o dificultad durante el proceso de desarrollo.

- **Sprint Retrospective:** Es una reunión en la que se cierra el Sprint, se dan por concluidas las tareas finalizadas, y se analiza el por qué algunas se terminaron demasiado rápido, o demasiado lento, o si algunas no se terminaron, en general un análisis completo del resultado obtenido del sprint para una mejora en el siguiente que se realice.

No se realizarán otras reuniones dentro de este proyecto debido al alcance que tiene, en este caso realizar más reuniones dentro de las que se hacen en SCRUM sería redundante, debido a que con las dailys se puede solventar todo lo que se haría en una Sprint Grooming por ejemplo.

Dentro de cada uno de los sprints se guarda la documentación necesaria, en este caso como se trabajará de forma colaborativa entre cinco integrantes de un grupo, la manera comercialmente más conocida de realizara esta documentación fácilmente, como se mencionó es JIRA, esto también en parte es subjetivo, porque dependiendo de las experiencias que los desarrolladores han tenido con respecto a cada uno de estos softwares.

## Descripción de la seguridad de la aplicación

La seguridad es un tema de gran importancia dentro de cualquier aplicación de consumo masivo, en este caso la seguridad de la información que se manejará dentro de la plataforma es de máxima prioridad, por lo que para ello se deberán realizar procedimientos adicionales para la realización de validaciones básicas, encriptación de datos, y verificación de origen y destino de las diferentes fuentes de información para evitar su vulnerabilidad.

Para asegurar la protección de los datos sensibles dentro de la base de datos se utilizará el estándar de encriptación **SHA-256** con el cual se encriptarán datos sensibles de cara al usuario y los administradores. Dentro de estos datos sensibles se encuentran la contraseña para el ingreso al sistema, datos de pago o cualquier información que sea relativa a una autenticación fraudulenta o datos para la compra de algún producto.

Luego se utilizará el protocolo de autorización **OAuth 2.0** para hacer énfasis en la simplicidad al mismo tiempo que proveer de una autorización específica a través de Bearer tokens los cuales se refrescarán y validarán cada vez que se realice una petición al servidor, con esto se garantiza que un usuario no autorizado consuma servicios del servidor de forma no segura, al mismo tiempo este tipo de autenticación brinda seguridad a la infraestructura que se encuentra detrás del backend, en este caso la base de datos misma.

Por último como última línea de defensa se tiene la configuración de la red interna montada en nube, utilizando esta **Virtual Private Cloud (VPC)** se pueden configurar **Reglas de Firewall** específicas para que únicamente el backend tenga acceso al servicio que alberga la base de datos, y además de esto que solo el cliente pueda acceder servidor de Kubernetes para la gestión de peticiones.

La combinación de todos estos métodos de seguridad hacen que la estructura de cara a la seguridad que se ofrece en todos los aspectos al usuario y al adminsitrador, además que este tipo de solución necesita de la certificación SSL para la encriptación de datos entre máquinas y la consulta de datos a través del protocolo HTTPS.

## Comunicación de los servicios disponibles para todos los grupos

Para una implementación inter-plataforma se estableció un estándar el cual se debe seguir para todas las plataformas, tanto para generar los servicios como para consumirlos, se estableció en la documentación siguiente los parámetros que se deben enviar y lo que se debe recibir para cada uno, esto es un acuerdo consensuado entre las distintas plataformas por lo que debe ser respetado y seguido al pie de la letra. La documentación será del mismo tipo que para el resto de microservicios en este documento por lo que los microservicios para el consumo global serán los siguientes.

#### **Ver desarrolladores de juegos**

Esta funcionalidad fue elegida para visualizar los desarrolladores en general de todos los juegos entre distintas plataformas.

| <!-- -->                 | <!-- -->                                                                                                                                    |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | XX1                                                                                                                                         |
| **Prioridad:**           | Máxima                                                                                                                                      |
| **Módulo:**              | Interconectividad                                                                                                                           |
| **Nombre:**              | Microservicio de Desarrolladores                                                                                                            |
| **Historia de usuario:** | Como usuario necesito la posibilidad de ver todos los desarrolladores que están actualmente desarrollando juegos en diferentes plataformas. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /developers
>
> **Método:** GET
>
> **Formato de entrada:** Sin entrada
>
> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| data     | array   | Lista de desarrolladores resultado de la petición.      |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                    |
| :----- | :----------------------------- |
| 404    | Desarrolladores no encontrados |
| 500    | Error interno del servidor     |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                              |
| :------- | :------ | :------------------------------------------------------- |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición.  |
| data     | array   | Una lista vacía que representa los datos no encontrados. |

**Validaciones requeridas:**

- Ninguna

`Ejemplos`

> Respuesta **exitosa**

```
{
	"success": true,
	"data": [
		{
			"developerId": 0,
			"name": "",
			"image": ""
		},
		{
			"developerId": 0,
			"name": "",
			"image": ""
		},
	]
}
```

> Respuesta **fallida 404**

```
{
	"success": false,
	"data": [	]
}
```

> Respuesta **fallida 500**

```
{
	"success": false,
	"data": [	]
}
```

#### **Ver juegos**

Esta funcionalidad fue elegida para visualizar todos los juegos entre distintas plataformas.

| <!-- -->                 | <!-- -->                                                                                                                  |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | XX2                                                                                                                       |
| **Prioridad:**           | Máxima                                                                                                                    |
| **Módulo:**              | Interconectividad                                                                                                         |
| **Nombre:**              | Microservicio de Visualización de juegos                                                                                  |
| **Historia de usuario:** | Como usuario necesito la posibilidad de ver todos los juegos que están actualmente disponibles en diferentes plataformas. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /games
>
> **Método:** GET
>
> **Formato de entrada:** Sin entrada
>
> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                             |
| :------- | :------ | :------------------------------------------------------ |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición. |
| data     | array   | Lista de juegos resultado de la petición.               |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 404    | Juegos no encontrados      |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                              |
| :------- | :------ | :------------------------------------------------------- |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición.  |
| data     | array   | Una lista vacía que representa los datos no encontrados. |

**Validaciones requeridas:**

- Ninguna

`Ejemplos`

> Respuesta **exitosa**

```
{
	"success": true,
	"data": [
			{
				"gameId": 1,
				"name": "",
				"image": "",
				"releaseDate": "DD/MM/YYYY",
				"restrictionAge": "T",
				"price": 0,
				"discount": 0,
				"group": 1,
				"developer": [],
				"category": [],
				"region": []
			},
			{
				"gameId": 2,
				"name": "",
				"image": "",
				"releaseDate": "DD/MM/YYYY",
				"restrictionAge": "T",
				"price": 0,
				"discount": 0,
				"group": 1,
				"developer": [],
				"category": [],
				"region": []
			}
	]
}
```

> Respuesta **fallida 404**

```
{
	"success": false,
	"data": [	]
}
```

> Respuesta **fallida 500**

```
{
	"success": false,
	"data": [	]
}
```

#### **Compra de juegos**

Esta funcionalidad fue elegida para la compra de los juegos entre distintas plataformas.

| <!-- -->                 | <!-- -->                                                                                                                  |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------- |
| **ID**                   | XX3                                                                                                                       |
| **Prioridad:**           | Máxima                                                                                                                    |
| **Módulo:**              | Interconectividad                                                                                                         |
| **Nombre:**              | Microservicio de compra de juegos                                                                                         |
| **Historia de usuario:** | Como usuario necesito la posibilidad de poder comprar juegos que están actualmente disponibles en diferentes plataformas. |

`Criterios de aceptación`

El servicio debe contener la siguiente configuración

> **Ruta:** /shopping
>
> **Método:** POST
>
> **Formato de entrada:** JSON

| Atributo      | Tipo   | Descripción                                                                         |
| :------------ | :----- | :---------------------------------------------------------------------------------- |
| games         | array  | Lista de juegos que se comprará en la petición.                                     |
| Authorization | Header | Bearer token el cual tendrá la información pertinente al usuario dentro del payload |

> **Formato de salida:** JSON
>
> **Código de respuesta exitosa:** HTTP 200

| Atributo | Tipo    | Descripción                                              |
| :------- | :------ | :------------------------------------------------------- |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición.  |
| data     | object  | Información pertinente al estado de la compra realizada. |

`Fallo`

Se utilizará la siguiente lista de errores como referencia: https://docs.microsoft.com/en-us/partner-center/develop/error-codes

| Código | Descripción                |
| :----- | :------------------------- |
| 400    | Petición no válida         |
| 500    | Error interno del servidor |

Para obtener el siguiente **body de salida:**

| Atributo | Tipo    | Descripción                                               |
| :------- | :------ | :-------------------------------------------------------- |
| success  | boolean | Identifica a nivel abstracto el exitoso de la petición.   |
| data     | object  | Un objeto vacío que representa la transacción no exitosa. |

**Validaciones requeridas:**

- **games**: El campo es requerido, debe ser un campo tipo array y debe tener entradas válidas.
- **Header Authorization**: Campo requerido que debe tener el bearer token para la autenticación del usuario.

`Ejemplos`

> Entrada

```
Headers: Authorization -> Bearer <token>
PAYLOAD:
{
	"group": 7,
	"userId": 1,
	"email": "",
	"name": "",
	"region": ""
}
```

```
{
	"games": [
		{
			"gameId": 0,
			"gameName": "",
			"price": 0,
			"discount": 0
		},
		{
			"gameId": 0,
			"gameName": "",
			"price": 0,
			"discount": 0,
		}
	]
}
```

> Respuesta **exitosa**

```
{
	"success": true,
	"data": {
		"total": 0,
		"totalWithOffer": 0,
		"date": "DD/MM/YYYY HH:MM:SS",
		"games": [
			{"name": "", "offer": "", "price": 0},
			{"name": "", "offer": "", "price": 0}
		]
	}
}
```

> Respuesta **fallida 404**

```
{
	"success": false,
	"data": [	]
}
```

> Respuesta **fallida 500**

```
{
	"success": false,
	"data": [	]
}
```
