# Sprint 2

## Bitácora 19/09/2022

Este día se realizó la primera conferencia para la definición principal de las tareas con respecto a la solicitud realizada, la plataforma para llevar seguimiento de las tareas y la distribución de ramas de desarrollo de nuevas características de la aplicación será JIRA, el recuento de tareas que se trabajaorn se puede ver en el backlog del proyecto en el enlace [aquí.](https://ayd2-grupo11.atlassian.net/jira/software/projects/SAG6/boards/4/backlog)

Una vez definidas las actividades también se estandarizó el formato que se seguirá en GitFlow dentro del repositorio, nombres, descripciones tanto dentro de las actividades en JIRA como dentro de las operaciones que se puedan hacer de forma colaborativa en el repositorio como merge requests y commits.

La tareas que se definieron para el primer sprint fueron sencillas y se conformaron por las siguientes:

![Sprint 2](./imgs/sprint1.jpg)

El sprint estará previsto para cerrar en una semana aproximaamente, es decir hasta el siguiente fin de semana entre 24 y 25 de septiembre dependiendo de como se desenvuelva el proyecto, los avances de cada uno de los días se darán en este mismo formato correspondientemente.

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 0% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 0% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 0% |


## Bitácora 20/09/2022

Se empezó la documentación del proyecto, esta bitácora específicamente se empezó este día para llevar registro diario de todos los avances que se tengan cada día del proyecto, cada una de las actividades definidas ahora ya poseen una descripción clara y ya tienen integrante definido para su implementación, se realizó una bitácora también agendada en google calendar para finalizar una vez esté completado el proyecto, una reunión diaria a una hora acordada entre todos los integrantes la cual servirá como Daily Scrum Meeting la cual tiene el siguiente [enlace](https://meet.google.com/kke-urpz-ign) por si se necesita verificar algo dentro de la reunión.

Avances dentro de las actividades definidas como tal no se reportaron, a pesar de que se habló que se empezaría con las primeras actividades desde este día en progreso para un estimado de uno o dos días para su finalización como se puede ver en el diagrama del tablero.

![Tablero1](./imgs/tablero1.jpg)

(Imagen de referencia debido a que se tomaron las capturas una vez culminadas las actividades del sprint)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 50% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 50% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 0% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 50% |

## Bitácora 21/09/2022

Se culminaron algunas de las actividades definidas, dentro de ellas se encuentran actividades importantes como la creación del modelo de base de datos y su transformación a script en la nube para una conexión directa posteriormente, también se culminaron algunos microservicios sencillos como el registro, el login y cierto progreso en la autenticación a través de códigos OTP y verificación de tokens para la autenticación del usuarios. Esto al mismo tiempo que se preparaba el diseño principal de la aplicación la cual se decidió por tomarlo de referencia de la gran empresa de venta y distribución de videojuegos EPIC Games, por lo que se desarrollaron algunos bocetos para las vistas de cada una de las páginas que tendría la aplicación al menos de forma inicial.

![Tablero1](./imgs/tablero2.jpg)

(Imagen de referencia debido a que se tomaron las capturas una vez culminadas las actividades del sprint)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 75% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 0% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 10% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 100% |


## Bitácora 22/09/2022

Se implementaron las pantallas principales de frontend para empezar a consumir los endpoints que provee los microservicios de backend, además de la confirmación y validación de los códigos OTP para que sean funcionales de parte del frontend. No se implementaron todas las vistas propuestas debido a que las vistas aumentaron su número para una entrega que se tenía de las vistas anterior, por lo que se le dió prioridad a las generación de las vistas en el día anterior y hasta ahora se empezó con su implementación pero se prevee terminar en uno o dos días la programación de todas las páginas documentadas y mostradas al cliente dias anteriores.

![Tablero1](./imgs/tablero3.jpg)

(Imagen de referencia debido a que se tomaron las capturas una vez culminadas las actividades del sprint)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 50% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 0% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 50% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 100% |

## Bitácora 23/09/2022

Se culminaron los endpoints y microservicios de cara a validación y registro de usuarios, esto considerando todas las pruebas unitarias que fueron necesarias para verificar que casi todas las posibilidades y caminos posibles estén cubiertos y los tests sean efectivos. Se empezó con el proceso más difícil que fue la implementación de integración continua y despliegue continuo usando la plataforma de versionamiento GitLab, para ello fue necesario el script que se tenía hasta el momento de la base de datos y una cuenta disponible en Google CLoud Platform para implementar la base de datos en PostgreSQL de forma nativa en el proveedor de nube descrito, con esto todos los integrantes del grupo tienen acceso a una misma base de datos por lo que se puede trabajar de forma colaborativa las funcionalidades ya sea para nuevas implementaciones o pruebas de las funcionalidades ya culminadas.

![Tablero1](./imgs/tablero4.jpg)

(Imagen de referencia debido a que se tomaron las capturas una vez culminadas las actividades del sprint)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 75% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 20% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 20% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 20% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 20% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 20% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 75% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 100% |

## Bitácora 24/09/2022

Se terminó con la implementación del frontend con todas las ventanas que se definieron para este sprint, además de la implementación de una actualización en la base de datos para tener en cuenta parámetros que no se habían considerado en el script inicial, finalización de la documentación para este Sprint como último día y el cierre del sprint. Sin embargo ocurrió un problema con respecto a la plaaforma JIRA, la cual después de cerrar el sprint (una vez ya culminadas las labores) falló de forma inesperada y dió de baja el servicio hasta que se reportó el problema al día siguiente como se detallará.

![Tablero1](./imgs/tablero5.jpg)

(Imagen de referencia debido a que se tomaron las capturas una vez culminadas las actividades del sprint)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 75% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 75% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 75% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 75% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 100% |

## Bitácora 25/09/2022

Este día se iba a descansar inicialmente, sin embargo se reportó un problema en el tablero de trabajo en JIRA, por tanto se solicitó asistencia respecto al problema, al final luego de esperar respuesta se decidió empezar con otro tablero para el siguiente sprint en la siguiente reunión diaria para no generar más problemas, al final se recuperó el tablero para poder seguir trabajando en el mismo, el tiempo total sin la plataforma fue de 3 días debido a que se corrigió el problema hasta el inicio del segundo sprint al día siguiente como se reportará en la bitácora.

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-54 | API - Generación de plantilla de trabajo | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-49 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-52 | DB - Implementacion | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-59 | API - Registro | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-48 | APP - Login | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-47 | APP - Registro | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-61 | API - Validación de OTP | 2 | Alex Rene López Rosa | 100% |
| SAG6-62 | API - Login | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-60 | API - Generacion de OTP | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-51 | DB - Actualización de modelo | 5 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-53 | DOC - Bitácora de reuniones | 3 | Romeo Ernesto Marroquín Sánchez | 100% |

## Recuento de actividades realizadas

Para realizar el reporte gráfico de actividades realizadas durante todo el sprint basados en los story points asignados a cada una de las actividades programadas se puede observar el siguiente informe extraído a partir de la plataforma de JIRA:

![Informe2](./imgs/informe2.jpg)

<br>

<br>

# Sprint 3

## Bitácora 26/09/2022

Se realizó la definicion y distribución de las nuevas tareas de cara a terminar la fase dos del proyecto al final de la semana, este número de actividades a pesar de ser más elevada, esto se debe a que la granularidad de las mismas es más elevada, además que al contar con el tiempo reducido se debe trabajar lo más que se pueda en el menor tiempo posible para lograr una entrega óptima sin atrasos de la solicitud realizada, se definieron prioridades también con respecto a las tareas de las que depende el resto del equipo para poder continuar trabajando, y otras de menor prioridad que deben ser completadas lo más rápido posible para reasignar las actividades que aún no fueron distribuidas por nivel de prioridad entre los integrantes del equipo.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Sprint 3.1](./imgs/sprint31.jpg)

![Sprint 3.2](./imgs/sprint32.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 0% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 0% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 0% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 0% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 0% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 0% |

## Bitácora 27/09/2022

Se realizó la implementación de algunos endpoints para los microservicios tanto de productos como de DLCs, con sus respectivas pruebas a espera de que frontend confirme el estándar establecido para poder consumirlo desde la aplicación, además que se empezó a llenar la base de datos con títulos de ejemplo para realizar pruebas posteriores de la misma manera con frontend. La priordiad en este momento se tiene en el flujo principal de la aplicación que se pueda interactuar con los productos que ofrecel a empresa, al ser la experiencia de usuario máxima prioridad se hará enfasis en los filtros y la forma de obtener datos de forma eficiente de cara al usuario final para que se obtenga información útil e intuitiva de la forma más rápida posible.

Algunas de las actividades relacionadas a este proceso son las siguientes:

![Tablero 6](./imgs/tablero6.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 10% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 10% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 10% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 0% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 10% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 50% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 0% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 10% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 10% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 0% |

## Bitácora 28/09/2022

Se intentó terminar con la mayor cantidad de microservicios posibles, debido a que se debe avanzar próximamente con el fontend de la aplicación, los servicios de DLCs y gestión de la lista de deseos se terminaron completamente a falta de probar la autorización con tokens de seguridad del lado de producción, se generaron también los reportes de los catálogos para diferentes consultas de los juegos, y la gestión de desarrolladores y categorías como filtros de estos reportes, estos de forma integrada en un solo servicio más complejo el cual se puede acceder a través de dos parámetros desde el frontend. La gestión de OTP también está implementada a falta de pruebas de QA en producción. Mayoritariamente las carencias del proyecto se encuentran en el desarrollo de las páginas que relacionaban a los juegos y los nuevos endpoints implementados.

Algunas de las actividades relacionadas a este proceso son las siguientes:

![Tablero 7](./imgs/tablero7.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 50% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 50% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 50% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 0% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 50% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 50% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 75% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 30% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 50% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 30% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 0% |

## Bitácora 29/09/2022

Pantalla principal de juegos correctamente implementada con nueva información cargada en una nueva base de datos de producción, filtros aplicados tanto para las listas de deseos como para los juegos y DLCs, la implementación de estos endpoints ahora cuenta con autenticación dependiendo de lo que se necesite (login normal para la gestión de la lista de deseos y autenticación como administrador para la gestión de DLC crear, editar y eliminar). También se empezó a decidir el uso de la api para la obtención de la región, las regiones nuevas están cargadas en la base de datos de producción y las prohibiciones de ciertos juegos a ciertos departamentos ya está implementado, sin embargo, aún quedan pendientes validaciones de precios, ofertas y disponibilidad.

Algunas de las actividades relacionadas a este proceso son las siguientes:

![Tablero 8](./imgs/tablero8.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 50% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 60% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 75% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 75% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 20% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 10% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 80% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 80% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 50% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 50% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 50% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 90% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 50% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 0% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 0% |

## Bitácora 30/09/2022

Pantalla principal de DLCs implementada, todos los DLCs para los 30 juegos fueron agregados correctamente, claificados por su categoría y desarrollador correspondientemente, además se agregaron los precios y regiones para su debida clasificación de parte del frontend. Esto solo del lado del cliente principal, de momento los endpoints destinados al usuario administrador (o plataforma de CMS) no está siendo tomada en cuenta, esas implementaciones se tratarán el siguiente día junto con wish list, el cual se deben modificar algunos de sus parámetros para ordenarlos de una mejor manera en la vista correspondiente.

Algunas de las actividades relacionadas a este proceso son las siguientes:

![Tablero 9](./imgs/tablero9.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 75% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 75% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 90% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 90% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 70% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 30% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 75% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 70% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 100% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 80% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 30% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 0% |

## Bitácora 01/10/2022

Endpoints y pantallas de CMS implementadas de la mejor forma posible, documentación finalizada y actualizaciones para los entornos de producción realizadas tanto en la base de datos, como en la API como en la APP. Reporte de JIRA generado y el sprint se cerró con las tareas que se completaron. También se desarrollaron las pruebas unitarias de los endpoints como petición del cliente adicional para esta entrega al finalizar este sprint, la lista de deseos también fue implementada y documentada exitosamente y luego de la reunión de retrospectiva, se cerró el sprint exitosamente.

Algunas de las actividades relacionadas a este proceso son las siguientes:

![Tablero 9](./imgs/tablero9.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-57 | API - Lista de deseos | 3 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-56 | API - Contenido descargable | 5 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-76 | API - Creación de desarrolladores | 3 | Airton Yelstin de León Obando | 100% |
| SAG6-75 | API - Crear y dar de baja juegos | 3 | Airton Yelstin de León Obando | 100% |
| SAG6-77 | API - Restricciones por región |5 | Airton Yelstin de León Obando | 100% |
| SAG6-74 | API - Creación y extensión de ofertas | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-73 | API - Listado de juegos con respectivos filtros | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-55 | API - Catálogo de juegos | 3 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-79 | APP - Categorías | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-78 | APP - Home | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-64 | APP - OTP | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-81 | APP - Extras | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-80 | APP - Desarrolladores | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-82 | APP - Detalles | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-85 | CI/CD - API | 5 | Fernando José Vásquez Castillo | 100% |
| SAG6-70 | DB - Llenar base de datos con desarrolladores categorías y juegos | 5 | Alex Rene López Rosa | 100% |
| SAG6-72 | DOC - Realizar documentación | 3 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-83 | APP - Lista de deseos | 3 | Fernando José Vásquez Castillo | 100% |
| SAG6-84 | APP - CMS | 3 | Fernando José Vásquez Castillo | 100% |

## Recuento de actividades realizadas

Para realizar el reporte gráfico de actividades realizadas durante todo el sprint basados en los story points asignados a cada una de las actividades programadas se puede observar el siguiente informe extraído a partir de la plataforma de JIRA:

![Informe3](./imgs/informe3.jpg)

<br>

<br>

# Sprint 4

## Bitácora 11/10/2022

Se realizó la definicion y distribución de las nuevas tareas de cara a terminar la fase tres del proyecto al final de la semana, se segmentó y distribuyó de mejor forma el trabajo, esto se debe a que las veces pasadas se tuvo problemas de administración de tiempo, con esta nueva distribución de actividades se espera obtener un mejor resultado de cara a terminar todo lo que corresponde a la fase tres del proyecto de forma satisfactoria en un tiempo cómodo para la entrega final.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Sprint 4](./imgs/board3.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 0% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 0% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 14% |


## Bitácora 12/10/2022

Se empezaron a realizar los endpoints acordados, además de la actualización del modelo de base de datos que se tenía en un principio, esto debido a que en la nueva fase se necesita generar reportes sobre usuarios, por lo tanto los cambios en las tablas relacionadas y la creación de una nueva tablas de reportes se tuvo que realizar en la base de datos de pruebas o "staging", además de la culminación de dos endpoints importantes, el login con los nuevos cambios y el de obtener los usuarios con un usuario ya logueado para poder reportar a otros usuarios.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.1](./imgs/tablero10.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 90% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 0% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 0% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 28% |


## Bitácora 13/10/2022

El frontend empezó con el desarrollo de las páginas que harán falta para la implementación de las nuevas funcionalidade que requiere la app para este nuevo sprint, mientras los endpoints de backend se están fializando, el día de mañana se espera que los endpoints queden completamente implementados para que el frontend pueda enfocar todo el tiempo en consumirlos de forma exitosa y que el flujo de trabajo se genere de la forma correcta, el progreso que se ha tenido en el avance de los endpoints se puede ver en la tabla que se adjunta por si se quiere validar específicamente alguno de ellos.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.2](./imgs/tablero11.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 50% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 20% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 20% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 70% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 50% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 20% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 40% |


## Bitácora 14/10/2022

Todos los endpoints del backend fueron implementados exitosamente, algunos a falta de pruebas que se harán ya en la aplicación, y las páginas que servirán para cada una de ellas se implementarán mañana (sábado) para poder descansar domingo antes empezar el siguiente sprint.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.3](./imgs/tablero12.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 50% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 50% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 50% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 50% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 52% |

## Bitácora 15/10/2022

Endpoints que se tenían para este sprint completados y revisados exitosamente, se hizo merge de los endpoints a develop para proceder con su despliegue en la nube correspondientemente, además de aprobar y mergear los pull requests que se realizaron para estos endpoints, además de que casi todas las páginas de frontend están realizadas solo con falta de pruebas de los nuevos endpoints implementados cuandos se encuentren en producción, los despliegues se tienen planeados para mañana, pero aún queda pendiente por si se realizan el lunes.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/tablero13.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 64% |

## Bitácora 16/10/2022

Todas las vistas de los endpoints creados se ha realizado exitosamente, se proceden a hacer las pruebas necesarias para cada uno de estos endpoints desde frontend para validar que todo se haya hecho como se tenía planificado en la descripción de cada una de estas tareas en el tablero de gestión de SCRUM, también validando que el diseño sea el acordado según los mockups de diseño que se tenían previstos para las vistas correspondientes.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/tablero13.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 90% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 90% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 90% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 78% |

## Bitácora 17/10/2022

Culminación de todas las vistas programadas para esta fase, y adicionalmente, preparación para la presentación de avances para el día de mañana, se tiene previsto un pequeño descanso el día jueves 20 de octubre el cual se utilizará para realizar otras actividades fuera del proyecto para luego continuar con el siguiente sprint la próxima semana según el calendario de actividades.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/tablero13.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 70% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 88% |

## Bitácora 18/10/2022

Presentación de avances al cliente, tanto de vistas como de endpoints realizados, también se tiene el 100 por ciento de todas las tareas realizadas a estas alturas, solo a revisiones finales para formalizar la documentación, por lo que se espera que el día de mañana se cierre el sprint por completo.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/tablero14.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 95% |

## Bitácora 19/10/2022

Culminación del sprint 4 con todas las actividades cerradas, algunas que se pasarán al siguiente (que ya no fueron considerados en la tabla de la bitácora de registros anteriores), y descanso calendarizado para el día de mañana 20 de octubre para no generar estrés en los integrantes del grupo de desarrollo, la planificación del siguiente sprint está prevista para el viernes 21 de octubre.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/tablero15.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-86 | API - Creación de administrador | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-87 | API - Obtener reportes | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-88 | API - Reportar | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-89 | API - Obtener usuarios | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-90 | DB - Creación de la tabla de reportes | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-91 | API - Verificar reporte | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-92 | BD - Creación de la tabla de tarjetas de créditos | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-93 | API - Creación de tarjetas | 2 | Alex Rene López Rosa | 100% |
| SAG6-94 | BD - Agregar el campo fecha suspensión y lógica de login | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-95 | API - Tarjeta favorita | 2 | Alex Rene López Rosa | 100% |
| SAG6-96 | API - Corrección en lógica de ofertas | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-97 | APP - Corrección en lógica de ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-99 | APP - Creación de administrador | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-100 | APP - Administrar reportes | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-101 | APP - Reportar como usuario | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-103 | APP - Contenido bloqueado | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-104 | DOC - Generación de documentación y reportes | 7 | Romeo Ernesto Marroquín Sánchez | 95% |

## Recuento de actividades realizadas

Para realizar el reporte gráfico de actividades realizadas durante todo el sprint basados en los story points asignados a cada una de las actividades programadas se puede observar el siguiente informe extraído a partir de la plataforma de JIRA:

![Informe3](./imgs/board4.jpg)

<br>

<br>

# Sprint 5

## Bitácora 21/10/2022

Luego del descanso, se empezó con la planificación del sprint 5, definiendo las tareas para esta última semana de trabajo, considerando que este es el último trabajo que se realizará antes de la entrega se tomaron en cuenta ya los endpoints relacionados con las compras, el carrito, la interconectividad con los demás grupos y todo este tipo de gestiones, se realizará fuera de bitácora una reunión con todos los integrantes para poder llegar a un acuerdo para los endpoints enfocados a la interconectividad, pero el progreso podrá verse a través de porcentajes de la misma forma.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 4.4](./imgs/board5.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 0% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |

## Bitácora 22/10/2022

Se empezaron los endpoints tanto de carrito (agregar al carrito) como endpoints de interconectividad como el get de todos los juegos hasta tenerlos listos para su posterior revisión, no se trabajó de manera ardua debido a que se realizó la reunión entre los grupos para definir estándares para interconectar los endpoints de todos con las aplicaciones de todos.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 16](./imgs/tablero16.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 0% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 40% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 0% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |

## Bitácora 23/10/2022

Avances considerables en los endpoints de carrito, ahora ya están completamente implementados los endpoints de agragar, eliminar y obtener carrito con toda la lógica interna ya bien implementada, todo a espera de revisión para poder hacer merge a develop, también se empezó con el endpoint de compras, y se finalizó la interconectividad del get de todos los juegos a espera de ser aprobado y mergeado también.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 17](./imgs/tablero17.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 0% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 40% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |

## Bitácora 24/10/2022

Avances considerables en los endpoints de carrito, ahora ya están completamente implementados los endpoints de agragar, eliminar y obtener carrito con toda la lógica interna ya bien implementada, todo a espera de revisión para poder hacer merge a develop, también se empezó con el endpoint de compras, y se finalizó la interconectividad del get de todos los juegos a espera de ser aprobado y mergeado también.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 17](./imgs/tablero17.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 0% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 0% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 40% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |

## Bitácora 25/10/2022

Implementación de la corrección lógica de ofertas en la app además del añadido de las tarjetas por parte del frontend, las páginas ya están realizadas por lo que sólo la lógica del frontend se cambió para hacer match con lo que se tenía de parte del backend, además de parte del backend el endpoint de compra está finalizado a falta de pruebas de parte de frontend y se realizaron algunos convenios internos para el manejo de los endpoints de interconectividad con los otros proveedores.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 18](./imgs/tablero18.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 20% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 0% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 0% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 90% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |

## Bitácora 26/10/2022

Toda la lógica de tarjetas está implementada tanto en backend como en frontend, los get de juegos y desarrolladores se estuvieron trabajando en base a algunos grupos que proporcionaron sus enlaces para la realización de pruebas, pero aún no todos los grupos tienen sus endpoints disponibles, por este motivo no se puede considerar como culminado este endpoint porque puede fallar con alguno de los grupos y puede necesitar adaptaciones, además el endpoint de devolución de forma interna ya está implementado, de la misma forma la devolución de forma externa con otros grupos no se pueden implementar con todos los grupos.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 19](./imgs/tablero19.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 50% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 10% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 50% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 0% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 90% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |

## Bitácora 27/10/2022

La implementación de endpoints de interconectividad está completamente realizada, a falta de pruebas de parte del frontend, se realizaron los endpoints correspondientes para la compra en interconectividad, y además de esto se terminaron los endpoints de obtener datos de otros proveedores, tanto desarrolladores como juegos, se están realizando los endpoints de devolución y compra, el de compra está completamente implementado a falta de pruebas de parte de frontend.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 19](./imgs/tablero19.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 70% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 100% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 70% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 90% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |

## Bitácora 28/10/2022

El endpoint de compras para interconectividad está completamente implementado, también se validó desde el frontend los endpoints que obtienen los datos de desarrolladores y juegos de otros grupos, las compras también se probaron de parte de frontend con grupos que tenían implementados y desplegados sus servicios en la nube,  por lo que ahora se están realizando los reportes correspondientes y revisando toda la aplicación a nivel general, corrigiendo errores que surgen al momento de realizar flujos nuevos o comportamientos inesperados.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 20](./imgs/tablero20.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 100% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |

## Bitácora 29/10/2022

Correcciones generales para el cierre del sprint el día de mañana, surgieron errores inesperados para la devolución y compra de juegos que se tuvieron que validar con otros grupos para confirmar que no fuera error de implementación de Obtic Games, una vez confirmado se procedió a probar toda la aplicación con todos los grupos para verificar que todos los flujos (que se probaran). También se implementó la actualización completa de esta bitácora para las últimas acciones implementadas.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 21](./imgs/tablero21.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 100% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |

## Bitácora 30/10/2022

Implementación de toda la documentación faltante (otros reportes y diagramas que se solicitaban) y despliegue final de la aplicación con todas las funcionalidades que se pudieron realizar, también se hizo el release de la aplicación en gitlab y las pruebas finales de la aplicación ya en producción, también en JIRA se culminó el sprint a tiempo con todas las tareas finalizadas para un cierre exitoso.

Un resumen de las actividades se puede ver en las capturas que se muestran a continuación:

![Tablero 22](./imgs/tablero22.jpg)

Resumen de trabajo realizado:
| # Ticket | Tarea | Story Points | Encargado | % Finalizacion |
| - | - | - | - | - |
| SAG6-102 | APP - Tarjetas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-97 | APP - Corrección lógica en ofertas | 2 | Fernando José Vásquez Castillo | 100% |
| SAG6-107 | API - Interconectividad: Get de juegos por ID | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-110 | API - Interconectividad: Compra | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-114 | API - Interconectividad: Get de desarrolladores | 2 | Alex Rene López Rosa | 100% |
| SAG6-111 | API - Interconectividad: Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-112 | API - Compra | 1 | Welmann Saúl Paniagua Illescas | 100% |
| SAG6-106 | API - Interconectividad: Get de todos los juegos | 2 | Airton Yelstin de León Obando | 100% |
| SAG6-109 | API - Eliminar de carrito | 1 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-108 | API - Agregar a carrito | 2 | Romeo Ernesto Marroquín Sánchez | 100% |
| SAG6-113 | API - Devolución | 2 | Welmann Saúl Paniagua Illescas | 100% |

## Recuento de actividades realizadas

Para realizar el reporte gráfico de actividades realizadas durante todo el sprint basados en los story points asignados a cada una de las actividades programadas se puede observar el siguiente informe extraído a partir de la plataforma de JIRA:

![Informe4](./imgs/informe4.jpg)

<br>

Copyright© todos los derechos reservados por Obtic Games S.A.