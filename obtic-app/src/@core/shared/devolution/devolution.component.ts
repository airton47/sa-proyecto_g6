import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '@services';

@Component({
  selector: 'app-devolution',
  templateUrl: './devolution.component.html',
  styleUrls: ['./devolution.component.scss'],
})
export class DevolutionComponent implements OnInit {
  devolutionForm = this.fb.group({
    message: [null, [Validators.required]],
  });
  submitted = false;
  errorMsg: any = null;

  constructor(
    private dialogRef: MatDialogRef<DevolutionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  get f() {
    return this.devolutionForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close(false);
  }

  async devolution() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.devolutionForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = {
      ...this.devolutionForm.value,
      ...this.data,
      toWallet: true,
    };

    try {
      await this.userService.devolution(req);
      this.dialogRef.close(true);
    } catch (error) {
      this.errorMsg = error?.error?.data || 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
