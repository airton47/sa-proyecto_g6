import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit {
  @Input() content: any = [];
  @Input() title = '';

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.content.forEach((c: any) => {
      if (c.discount != 0) {
        const aux = c.price / c.discount;
        c.percentage_discount = 100 / aux;
      }
    });
  }

  selectItem(e: any) {
    this.router.navigate(['/external-detail'], {
      queryParams: { group: e.group, content: e.gameId },
    });
  }
}
