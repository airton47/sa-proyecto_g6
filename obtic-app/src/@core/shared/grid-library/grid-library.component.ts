import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-grid-library',
  templateUrl: './grid-library.component.html',
  styleUrls: ['./grid-library.component.scss'],
})
export class GridLibraryComponent implements OnInit {
  @Input() content: any = [];
  @Input() title = '';
  internal: any = [];
  external: any = [];

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.internal = this.content?.normalProducts || [];
    this.external = this.content?.externalProducts || [];
  }

  selectItem(e: any) {
    this.router.navigate(['/detail'], {
      queryParams: { type: e.is_game ? 1 : 2, content: e.id_product },
    });
  }

  selectExternalItem(e: any) {
    this.router.navigate(['/external-detail'], {
      queryParams: { group: e.group, content: e.id_product },
    });
  }
}
