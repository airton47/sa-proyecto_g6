import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @ViewChild('container') container: ElementRef;
  @Input() content: any = [];
  @Input() title = '';
  @Input() type = 1;

  position = 0;
  step = 500;
  width = 208;
  constructor(private router: Router) {}

  ngOnInit(): void {
    if (this.type != 1) {
      this.width = 240;
    }
    this.content.forEach((c: any) => {
      if (c.discount != 0) {
        const aux = c.price / c.discount;
        c.percentage_discount = 100 / aux;
      }
    });
  }

  left() {
    if (this.position + this.step >= 0) {
      this.position = 0;
      return;
    }
    this.position += this.step;
  }

  right() {
    const length = this.content.length;
    const total = length * this.width + (length - 1) * 16;
    const width = this.container?.nativeElement.offsetWidth;
    const limit = width - total;

    if (width > total) return;
    if (this.position - this.step <= limit) {
      this.position = limit;
      return;
    }
    this.position -= this.step;
  }

  selectItem(e: any) {
    if (!e.available) return;
    this.router.navigate(['/detail'], {
      queryParams: { type: this.type, content: e.id_product },
    });
  }
}
