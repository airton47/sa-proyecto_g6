import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  AuthenticationService,
  ShoppingCartService,
  UserService,
  WishListService,
} from '@services';
import { DevolutionComponent } from '../devolution/devolution.component';

@Component({
  selector: 'app-external-detail-hero',
  templateUrl: './external-detail-hero.component.html',
  styleUrls: ['./external-detail-hero.component.scss'],
})
export class ExternalDetailHeroComponent implements OnInit {
  @Input() content: any = null;
  genders = '';
  authUser = false;
  authUserValue: any = null;
  showWishList = false;
  showShoppingCart = false;
  own = false;
  loaded = false;
  constructor(
    public dialogRef: MatDialogRef<DevolutionComponent>,
    private wishListService: WishListService,
    private shoppingCartService: ShoppingCartService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  async ngOnInit() {
    try {
      this.authenticationService.user.subscribe((user) => {
        this.authUserValue = user;
        this.authUser = !!user;
      });

      if (this.authUser) {

        const shoppingCart: any = await this.shoppingCartService.getCart();
        const externalProducts = shoppingCart?.externalProducts || [];

        const library: any = await this.userService.getLibrary();
        const externalLibrary = library?.externalProducts || [];
  
        let validation = externalLibrary.find((v: any) => {
          return (
            v?.id_product == this.content.gameId &&
            this.content.group == v?.group
          );
        });
        this.own = validation;


        validation = externalProducts.find((v: any) => {
          return (
            v?.id_product == this.content.gameId &&
            this.content.group == v?.group
          );
        });
        this.showShoppingCart = !validation;
      }

      this.genders = this.content.category.join(', ');
      if (this.content.discount != 0) {
        const aux = this.content.price / this.content.discount;
        this.content.percentage_discount = 100 / aux;
      }

      this.loaded = true;
    } catch (error) {
      this.loaded = true;
    }
  }

  async addToWishList() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    await this.wishListService.addItem(this.content.id_product);
    this.showWishList = false;
  }

  async addToCart() {
    try {
      const req = {
        idProduct: this.content.gameId,
        isGroup: true,
        game: {
          image: this.content.image,
          finalPrice: this.content.price - this.content.discount,
          group: this.content.group,
          name: this.content.name,
        },
      };
      await this.shoppingCartService.addToCart(req);
      this.showShoppingCart = false;
    } catch (error) {}
  }

  async buyNow() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    if (this.showShoppingCart) {
      await this.addToCart();
    }
    this.router.navigateByUrl('/shopping-cart');
  }

  devolution() {
    this.dialogRef = this.dialog.open(DevolutionComponent, {
      disableClose: true,
      panelClass: 'solid-background',
      backdropClass: 'solid-backdrop',
      data: { productId: this.content.gameId, group: this.content.group },
    });

    this.dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        this.ngOnInit();
        const response: any = await this.userService.getWallet();
        if (response) {
          this.authUserValue.wallet = response.wallet;
          this.authenticationService.currentUser = this.authUserValue;
        }
      }
    });
  }
}
