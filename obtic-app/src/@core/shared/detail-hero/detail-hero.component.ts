import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  AuthenticationService,
  ShoppingCartService,
  UserService,
  WishListService,
} from '@services';
import { DevolutionComponent } from '../devolution/devolution.component';

@Component({
  selector: 'app-detail-hero',
  templateUrl: './detail-hero.component.html',
  styleUrls: ['./detail-hero.component.scss'],
})
export class DetailHeroComponent implements OnInit {
  @Input() content: any = null;
  @Input() type = 1;
  images: any = [];
  current: any = null;
  currentID = 0;
  interval: any;
  genders = '';
  authUser = false;
  authUserValue: any = null;
  showWishList = false;
  showShoppingCart = false;
  own = false;
  loaded = false;
  constructor(
    public dialogRef: MatDialogRef<DevolutionComponent>,
    private wishListService: WishListService,
    private shoppingCartService: ShoppingCartService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  async ngOnInit() {
    try {
      this.authenticationService.user.subscribe((user) => {
        this.authUserValue = user;
        this.authUser = !!user;
      });

      if (this.authUser) {
        const wishList: any = await this.wishListService.getList();
        const shoppingCart: any = await this.shoppingCartService.getCart();
        const library: any = await this.userService.getLibrary();

        const internal = shoppingCart?.normalProducts || [];
        const internalLibrary = library?.normalProducts || [];

        let validation = internalLibrary.find((v: any) => {
          return v?.id_product == this.content.id_product;
        });
        this.own = validation;

        validation = wishList.find((v: any) => {
          return v?.id_product == this.content.id_product;
        });
        this.showWishList = !validation;
        validation = internal.find((v: any) => {
          return v?.id_product == this.content.id_product;
        });
        this.showShoppingCart = !validation;
      }

      this.images =
        this.type == 1
          ? this.content.images
          : [{ url: this.content.desktop_image }];

      this.current = this.images[0];
      this.interval = setInterval(this.setCurrent, 5000);
      this.genders = this.content.category.join(', ');
      if (this.content.discount != 0) {
        const aux = this.content.price / this.content.discount;
        this.content.percentage_discount = 100 / aux;
      }

      this.loaded = true;
    } catch (error) {
      this.loaded = true;
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  setCurrent = () => {
    if (!!this.images) {
      const length = this.images.length;
      if (this.currentID + 1 < length) {
        this.currentID++;
        this.current = this.images[this.currentID];
      } else {
        this.currentID = 0;
        this.current = this.images[this.currentID];
      }
    }
  };

  changeCurrent(id: number) {
    this.currentID = id;
    this.current = this.images[this.currentID];
    clearInterval(this.interval);
    this.interval = setInterval(this.setCurrent, 5000);
  }

  async addToWishList() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    await this.wishListService.addItem(this.content.id_product);
    this.showWishList = false;
  }

  async addToCart() {
    try {
      const req = {
        idProduct: this.content.id_product,
        isGroup: false,
      };
      await this.shoppingCartService.addToCart(req);
      this.showShoppingCart = false;
    } catch (error) {}
  }

  async buyNow() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    if (this.showShoppingCart) {
      await this.addToCart();
    }
    this.router.navigateByUrl('/shopping-cart');
  }

  devolution() {
    this.dialogRef = this.dialog.open(DevolutionComponent, {
      disableClose: true,
      panelClass: 'solid-background',
      backdropClass: 'solid-backdrop',
      data: { productId: this.content.id_product, group: null },
    });

    this.dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        this.ngOnInit();
        const response: any = await this.userService.getWallet();
        if (response) {
          this.authUserValue.wallet = response.wallet;
          this.authenticationService.currentUser = this.authUserValue;
        }
      }
    });
  }
}
