import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AuthenticationService,
  ShoppingCartService,
  UserService,
  WishListService,
} from '@services';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
})
export class HeroComponent implements OnInit, OnDestroy {
  @Input() content: any = [];
  @Input() current: any;

  currentID = 0;
  interval: any;
  authUser = false;

  constructor(
    private wishListService: WishListService,
    private shoppingCartService: ShoppingCartService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private userService: UserService
  ) {}

  async ngOnInit() {
    this.interval = setInterval(this.setCurrent, 5000);

    this.authenticationService.user.subscribe((user) => {
      this.authUser = !!user;
    });

    if (this.authUser) {
      const wishList: any = await this.wishListService.getList();
      const shoppingCart: any = await this.shoppingCartService.getCart();
      const internal = shoppingCart?.normalProducts || [];

      const library: any = await this.userService.getLibrary();
      const internalLibrary = library?.normalProducts || [];

      this.content.forEach((c: any) => {
        c.showWishList = true;
        c.showShoppingCart = true;
        let validation = wishList.find((v: any) => {
          return v?.id_product == c.id_product;
        });
        c.showWishList = !validation;

        validation = internal.find((v: any) => {
          return v?.id_product == c.id_product;
        });
        c.showShoppingCart = !validation;

        validation = internalLibrary.find((v: any) => {
          return v?.id_product == c.id_product;
        });
        c.own = validation;
      });
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  setCurrent = () => {
    if (!!this.content) {
      const length = this.content.length;
      if (this.currentID + 1 < length) {
        this.currentID++;
        this.current = this.content[this.currentID];
      } else {
        this.currentID = 0;
        this.current = this.content[this.currentID];
      }
    }
  };

  changeCurrent(id: number) {
    this.currentID = id;
    this.current = this.content[this.currentID];
    clearInterval(this.interval);
    this.interval = setInterval(this.setCurrent, 5000);
  }

  selectItem() {
    this.router.navigate(['/detail'], {
      queryParams: { type: 1, content: this.current.id_product },
    });
  }

  addToWishList() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    this.wishListService.addItem(this.current.id_product);
    this.current.showWishList = false;
  }

  async addToCart() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    const req = {
      idProduct: this.current.id_product,
      isGroup: false,
    };
    await this.shoppingCartService.addToCart(req);
    this.current.showShoppingCart = false;
  }
}
