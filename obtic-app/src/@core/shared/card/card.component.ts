import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() data: any = [];
  @Output() favorite = new EventEmitter<any>();
  constructor() {}

  ngOnInit(): void {
  }

  setFavorite() {
    if (this.data?.is_favorite) return;
    this.favorite.emit(this.data.id_credit_card);
  }
}
