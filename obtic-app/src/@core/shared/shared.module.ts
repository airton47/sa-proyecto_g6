import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { IconsModule } from '@core/icons/icons.module';
import { HeroComponent } from './hero/hero.component';
import { CarouselComponent } from './carousel/carousel.component';
import { DetailHeroComponent } from './detail-hero/detail-hero.component';
import { FilterComponent } from './filter/filter.component';
import { WishListItemComponent } from './wish-list-item/wish-list-item.component';
import { GridComponent } from './grid/grid.component';
import { CardComponent } from './card/card.component';
import { ShoppingCartItemComponent } from './shopping-cart-item/shopping-cart-item.component';
import { ExternalDetailHeroComponent } from './external-detail-hero/external-detail-hero.component';
import { GridLibraryComponent } from './grid-library/grid-library.component';
import { DevolutionComponent } from './devolution/devolution.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HeroComponent,
    CarouselComponent,
    DetailHeroComponent,
    FilterComponent,
    WishListItemComponent,
    GridComponent,
    CardComponent,
    ShoppingCartItemComponent,
    ExternalDetailHeroComponent,
    GridLibraryComponent,
    DevolutionComponent,
  ],
  imports: [
    CommonModule,
    IconsModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {},
    },
  ],
  bootstrap: [],
  exports: [
    HeroComponent,
    CarouselComponent,
    DetailHeroComponent,
    FilterComponent,
    WishListItemComponent,
    GridComponent,
    CardComponent,
    ShoppingCartItemComponent,
    ExternalDetailHeroComponent,
    GridLibraryComponent,
  ],
})
export class SharedModule {}
