import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-shopping-cart-item',
  templateUrl: './shopping-cart-item.component.html',
  styleUrls: ['./shopping-cart-item.component.scss'],
})
export class ShoppingCartItemComponent implements OnInit {
  @Input() content: any = null;
  @Output() remove = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {}

  async removeItem() {
    this.remove.emit(this.content);
  }
}
