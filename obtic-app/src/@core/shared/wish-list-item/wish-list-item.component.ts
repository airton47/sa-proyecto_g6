import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-wish-list-item',
  templateUrl: './wish-list-item.component.html',
  styleUrls: ['./wish-list-item.component.scss'],
})
export class WishListItemComponent implements OnInit {
  @Input() content: any = null;
  @Output() remove = new EventEmitter<any>();
  @Output() shoppingCart = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {
    if (this.content.discount != 0) {
      const aux = this.content.price / this.content.discount;
      this.content.percentage_discount = 100 / aux;
    }
  }

  async removeItem() {
    this.remove.emit(this.content);
  }

  async addToCart() {
    this.shoppingCart.emit(this.content);
  }
}
