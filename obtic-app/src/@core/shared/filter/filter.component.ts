import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  @Input() content: any = [];
  @Output() itemClicked = new EventEmitter<number[]>();

  unselected: any = [];
  constructor() {}

  ngOnInit(): void {}

  unselect(e: any) {
    if (this.unselected.includes(e.id)) {
      const index = this.unselected.indexOf(5);
      this.unselected = this.unselected.filter((v: any) => v != e.id);
      // this.unselected.splice(index, 1);
      e.active = true;
    } else {
      this.unselected.push(e.id);
      e.active = false;
    }
    this.itemClicked.emit(this.unselected);
  }

  toggle() {
    const copy = [...this.unselected];
    this.unselected = [];
    this.content.forEach((e: any) => {
      if (!copy.includes(e.id)) {
        this.unselected.push(e.id);
        e.active = false;
      } else {
        e.active = true;
      }
    });
    this.itemClicked.emit(this.unselected);
  }
}
