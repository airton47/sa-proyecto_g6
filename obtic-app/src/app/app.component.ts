import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'obtic-app';
  user = null;

  constructor(private authenticationService: AuthenticationService) {
    const token = localStorage.getItem('_token');
    const authUser = localStorage.getItem('_auth-user');

    this.authenticationService.user.subscribe((user) => {
      this.user = user;
    });

    if (token && authUser) {
      this.authenticationService.verifyJWT(token).subscribe({
        next: (result) => {
          this.authenticationService.currentUser = JSON.parse(authUser);
        },
        error: (error) => {
          this.authenticationService.clearStorage();
        },
      });
    } else {
      this.user = null;
      this.authenticationService.clearStorage();
    }
  }
  async ngOnInit() {
    const client: any = await this.authenticationService.getIpCliente();
    let region: string = (await this.authenticationService.getRegion(client.ip))
      ?.regionName;
    region = region.replace('Departamento de ', '');
    localStorage.setItem('_region', region);
    localStorage.setItem('_ip', client.ip);
  }
}
