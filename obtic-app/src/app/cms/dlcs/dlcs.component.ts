import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-dlcs',
  templateUrl: './dlcs.component.html',
  styleUrls: ['./dlcs.component.scss'],
})
export class DLCsComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'releaseDate',
    'registrationDate',
    'ageRestriction',
    'developer',
    'active',
    'actions',
  ];
  data: any = [];
  errorMsg: any = null;
  constructor(private adminService: AdminService, private router: Router) {}

  async ngOnInit() {
    this.data = await this.adminService.getDLCS();
  }

  async deleteGame(e: any) {
    try {
      this.errorMsg = null;
      await this.adminService.deleteGame(e.id_product);
      window.location.reload();
    } catch (error) {
      this.errorMsg =
        'No se ha podido ingresar el registro de manera correcta.';
    }
  }

  selectItem(e: any) {
    if (!e.available) return;
    this.router.navigate(['/cms/prices'], {
      queryParams: { type: 2, content: e.id_product },
    });
  }

  listRestrictions(e: any) {
    if (!e.available) return;
    this.router.navigate(['/cms/restrictions'], {
      queryParams: { type: 2, content: e.id_product },
    });
  }

  getStats(e: any) {
    this.router.navigate(['/cms/game-stats'], {
      queryParams: { content: e.id_product },
    });
  }
}
