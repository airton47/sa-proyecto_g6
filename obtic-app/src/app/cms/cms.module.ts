import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@core/shared/shared.module';
import { CanActiveCMSGuard } from './cms.guard';
import { DevelopersComponent } from './developers/developers.component';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { UsersComponent } from './users/users.component';
import { MatIconModule } from '@angular/material/icon';
import { IconsModule } from '@core/icons/icons.module';
import { GamesComponent } from './games/games.component';
import { CreateGameComponent } from './create-game/create-game.component';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { DLCsComponent } from './dlcs/dlcs.component';
import { CreateDLCComponent } from './create-dlc/create-dlc.component';
import { PricesComponent } from './prices/prices.component';
import { RestrictionsComponent } from './restrictions/restrictions.component';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { ReportsComponent } from './reports/reports.component';
import { VerifyReportComponent } from './verify-report/verify-report.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShoppingDashboardComponent } from './shopping-dashboard/shopping-dashboard.component';
import { RefundDashboardComponent } from './refund-dashboard/refund-dashboard.component';
import { GameStatsComponent } from './game-stats/game-stats.component';

const routes: Routes = [
  {
    path: 'developers',
    pathMatch: 'full',
    component: DevelopersComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'admins',
    pathMatch: 'full',
    component: UsersComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'users',
    pathMatch: 'full',
    component: UsersComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'games',
    pathMatch: 'full',
    component: GamesComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'create-game',
    pathMatch: 'full',
    component: CreateGameComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'dlcs',
    pathMatch: 'full',
    component: DLCsComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'create-dlc',
    pathMatch: 'full',
    component: CreateDLCComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'prices',
    pathMatch: 'full',
    component: PricesComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'restrictions',
    pathMatch: 'full',
    component: RestrictionsComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'create-admin',
    pathMatch: 'full',
    component: CreateAdminComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'reports',
    pathMatch: 'full',
    component: ReportsComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'verify-report',
    pathMatch: 'full',
    component: VerifyReportComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'dashboard',
    pathMatch: 'full',
    component: DashboardComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'shopping-dashboard',
    pathMatch: 'full',
    component: ShoppingDashboardComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'refund-dashboard',
    pathMatch: 'full',
    component: RefundDashboardComponent,
    canActivate: [CanActiveCMSGuard],
  },
  {
    path: 'game-stats',
    pathMatch: 'full',
    component: GameStatsComponent,
    canActivate: [CanActiveCMSGuard],
  },
  // {
  //   path: 'categories',
  //   pathMatch: 'full',
  //   component: CategoriesComponent,
  // },
  // {
  //   path: 'developers',
  //   pathMatch: 'full',
  //   component: DevelopersComponent,
  // },
  // {
  //   path: 'dlcs',
  //   pathMatch: 'full',
  //   component: DlcsComponent,
  // },
  // {
  //   path: 'detail',
  //   pathMatch: 'full',
  //   component: DetailComponent,
  // },
  // {
  //   path: 'wish-list',
  //   pathMatch: 'full',
  //   component: WishListComponent,
  //   canActivate: [CanActivePagesGuard],
  // },
];

@NgModule({
  declarations: [
    DevelopersComponent,
    UsersComponent,
    GamesComponent,
    CreateGameComponent,
    DLCsComponent,
    CreateDLCComponent,
    PricesComponent,
    RestrictionsComponent,
    CreateAdminComponent,
    ReportsComponent,
    VerifyReportComponent,
    DashboardComponent,
    ShoppingDashboardComponent,
    RefundDashboardComponent,
    GameStatsComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatSortModule,
    IconsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatFormFieldModule,
    MatSelectModule,
    NgApexchartsModule,
  ],
  exports: [],
  providers: [CanActiveCMSGuard],
})
export class CMSModule {}
