import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.scss'],
})
export class CreateGameComponent implements OnInit {
  gameForm = this.fb.group({
    productName: [null, [Validators.required]],
    description: [null, [Validators.required]],
    shortDescription: [null],
    releaseDate: [null, [Validators.required]],
    desktopImage: [null],
    mobileImage: [null, [Validators.required]],
    logo: [null, [Validators.required]],
    ageRestriction: [null, [Validators.required]],
    isGame: [true],
    developer: [null, [Validators.required]],
    categories: [null, [Validators.required]],
    price: [null, [Validators.required]],
    images: [[]],
    parentGame: [null],
  });

  submitted = false;
  errorMsg: any = null;
  developers: any = [];
  categories: any = [];
  types = ['EC', 'E', 'E10+', 'T', 'M', 'AO'];
  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.developers = await this.adminService.getDevelopers();
    this.categories = await this.adminService.getCategory();
  }

  get f() {
    return this.gameForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  async createGame() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.gameForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }
    let req: any = this.gameForm.value;
    req.categories = [req.categories];

    try {
      await this.adminService.createGame(req);
      this.router.navigateByUrl('/cms/games');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }

  validateNumber(event: any): boolean {
    try {
      if (event.key == '.') return true;

      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }
}
