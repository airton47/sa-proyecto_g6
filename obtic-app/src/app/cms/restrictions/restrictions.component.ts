import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService, DLCsService, GamesService } from '@services';

@Component({
  selector: 'app-restrictions',
  templateUrl: './restrictions.component.html',
  styleUrls: ['./restrictions.component.scss'],
})
export class RestrictionsComponent implements OnInit {
  displayedColumns: string[] = ['id', 'region', 'actions'];
  restrictions: any = [];
  information: any = null;
  regionId = null;
  errorMsg: any = null;
  regions: any = [];
  productId: any;
  name: null;
  type = 1;

  constructor(
    private adminService: AdminService,
    private route: ActivatedRoute,
    private router: Router,
    private gamesService: GamesService,
    private dlcsService: DLCsService
  ) {}

  async ngOnInit() {
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const type = params.get('type');
        const content = params.get('content');
        this.productId = content;

        if (!type || !content) {
          this.router.navigateByUrl('/');
          return;
        }
        this.type = Number(type);
        this.information =
          this.type == 1
            ? await this.gamesService.getInformation(content)
            : await this.dlcsService.getInformation(content);

        this.name = this.information.product_name;

        if (!this.information) {
          this.router.navigateByUrl('/');
          return;
        }

        this.restrictions = await this.adminService.getRestrictions(content);
      } catch (error) {
        this.router.navigateByUrl('/');
      }
    });
    this.regions = await this.adminService.getRegion();
  }

  async removeRestriction(e: any) {
    await this.adminService.removeRestriction(e.id_product, e.id_region);
    window.location.reload();
  }

  validate() {
    this.errorMsg = null;
  }

  async addRestriction() {
    this.errorMsg = null;
    if (this.regionId) {
      try {
        await this.adminService.addRestriction({
          productId: this.productId,
          regionId: this.regionId,
        });
        this.productId = null;
        window.location.reload();
      } catch (error) {
        this.errorMsg =
          'La restricción ya existe, no se puede crear nuevamente.';
      }
    } else {
      this.errorMsg = 'Debes seleccionar una región.';
    }
  }
}
