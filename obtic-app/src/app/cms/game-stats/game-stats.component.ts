import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '@services';
import templates from './game-stats.template';
@Component({
  selector: 'app-game-stats',
  templateUrl: './game-stats.component.html',
  styleUrls: ['./game-stats.component.scss'],
})
export class GameStatsComponent implements OnInit {
  public firstPieChart: Partial<any>;
  public secondPieChart: Partial<any>;
  name = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private adminService: AdminService
  ) {
    this.firstPieChart = { ...templates.pie };
    this.secondPieChart = { ...templates.pie };
  }

  async ngOnInit() {
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const content = params.get('content');
        if (!content) {
          this.router.navigateByUrl('/');
          return;
        }

        const firstPie: any = await this.adminService.getStatisticsByGame(
          16,
          content
        );
        this.name = firstPie.game_name;

        const secondPie: any = await this.adminService.getStatisticsByGame(
          17,
          content
        );

        this.firstPieChart.series = firstPie.series;
        this.firstPieChart.labels = firstPie.labels;

        this.secondPieChart.series = secondPie.series;
        this.secondPieChart.labels = secondPie.labels;
      } catch (error) {
        this.router.navigateByUrl('/');
      }
    });
  }
}
