class Template {
  pie = {
    series: [],
    chart: {
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      sparkline: {
        enabled: false,
      },
      type: 'pie',
      height: 350,
      foreColor: '#fcfcfc',
    },
    plotOptions: {
      bar: {
        borderRadius: 10,
        dataLabels: {
          position: 'top', // top, center, bottom
        },
        horizontal: false,
        columnWidth: '55%',
        endingShape: 'rounded',
      },
    },
    dataLabels: {
      enabled: true,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    labels: [],
    fill: {
      opacity: 1,
    },
    tooltip: {
      enabled: true,
      fillSeriesColor: true,
      theme: true,
    },
  };
}

let templates = new Template();
export default templates;
