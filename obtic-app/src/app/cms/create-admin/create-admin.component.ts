import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.scss'],
})
export class CreateAdminComponent implements OnInit {
  adminForm = this.fb.group({
    username: [null, [Validators.required]],
    email: [null, [Validators.required, Validators.email]],
    name: [null, [Validators.required]],
    lastName: [null, [Validators.required]],
  });

  submitted = false;
  errorMsg: any = null;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router
  ) {}

  async ngOnInit() {}

  get f() {
    return this.adminForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  async createAdmin() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.adminForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = this.adminForm.value;

    try {
      await this.adminService.createAdmin(req);
      this.router.navigateByUrl('/cms/admins');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
