import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'username',
    'email',
    'name',
    'registrationDate',
    'active',
    'confirmed',
    'actions',
  ];
  data: any = [];
  errorMsg: any = null;
  type = 2;
  constructor(private adminService: AdminService, private router: Router) {
    this.type = this.router.url.endsWith('users') ? 2 : 1;
  }

  async ngOnInit() {
    const data: any = await this.adminService.getUsers();
    this.data = data.filter((d: any) => {
      return d.id_user_type == this.type;
    });
  }

  async deleteUser(e: any) {
    try {
      this.errorMsg = null;
      await this.adminService.deleteUser(e.id_user);
      window.location.reload();
    } catch (error) {
      this.errorMsg =
        'No se ha podido ingresar el registro de manera correcta.';
    }
  }
}
