import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-verify-report',
  templateUrl: './verify-report.component.html',
  styleUrls: ['./verify-report.component.scss'],
})
export class VerifyReportComponent implements OnInit {
  resolutions = [
    { id: 1, desc: 'Baja permanente' },
    { id: 2, desc: 'Baja temporal' },
    { id: 3, desc: 'Advertencia' },
    { id: 4, desc: 'Sin efecto' },
  ];
  information: any = null;
  resolutionForm = this.fb.group({
    reportId: [null],
    resolutionId: [null, [Validators.required]],
    resolutionDetail: [null, [Validators.required, Validators.minLength(5)]],
    suspensionDays: [1],
  });
  submitted = false;
  errorMsg: any = null;
  openReports = 0;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const reported = params.get('case');

        if (!reported) {
          this.router.navigateByUrl('/reports');
          return;
        }

        this.information = await this.adminService.getReports(reported);
        this.openReports = this.information.reports.filter(
          (r: any) => !r.attended
        ).length;
        if (!this.information) {
          this.router.navigateByUrl('/reports');
          return;
        }
      } catch (error) {
        this.router.navigateByUrl('/reports');
      }
    });
  }

  get f() {
    return this.resolutionForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  getResolution(value?: any) {
    if (!value) return;
    return this.resolutions.find((r: any) => {
      return r.id == value;
    })?.desc;
  }

  validateNumber(event: any): boolean {
    try {
      if (event.key == '.') return true;
      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }

  async verifyReport() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.resolutionForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }
    let req: any = { ...this.resolutionForm.value };
    req.reportId = this.information.reports[0].id_report;
    if (req.resolutionId != 2) req.suspensionDays = null;
``
    try {
      await this.adminService.verifyReport(req);
      this.router.navigateByUrl('/cms/reports');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
