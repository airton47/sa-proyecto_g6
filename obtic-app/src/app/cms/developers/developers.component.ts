import { Component, OnInit } from '@angular/core';
import { AdminService } from '@services';
@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss'],
})
export class DevelopersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name'];
  data: any = [];
  developerName = '';
  errorMsg: any = null;
  constructor(private adminService: AdminService) {}

  async ngOnInit() {
    this.data = await this.adminService.getDevelopers();
  }

  validate() {
    this.errorMsg = null;
  }

  async createDeveloper() {
    this.errorMsg = null;
    if (this.developerName.length >= 3) {
      try {
        await this.adminService.createDeveloper(this.developerName);
        this.developerName = '';
        window.location.reload();
      } catch (error) {
        this.errorMsg =
          'No se ha podido ingresar el registro de manera correcta.';
      }
    } else {
      this.errorMsg = 'Debe ingresar un nombre válido.';
    }
  }
}
