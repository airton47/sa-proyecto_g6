import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '@services';
import templates from './shopping.template';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  summary: any = null;
  detail: any = null;

  public firstChart: Partial<any>;
  public secondChart: Partial<any>;
  public thirdChart: Partial<any>;

  constructor(private adminService: AdminService, private router: Router) {
    this.firstChart = { ...templates.bar };
    this.secondChart = { ...templates.bar };
    this.thirdChart = { ...templates.bar };
  }

  async ngOnInit() {
    this.summary = await this.adminService.getStatistics(1);
    this.detail = (
      (await this.adminService.getStatistics(2)) as any
    ).movementTypes;

    const first: any = await this.adminService.getStatistics(11);
    const second: any = await this.adminService.getStatistics(10);
    const third: any = await this.adminService.getStatistics(9);

    this.firstChart.series = [
      {
        name: 'Ventas',
        data: first.incomes,
      },
      {
        name: 'Devoluciones',
        data: first.outcomes,
      },
    ];
    this.firstChart.xaxis = {
      categories: first.fields,
      title: {
        text: 'Mes',
      },
      position: 'top',
    };

    this.secondChart.series = [
      {
        name: 'Ventas',
        data: second.incomes,
      },
      {
        name: 'Devoluciones',
        data: second.outcomes,
      },
    ];
    this.secondChart.xaxis = {
      categories: second.fields,
      title: {
        text: 'Origen',
      },
      position: 'top',
    };

    this.thirdChart.series = [
      {
        name: 'Ventas',
        data: third.income,
      },
      {
        name: 'Devoluciones',
        data: third.outcome,
      },
    ];

    this.thirdChart.xaxis = {
      categories: third.fields,
      title: {
        text: 'Clasificación',
      },
      position: 'top',
    };
  }
}
