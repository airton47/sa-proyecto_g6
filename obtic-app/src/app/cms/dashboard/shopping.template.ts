class Template {
  bar = {
    series: [
      {
        name: 'Usage time',
        data: [],
      },
      {
        name: 'Flowtime',
        data: [],
      },
    ],
    chart: {
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      sparkline: {
        enabled: false,
      },
      type: 'bar',
      height: 350,
      foreColor: '#fcfcfc',
    },
    colors: ['#00E396', '#ef4444'],
    plotOptions: {
      bar: {
        borderRadius: 10,
        dataLabels: {
          position: 'top', // top, center, bottom
        },
        horizontal: false,
        columnWidth: '55%',
        endingShape: 'rounded',
      },
    },
    dataLabels: {
      enabled: true,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [],
      position: 'top',
    },
    fill: {
      opacity: 1,
    },
    tooltip: {
      enabled: true,
      fillSeriesColor: true,
      theme: true,
    },
  };
}

let templates = new Template();
export default templates;
