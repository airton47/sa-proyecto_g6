import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '@services';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'username',
    'email',
    'name',
    'openReports',
    'reports',
    'actions',
  ];
  data: any = [];

  constructor(private adminService: AdminService, private router: Router) {}

  async ngOnInit() {
    this.data = await this.adminService.getReports(0);
    this.data.map((d: any) => {
      d.open_reports = d.reports.filter((r: any) => !r.attended).length;
    });
  }

  verifyReport(e: any) {
    this.router.navigate(['/cms/verify-report'], {
      queryParams: { case: e.id_user },
    });
  }
}
