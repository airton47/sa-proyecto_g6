import { Component, OnInit } from '@angular/core';
import { AdminService } from '@services';
import templates from './shopping.template';

@Component({
  selector: 'app-shopping-dashboard',
  templateUrl: './shopping-dashboard.component.html',
  styleUrls: ['./shopping-dashboard.component.scss'],
})
export class ShoppingDashboardComponent implements OnInit {
  public thirdChart: Partial<any>;
  public fourthChart: Partial<any>;
  public fifthChart: Partial<any>;

  public firstPieChart: Partial<any>;
  public secondPieChart: Partial<any>;

  constructor(private adminService: AdminService) {
    this.thirdChart = { ...templates.bar };
    this.fourthChart = { ...templates.bar };
    this.fifthChart = { ...templates.bar };
    this.firstPieChart = { ...templates.pie };
    this.secondPieChart = { ...templates.pie };
  }

  async ngOnInit() {
    const third: any = await this.adminService.getStatistics(3);
    const fourth: any = await this.adminService.getStatistics(4);
    const fifth: any = await this.adminService.getStatistics(5);


    const firstPie: any = await this.adminService.getStatistics(12);
    const secondPie: any = await this.adminService.getStatistics(13);

    this.fifthChart.series = [
      {
        name: 'Total',
        data: fifth.amounts,
      },
    ];
    this.fifthChart.xaxis = {
      categories: fifth.fields,
      title: {
        text: 'Mes',
      },
      position: 'top',
    };

    this.thirdChart.series = [
      {
        name: 'Total',
        data: third.amounts,
      },
    ];
    this.thirdChart.xaxis = {
      categories: third.fields,
      title: {
        text: 'Clasificación',
      },
      position: 'top',
    };

    this.fourthChart.series = [
      {
        name: 'Total',
        data: fourth.amounts,
      },
    ];
    this.fourthChart.xaxis = {
      categories: fourth.fields,
      title: {
        text: 'Origen',
      },
      position: 'top',
    };

    this.firstPieChart.series = firstPie.series;
    this.firstPieChart.labels = firstPie.labels;


    this.secondPieChart.series = secondPie.series;
    this.secondPieChart.labels = secondPie.labels;
  }
}
