import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DLCsService } from 'src/app/shared/services/dlcs.service';
import { GamesService } from 'src/app/shared/services/games.service';
import { FormBuilder, Validators } from '@angular/forms';
import { AdminService } from 'src/app/shared/services/admin.service';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss'],
})
export class PricesComponent implements OnInit {
  priceForm = this.fb.group({
    idProduct: [null],
    price: [null],
    discount: [null, [Validators.required]],
    byRegion: [false],
    regions: [[]],
  });

  information: any = null;
  type = 1;

  submitted = false;
  errorMsg: any = null;
  regions: any = [];
  options = [
    {
      id: true,
      name: 'Sí',
    },
    {
      id: false,
      name: 'No',
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private gamesService: GamesService,
    private dlcsService: DLCsService,
    private fb: FormBuilder,
    private adminService: AdminService
  ) {}

  get f() {
    return this.priceForm.controls;
  }

  async ngOnInit() {
    this.regions = await this.adminService.getRegion();
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const type = params.get('type');
        const content = params.get('content');
        if (!type || !content) {
          this.router.navigateByUrl('/');
          return;
        }
        this.type = Number(type);
        this.information =
          this.type == 1
            ? await this.gamesService.getInformation(content)
            : await this.dlcsService.getInformation(content);
        if (!this.information) {
          this.router.navigateByUrl('/');
          return;
        }
      } catch (error) {
        this.router.navigateByUrl('/');
      }
    });
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  validateNumber(event: any): boolean {
    try {
      if (event.key == '.') return true;
      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }

  async setPrice() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.priceForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = this.priceForm.value;
    if (!req.byRegion) {
      req.regions = [];
    } else {
      req.regions = [req.regions];
    }
    req.idProduct = this.information.id_product;
    req.price = this.information.price;

    try {
      await this.adminService.createOffer(req);
      this.router.navigateByUrl('/cms/games');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
