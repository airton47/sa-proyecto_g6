import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule } from 'src/@core/icons/icons.module';
import { AuthComponent } from './auth.component';
import { CanActiveLoginGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OptComponent } from './opt/opt.component';
import { OtpBoxesComponent } from './opt/otp-boxes/otp-boxes.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'login',
        pathMatch: 'full',
        component: LoginComponent,
        canActivate: [CanActiveLoginGuard],
      },
      {
        path: 'register',
        pathMatch: 'full',
        component: RegisterComponent,
        canActivate: [CanActiveLoginGuard],
      },
      {
        path: 'otp',
        pathMatch: 'full',
        component: OptComponent,
        canActivate: [CanActiveLoginGuard],
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    CommonModule,
    IconsModule,
    MatIconModule,
    HttpClientModule,
  ],
  exports: [],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    OptComponent,
    OtpBoxesComponent,
  ],
  providers: [CanActiveLoginGuard],
})
export class AuthModule {}
