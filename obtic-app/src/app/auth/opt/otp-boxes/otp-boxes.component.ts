import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-otp-boxes',
  templateUrl: './otp-boxes.component.html',
  styleUrls: ['./otp-boxes.component.scss'],
})
export class OtpBoxesComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {
    this.document.getElementById('otp0')?.focus();
  }

  validateNumber(event: any): boolean {
    try {
      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }
}
