import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@services';
import * as _ from '@models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-opt',
  templateUrl: './opt.component.html',
  styleUrls: ['./opt.component.scss'],
})
export class OptComponent implements OnInit {
  otpForm = this.fb.group({
    code: [
      '',
      [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
    ],
  });
  submitted = false;
  errorMsg = '';
  email = '';

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.email = localStorage.getItem('ml') || '';
    if (this.email == '') {
      this.router.navigateByUrl('/auth/login');
      return;
    }
    localStorage.removeItem('ml');
  }

  get f() {
    return this.otpForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  async submit() {
    this.otpForm.disable();
    this.errorMsg = '';
    this.submitted = true;
    const code = this.f.code.value || '';

    if (code.length < 6) {
      this.otpForm.enable();
      this.errorMsg = this.f.code.errors?.required
        ? 'Todos los campos deben ser llenados.'
        : 'El OTP debe ser de 6 dígitos.';
      return;
    }
    const req = { ...(this.otpForm.value as _.ValidateOTP), email: this.email };
    try {
      await this.authService.verifyOTP(req);
      this.router.navigateByUrl('/auth/login');
    } catch (error) {
      this.errorMsg = JSON.stringify(
        error?.error?.data?.msg || 'An unexpected error has occurred'
      );
    }
    this.submitted = false;
    this.otpForm.enable();
  }

  validateNumber(event: any): boolean {
    try {
      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }
}
