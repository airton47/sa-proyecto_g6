import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@services';
import * as _ from '@models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    region: ['', [Validators.required]],
    name: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    username: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  });
  submitted = false;
  errorMsg = '';
  showPassword = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  get f() {
    return this.registerForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  togglePasswordView(): void {
    this.showPassword = !this.showPassword;
  }

  async submit() {
    this.errorMsg = '';
    this.submitted = true;
    if (this.registerForm.invalid) {
      this.errorMsg =
        this.f.email.errors?.required || this.f.password.errors?.required
          ? 'Todos los campos deben ser llenados.'
          : 'Verifique el correo.';
      return;
    }
    const req = {
      ...(this.registerForm.value as _.RegisterRequest),
      profileImage:
        'https://scontent.fgua3-4.fna.fbcdn.net/v/t39.30808-6/259086139_10159476490871023_8896012446533932855_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=174925&_nc_ohc=G3oi4j_5CKoAX_vF0Dh&_nc_ht=scontent.fgua3-4.fna&oh=00_AT-ZLqXTXyGaYBhhrL-ltrgwPVgWFrQ9EGxOjmF6TjA0GA&oe=6339812A',
    };
    try {
      await this.authService.register(req);
      const email = req.email;
      this.authService.generateOTP({ email });
      localStorage.setItem('ml', email);
      this.router.navigateByUrl('/auth/otp');
      return;
    } catch (error) {
      this.errorMsg = JSON.stringify(error);
    }
    this.submitted = false;
  }
}
