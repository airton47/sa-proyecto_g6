import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@services';
import * as _ from '@models';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  });
  submitted = false;
  errorMsg = '';
  showPassword = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  get f() {
    return this.loginForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  togglePasswordView(): void {
    this.showPassword = !this.showPassword;
  }

  async submit() {
    this.errorMsg = '';
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.errorMsg =
        this.f.email.errors?.required || this.f.password.errors?.required
          ? 'Todos los campos deben ser llenados.'
          : 'Verifique el correo.';
      return;
    }
    const req = this.loginForm.value as _.LoginRequest;
    try {
      await this.authService.login(req);
      this.router.navigateByUrl('/');
    } catch (error) {
      if (error == 'EL-004') {
        const email = req.email;
        this.authService.generateOTP({ email });
        localStorage.setItem('ml', email);
        this.router.navigateByUrl('/auth/otp');
        return;
      }
      this.errorMsg = JSON.stringify(error);
    }
    this.submitted = false;
  }
}
