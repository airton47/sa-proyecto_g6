import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DLCsService, GamesService } from '@services';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  information: any = null;
  dlcs: any = null;
  type = 1;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private gamesService: GamesService,
    private dlcsService: DLCsService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const type = params.get('type');
        const content = params.get('content');
        if (!type || !content) {
          this.router.navigateByUrl('/');
          return;
        }
        this.type = Number(type);
        this.information =
          this.type == 1
            ? await this.gamesService.getInformation(content)
            : await this.dlcsService.getInformation(content);

        if (!this.information) {
          this.router.navigateByUrl('/');
          return;
        }

        if (this.type == 1) {
          this.dlcs = await this.dlcsService.getGame(content);
        }
      } catch (error) {
        this.router.navigateByUrl('/');
      }
    });
  }
}
