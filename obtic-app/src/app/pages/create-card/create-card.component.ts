import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService, UserService } from '@services';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.scss'],
})
export class CreateCardComponent implements OnInit {
  cardForm = this.fb.group({
    cardNumber: [null, [Validators.required]],
    expirationDate: [null, [Validators.required]],
    cvv: [null, [Validators.required]],
    alias: [null, [Validators.required]],
  });

  submitted = false;
  errorMsg: any = null;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  async ngOnInit() {}

  get f() {
    return this.cardForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  async createAdmin() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.cardForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = { ...this.cardForm.value };

    const month = req.expirationDate.substring(0, 2);
    const year = req.expirationDate.substring(2, req.expirationDate.length);
    if (month * 1 < 1 || month * 1 > 12) {
      this.errorMsg = 'La fecha de expiración no es válida.';
      return;
    }
    req.expirationDate = `${month}/${year}`;

    try {
      await this.userService.createCard(req);
      this.router.navigateByUrl('/cards');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
