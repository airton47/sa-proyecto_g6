import { Component, OnInit } from '@angular/core';
import { GamesService } from '@services';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss'],
})
export class PartnersComponent implements OnInit {
  external: any = null;
  data: any = null;
  filter: any = null;
  message: any = 'cargando...';

  constructor(private gamesService: GamesService) {}

  async ngOnInit() {
    this.data = await this.gamesService.getExternal();
    this.external = this.data;

    const filter: any = [];
    this.external.forEach((e: any) => {
      filter.push({
        id: e.id,
        name: `Grupo ${e.id}`,
        active: true,
      });
    });

    if (filter.length != 0) {
      this.filter = filter;
    }
    this.message = null;
  }

  apply(e: any) {
    this.external = this.data.filter((item: any) => {
      return !e.includes(item.id);
    });
  }
}
