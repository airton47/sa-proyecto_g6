import { Component, OnInit } from '@angular/core';
import { DLCsService } from '@services';

@Component({
  selector: 'app-dlcs',
  templateUrl: './dlcs.component.html',
  styleUrls: ['./dlcs.component.scss'],
})
export class DlcsComponent implements OnInit {
  dlcsContent: any = null;
  data: any = null;
  filter: any = null;

  constructor(private dlcsService: DLCsService) {}

  async ngOnInit() {
    this.data = await this.dlcsService.getGames();
    this.dlcsContent = this.data;
    const filter: any = [];

    this.dlcsContent.forEach((e: any) => {
      filter.push({
        id: e.id,
        name: e.name,
        active: true,
      });
    });

    if (filter.length != 0) {
      this.filter = filter;
    }
  }

  apply(e: any) {
    this.dlcsContent = this.data.filter((item: any) => {
      return !e.includes(item.id);
    });
  }
}
