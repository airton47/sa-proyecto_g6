import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, UserService } from '@services';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss'],
})
export class LibraryComponent implements OnInit {
  data: any = null;
  empty = true;
  authUser = null;

  constructor(
    private userService: UserService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.user.subscribe((user) => {
      this.authUser = user;
    });
  }

  async ngOnInit() {
    if (!this.authUser) {
      this.router.navigateByUrl('/auth/login');
    }
    this.data = await this.userService.getLibrary();
    const internal = this.data?.normalProducts || [];
    const external = this.data?.externalProducts || [];
    this.empty = internal.length == 0 && external.length == 0;
  }
}
