import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '@services';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  selected = 1;
  cardForm = this.fb.group({
    cardId: [null, [Validators.required]],
    cvv: [null, [Validators.required]],
  });

  favoriteCardForm = this.fb.group({
    cvv: [null, [Validators.required]],
  });

  submitted = false;
  errorMsg: any = null;
  favorite: any = null;

  constructor(
    private dialogRef: MatDialogRef<PaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  get f() {
    return this.cardForm.controls;
  }

  get fc() {
    return this.favoriteCardForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  reset() {
    this.validate();
    this.f['cvv'].setValue(null);
  }

  ngOnInit(): void {
    this.favorite = this.data?.cards.find((c: any) => {
      return c.is_favorite;
    });
  }

  changeOption(id: number) {
    if (id == this.selected) return;
    this.selected = id;
    this.cardForm = this.fb.group({
      cardId: [null, [Validators.required]],
      cvv: [null, [Validators.required]],
    });
  }

  close() {
    this.dialogRef.close(1);
  }

  async pay() {
    this.errorMsg = '';
    this.submitted = true;

    let req: any = { ...this.cardForm.value };
    if (this.selected == 2) {
      if (this.cardForm.invalid) {
        this.errorMsg = 'Todos los campos deben ser llenados.';
        return;
      }
    } else if (this.selected == 3) {
      if (this.favoriteCardForm.invalid) {
        this.errorMsg = 'Todos los campos deben ser llenados.';
        return;
      }
      req = {
        ...this.favoriteCardForm.value,
        cardId: this.favorite.id_credit_card,
      };
    }

    try {
      await this.userService.purchase(req);
      this.dialogRef.close(2);
    } catch (error) {
      this.errorMsg = error?.error?.data || 'Ha ocurrido un problema';
      if (this.errorMsg.startsWith('P-EGR:')) {
        setTimeout(() => {
          this.dialogRef.close(3);
        }, 2000);
      }
    }
    this.submitted = false;
  }
}
