import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@core/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';

import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './categories/categories.component';
import { DevelopersComponent } from './developers/developers.component';
import { DlcsComponent } from './dlcs/dlcs.component';
import { DetailComponent } from './detail/detail.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { CanActivePagesGuard } from './pages.guard';
import { CanActiveNotCMSGuard } from './not-cms.guard';
import { ReportComponent } from './report/report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PartnersComponent } from './partners/partners.component';
import { CardsComponent } from './cards/cards.component';
import { CreateCardComponent } from './create-card/create-card.component';
import { NgxMaskModule } from 'ngx-mask';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ExternalDetailComponent } from './external-detail/external-detail.component';
import { PaymentComponent } from './payment/payment.component';
import { IconsModule } from '@core/icons/icons.module';
import { MatIconModule } from '@angular/material/icon';
import { LibraryComponent } from './library/library.component';
import { TransactionComponent } from './transaction/transaction.component';

const routes: Routes = [
  {
    path: 'home',
    pathMatch: 'full',
    component: HomeComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'categories',
    pathMatch: 'full',
    component: CategoriesComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'developers',
    pathMatch: 'full',
    component: DevelopersComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'dlcs',
    pathMatch: 'full',
    component: DlcsComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'detail',
    pathMatch: 'full',
    component: DetailComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'wish-list',
    pathMatch: 'full',
    component: WishListComponent,
    canActivate: [CanActivePagesGuard, CanActiveNotCMSGuard],
  },
  {
    path: 'report',
    pathMatch: 'full',
    component: ReportComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'partners',
    pathMatch: 'full',
    component: PartnersComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'cards',
    pathMatch: 'full',
    component: CardsComponent,
    canActivate: [CanActivePagesGuard, CanActiveNotCMSGuard],
  },
  {
    path: 'create-card',
    pathMatch: 'full',
    component: CreateCardComponent,
    canActivate: [CanActivePagesGuard, CanActiveNotCMSGuard],
  },
  {
    path: 'shopping-cart',
    pathMatch: 'full',
    component: ShoppingCartComponent,
    canActivate: [CanActivePagesGuard, CanActiveNotCMSGuard],
  },
  {
    path: 'external-detail',
    pathMatch: 'full',
    component: ExternalDetailComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
  {
    path: 'library',
    pathMatch: 'full',
    component: LibraryComponent,
    canActivate: [CanActiveNotCMSGuard],
  },
];

@NgModule({
  declarations: [
    HomeComponent,
    CategoriesComponent,
    DevelopersComponent,
    DlcsComponent,
    DetailComponent,
    WishListComponent,
    ReportComponent,
    PartnersComponent,
    CardsComponent,
    CreateCardComponent,
    ShoppingCartComponent,
    ExternalDetailComponent,
    PaymentComponent,
    LibraryComponent,
    TransactionComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(),
    CommonModule,
    SharedModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    IconsModule,
    MatIconModule,
  ],
  exports: [TransactionComponent],
  providers: [
    CanActivePagesGuard,
    CanActiveNotCMSGuard,
    {
      provide: MatDialogRef,
      useValue: {},
    },
  ],
})
export class PagesModule {}
