import { Component, OnInit } from '@angular/core';
import { ShoppingCartService, UserService, WishListService } from '@services';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss'],
})
export class WishListComponent implements OnInit {
  wishList: any = [];
  empty = false;
  constructor(
    private wishListService: WishListService,
    private shoppingCartService: ShoppingCartService,
    private userService: UserService
  ) {}

  async ngOnInit() {
    const wishList: any = await this.wishListService.getList();

    const shoppingCart: any = await this.shoppingCartService.getCart();
    const internal = shoppingCart?.normalProducts || [];

    const library: any = await this.userService.getLibrary();
    const internalLibrary = library?.normalProducts || [];

    wishList.forEach((c: any) => {
      c.showShoppingCart = true;
      let validation = internal.find((v: any) => {
        return v?.id_product == c.id_product;
      });
      c.showShoppingCart = !validation;

      validation = internalLibrary.find((v: any) => {
        return v?.id_product == c.id_product;
      });
      c.own = validation;
    });
    this.wishList = wishList;
    this.empty = this.wishList.length == 0;
  }

  async removeItem(e: any) {
    try {
      await this.wishListService.removeItem(e.id_product);
      this.ngOnInit();
    } catch (error) {}
  }

  async addToCart(e: any) {
    try {
      const req = {
        idProduct: e.id_product,
        isGroup: false,
      };
      await this.shoppingCartService.addToCart(req);
      e.showShoppingCart = false;
    } catch (error) {}
  }
}
