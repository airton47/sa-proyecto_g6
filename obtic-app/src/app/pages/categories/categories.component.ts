import { Component, OnInit } from '@angular/core';
import { GamesService } from '@services';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  categoriesContent: any = null;
  data: any = null;
  filter: any = null;
  constructor(private gamesService: GamesService) {}

  async ngOnInit() {
    this.data = await this.gamesService.getCategoriesContent();
    this.categoriesContent = this.data;
    const filter: any = [];

    this.categoriesContent.forEach((e: any) => {
      filter.push({
        id: e.id,
        name: e.name,
        active: true,
      });
    });

    if (filter.length != 0) {
      this.filter = filter;
    }
  }

  apply(e: any) {
    this.categoriesContent = this.data.filter((item: any) => {
      return !e.includes(item.id);
    });
  }
}
