import { Component, OnInit } from '@angular/core';
import { GamesService } from '@services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  heroContent: any = null;
  currentHeroContent = null;
  offered: any = null;
  bestSellers: any = null;
  bestRated: any = null;
  mostRecommended: any = null;

  constructor(private gamesService: GamesService) {}

  async ngOnInit() {
    this.heroContent = await this.gamesService.getHeroContent();
    this.currentHeroContent = this.heroContent[0];

    this.offered = await this.gamesService.getOffered();
    this.bestSellers = await this.gamesService.getBestSellers();
    this.bestRated = await this.gamesService.getBestRated();
    this.mostRecommended = await this.gamesService.getMostRecommended();
  }
}
