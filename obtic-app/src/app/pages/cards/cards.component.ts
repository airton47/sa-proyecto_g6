import { Component, OnInit } from '@angular/core';
import { UserService } from '@services';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {
  cards: any = null;
  constructor(private userService: UserService) {}

  async ngOnInit() {
    this.cards = await this.userService.getCards();
  }

  async setFavorite(e: any) {
    try {
      await this.userService.favoriteCard(e);
      this.cards = await this.userService.getCards();
    } catch (error) {
      // this.errorMsg = 'Ha ocurrido un problema';
    }
  }
}
