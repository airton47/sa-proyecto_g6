import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@services';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {
  reportForm = this.fb.group({
    profile: [null, [Validators.required]],
    reason: [null, [Validators.required, Validators.minLength(10)]],
  });

  data: any = null;
  selectedUser: any;
  submitted = false;
  errorMsg: any = null;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  async ngOnInit() {
    this.data = await this.userService.getUsers();
  }

  get f() {
    return this.reportForm.controls;
  }

  reset() {
    this.validate();
    this.f['reason'].setValue(null);
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  async report() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.reportForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = { ...this.reportForm.value };
    req.reportedUser = req.profile.id_user;
    delete req.profile;

    try {
      await this.userService.reportUser(req);
      this.router.navigateByUrl('/home');
    } catch (error) {
      this.errorMsg = 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }
}
