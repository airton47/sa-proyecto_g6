import { Component, OnInit } from '@angular/core';
import { GamesService } from '@services';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss'],
})
export class DevelopersComponent implements OnInit {
  developersContent: any = null;
  data: any = null;
  filter: any = null;

  constructor(private gamesService: GamesService) {}

  async ngOnInit() {
    this.data = await this.gamesService.getDevelopersContent();

    this.developersContent = this.data;
    const filter: any = [];

    this.developersContent.forEach((e: any) => {
      filter.push({
        id: e.id,
        name: e.name,
        active: true,
      });
    });

    if (filter.length != 0) {
      this.filter = filter;
    }
  }

  apply(e: any) {
    this.developersContent = this.data.filter((item: any) => {
      return !e.includes(item.id);
    });
  }
}
