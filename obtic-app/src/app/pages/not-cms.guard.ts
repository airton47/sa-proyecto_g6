import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class CanActiveNotCMSGuard implements CanActivate {
  constructor(
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: unknown
  ) {}

  canActivate(): boolean {
    const token = localStorage.getItem('_token');
    const authUser = localStorage.getItem('_auth-user');
    const isAdmin = localStorage.getItem('_nimda') == 'true';

    if (token && authUser) {
      if (!isAdmin) return true;
      this.router.navigateByUrl('cms/developers');
      return false;
    }
    return true;
  }
}
