import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '@services';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  cardForm = this.fb.group({
    idCard: [null, [Validators.required]],
    CVV: [null, [Validators.required]],
    credit: [null, [Validators.required]],
  });
  cards: any = [];
  submitted = false;
  errorMsg: any = null;

  constructor(
    private dialogRef: MatDialogRef<TransactionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  async ngOnInit() {
    this.cards = await this.userService.getCards();
    this.cards.map((c: any) => {
      c.info = `XXXX XXXX XXXX ${c.last_digits} | ${c.alias}`;
    });
  }

  get f() {
    return this.cardForm.controls;
  }

  validate() {
    this.errorMsg = '';
    this.submitted = false;
  }

  reset() {
    this.validate();
    this.f['CVV'].setValue(null);
  }

  close() {
    this.dialogRef.close(false);
  }

  async pay() {
    this.errorMsg = '';
    this.submitted = true;

    if (this.cardForm.invalid) {
      this.errorMsg = 'Todos los campos deben ser llenados.';
      return;
    }

    let req: any = { ...this.cardForm.value };

    try {
      await this.userService.addCredit(req);
      this.dialogRef.close(true);
    } catch (error) {
      this.errorMsg = error?.error?.data?.error || 'Ha ocurrido un problema';
    }
    this.submitted = false;
  }

  validateNumber(event: any): boolean {
    try {
      if (event.key == '.') return true;

      return !isNaN(parseInt(event.key, 10));
    } catch (e) {
      return false;
    }
  }
}
