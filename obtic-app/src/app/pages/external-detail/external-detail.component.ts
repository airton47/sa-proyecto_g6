import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GamesService } from '@services';

@Component({
  selector: 'app-external-detail',
  templateUrl: './external-detail.component.html',
  styleUrls: ['./external-detail.component.scss'],
})
export class ExternalDetailComponent implements OnInit {
  information: any = null;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private gamesService: GamesService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(async (params) => {
      try {
        const group = params.get('group');
        const content = params.get('content');
        if (!group || !content) {
          this.router.navigateByUrl('/partners');
          return;
        }

        this.information = await this.gamesService.getExternalById(
          group,
          content
        );
        if (!this.information) {
          this.router.navigateByUrl('/partners');
          return;
        }
      } catch (error) {
        this.router.navigateByUrl('/partners');
      }
    });
  }
}
