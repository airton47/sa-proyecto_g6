import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  AuthenticationService,
  ShoppingCartService,
  UserService,
  WishListService,
} from '@services';
import { PaymentComponent } from '../payment/payment.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {
  shoppingCart: any = [];
  cart: any = null;
  internal: any = [];
  external: any = [];
  cards: any = null;
  empty = true;
  authUser: any = null;
  constructor(
    public dialogRef: MatDialogRef<PaymentComponent>,
    private shoppingCartService: ShoppingCartService,
    private userService: UserService,
    private dialog: MatDialog,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  async ngOnInit() {
    this.shoppingCart = await this.shoppingCartService.getCart();
    this.cart = this.shoppingCart?.cart;
    this.internal = this.shoppingCart?.normalProducts || [];
    this.external = this.shoppingCart?.externalProducts || [];
    this.empty = this.internal.length == 0 && this.external.length == 0;
    this.cards = await this.userService.getCards();

    this.authenticationService.user.subscribe((user) => {
      this.authUser = user;
    });

    this.cards.map((c: any) => {
      c.info = `XXXX XXXX XXXX ${c.last_digits} | ${c.alias}`;
    });
  }

  async removeItem(e: any) {
    try {
      if (!e.group) {
        await this.shoppingCartService.removeItem(e.id_product);
      } else {
        await this.shoppingCartService.removeExternalItem(
          e.id_product,
          e.group
        );
      }
      this.ngOnInit();
    } catch (error) {}
  }

  shop() {
    this.dialogRef = this.dialog.open(PaymentComponent, {
      disableClose: true,
      panelClass: 'solid-background',
      backdropClass: 'solid-backdrop',
      data: { cards: this.cards, user: this.authUser },
    });

    this.dialogRef.afterClosed().subscribe(async (result) => {
      if (result == 2 || result == 3) {
        const response: any = await this.userService.getWallet();
        if (response) {
          this.authUser.wallet = response.wallet;
          this.authenticationService.currentUser = this.authUser;
        }
        this.router.navigateByUrl('/');
      }
    });
  }
}
