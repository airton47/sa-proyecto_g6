import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class CanActivePagesGuard implements CanActivate {
  constructor(
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: unknown
  ) {}

  canActivate(): boolean {
    const token = localStorage.getItem('_token');
    const authUser = localStorage.getItem('_auth-user');

    if (token && authUser) return true;
    this.router.navigateByUrl('/auth/login');
    return false;
  }
}
