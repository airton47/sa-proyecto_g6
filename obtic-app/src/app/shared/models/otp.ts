export interface GenerateOTP {
  email: string;
}

export interface ValidateOTP {
  email: string;
  code: string;
}
