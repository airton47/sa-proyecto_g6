export interface ProcessResponse<T> {
  success: boolean;
  data: T;
}
