export interface Authenticated {
  email: string;
  email_verified: boolean;
  names?: string;
  surnames?: string;
  studentId?: number;
  family_name: string;
  name: string;
  sub: string;
  username: number;
}
