export interface RegisterRequest {
  region: string;
  name: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  profileImage: string;
}
