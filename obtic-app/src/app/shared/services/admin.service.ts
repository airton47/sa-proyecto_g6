import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class AdminService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getDevelopers() {
    try {
      const result = await this.get(Routes.Admin.GetDevelopers);
      return (this.validateData(result) as any).developers;
    } catch (error) {
      return [];
    }
  }

  async createDeveloper(developerName: string) {
    try {
      await this.post(Routes.Admin.CreateDeveloper, {
        developerName,
      });
    } catch (error) {
      throw '';
    }
  }

  async getUsers() {
    try {
      const result = await this.get(Routes.Admin.GetUsers);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async deleteUser(id: number | string) {
    try {
      const result = await this.delete(`${Routes.Admin.DeleteUser}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async getGames() {
    try {
      const result = await this.get(Routes.Admin.GetGames);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async createGame(req: any) {
    try {
      await this.post(Routes.Admin.CreateGame, req);
    } catch (error) {
      throw '';
    }
  }

  async getCategory() {
    try {
      const result = await this.get(Routes.Catalog.GetCategory);
      return (this.validateData(result) as any).Category;
    } catch (error) {
      return [];
    }
  }

  async getRegion() {
    try {
      const result = await this.get(Routes.Catalog.GetRegion);
      return (this.validateData(result) as any).Region;
    } catch (error) {
      return [];
    }
  }

  async deleteGame(id: number | string) {
    try {
      const result = await this.delete(`${Routes.Admin.DeleteGame}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async getDLCS() {
    try {
      const result = await this.get(Routes.Admin.GetDLCs);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async createOffer(req: any) {
    try {
      await this.post(Routes.Admin.CreateOffer, req);
    } catch (error) {
      throw '';
    }
  }

  async getRestrictions(id: string) {
    try {
      const result = await this.get(`${Routes.Admin.GetRestrictions}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async removeRestriction(productId: string, regionId: string) {
    try {
      const result = await this.delete(
        `${Routes.Admin.RemoveRestriction}?productId=${productId}&regionId=${regionId}`
      );
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async addRestriction(req: any) {
    try {
      await this.post(Routes.Admin.AddRestriction, req);
    } catch (error) {
      throw '';
    }
  }

  async createAdmin(req: any) {
    try {
      await this.post(Routes.Admin.CreateAdmin, req);
    } catch (error) {
      throw '';
    }
  }

  async getReports(id: any) {
    try {
      const result = await this.get(`${Routes.Admin.GetReports}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async verifyReport(req: any) {
    try {
      await this.post(Routes.Admin.VerifyReport, req);
    } catch (error) {
      throw '';
    }
  }

  async getStatistics(id: any) {
    try {
      const result = await this.get(`${Routes.Admin.GetStatistics}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async getStatisticsByGame(id: any, product: any) {
    try {
      const result = await this.get(
        `${Routes.Admin.GetStatistics}${id}&id_product=${product}`
      );
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }
}
