import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import * as _ from '../models';
import { Routes } from './routes';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private baseUri = '';
  private currentUserSubject = new BehaviorSubject<any>(null);
  public user = this.currentUserSubject.asObservable();

  constructor(private _http: HttpClient) {
    this.baseUri = (window as any).__env.apiUrl;
  }

  public get currentUser() {
    return this.currentUserSubject.value;
  }

  public set currentUser(user: _.Authenticated) {
    localStorage.setItem('_auth-user', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  async login(req: _.LoginRequest) {
    const ip = localStorage.getItem('_ip') || '181.209.195.245';
    return new Promise((resolve, reject) => {
      this._http
        .post<any>(`${this.baseUri}${Routes.Auth.Login}/`, { ...req, ip })
        .subscribe({
          next: (user) => {
            if (user.success) {
              localStorage.setItem(
                '_auth-user',
                JSON.stringify(user.data.user)
              );
              localStorage.setItem('_token', user.data.token);
              localStorage.setItem(
                '_nimda',
                JSON.stringify(user.data.is_admin)
              );
              this.currentUserSubject.next(user.data.user);
            }
            resolve(user);
          },
          error: (error) => {
            reject(
              error?.error?.data?.error || 'An unexpected error has occurred'
            );
          },
        });
    });
  }

  async register(req: _.RegisterRequest) {
    return new Promise((resolve, reject) => {
      this._http
        .post<any>(`${this.baseUri}${Routes.Auth.Register}/`, req)
        .subscribe({
          next: (result) => {
            resolve(result);
          },
          error: (error) => {
            reject(
              error?.error?.data?.error || 'An unexpected error has occurred'
            );
          },
        });
    });
  }

  verifyJWT(req: string) {
    return this._http.post<any>(`${this.baseUri}${Routes.Auth.Verify}/`, {
      token: req,
    });
  }

  generateOTP(req: _.GenerateOTP) {
    return this._http
      .post<any>(`${this.baseUri}${Routes.Auth.GenerateOTP}/`, req)
      .toPromise();
  }

  verifyOTP(req: _.ValidateOTP) {
    return this._http
      .post<any>(`${this.baseUri}${Routes.Auth.VerifyOTP}/`, req)
      .toPromise();
  }

  getIpCliente() {
    return this._http.get('http://api.ipify.org/?format=json').toPromise();
  }

  getRegion(ip: string) {
    return this._http.get<any>(`http://ip-api.com/json/${ip}`).toPromise();
  }

  logout() {
    this.clearStorage();
    this.currentUserSubject.next(null);
    window.location.reload();
  }

  clearStorage() {
    ['_token', '_auth-user', '_nimda'].forEach((n) => {
      localStorage.removeItem(n);
    });
  }
}
