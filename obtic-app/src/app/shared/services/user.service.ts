import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class UserService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getUsers() {
    try {
      const result = await this.get(Routes.User.GetUsers);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async reportUser(req: any) {
    try {
      await this.post(Routes.User.ReportUser, req);
    } catch (error) {
      throw '';
    }
  }

  async getCards() {
    try {
      const result = await this.get(Routes.User.GetCards);
      return (this.validateData(result) as any)?.cards;
    } catch (error) {
      return [];
    }
  }

  async createCard(req: any) {
    try {
      await this.post(Routes.User.CreateCard, req);
    } catch (error) {
      throw '';
    }
  }

  async favoriteCard(id: number) {
    try {
      await this.post(Routes.User.FavoriteCard, { creditCardId: id });
    } catch (error) {
      throw '';
    }
  }

  async purchase(req: any) {
    try {
      await this.post(Routes.User.Purchase, req);
    } catch (error) {
      throw error;
    }
  }

  async devolution(req: any) {
    try {
      await this.post(Routes.User.Devolution, req);
    } catch (error) {
      throw error;
    }
  }

  async getLibrary() {
    try {
      const result = await this.get(Routes.User.GetLibrary);
      return this.validateData(result);
    } catch (error) {
      return {};
    }
  }

  async getWallet() {
    try {
      const result = await this.get(Routes.User.GetWallet);
      return this.validateData(result);
    } catch (error) {
      return null;
    }
  }

  async addCredit(req: any) {
    try {
      await this.post(Routes.User.AddCredit, req);
    } catch (error) {
      throw error;
    }
  }
}
