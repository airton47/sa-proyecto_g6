import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class WishListService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getList() {
    try {
      const result = await this.get(Routes.WishList.GetList);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async addItem(id: number | string) {
    try {
      const result = await this.post(Routes.WishList.AddItem, {
        productId: id,
      });
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async removeItem(id: number | string) {
    try {
      const result = await this.delete(`${Routes.WishList.RemoveItem}${id}`);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }
}
