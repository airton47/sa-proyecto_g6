import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class StorageService {
    public saveItem(key: string, value: any): void {
        localStorage.setItem(key, JSON.stringify(value));
    }

    public deleteItem(key: string): void {
        localStorage.removeItem(key);
    }

    public getItem(key: string, mustParseToJSON?: boolean): any {
        let storedValue = localStorage.getItem(key);
        if (storedValue && mustParseToJSON) {
            return JSON.parse(storedValue);
        }

        return storedValue;
    }
}
