import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, first } from 'rxjs/operators';

import { AuthenticationService } from '../../authentication.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  baseUrl: string;
  /**
   * @param {Router} _router
   * @param {AuthenticationService} _authenticationService
   */
  constructor(
    private _router: Router,
    private _authenticationService: AuthenticationService
  ) {
    this.baseUrl = (window as any).__env.apiUrl;
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (
          event instanceof HttpResponse &&
          ~~(event.status / 200) < 3 &&
          !request.url.endsWith('/refresh/') &&
          !request.url.includes('/permission_by_user/') &&
          this._authenticationService.currentUser &&
          request.url.startsWith(this.baseUrl)
        ) {
          // this._authenticationService
          //   .refresh()
          //   .pipe(first())
          //   .subscribe((data) => {});
        }
        return event;
      }),
      catchError((err) => {
        if ([401, 403].indexOf(err.status) !== -1) {
          // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
          if (!request.url.endsWith('/auth/')) {
            this._router.navigate(['/pages/miscellaneous/not-authorized']);
            this._authenticationService.logout();
            location.reload();
          }

          // ? Can also logout and reload if needed
          // this._authenticationService.logout();
          // location.reload(true);
        }
        console.error(
          `Backend returned code ${err.status}, body was: ${
            err.error.message || err.statusText
          }`
        );
        // throwError
        const error = err.error.message || err.statusText;
        console.log(error);
        return throwError(error);
      })
    );
  }
}
