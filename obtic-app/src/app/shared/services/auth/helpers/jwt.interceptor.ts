import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as _ from '@models';
import { AuthenticationService } from '../../authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private baseUrl;

  /**
   *
   * @param {AuthenticationService} _authenticationService
   */
  constructor(private _authenticationService: AuthenticationService) {
    this.baseUrl = (window as any).__env.apiUrl;
  }

  /**
   * Add auth header with jwt if user is logged in and request is to api url
   * @param request
   * @param next
   */
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const currentUser: _.Authenticated =
      this._authenticationService.currentUser;
    // const isLoggedIn = currentUser && currentUser.access;
    // const isApiUrl = request.url.startsWith(this.baseUrl);
    // if (isLoggedIn && isApiUrl) {
    //   request = request.clone({
    //     setHeaders: {
    //       Authorization: `Bearer ${currentUser.access}`,
    //     },
    //   });
    // }

    return next.handle(request);
  }
}
