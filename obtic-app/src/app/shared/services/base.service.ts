import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProcessResponse } from '../models';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  private baseUrl = '';

  constructor(protected _http: HttpClient) {
    this.baseUrl = (window as any).__env.apiUrl;
  }

  protected get(path: string) {
    const headers = this.getHeaders();
    return this._http
      .get<any>(`${this.baseUrl}${path}`, { headers })
      .toPromise();
  }

  protected getById(path: string, id: string | number) {
    return this._http.get<any>(`${this.baseUrl}/${path}/${id}`).toPromise();
  }

  protected post(path: string, body: any) {
    const headers = this.getHeaders();
    return this._http
      .post<any>(`${this.baseUrl}${path}`, body, { headers })
      .toPromise();
  }

  protected delete(path: string) {
    const headers = this.getHeaders();
    return this._http
      .delete<any>(`${this.baseUrl}${path}`, { headers })
      .toPromise();
  }

  protected put(path: string, id: any, body: any) {
    return this._http
      .put<any>(`${this.baseUrl}${path}${id}/`, body)
      .toPromise();
  }

  protected validateData<T>(serviceResponse: ProcessResponse<T>) {
    if (serviceResponse.success && ~~serviceResponse.success) {
      return serviceResponse.data;
    } else {
      throw new Error('');
    }
  }

  private getHeaders() {
    const KEY_REGION = 'Region';
    const KEY_IP = 'IP';
    const KEY_AUTHORIZATION = 'Authorization';
    return new HttpHeaders({
      [KEY_REGION]: localStorage.getItem('_region') || 'guatemala',
      [KEY_IP]: localStorage.getItem('_ip') || '181.209.195.245',
      [KEY_AUTHORIZATION]: `Bearer ${localStorage.getItem('_token') || ''}`,
    });
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention

  //   public loadData<T>(
  //     source: T,
  //     target: FormGroup,
  //     toString?: boolean
  //   ): Promise<void> {
  //     if (!source) {
  //       throw new Error(
  //         'Debe proveer el objeto que servirá como fuente de datos.'
  //       );
  //     }

  //     if (!target) {
  //       throw new Error('Debe proveer el objeto que almacenará los datos.');
  //     }

  //     let targetProperties = Object.getOwnPropertyNames(source);

  //     targetProperties.forEach((property) => {
  //       let propertyFounded = target.get(property);
  //       if (propertyFounded) {
  //         if (toString && this.isNumeric(source[property])) {
  //           propertyFounded.setValue((source[property] * 1).toString());
  //         } else {
  //           propertyFounded.setValue(source[property]);
  //         }
  //       }
  //     });

  //     return Promise.resolve();
  //   }

  //   public getData(source: FormGroup) {
  //     var s = {};
  //     Object.keys(source.controls).forEach((key) => {
  //       let propertyFounded = source.get(key);
  //       if (propertyFounded.value || typeof propertyFounded.value == 'boolean') {
  //         s[key] = propertyFounded.value;
  //       }
  //     });
  //     return s;
  //   }

  //   isNumeric(val: any): boolean {
  //     return !(val instanceof Array) && val - parseFloat(val) + 1 >= 0;
  //   }
}
