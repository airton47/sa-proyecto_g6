import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class ShoppingCartService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getCart() {
    try {
      const result = await this.get(Routes.ShoppingCart.GetCart);
      return this.validateData(result);
    } catch (error) {
      return {};
    }
  }

  async addToCart(req: any) {
    try {
      const result = await this.post(Routes.ShoppingCart.AddToCart, req);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async removeItem(id: number | string) {
    try {
      const result = await this.delete(
        `${Routes.ShoppingCart.RemoveItem}${id}`
      );
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async removeExternalItem(id: number | string, group: number | string) {
    try {
      const result = await this.delete(
        `${Routes.ShoppingCart.RemoveItem}${id}&group=${group}`
      );
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }
}
