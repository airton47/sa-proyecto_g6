import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class DLCsService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getGames() {
    try {
      const result = await this.get(Routes.DLC.Games);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return [];
    }
  }

  async getGame(id: string) {
    try {
      const result = await this.get(`${Routes.DLC.Game}${id}`);
      const info: any = this.validateData(result);
      return info;
    } catch (error) {
      return [];
    }
  }

  async getInformation(id: string) {
    try {
      const result = await this.get(`${Routes.DLC.Information}${id}`);
      const info: any = this.validateData(result);
      return info[0];
    } catch (error) {
      return null;
    }
  }
}
