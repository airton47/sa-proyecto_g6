import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from '../models';
import { Routes } from './routes';
import { BaseService } from './base.service';

@Injectable({ providedIn: 'root' })
export class GamesService extends BaseService {
  constructor(protected override _http: HttpClient) {
    super(_http);
  }

  async getHeroContent() {
    try {
      const result = await this.get(Routes.Games.Hero);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return [];
    }
  }

  async getCategoriesContent() {
    try {
      const result = await this.get(Routes.Games.Categories);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return [];
    }
  }

  async getDevelopersContent() {
    try {
      const result = await this.get(Routes.Games.Developers);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return [];
    }
  }

  async getInformation(id: string) {
    try {
      const result = await this.get(`${Routes.Games.Information}${id}`);
      const info: any = this.validateData(result);
      return info[0];
    } catch (error) {
      return null;
    }
  }

  async getOffered() {
    try {
      const result = await this.get(Routes.Games.Offered);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return null;
    }
  }

  async getBestSellers() {
    try {
      const result = await this.get(Routes.Games.BestSellers);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return null;
    }
  }

  async getBestRated() {
    try {
      const result = await this.get(Routes.Games.BestRated);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return null;
    }
  }

  async getMostRecommended() {
    try {
      const result = await this.get(Routes.Games.MostRecommended);
      const games = this.validateData(result);
      return games;
    } catch (error) {
      return null;
    }
  }

  async getExternal() {
    try {
      const result = await this.get(Routes.Games.External);
      return this.validateData(result);
    } catch (error) {
      return [];
    }
  }

  async getExternalById(group: any, gameId: any) {
    try {
      const result = await this.get(
        `${Routes.Games.ExternalById}${group}&idGame=${gameId}`
      );
      return this.validateData(result);
    } catch (error) {
      return null;
    }
  }
}
