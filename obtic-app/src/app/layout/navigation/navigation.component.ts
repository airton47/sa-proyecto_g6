import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '@services';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  @Input() authUser: any;
  @Input() navigation: any[] = [];
  logged = false;

  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit(): void {}
}
