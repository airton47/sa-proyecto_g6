import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { IconsModule } from 'src/@core/icons/icons.module';

import { LayoutComponent } from './layout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NavigationItemComponent } from './navigation-item/navigation-item.component';
import { UserSectionComponent } from './navigation/user-section/user-section.component';
import { AnonymousSectionComponent } from './navigation/anonymous-section/anonymous-section.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    LayoutComponent,
    NavbarComponent,
    NavigationComponent,
    NavigationItemComponent,
    UserSectionComponent,
    AnonymousSectionComponent,
  ],
  imports: [
    NgxMaskModule.forRoot(),
    RouterModule,
    BrowserModule,
    IconsModule,
    MatIconModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
  ],
  exports: [LayoutComponent],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {},
    },
  ],
})
export class LayoutModule {}
