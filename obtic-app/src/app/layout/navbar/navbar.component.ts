import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthenticationService, UserService } from '@services';
import { TransactionComponent } from 'src/app/pages/transaction/transaction.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input() authUser: any;
  @Input() isMobile: boolean = false;
  @Input() navigation: any[] = [];

  cardsUrl = '/cards';
  wishListUrl = '/wish-list';
  shoppingCartUrl = '/shopping-cart';
  message: string = '';

  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<TransactionComponent>,
    private dialog: MatDialog,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    const hr = new Date().getHours();
    this.message = 'Buenas noches,';
    if (hr < 12) {
      this.message = 'Buenos días,';
    } else if (hr < 18) {
      this.message = 'Buenas tardes,';
    }
  }

  ngOnInit(): void {}

  transaction() {
    this.dialogRef = this.dialog.open(TransactionComponent, {
      disableClose: true,
      panelClass: 'solid-background',
      backdropClass: 'solid-backdrop',
      data: { user: this.authUser },
    });

    this.dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        const response: any = await this.userService.getWallet();
        if (response) {
          this.authUser.wallet = response.wallet;
          this.authenticationService.currentUser = this.authUser;
        }
      }
    });
  }
}
