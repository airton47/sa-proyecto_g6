import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';
import { defaultNavigation } from './navigation';
import { NavigationEnd, Router } from '@angular/router';
import { AuthenticationService } from '@services';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  authUser = null;
  navigation: any = [];
  isMobile = false;
  layout = false;
  private screenBreakpoints = {
    phone: '(max-width: 480px)',
    tablet: '(min-width: 481px) and (max-width: 899px)',
    desktop: '(min-width: 900px)',
  };
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.breakpointObserver
      .observe([
        this.screenBreakpoints.phone,
        this.screenBreakpoints.tablet,
        this.screenBreakpoints.desktop,
      ])
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (result.matches) {
          Object.entries(result.breakpoints).forEach(([key, value]) => {
            if (value) {
              if (
                [
                  this.screenBreakpoints.phone,
                  this.screenBreakpoints.tablet,
                ].includes(key)
              ) {
                this.isMobile = true;
              } else {
                this.isMobile = false;
              }
            }
          });
        }
      });

    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.layout = !val.url.startsWith('/auth');
      }
    });
  }

  ngOnInit(): void {
    this.authenticationService.user.subscribe((user) => {
      this.authUser = user;

      const isAdmin = localStorage.getItem('_nimda') == 'true';

      this.navigation = defaultNavigation.filter((df: any) => {
        return df.admin == isAdmin;
      });
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
