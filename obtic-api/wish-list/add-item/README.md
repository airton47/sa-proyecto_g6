# DOCKER

```
docker build . -t fernandvx/obtic-wish-list-add-item-microservice
```

```
docker push fernandvx/obtic-wish-list-add-item-microservice
```

```
docker run --name wish-list-add-item-microservice -p 3000:80 fernandvx/obtic-wish-list-add-item-microservice
```
