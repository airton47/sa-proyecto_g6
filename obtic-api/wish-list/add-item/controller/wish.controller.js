const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const AddItem = async (req, res) => {
  const { productId } = req.body;
  const {success, payload} = await verify(req.headers.authorization, false);
  if (!success) return  res.status(401).json(response(false, {error: 'EWC-005'}));
  const userId = payload.id_user;
  if (!productId) return res.status(400).json(response(false, {error: 'EWC-003'})); //faltan parametros
  try {
    const user = await db.query(`SELECT * FROM "user" WHERE id_user = ${userId}`);
    if (user.rowCount < 1) return res.status(400).json(response(false, {error: 'EWC-001'})); // Usuario no existe
    const product = await db.query(`SELECT * FROM product WHERE id_product = ${productId}`);
    if (product.rowCount < 1) return res.status(400).json(response(false, {error: 'EWC-002'})); // Producto no existe
    const repeated = await db.query(`SELECT * FROM wish_list WHERE id_product = ${productId} AND id_user = ${userId}`);
    if (repeated.rowCount > 0) return res.status(400).json(response(false, {error: 'EWC-006'})); // Favorito ya existente
    await db.query(`INSERT INTO wish_list (id_user, id_product) VALUES (${userId}, ${productId})`);
    return res.status(200).json(response(true, {userId, productId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'EWC-004'}));//Error desconocido (bae de datos no conectada)
  }
};

router.post("/addItem", AddItem);

module.exports = router;
