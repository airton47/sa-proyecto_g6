# DOCKER

```
docker build . -t fernandvx/obtic-wish-list-get-list-microservice
```

```
docker push fernandvx/obtic-wish-list-get-list-microservice
```

```
docker run --name wish-list-get-list-microservice -p 3000:80 fernandvx/obtic-wish-list-get-list-microservice
```
