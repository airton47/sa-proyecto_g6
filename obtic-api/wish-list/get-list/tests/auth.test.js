require("dotenv").config();
const supertest = require("supertest");
const app = require("..");

//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("get-list controller", () => {
  it("Should return 401 status code, authorization", async () => {
    const response = await supertest(app).get("/wishList/getList").query({});
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status code, authorization", async () => {
    const response = await supertest(app)
      .get("/wishList/getList")
      .query({})
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
