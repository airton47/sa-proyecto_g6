const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');
const gameFunction = require("../utils/game.utils");

const GetList = async (req, res) => {
  const region = req.headers.region;
  const {success, payload} = await verify(req.headers.authorization, false);
  if (!success) return  res.status(401).json(response(false, {error: 'EWD-004'}));
  const userId = payload.id_user;
  const exists = await db.query(`SELECT * FROM "user" WHERE id_user=${userId}`);
  if (exists.rowCount < 1) return res.status(404).json(response(false, {error: 'EWR-001'}));// Usuario no existente
  try {
    const query = `SELECT product.* FROM product INNER JOIN wish_list ON wish_list.id_product=product.id_product WHERE wish_list.id_user = ${userId}`;
    const data = await db.query(query);
    let games = data.rows
    if (data.rowCount == 0) return res.status(200).json(response(true, []));
    const priceByRegion = await gameFunction.getPriceByProductAndRegion(
      query,
      region
    );
    const priceGeneral = await gameFunction.getPriceByProduct(query);
    const availableRegions = await gameFunction.getAvailabilityByRegion(
      query,
      region
    );
    for (let game of games) {
      game.developer = (await db.query(`SELECT d.id_developer, d.developer_name FROM developer d INNER JOIN developer_product dp ON d.id_developer = dp.id_developer WHERE dp.id_product = ${game.id_product}`)).rows;
      game.category = (await db.query(`SELECT c.id_category, c.category_name FROM category c INNER JOIN category_game cg ON c.id_category = cg.id_category WHERE cg.id_product = ${game.id_product}`)).rows;
      
      let temporalPrice = gameFunction.getPrice(game.id_product, priceByRegion);
      if (temporalPrice.price === -1) {
        temporalPrice = gameFunction.getPrice(game.id_product, priceGeneral);
      }
      game.price = temporalPrice.price;
      game.discount = temporalPrice.discount;
      game.end_date = temporalPrice.end_date;
      game.available = gameFunction.getAvailable(
        game.id_product,
        availableRegions
      );
    }
    return res.status(200).json(response(true, games));
  } catch (error) {
    console.log(error)
    return res.status(500).json(response(false, {error: 'EWD-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.get("/getList", GetList);

module.exports = router;
