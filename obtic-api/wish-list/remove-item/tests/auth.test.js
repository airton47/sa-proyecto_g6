require("dotenv").config();
const supertest = require("supertest");
const app = require("..");

//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("get-list controller", () => {
  it("Should return 401 status code, authorization", async () => {
    const response = await supertest(app)
      .delete("/wishList/removeItem")
      .query({});
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status, faltan parametros", async () => {
    const response = await supertest(app)
      .delete("/wishList/removeItem")
      .send({})
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
  it("Should return 200 status", async () => {
    const response = await supertest(app)
      .delete("/wishList/removeItem")
      .query({ gameId: 31 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
  it("Should return 400 status. Favorito no existe", async () => {
    const response = await supertest(app)
      .delete("/wishList/removeItem")
      .query({ gameId: 31 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
});
