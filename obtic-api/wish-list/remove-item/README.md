# DOCKER

```
docker build . -t fernandvx/obtic-wish-list-remove-item-microservice
```

```
docker push fernandvx/obtic-wish-list-remove-item-microservice
```

```
docker run --name wish-list-remove-item-microservice -p 3000:80 fernandvx/obtic-wish-list-remove-item-microservice
```
