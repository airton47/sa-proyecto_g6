const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const DeleteItem = async (req, res) => {
  const { gameId } = req.query;
  const {success, payload} = await verify(req.headers.authorization, false);
  if (!success) return  res.status(401).json(response(false, {error: 'EWD-004'}));
  const id = Number(gameId);
  if (isNaN(id)) return res.status(400).json(response(false, {error: 'EWD-002'})); //faltan parametros
  try {
    const list = await db.query(`SELECT * FROM wish_list WHERE id_product = ${id} AND id_user = ${payload.id_user}`);
    if (list.rowCount < 1) return res.status(400).json(response(false, {error: 'EWD-001'})); // Favorito no existe
    await db.query(`DELETE FROM wish_list WHERE id_product = ${id} AND id_user = ${payload.id_user}`);
    return res.status(200).json(response(true, {wishListId: id}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'EWD-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.delete("/removeItem", DeleteItem);

module.exports = router;
