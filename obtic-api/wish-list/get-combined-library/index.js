require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const GameController = require("./controller/game.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/wishList", GameController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3020;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
