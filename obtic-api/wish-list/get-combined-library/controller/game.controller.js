const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const gameFunction = require("../utils/game.utils");
const axios = require("axios");
const verify = require("../utils/verify");

/**
 * Funcion para formatear una cadena con una fecha y obtener una con el formato: "DD/MM/YYYY"
 * @param {Date} inputDate : Cadena de texto con fecha,
 * debe tener el formato adecuado para convertirse en un objeto
 * de tipo Date.
 * @returns string
 */
function formatDate(inputDate) {
  inputDate = new Date(inputDate);
  let date, month, year;

  date = inputDate.getDate();
  month = inputDate.getMonth() + 1;
  year = inputDate.getFullYear();

  date = date.toString().padStart(2, "0");

  month = month.toString().padStart(2, "0");

  return `${date}/${month}/${year}`;
}

/**
 * Realiza la peticion POST http a traves de axios
 * @param {*} link : string, cadena con link de grupo para peticion
 * @param {*} gamesIds : Array de integers can ids de juegos
 * @returns
 */
function doRequest(link, gamesIds) {
  let body = { games: gamesIds.constructor === Array ? gamesIds : [] };
  return axios.post(link, body);
}

const getGames = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) {
    return res.status(401).json(response(false, { error: "Error de autenticacion" }));
  }

  const userId = payload.id_user;

  try {
    /**
     * ========= CONSULTA DE JUEGOS LOCALES PARA EL USUARIO =========
     */

    // Haciendo consulta de ids de los juegos locales la en tabla <library>
    let query = `select distinct id_product  from "library" where id_user = ${userId}`;

    let data_local = await db.query(query);
    let ids_local = data_local.rows;
    console.log(ids_local);

    // Armando condicion en query para la consulta
    let ids_query = "id_product in (";
    for (let i = 0; i < ids_local.length; i++) {
      if (i !== ids_local.length - 1) {
        ids_query += `${Number(ids_local[i].id_product)} , `;
      } else {
        ids_query += `${Number(ids_local[i].id_product)} )`;
      }
    }

    query = `select * from product where active = true and is_game = true and ${ids_query}`;
    let data = await db.query(query);

    let responseGame = [];
    let region = "";
    if (data.rows.length !== 0) {
      let games = data.rows;
      const developers = await gameFunction.getDevelopers(query, db);
      const categories = await gameFunction.getCategories(query, db);
      const priceByRegion = await gameFunction.getPriceByProductAndRegion(
        query,
        region
      );
      const priceGeneral = await gameFunction.getPriceByProduct(query);
      const availableRegions = await gameFunction.getAvailabilityByRegion(
        query,
        region
      );

      // Seteando los atributos del los elementos del array: developer, category, price, discount
      for (let game of games) {
        game.developer = gameFunction.getDevelopersByGame(
          game.id_product,
          developers
        );
        game.category = gameFunction.getCategoriesByGame(
          game.id_product,
          categories
        );
        let temporalPrice = gameFunction.getPrice(
          game.id_product,
          priceByRegion
        );
        if (temporalPrice.price === -1) {
          temporalPrice = gameFunction.getPrice(game.id_product, priceGeneral);
        }
        game.price = temporalPrice.price;
        game.discount = temporalPrice.discount;
        game.end_date = temporalPrice.end_date;
        game.available = gameFunction.getAvailable(
          game.id_product,
          availableRegions
        );
      }

      // Filtrar arreglo, obtener solo aquellos que tienen el atributo 'available' igual a TRUE
      games = games.filter((game) => game.available === true);

      // Formateando el array para la interconectividad segun especificaciones
      responseGame = games.map(
        ({
          id_product: gameId,
          product_name: name,
          mobile_image: image,
          description: description,
          release_date: releaseDate,
          age_restriction: restrictionAge,
          price: price,
          discount: discount,
          developer: developer,
          category: category,
        }) => ({
          gameId,
          name,
          image,
          description,
          releaseDate: formatDate(releaseDate),
          restrictionAge: restrictionAge.replace(/ /g, ""),
          price: Number(price),
          discount: Number(discount),
          group: 6,
          developer,
          category,
          isExternal: false,
        })
      );

      // console.log("No games were found");
      // return res.status(400).json(response(false, [])); //No existen juegos
    }

    /**
     * ========= CONSULTA DE JUEGOS EXTERNOS PARA EL USUARIO =========
     */

    // Links, numero de identificacion y lista de juegos de los grupos externos
    const groups = [
      {
        id: process.env.ID_G1,
        link: process.env.URL_G1,
        gameIds: [],
      },
      {
        id: process.env.ID_G2,
        link: process.env.URL_G2,
        gameIds: [],
      },
      {
        id: process.env.ID_G3,
        link: process.env.URL_G3,
        gameIds: [],
      },
    ];

    // Consulta para obtener todos los juegos que pertenecen a otros grupo y que estan
    // asociados al usuario (del payload) en la tabla <external_library>.
    // query = `select distinct "group", id_product  from external_library`;
    query = `select distinct "group", id_product  from external_library where id_user = ${userId}`;
    let idsExternal = await db.query(query);
    console.log(idsExternal.rows);

    let list_g1,
      list_g2,
      list_g3 = [];

    // Ahora se hacen un filtro para obtener todos los objetos/registros que
    // pertenecen a cada grupo, segun las variables de entorno.
    // Luego se reasigna el arreglo a uno nuevo arreglo que contiene solo los numeros (ids)
    // de los juegos, ya no sera un arreglo de objetos
    list_g1 = idsExternal.rows.filter((row) => row.group == groups[0].id); //id: process.env.ID_G1
    list_g1 = list_g1.map((item) => item.id_product);

    list_g2 = idsExternal.rows.filter((row) => row.group == groups[1].id); //id: process.env.ID_G2
    list_g2 = list_g2.map((item) => item.id_product);

    list_g3 = idsExternal.rows.filter((row) => row.group == groups[2].id); //id: process.env.ID_G3
    list_g3 = list_g3.map((item) => item.id_product);

    groups[0].gameIds = list_g1; //id: process.env.ID_G1
    groups[1].gameIds = list_g2; //id: process.env.ID_G2
    groups[2].gameIds = list_g3; //id: process.env.ID_G3

    console.log(groups);

    let errors = []; // Para almacenar errores, si los hay, y devolverlos en respuesta del Request.
    let externalGames = []; // Almacena los juegos de los grupos externos.

    // Ahora se hace la consulta a los juegos externos para obtener su respectiva informacion.
    for (let i = 0; i < groups.length; i++) {
      if (groups[i].gameIds.length === 0) continue; // Si el arreglo de ids para el grupo esta vacio, omitido
      await doRequest(groups[i].link, groups[i].gameIds)
        .then((result) => {
          let res_data = result?.data?.data;
          if (res_data && res_data?.length !== 0) {
            res_data.forEach((element) => {
              element.isExternal = true;
            });
            externalGames = [...externalGames, ...res_data];
          }
        })
        .catch((err) => {
          errors = [
            ...errors,
            {
              group: groups[i].id,
              message: `[Error in request] | ${err.toString()}`,
              link: groups[i].link,
            },
          ];
        });
    }

    console.log(externalGames);

    return res.status(200).json(
      response(true, {
        games: [...responseGame, ...externalGames],
        errors: errors,
      })
    );
  } catch (error) {
    console.log(error);
    return res.status(500).json(response(false, {})); //ERROR DEL SERVIDOR
  }
};

router.get("/getCombinedList", getGames);

module.exports = router;
