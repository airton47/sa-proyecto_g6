# DOCKER

```
docker build . -t fernandvx/obtic-wish-list-get-combined-library-microservice
```

```
docker push fernandvx/obtic-wish-list-get-combined-library-microservice
```

```
docker run --name wish-list-get-combined-library-microservice -p 3000:80 fernandvx/obtic-wish-list-get-combined-library-microservice
```

## Documentacion de Servicio

`Descripcion`: Este servicio obtiene la lista conbinada de los juegos y su respectviva informacion para aquellos que se encuentran en la tabla `library` y `external_labrary`

- Tipo de Request: GET
- Requisitos
  - Estar logeado
    - Header de autenticacion con token
  - Agregar los links y los id de los grupos en las variables de entorno (ejemplo):
    - ID_G1=1
    - ID_G2=7
    - ID_G3=8
    - URL_G1=http://35.223.53.97:3001/games
    - URL_G2=http://api.killjoygt.tk/shared/games
    - URL_G3=http://34.135.218.4/games

- Ejemplo de peticion: http://localhost:3002/wishList/getCombinedList

### Ejemplos de respuesta

- Exitoso ✅ Obtiene toda la informacion de
- Codigo 200 OK

- `IMPORTATE:` se usa el formato de interconexion para devolver los resultados, pero ahora se `AGREGA UN NUEVO TRIBUTO EN LOS JUEGOS:` `[isExternal: boolean] => true/false`, que solamente sirve para identificar cuales juegos son locales y cuales son externos pues pertenecen a los otros grupos.

- Ademas tambien se cuenta con un atributo llamado `[errors: array]`, en donde se colocan los errores, si es que ocurren, la momento de hacer request a servicio de un grupo, tambien se agrega el numero de grupo, el detalle del error y el link de dicho grupo.

```json
{
  "success": true,
  "data": {
    "name": "Grand Theft Auto IV",
    "image": "https://storage.googleapis.com/sa-g1/covers-juegos/grand-theft-auto-IV.jpg",
    "releaseDate": "1995-06-12T00:00:00.000Z",
    "restrictionAge": "E",
    "price": 9.99,
    "discount": 5,
    "description": "The legendary Grand Theft Auto games utilize a design theme that is recognizable from across the room. Covers feature split section artwork capturing the characters and scenes you'll find inside the game.",
    "category": ["Acción", "Aventura"],
    "developer": [],
    "gameId": 19,
    "group": 1,
    "isExternal": false
  },
  "errors": [
    {
      "group": "5",
      "message": "[Error in request] | AxiosError: Request failed with status code 400",
      "link": "http://34.71.181.115:3000/games"
    }
  ]
}
```

- Fallido ❌ | Caso 4, Error interno del servidor, puede ocurris por cualquier causa, por ejemplo la base de datos no esta funcionando o algo por el estilo.
- Codigo 500 Error

```json
{
  "success": false,
  "data": {
    "message": "Missing data: <groupNumber>, <idGame> must be included."
  }
}
```

- Fallido ❌ | Caso 5, ocurreo cuando el usuario no esta autenticado.
- Codigo 401 Error

```json
{
  "success": false,
  "data": {
    "error": "Error de credenciales"
  }
}
```
