const db = require("../db/database");

const verifyPrice = async (idProduct, byRegion, regions, idPrice) => {
  if (byRegion) {
    return await regionVerify(idProduct, regions, idPrice);
  } else {
    return await simpleVerify(idProduct, idPrice);
  }
};

const simpleVerify = async (idProduct, idPrice) => {
  const response = await db.query(
    `SELECT * FROM price WHERE id_product=${idProduct} and active=true and id_price != ${idPrice}
      and by_region = false`
  );
  if (response.rows.length > 0) {
    return false;
  } else {
    return true;
  }
};

const regionVerify = async (idProduct, regions, idPrice) => {
  let expressions = "";
  for (let region of regions) {
    if (expressions === "") {
      expressions = region;
    } else {
      expressions = `${expressions}, ${region}`;
    }
  }
  const query = `
    SELECT 
      p.*
    FROM
      price p
    INNER JOIN
      price_region pr
    ON  
      p.id_price = pr.id_price
    WHERE
      p.id_product = ${idProduct} AND pr.id_region IN (${expressions}) AND p.active = true 
      AND pr.id_price != ${idPrice}
  `;
  const response = await db.query(query);
  if (response.rows.length > 0) {
    return false;
  } else {
    return true;
  }
};

module.exports = {
  verifyPrice,
};
