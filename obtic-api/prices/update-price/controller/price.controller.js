const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const priceFunctions = require("../utils/price.utils");
const verify = require("../utils/verify");

const updatePrice = async (req, res) => {
  const { idPrice, price, discount, active, regions, byRegion, idProduct } =
    req.body;
  if (!idPrice || !price || active == null || !discount || byRegion == null) {
    return res.status(400).json(
      response(false, {
        error:
          "No se enviaron todos los datos necesarios para la actualización",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    if (active) {
      if (
        !(await priceFunctions.verifyPrice(
          idProduct,
          byRegion,
          regions,
          idPrice
        ))
      ) {
        return res.status(400).json(
          response(false, {
            error:
              "No pueden existir 2 precios activos para el mismo producto, a menos que sea para una región especifica, la cual tampoco debe repetirse; desactive el otro precio activo o ingrese este precio como desactivo",
          })
        );
      }
    }
    const updatePrice = `
      UPDATE
        price
      SET
        active = ${active}, 
        price = ${price},
        discount = ${discount}
      WHERE 
        id_price = ${idPrice}
    `;
    await db.query(updatePrice);
    if (byRegion) {
      await db.query(`DELETE FROM price_region WHERE id_price = ${idPrice}`);
      for (let region of regions) {
        await db.query(
          `INSERT INTO price_region(id_region, id_price) VALUES (${region}, ${idPrice})`
        );
      }
    }
    return res
      .status(200)
      .json(response(true, "Se actualizo el precio de manera correcta"));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.post("/updatePrice", updatePrice);

module.exports = router;
