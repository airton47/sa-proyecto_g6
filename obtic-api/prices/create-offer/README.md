# DOCKER

```
docker build . -t fernandvx/obtic-prices-create-offer-microservice
```

```
docker push fernandvx/obtic-prices-create-offer-microservice
```

```
docker run --name prices-create-offer-microservice -p 3000:80 fernandvx/obtic-prices-create-offer-microservice
```
