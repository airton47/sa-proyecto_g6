const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const createPrice = async (req, res) => {
  let { idProduct, price, discount, byRegion, regions } = req.body;
  if (price == null || discount == null || byRegion == null || !idProduct) {
    return res.status(400).json(
      response(false, {
        error: "No se enviaron todos los datos necesarios para la creación",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    if (byRegion && !regions) {
      return res.status(400).json(
        response(false, {
          error:
            "Para un precio por región debe enviar las regiones en las que estara disponible",
        })
      );
    }
    const priceRegister = (
      await db.query(
        `SELECT * FROM price WHERE id_product = ${idProduct} AND by_region = ${byRegion}`
      )
    ).rows[0];
    const updateQuery = ` UPDATE 
                            price
                          SET
                            price = ${price}, 
                            discount = ${discount} 
                          WHERE id_price = ${priceRegister.id_price}`;
    await db.query(updateQuery);
    if (byRegion) {
      await db.query(
        `DELETE FROM price_region WHERE id_price = ${priceRegister.id_price}`
      );
      if (discount === 0) {
        regions = [];
      }
      for (let region of regions) {
        await db.query(
          `INSERT INTO price_region(id_region, id_price) VALUES (${region}, ${priceRegister.id_price})`
        );
      }
    }
    return res
      .status(200)
      .json(response(true, "Se actualizo la oferta de manera correcta"));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.post("/createOffer", createPrice);

module.exports = router;
