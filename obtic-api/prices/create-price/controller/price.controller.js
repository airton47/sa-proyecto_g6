const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const priceFunctions = require("../utils/price.utils");
const verify = require("../utils/verify");

const createPrice = async (req, res) => {
  const {
    startDate,
    endDate,
    active,
    price,
    discount,
    idProduct,
    byRegion,
    regions,
  } = req.body;
  if (
    !startDate ||
    !endDate ||
    active == null ||
    !price ||
    !idProduct ||
    byRegion == null
  ) {
    return res.status(400).json(
      response(false, {
        error: "No se enviaron todos los datos necesarios para la creación",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, false);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    if (byRegion && !regions) {
      return res.status(400).json(
        response(false, {
          error:
            "Para un precio por región debe enviar las regiones en las que estara disponible",
        })
      );
    }
    if (active) {
      if (!(await priceFunctions.verifyPrice(idProduct, byRegion, regions))) {
        return res.status(400).json(
          response(false, {
            error:
              "No pueden existir 2 precios activos para el mismo producto, a menos que sea para una región especifica, la cual tampoco debe repetirse; desactive el otro precio activo o ingrese este precio como desactivo",
          })
        );
      }
    }
    const insertQuery = `
      INSERT INTO 
        price(
          start_date,
          end_date,
          active,
          price,
          discount,
          id_product,
          by_region
        )
        VALUES(
          '${startDate}',
          '${endDate}',
          ${active},
          ${price},
          ${discount == null ? 0 : discount},
          ${idProduct},
          ${byRegion}
        ) RETURNING id_price
    `;
    let result = await db.query(insertQuery);
    const idPrice = result.rows[0].id_price;
    if (byRegion) {
      for (let region of regions) {
        await db.query(
          `INSERT INTO price_region(id_region, id_price) VALUES (${region}, ${idPrice})`
        );
      }
    }
    return res
      .status(200)
      .json(response(true, "Se creo el precio de manera correcta"));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.post("/createPrice", createPrice);

module.exports = router;
