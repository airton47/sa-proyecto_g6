require("dotenv").config();
const supertest = require("supertest");
const app = require("..");
const axios = require("axios");
jest.mock("axios");

describe("Price controller", () => {
  it("Should return 400 status code", async () => {
    const response = await supertest(app).post("/prices/createPrice").send({});
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 401 status code, sin token", async () => {
    const response = await supertest(app).post("/prices/createPrice").send({
      startDate: "20220101",
      endDate: "20210101",
      active: "true",
      price: "10",
      discount: "10",
      idProduct: "1",
      byRegion: "true",
      regions: "1",
    });
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 500 status code, Autenticación errónea", async () => {
    axios.post.mockImplementation(() =>
      Promise.resolve({
        data: {
          data: {
            success: true,
            payload: {
              id_user: 2,
              email: "admin@admin.admin",
              username: "admin",
              id_user_type: 1,
              iat: 1664667741,
              exp: 1664674941,
            },
          },
        },
      })
    );
    const response = await supertest(app)
      .post("/prices/createPrice")
      .send({
        startDate: "20220101",
        endDate: "20210101",
        active: "true",
        price: "10",
        discount: "1",
        idProduct: "1",
        byRegion: "true",
        regions: "1",
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(500);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, Para un precio por región debe enviar las regiones en las que estara disponible", async () => {
    axios.post.mockImplementation(() =>
      Promise.resolve({
        data: {
          data: {
            success: true,
            payload: {
              id_user: 2,
              email: "admin@admin.admin",
              username: "admin",
              id_user_type: 1,
              iat: 1664667741,
              exp: 1664674941,
            },
          },
        },
      })
    );
    const response = await supertest(app)
      .post("/prices/createPrice")
      .send({
        startDate: "20220101",
        endDate: "20210101",
        active: "true",
        price: "10",
        discount: "1",
        idProduct: "1",
        byRegion: true,
        regions: false,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
});

//const verifyPrice = require("../utils/price.utils");
//jest.mock("../utils/price.utils");
