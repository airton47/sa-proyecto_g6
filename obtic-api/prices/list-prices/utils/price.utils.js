const db = require("../db/database");

const getRegions = async (query) => {
  const regionsQuery = `
    SELECT
      r.region, pr.id_price, r.id_region
    FROM
      (${query}) p
    INNER JOIN
      price_region pr
    ON
      p.id_price = pr.id_price
    INNER JOIN
      region R
    ON
      pr.id_region = r.id_region
  `;
  return (await db.query(regionsQuery)).rows;
};

module.exports = {
  getRegions,
};
