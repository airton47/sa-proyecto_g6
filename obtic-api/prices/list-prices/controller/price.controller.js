const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const { getRegions } = require("../utils/price.utils");
const verify = require("../utils/verify");

const listPrice = async (req, res) => {
  const { id_product } = req.query;
  if (id_product == null) {
    return res.status(400).json(
      response(false, {
        error:
          "No se enviaron todos los datos necesarios para la actualización",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    let query = `
      SELECT * FROM price 
    `;
    if (id_product != 0) {
      query = `${query} WHERE id_product = ${id_product}`;
    }
    let prices = (await db.query(query)).rows;
    let regions = await getRegions(query);
    for (let price of prices) {
      price.regions = [];
      for (let region of regions) {
        if (region.id_price == price.id_price) {
          price.regions.push(region);
        }
      }
    }
    let responsePrices = prices;
    if (id_product == 0) {
      let products = (
        await db.query(`
        SELECT
          id_product, product_name
        FROM
          product
      `)
      ).rows;
      for (let product of products) {
        product.prices = [];
        for (let price of prices) {
          if (price.id_product == product.id_product) {
            product.prices.push(price);
          }
        }
      }
      responsePrices = products;
    }
    return res.status(200).json(response(true, responsePrices));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.get("/listPrice", listPrice);

module.exports = router;
