const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const gameFunction = require("../utils/game.utils");

const getGames = async (req, res) => {
  const { id_catalog, filter } = req.query;
  if (!id_catalog || !filter)
    return res.status(400).json(response(false, { error: "GG-005" })); //faltan parametros
  try {
    const query = gameFunction.generateQuery(Number(id_catalog), filter);
    let data = await db.query(query);
    if (data.rows.length == 0) {
      return res.status(400).json(response(false, { error: "GG-001" })); //No existen juegos
    }
    let games = data.rows;
    const developers = await gameFunction.getDevelopers(query, db);
    const categories = await gameFunction.getCategories(query, db);
    const images = await gameFunction.getImages(query, db);
    for (let game of games) {
      game.developer = gameFunction.getDevelopersByGame(
        game.id_product,
        developers
      );
      game.category = gameFunction.getCategoriesByGame(
        game.id_product,
        categories
      );
      game.images = gameFunction.getImagesByGame(game.id_product, images);
      game.available = true;
      game.price = Math.random() * 500;
    }
    let responseGame = [...games];
    if (id_catalog === "0" && filter !== "0") {
      responseGame = [];
      if (filter === "1") {
        const allCategories = await db.query(
          "SELECT id_category, category_name FROM category"
        );
        for (let category of allCategories.rows) {
          let responseGames = [];
          for (let game of games) {
            for (let categoryGame of game.category) {
              if (categoryGame === category.category_name) {
                responseGames.push(game);
                break;
              }
            }
          }
          responseGame.push({
            id: category.id_category,
            name: category.category_name,
            games: responseGames,
          });
        }
      } else if (filter === "2") {
        const allDevelopers = await db.query(
          "SELECT id_developer, developer_name FROM developer"
        );
        for (let developer of allDevelopers.rows) {
          let responseGames = [];
          for (let game of games) {
            for (let developerGame of game.developer) {
              if (developerGame.developerId === developer.id_developer) {
                responseGames.push(game);
                break;
              }
            }
          }
          responseGame.push({
            id: developer.id_developer,
            name: developer.developer_name,
            games: responseGames,
          });
        }
      }
    }

    return res.status(200).json(response(true, responseGame));
  } catch (error) {
    console.log(error);
    return res.status(500).json(response(false, { error: "GG-006" })); //ERROR DEL SERVIDOR
  }
};

router.get("/getGames", getGames);

module.exports = router;
