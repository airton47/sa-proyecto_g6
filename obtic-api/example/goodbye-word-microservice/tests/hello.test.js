
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('HelloController', () => {
  it('Should return 200 status code', async () => {
    const response = await supertest(app).get('/world/goodbye');
    expect(response.statusCode).toBe(200);
  });
});
