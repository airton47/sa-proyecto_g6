const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");

const goodbyeWorld = async (req, res) => {
  try {
    const data = await db.query(`select * from hi`);
    return res.status(200).send(response(true, data.rows[1].text));
  } catch (error) {
    return res.status(500).send(response(false, error.toString()));
  }
};

router.get("/goodbye", goodbyeWorld);

module.exports = router;
