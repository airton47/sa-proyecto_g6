const router = require("express").Router();
const response = require("../utils/response");
const jwt = require("jsonwebtoken");

const verify = async (req, res) => {
  const { token } = req.body;
  if (!token) return res.status(400).json(response(false, { error: "EV-001" })); //Faltan parametros
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY);

    return res.status(200).json(
      response(true, {
        payload: decoded,
      })
    );
  } catch (error) {
    return res.status(401).json(response(false, { error: "EV-002" })); //Autenticacion erronea
  }
};

router.post("/verify", verify);

module.exports = router;
