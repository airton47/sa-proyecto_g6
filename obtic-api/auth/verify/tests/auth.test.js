
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Register controller', () => {
  it('Should return 401 unauthorized', async () => {
    const response = await supertest(app).post('/auth/verifyAuthentication').send({});
    expect(response.statusCode).toBe(404);
  });
  it('Should return 401 unauthorized', async () => {
    const response = await supertest(app).post('/auth/verify').send({
      token: 'notThisToken'
    });
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });
});