# DOCKER

```
docker build . -t fernandvx/obtic-auth-verify-microservice
```

```
docker push fernandvx/obtic-auth-verify-microservice
```

```
docker run --name auth-verify-microservice -p 3000:80 fernandvx/obtic-auth-verify-microservice
```
