# DOCKER

```
docker build . -t fernandvx/obtic-auth-register-microservice
```

```
docker push fernandvx/obtic-auth-register-microservice
```

```
docker run --name auth-register-microservice -p 3000:80 fernandvx/obtic-auth-register-microservice
```
