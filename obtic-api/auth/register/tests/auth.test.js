
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Register controller', () => {
  it('Should return 200 status code', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "test01",
      lastName: "test01",
      username: `test01${(Date.now()).toString()}`,
      email: `test01${(Date.now()).toString()}@gmail.com`,
      password: "123456",
      profileImage: "https://play-lh.googleusercontent.com/wPYYeEooM8vxYp_j4dS5qDdhTzOX4Kq787SqWd_3NRLPCqpRKP50ABpx-0ikn8fsAw"
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it('Should return 200 status code', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "test01",
      lastName: "test01",
      username: "test01",
      email: "test01@gmail.com",
      password: "123456",
      profileImage: "https://play-lh.googleusercontent.com/wPYYeEooM8vxYp_j4dS5qDdhTzOX4Kq787SqWd_3NRLPCqpRKP50ABpx-0ikn8fsAw"
    });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('ER-001');
  });

  it('Should return 200 status code', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "test01",
      lastName: "test01",
      username: "test01",
      email: `test01${(Date.now()).toString()}@gmail.com`,
      password: "123456",
      profileImage: "https://play-lh.googleusercontent.com/wPYYeEooM8vxYp_j4dS5qDdhTzOX4Kq787SqWd_3NRLPCqpRKP50ABpx-0ikn8fsAw"
    });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('ER-002');
  });
  
  it('Should return 400 bad request', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "test01",
      lastName: "test01",
      username: "test01",
      email: "test01@gmail.com",
      profileImage: "https://play-lh.googleusercontent.com/wPYYeEooM8vxYp_j4dS5qDdhTzOX4Kq787SqWd_3NRLPCqpRKP50ABpx-0ikn8fsAw"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('ER-004');
  });
});