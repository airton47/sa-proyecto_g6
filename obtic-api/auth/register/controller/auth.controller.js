const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const bcrypt = require("bcrypt");

const Register = async (req, res) => {
  const { region, name, lastName, username, email, password, profileImage } =
    req.body;
  if (
    !region ||
    !name ||
    !lastName ||
    !username ||
    !email ||
    !password ||
    !profileImage
  )
    return res.status(400).json(response(false, { error: "ER-004" })); //faltan parametros
  try {
    const duplicatedEmail = await db.query(
      `SELECT email, username FROM "user" WHERE email = '${email}'`
    );
    if (duplicatedEmail.rowCount > 0)
      return res.status(400).json(response(false, { error: "ER-001" })); // Correo ya registrado
    const duplicatedUsername = await db.query(
      `SELECT email, username FROM "user" WHERE username = '${username}'`
    );
    if (duplicatedUsername.rowCount > 0)
      return res.status(400).json(response(false, { error: "ER-002" })); //Nombre de usuario ya existe
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    const query = `INSERT INTO "user" (name, last_name, username, email, password, profile_image, id_user_type) VALUES ('${name}', '${lastName}', '${username}', '${email}', '${hash}', '${profileImage}', 2);`;
    await db.query(query);
    return res.status(200).json(
      response(true, {
        region,
        name,
        lastName,
        username,
        email,
        password,
        profileImage,
      })
    );
  } catch (error) {
    console.log(error);
    return res.status(500).json(response(false, { error: "ER-003" })); //Error desconocido (bae de datos no conectada)
  }
};

router.post("/register", Register);

module.exports = router;
