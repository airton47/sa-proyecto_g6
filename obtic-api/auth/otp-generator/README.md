# DOCKER

```
docker build . -t fernandvx/obtic-auth-otp-generator-microservice
```

```
docker push fernandvx/obtic-auth-otp-generator-microservice
```

```
docker run --name auth-otp-generator-microservice -p 3000:80 fernandvx/obtic-auth-otp-generator-microservice
```
