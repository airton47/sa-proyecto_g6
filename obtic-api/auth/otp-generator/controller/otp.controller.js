const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const mail = require("../helpers/mailer");

const GenerateOTP = async (req, res) => {
  const { email } = req.body;
  if (!email) {
    return res.status(400).json(response(false, { error: "EL-001" })); //faltan parametros
  }

  try {
    let query = `select * from "user" where email='${email}'`;
    const data = await db.query(query);

    if (data.rows.length == 0) {
      return res.status(400).json(response(false, { error: "EL-002" })); //no existe cuenta para el correo
    }

    let { id_user } = data.rows[0];

    let token = Math.floor(Math.random() * (999999 - 111111) + 111111);
    query = `insert into confirmation (code,confirmed_date,id_user) values (${token}, NOW() + interval '5 minute', ${id_user});`;
    await db.query(query);

    let to = email;
    let txtSubject = "Obtic Games confirmación de correo - OTP ";

    let txt = `
    <div id="mail">
    <div>
      <div style="background-color: #ffffff">
        <table
          style="
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            border-spacing: 0;
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
            background-repeat: repeat;
            background-position: top;
          "
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <td style="margin: 0" valign="top">
                <table
                  style="
                    border-collapse: collapse;
                    border-spacing: 0;
                    table-layout: fixed !important;
                    width: 100%;
                  "
                  cellspacing="0"
                  cellpadding="0"
                  bgcolor="#0c171f"
                  align="center"
                >
                  <tbody>
                    <tr>
                      <td align="Center">
                        <img
                          loading="lazy"
                          src="https://i.ibb.co/bXJqn1H/Logo.png"
                          style="border: 0; margin: 32px 0 0; display: block"
                          alt=""
                        />
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px 0;
                          line-height: 40px;
                          color: #ffffff;
                          font-size: 22px;
                        "
                        align="center"
                      >
                        <b>
                          Estás a solo un paso de activar tu cuenta Obtic
                          Games.</b
                        >
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px 0;
                          line-height: 20px;
                          color: #ffffff;
                          font-size: 16px;
                        "
                        align="center"
                      >
                        Para completar tu registro utiliza el siguiente One Time
                        PIN (OTP): <b>${token}</b>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px;
                          line-height: 20px;
                          color: #ffffff;
                          font-size: 16px;
                        "
                        align="center"
                      >
                      ¡Bienvenido! <br />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table
                          style="
                            border-collapse: collapse;
                            border-spacing: 0;
                            width: 100%;
                            background-color: #15202d;
                          "
                          cellspacing="0"
                          cellpadding="0"
                          bgcolor="#637180
                          "
                          align="center"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  padding: 3% 0;
                                  font-family: arial, 'helvetica neue', helvetica,
                                    sans-serif;
                                  line-height: 21px;
                                  color: #637180;
                                  font-size: 14px;
                                "
                                align="center"
                              >
                                Este es un correo auto-generado por
                                <a
                                  href="mailto:pruebasusac2022@gmail.com"
                                  style="
                                    text-decoration: none;
                                    text-transform: lowercase;
                                    color: #5b43f4;
                                  "
                                  >pruebasusac2022@gmail.com</a
                                >
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
    `;
    mail.enviar_mail(txtSubject, to, txt);

    return res.status(200).json(
      response(true, {
        message: "El correo ha sido enviado con exito",
        email: email,
      })
    );
  } catch (error) {
    return res.status(500).json(response(false, { error: "EL-003" })); //error del servidor
  }
};

router.post("/generateOTP", GenerateOTP);

module.exports = router;
