require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const AuthController = require("./controller/auth.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/auth", AuthController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3010;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
