
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Register controller', () => {
  it('Should return 200 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann2903@gmail.com",
      password: "HolaMundo"
  });
    expect(response.statusCode).toBe(500);
    expect(response.body.success).toBe(false);
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann2903451@gmail.com",
      password: "HolaMundo"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('EL-001');
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann2903@gmail.com",
      password: "HolaMund"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('EL-002');
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann290@gmail.com",
      password: "HolaMundo"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('EL-003');
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann29033@gmail.com",
      password: "HolaMundo"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('EL-003');
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).post('/auth/login').send({
      email:  "welmann29033@gmail.com"
  });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
    expect(response.body.data.error).toBe('EL-005');
  });
});