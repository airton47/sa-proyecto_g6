const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const login = async (req, res) => {
  const { email, password, ip } = req.body;
  if (!email || !password)
    return res.status(400).json(response(false, { error: "EL-005" })); //faltan parametros
  try {
    const query = `SELECT 
                      *
                   FROM 
                      "user" 
                   WHERE email='${email}'`;
    const data = await db.query(query);
    if (data.rows.length == 0) {
      return res.status(400).json(
        response(false, {
          error: "No existe una cuenta asociada al correo proporcionado",
        })
      );
    }
    const user = data.rows[0];
    if (!bcrypt.compareSync(password, user.password)) {
      return res
        .status(400)
        .json(
          response(false, { error: "La contraseña ingresada es incorrecta" })
        );
    }
    if (!user.active) {
      return res
        .status(400)
        .json(
          response(false, { error: "El usuario se encuentra inhabilitado" })
        );
    }
    if (!user.confirmed) {
      return res.status(400).json(
        response(false, {
          error: "EL-004",
        })
      ); // No confirmado
    }
    if (user.suspension_date != null) {
      const today = new Date(Date.now());
      if (today.getDate() <= user.suspension_date.getDate()) {
        return res.status(400).json(
          response(false, {
            error: `El usuario ${
              user.username
            } tiene una baja temporal hasta la fecha ${
              user.suspension_date.toISOString().split("T")[0]
            }`,
          })
        );
      }
    }

    const token_interconectividad = jwt.sign(
      {
        group: 6,
        userId: user.id_user,
        email: user.email,
        name: `${user.name} ${user.last_name}`,
        region: user.region,
        ip: ip,
      },
      process.env.COMMON_KEY,
      {
        expiresIn: "12h",
      }
    );

    const token = jwt.sign(
      {
        id_user: user.id_user,
        email: user.email,
        username: user.username,
        id_user_type: user.id_user_type,
        interconectividad: token_interconectividad,
      },
      process.env.TOKEN_KEY,
      {
        expiresIn: "12h",
      }
    );

    delete user.password;

    return res.status(200).json(
      response(true, {
        token: token,
        is_admin: user.id_user_type == 1 ? true : false,
        user: user,
      })
    );
  } catch (error) {
    return res.status(500).json(response(false, { error: "EL-006" })); //ERROR DEL SERVIDOR
  }
};

router.post("/login", login);

module.exports = router;
