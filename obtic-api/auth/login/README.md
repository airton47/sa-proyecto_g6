# DOCKER

```
docker build . -t fernandvx/obtic-auth-login-microservice
```

```
docker push fernandvx/obtic-auth-login-microservice
```

```
docker run --name auth-login-microservice -p 3000:80 fernandvx/obtic-auth-login-microservice
```
