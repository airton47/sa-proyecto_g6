const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const mail = require("../helpers/mailer");

const ValidateOTP = async (req, res) => {
  const { email } = req.body;
  const { code } = req.body;

  if (!email || !code) {
    return res.status(400).send(
      response(false, {
        error: "OV-001",
      }) //faltan parametros
    );
  }
  try {
    //Verificar correo o codigo incorrecto
    let query =
      'select  u.id_user, c.id_confirmation from "user" u inner join confirmation c on c.id_user = u.id_user where u.email = $1 and c.code = $2';
    const data = await db.query(query, [email, code]);
    if (data.rows.length != 0) {
      //verificar Tiempo incorrecto
      query =
        'select  u.id_user, c.id_confirmation from "user" u inner join confirmation c on c.id_user = u.id_user where u.email = $1 and c.code = $2 and now()< c.confirmed_date;';
      const data2 = await db.query(query, [email, code]);
      if (data2.rows.length == 0) {
        return res
          .status(400)
          .send(
            response(false, { error: "OV-002", msg: "El token ha expirado" })
          );
      }
      //Actualizar la tabla de usuario
      query = 'UPDATE "user" SET confirmed=true WHERE id_user = $1;';
      console.log(data.rows[0].id_user);
      await db.query(query, [data.rows[0].id_user]);
      // Ahora se envia el correo electronico
      let to = email;
      let txtSubject = "Cuenta Obtic Games confirmada";

      let txt = `
      <div id="mail">
      <div>
        <div style="background-color: #ffffff">
          <table
            style="
              font-family: Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              border-spacing: 0;
              padding: 0;
              margin: 0;
              width: 100%;
              height: 100%;
              background-repeat: repeat;
              background-position: top;
            "
            width="100%"
            cellspacing="0"
            cellpadding="0"
          >
            <tbody>
              <tr>
                <td style="margin: 0" valign="top">
                  <table
                    style="
                      border-collapse: collapse;
                      border-spacing: 0;
                      table-layout: fixed !important;
                      width: 100%;
                    "
                    cellspacing="0"
                    cellpadding="0"
                    bgcolor="#0c171f"
                    align="center"
                  >
                    <tbody>
                      <tr>
                        <td align="Center">
                          <img
                            loading="lazy"
                            src="https://i.ibb.co/bXJqn1H/Logo.png"
                            style="border: 0; margin: 32px 0 0; display: block"
                            alt=""
                          />
                        </td>
                      </tr>
                      <tr>
                        <td
                          style="
                            padding: 30px 20px 0;
                            line-height: 40px;
                            color: #ffffff;
                            font-size: 22px;
                          "
                          align="center"
                        >
                          <b> Tu cuenta ha sido verificada de manera exitosa.</b>
                        </td>
                      </tr>
                      <tr>
                        <td
                          style="
                            padding: 30px 20px;
                            line-height: 20px;
                            color: #ffffff;
                            font-size: 16px;
                          "
                          align="center"
                        >
                          ¡Bienvenido! <br />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table
                            style="
                              border-collapse: collapse;
                              border-spacing: 0;
                              width: 100%;
                              background-color: #15202d;
                            "
                            cellspacing="0"
                            cellpadding="0"
                            bgcolor="#637180
                            "
                            align="center"
                          >
                            <tbody>
                              <tr>
                                <td
                                  style="
                                    padding: 3% 0;
                                    font-family: arial, 'helvetica neue', helvetica,
                                      sans-serif;
                                    line-height: 21px;
                                    color: #637180;
                                    font-size: 14px;
                                  "
                                  align="center"
                                >
                                  Este es un correo auto-generado por
                                  <a
                                    href="mailto:pruebasusac2022@gmail.com"
                                    style="
                                      text-decoration: none;
                                      text-transform: lowercase;
                                      color: #5b43f4;
                                    "
                                    >pruebasusac2022@gmail.com</a
                                  >
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>    
      `;
      mail.enviar_mail(txtSubject, to, txt);

      return res.status(200).send(response(true, "Cuenta confirmada"));
    } else {
      return res.status(400).send(
        response(false, {
          error: "OV-003",
          msg: "El Correo o el token son incorrectos",
        })
      );
    }
  } catch (error) {
    return res.status(500).send(response(false, { error: "OV-004" }));
  }
};

router.post("/verifyOTP", ValidateOTP);

module.exports = router;
