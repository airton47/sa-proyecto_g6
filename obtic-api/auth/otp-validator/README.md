# DOCKER

```
docker build . -t fernandvx/obtic-auth-otp-validator-microservice
```

```
docker push fernandvx/obtic-auth-otp-validator-microservice
```

```
docker run --name auth-otp-validator-microservice -p 3000:80 fernandvx/obtic-auth-otp-validator-microservice
```
