require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const OTPController = require("./controller/otp.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/auth", OTPController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3010;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
