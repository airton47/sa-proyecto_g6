const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");

const getRegionList = async (req, res) => {
  try {
    const { rows } = await db.query(`select * from region;`);
    return res.status(200).send(response(true, { Region: rows }));
  } catch (error) {
    return res.status(500).send(response(false, error.toString()));
  }
};

router.get("/getRegion", getRegionList);

module.exports = router;
