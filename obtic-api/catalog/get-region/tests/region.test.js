require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Region controller', () => {
  it('Should return 200 status code', async () => {
    const response = await supertest(app).get('/catalog/getRegion');
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});