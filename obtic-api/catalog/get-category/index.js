require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const categoryController = require("./controller/category.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/catalog", categoryController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3010;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
