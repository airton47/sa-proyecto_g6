const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");

const getCategoryList = async (req, res) => {
  try {
    const { rows } = await db.query(`select * from category;`);
    return res.status(200).send(response(true, { Category: rows }));
  } catch (error) {
    return res.status(500).send(response(false, error.toString()));
  }
};

router.get("/getCategory", getCategoryList);

module.exports = router;
