const db = require("../db/database");

// Sera utilizado en la integracion
const formatGameForIntegration = (game) => {
  delete game.parent_game;
  return {
    gameId: game.id_product,
    name: game.product_name,
    image: game.logo,
    desktopImage: game.desktop_image,
    mobileImage: game.mobile_image,
    description: game.description,
    shortDescription: game.short_description,
  };
};

const formatGame = (game) => {
  delete game.parent_game;
  delete game.is_game;
  return game;
};

const generateQuery = (id_catalog, filter) => {
  let principalQuery = `SELECT 
                             p.*
                          FROM 
                             product p`;
  let whereStatements = "p.is_game = true and p.active = true";
  switch (id_catalog) {
    case 0:
      break;
    case 1:
      principalQuery = `
                  SELECT
                    p.*, purchase
                  FROM
                    (
                      select 
                      	id_product, count(*) as purchase
                      from 
                      	"library" l 
                      group by
                      	id_product 
                      order by 
                      	purchase desc
                      fetch first ${filter} rows only
                    ) ms
                  INNER JOIN
                    product p
                  ON
                    p.id_product = ms.id_product
                `;
      whereStatements = `${whereStatements} ORDER BY purchase DESC`;
      break; //Deshabilitado en esta fase
    case 2:
      principalQuery = `${principalQuery} 
                          INNER JOIN 
                            category_game cg ON p.id_product = cg.id_product`;
      whereStatements = `${whereStatements} AND cg.id_category = ${filter}`;
      break;
    case 3:
      principalQuery = `${principalQuery} 
                          INNER JOIN 
                            developer_product dp ON p.id_product = dp.id_product`;
      whereStatements = `${whereStatements} AND dp.id_developer = ${filter}`;
      break;
    case 4:
      principalQuery = `
                        SELECT
                          p.*, recommended
                        FROM
                          (
                            select 
                              id_product, count(*) as recommended
                            from 
                              recommended r 
                            group by
                              id_product 
                            order by 
                              recommended desc
                            fetch first ${filter} rows only
                          ) ms
                        INNER JOIN
                          product p
                        ON
                          p.id_product = ms.id_product`;
      whereStatements = `${whereStatements} ORDER BY recommended DESC`;
      break;
    case 5:
      principalQuery = `
                      SELECT 
                        p.*, avg_score
                      FROM
                        (
                          select
                            id_product, avg(score) as avg_score
                          from 
                            score s 
                          group by
                            id_product 
                          order by 
                            avg_score desc
                          fetch first ${filter} rows only
                        ) ms
                      INNER JOIN
                        product p
                      ON
                        p.id_product = ms.id_product
                    `;
      whereStatements = `${whereStatements} ORDER BY avg_score DESC`;
      break;
    case 6:
      whereStatements = `${whereStatements} AND product_name ILIKE '%${filter}%'`;
      break;
    case 7:
      whereStatements = `${whereStatements} AND id_product = ${filter}`;
      break;
    case 8:
      whereStatements = `${whereStatements} AND not (desktop_image is null)`;
      break;
    case 9:
      break;
  }

  /**
   * id_catalog: 0 orderBy
   * filter: 1. categoria, 2. developer, 0. sin orden
   */
  return `  ${principalQuery} 
            WHERE 
              ${whereStatements}`;
};

const getCategories = async (query, db) => {
  query = query.replace("p.*", "p.id_product");
  const categoryQuery = `
        select 
        	p.id_product, c.id_category, c.category_name 
        from
        	(${query}) p
        inner join
        	category_game cg
        on
        	p.id_product = cg.id_product
        inner join
        	category c
        on
        	cg.id_category = c.id_category
    `;
  let result = await db.query(categoryQuery);
  return result.rows;
};

const getDevelopers = async (query, db) => {
  query = query.replace("p.*", "p.id_product");
  const developerQuery = `
        select 
        	p.id_product, d.id_developer, developer_name
        from
        	(${query}) p
        inner join
        	developer_product dp
        on
        	p.id_product = dp.id_product
        inner join
        	developer d
        on
        	dp.id_developer = d.id_developer
    `;
  let result = await db.query(developerQuery);
  return result.rows;
};

const getImages = async (query, db) => {
  query = query.replace("p.*", "p.id_product");
  const imageQuery = `
      SELECT
        P.id_product, url_image, is_image
      from
        (${query}) p
      inner join
        product_image pi
      on
        p.id_product = pi.id_product
  `;
  let result = await db.query(imageQuery);
  return result.rows;
};

const getDevelopersByGame = (id_game, developers) => {
  let developersByGame = [];
  for (let developer of developers) {
    if (developer.id_product === id_game) {
      developersByGame.push({
        developerId: developer.id_developer,
        name: developer.developer_name,
        image: "",
      });
    }
  }
  return developersByGame;
};

const getCategoriesByGame = (id_game, categories) => {
  let categoriesByGame = [];
  for (let category of categories) {
    if (category.id_product === id_game) {
      categoriesByGame.push(category.category_name);
    }
  }
  return categoriesByGame;
};

const getImagesByGame = (id_game, images) => {
  let imagesByGame = [];
  for (let image of images) {
    if (image.id_product === id_game) {
      imagesByGame.push({
        url: image.url_image,
        is_image: image.is_image,
      });
    }
  }
  return imagesByGame;
};

const getPrice = (id_product, prices) => {
  for (let price of prices) {
    if (price.id_product === id_product) {
      return price;
    }
  }
  return { price: -1, discount: 0, end_date: "2022-12-31" };
};

const getPriceByProduct = async (query) => {
  const today = new Date(Date.now()).toISOString().split("T")[0];
  query = query.replace("p.*", "p.id_product");
  const queryPrice = `
                  select 
                  	pr.*
                  from
                    (${query}) p
                  inner join
                  	price pr 
                  on
                    p.id_product = pr.id_product
                  where 
                  	pr.active = true and pr.by_region = false
                      and
                          '${today}' >= pr.start_date and '${today}' <= pr.end_date`;
  return (await db.query(queryPrice)).rows;
};

const getPriceByProductAndRegion = async (query, region) => {
  const today = new Date(Date.now()).toISOString().split("T")[0];
  query = query.replace("p.*", "p.id_product");
  const queryPrice = `
                  select 
                  	pri.*, pr.id_region 
                  from
                    (${query}) p
                  inner join
                  	price pri 
                  on
                    pri.id_product = p.id_product
                  inner join
                      price_region pr 
                  on
                      pri.id_price = pr.id_price
                  inner join 	
                      (
                      	select 
                          	id_region
                          from
                          	region r 
                          where 
                          	r.region ilike '%${region}%'
                      ) r
                  on 
                      r.id_region = pr.id_region
                  where 
                    pri.active = true and pri.by_region = true
                      and
                          '${today}' >= pri.start_date and '${today}' <= pri.end_date`;
  return (await db.query(queryPrice)).rows;
};

const getAvailabilityByRegion = async (query, region) => {
  query = query.replace("p.*", "p.id_product");
  const queryAvailable = `
                  select 
                  	re.*
                  from
                  	region_restriction re    
                  inner join 	
                    (
                    	select 
                        	id_region
                        from
                        	region r 
                        where 
                        	r.region ilike '%${region}%'
                    ) r
                  on 
                    r.id_region = re.id_region
                  inner join
                    (${query}) p
                  on
                    p.id_product = re.id_product`;
  return (await db.query(queryAvailable)).rows;
};

const getAvailable = (id_product, availableRegions) => {
  for (let availableRegion of availableRegions) {
    if (availableRegion.id_product === id_product) {
      return false;
    }
  }
  return true;
};

module.exports = {
  formatGameForIntegration,
  formatGame,
  generateQuery,
  getCategories,
  getDevelopers,
  getDevelopersByGame,
  getCategoriesByGame,
  getImages,
  getImagesByGame,
  getPriceByProduct,
  getPriceByProductAndRegion,
  getAvailabilityByRegion,
  getPrice,
  getAvailable,
};
