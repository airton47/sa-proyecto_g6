const jwt = require("jsonwebtoken");

/**
 * Funcion que verifica que el token y que los datos contenidos, sus atributos,
 * sean correctos segun especificaciones de interconectividad.
 * @param {*} header: String, cadena con formato: - Bearer {token} -
 * @returns Object
 */
const verify = (header) => {
  if (!header) {
    console.log("No header authentication provided");
    return undefined;
  }

  if (!header.startsWith("Bearer ")) {
    console.log("Header wrong format");
    return undefined;
  }

  let token = header.substring(7, header.length);
  const decoded = jwt.verify(token, process.env.INTERCONNECTION_TOKEN_KEY);

  if (
    !decoded.group ||
    !decoded.userId ||
    !decoded.email ||
    !decoded.name ||
    !decoded.region ||
    !decoded.ip
  ) {
    console.log("Datos incompletos en Token");
    return undefined;
  } else {
    return decoded;
  }
};

module.exports = verify;
