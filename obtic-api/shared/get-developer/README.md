# DOCKER

```
docker build . -t fernandvx/obtic-shared-get-developer-microservice
```

```
docker push fernandvx/obtic-shared-get-developer-microservice
```

```
docker run --name shared-get-developer-microservice -p 3000:80 fernandvx/obtic-shared-get-developer-microservice
```
