const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");

const getDevelopersList = async (req, res) => {
  try {
    const { rows } = await db.query(`select * from developer;`);
    DataRes = [];
    rows.forEach((element) => {
      DataRes.push({
        developerId: element.id_developer,
        name: element.developer_name,
        image: "https://ui-avatars.com/api/?name=" + element.developer_name,
      });
    });

    return res.status(200).send(response(true, DataRes));
  } catch (error) {
    return res.status(404).send(response(false, []));
  }
};

router.get("/developers", getDevelopersList);

module.exports = router;
