require("dotenv").config();
const supertest = require("supertest");
const app = require("..");

describe("game controller", () => {
  it("Should return 400 status code, dont send parameters", async () => {
    const response = await supertest(app).get("/games/getGames");
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status code, send parameters filter 1", async () => {
    const response = await supertest(app).get("/games/getGames").query({
      id_catalog: "0",
      filter: "1",
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it("Should return 200 status code, send parameters filter 2", async () => {
    const response = await supertest(app).get("/games/getGames").query({
      id_catalog: "0",
      filter: "2",
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
  for (let index = 0; index <= 9; index++) {
    it(
      "Should return 200 status code, send parameters Category=" + index,
      async () => {
        const response = await supertest(app).get("/games/getGames").query({
          id_catalog: index,
          filter: "2",
        });
        expect(response.statusCode).toBe(index == 6 ? 400 : 200);
        expect(response.body.success).toBe(index == 6 ? false : true);
      }
    );
  }
});
