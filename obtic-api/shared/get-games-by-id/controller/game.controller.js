const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const gameFunction = require("../utils/game.utils");
const axios = require("axios");

/**
 * Funcion para formatear una cadena con una fecha y obtener una con el formato: "DD/MM/YYYY"
 * @param {Date} inputDate : Cadena de texto con fecha,
 * debe tener el formato adecuado para convertirse en un objeto
 * de tipo Date.
 * @returns string
 */
function formatDate(inputDate) {
  inputDate = new Date(inputDate);
  let date, month, year;

  date = inputDate.getDate();
  month = inputDate.getMonth() + 1;
  year = inputDate.getFullYear();

  date = date.toString().padStart(2, "0");

  month = month.toString().padStart(2, "0");

  return `${date}/${month}/${year}`;
}

const getGames = async (req, res) => {
  // Verificacion de JWT
  // let payload = verify(req?.headers?.authorization);
  // if (!payload) {
  //   console.log("Error de autenticacion");
  //   return res.status(401).json(response(false, []));
  // }

  // Extrayendo y verificando ip de los headers
  const ip = req?.headers?.ip;

  // Extrayendo y verificando arreglo que contiene los id's de lso juegos solicitados
  const games_list = req.body.games;
  if (!games_list || games_list.length === 0) {
    console.log("No games atribute provided, empty or invalid: <games: []>");
    return res.status(404).json(response(false, [])); //faltan parametros
  }

  try {
    // Obteniendo ubicacion con direccion IP
    const resp = await axios.get(`http://ip-api.com/json/${ip}`);

    // Extrayendo la region
    let region =
      resp?.data?.regionName !== undefined
        ? resp.data.regionName.replace("Departamento de ", "")
        : "";

    // Armando query para la consulta
    let ids_query = "id_product in (";
    for (let i = 0; i < games_list.length; i++) {
      if (i !== games_list.length - 1) {
        ids_query += `${Number(games_list[i])} , `;
      } else {
        ids_query += `${Number(games_list[i])} )`;
      }
    }

    const query = `select * from product where active = true and is_game = true and ${ids_query}`;
    let data = await db.query(query);

    if (data.rows.length == 0) {
      console.log("No games were found");
      return res.status(400).json(response(false, [])); //No existen juegos
    }

    let games = data.rows;
    const developers = await gameFunction.getDevelopers(query, db);
    const categories = await gameFunction.getCategories(query, db);
    const priceByRegion = await gameFunction.getPriceByProductAndRegion(
      query,
      region
    );
    const priceGeneral = await gameFunction.getPriceByProduct(query);
    const availableRegions = await gameFunction.getAvailabilityByRegion(
      query,
      region
    );

    // Seteando los atributos del los elementos del array: developer, category, price, discount
    for (let game of games) {
      game.developer = gameFunction.getDevelopersByGame(
        game.id_product,
        developers
      );
      game.category = gameFunction.getCategoriesByGame(
        game.id_product,
        categories
      );
      let temporalPrice = gameFunction.getPrice(game.id_product, priceByRegion);
      if (temporalPrice.price === -1) {
        temporalPrice = gameFunction.getPrice(game.id_product, priceGeneral);
      }
      game.price = temporalPrice.price;
      game.discount = temporalPrice.discount;
      game.end_date = temporalPrice.end_date;
      game.available = gameFunction.getAvailable(
        game.id_product,
        availableRegions
      );
    }

    // Filtrar arreglo, obtener solo aquellos que tienen el atributo 'available' igual a TRUE
    games = games.filter((game) => game.available === true);

    // Formateando el array para la interconectividad segun especificaciones
    let responseGame = games.map(
      ({
        id_product: gameId,
        product_name: name,
        mobile_image: image,
        description: description,
        release_date: releaseDate,
        age_restriction: restrictionAge,
        price: price,
        discount: discount,
        developer: developer,
        category: category,
      }) => ({
        gameId,
        name,
        image,
        description,
        releaseDate: formatDate(releaseDate),
        restrictionAge: restrictionAge.replace(/ /g, ""),
        price: Number(price),
        discount: Number(discount),
        group: 6,
        developer,
        category,
      })
    );

    return res.status(200).json(response(true, responseGame));
  } catch (error) {
    console.log(error);
    return res.status(500).json(response(false, { data: {} })); //ERROR DEL SERVIDOR
  }
};

router.post("/games", getGames);

module.exports = router;
