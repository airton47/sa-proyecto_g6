# DOCKER

```
docker build . -t fernandvx/obtic-shared-get-games-by-id-microservice
```

```
docker push fernandvx/obtic-shared-get-games-by-id-microservice
```

```
docker run --name shared-get-games-by-id-microservice -p 3000:80 fernandvx/obtic-shared-get-games-by-id-microservice
```
