const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const gameFunction = require("../utils/game.utils");
const axios = require("axios");

/**
 * Realiza la peticion a traves de axios
 * @param {*} ip
 * @param {*} link
 * @returns Promise
 */
function doRequest(ip, link) {
  return axios.get(link, {
    headers: {
      IP: ip,
    },
  });
}

const getGames = async (req, res) => {
  // Extrayendo y verificando ip de token: payload
  const ip = req?.headers?.ip;
  if (!ip) {
    console.log("Missing headers, not provided: <ip>");
    return res.status(404).json(response(false, [])); //faltan parametros
  }

  // Links de los grupos
  const groups = [
    {
      id: process.env.INTERCONNECTION_ID1,
      link: process.env.INTERCONNECTION_LINK1,
    },
    {
      id: process.env.INTERCONNECTION_ID2,
      link: process.env.INTERCONNECTION_LINK2,
    },
    {
      id: process.env.INTERCONNECTION_ID3,
      link: process.env.INTERCONNECTION_LINK3,
    },
  ];

  let content = [];

  for (let index = 0; index < groups.length; index++) {
    await doRequest(ip, groups[index].link)
      .then((res) => {
        if (!res?.data?.data || res?.data?.data?.length === 0) {
          content.push({
            id: groups[index].id,
            message: "Lista no pudo ser recuperada, vacia",
            link: groups[index].link,
          });
        } else {
          content.push({
            id: groups[index].id,
            games: res.data.data,
          });
        }
      })
      .catch((err) => {
        console.log(err);
        content.push({
          id: groups[index].id,
          message: `[Error en request], ${err.toString()}`,
          link: groups[index].link,
        });
      });
  }

  if (content === undefined || content === null || content.length === 0) {
    return res.status(400).json(
      response(false, {
        message: "No se ha podido obtener las listas de juegos de ningun grupo",
      })
    );
  }

  return res.status(200).json(response(true, content));
};

router.get("/externalGames", getGames);

module.exports = router;
