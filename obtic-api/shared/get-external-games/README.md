# DOCKER

```
docker build . -t fernandvx/obtic-shared-get-external-games-microservice
```

```
docker push fernandvx/obtic-shared-get-external-games-microservice
```

```
docker run --name shared-get-external-games-microservice -p 3000:80 fernandvx/obtic-shared-get-external-games-microservice
```

## Documentacion de Servicio

`Descripcion`: Este servicio obtiene la lista de los juegos general de los otros tres grupos por medio de sus servicios de interconectividad

- Tipo de Request: GET
- Requisitos
  - Agregar un header llamado "IP" al request.
  - Agregar los links de los grupos en tres variables de entorno:
    - INTERCONNECTION_LINK1
    - INTERCONNECTION_LINK2
    - INTERCONNECTION_LINK3
- Ejemplo de peticion: http://localhost:3002/interconnection/externalGames

### Ejemplos de respuesta

- Exitoso ✅ Todos los juegos obtenidos, de los tres grupos, se colocaran en un arrelgo llamado: "games" si ocurrio algun error durante la peticion con alguno de los tres grupos, este sera colocado en un arreglo llamado: "error", es este arreglo se indica el link del grupo en donde ocurrio el error y cual fue el motivo, ya sea porque estaba vacio o bien por otra causa.
- Codigo 200 OK

```json
{
  "success": true,
  "data": {
    "games": [
      {
        "gameId": 1,
        "name": "Grand Theft Auto V",
        "image": "https://project-videogame.s3.amazonaws.com/games/281c7376622280366e02c07dc3a3b313.jpg",
        "description": "Un joven estafador callejero, un ladrón de bancos retirado y un psicópata aterrador se meten en un lío, y tendrán que llevar a cabo una serie de peligrosos golpes para sobrevivir en una ciudad en la que no pueden confiar en nadie, y mucho menos los unos en los otros.",
        "releaseDate": "14 abril, 2015",
        "restrictionAge": "AO",
        "price": 141.94,
        "discount": 0,
        "group": 7,
        "developer": [
          {
            "name": "Rockstar Games",
            "image": "https://project-videogame.s3.amazonaws.com/developers/0f2a0b664fba9c72517eb65f88fc5258.png"
          },
          {
            "name": "Playground Games",
            "image": "https://project-videogame.s3.amazonaws.com/developers/292efcd3fc42d3426023035b29adf9a6.png"
          }
        ],
        "category": ["Acción", "Multijugador", "Crimen", "Aventura", "Carreras"]
      },
      {
        "gameId": 2,
        "name": "TRANSFORMERS: BATTLEGROUNDS",
        "image": "https://project-videogame.s3.amazonaws.com/games/6d70aa43b8144836df57603e9e6fa5ac.png",
        "description": "Bumblebee y los Autobots tienen un nuevo comandante: ¡tú! Forma tu escuadrón para lanzarte a la batalla contra los Decepticons... ¡y juega con otros en el modo multijugador local!",
        "releaseDate": "21 diciembre, 2020",
        "restrictionAge": "T",
        "price": 197.17,
        "discount": 0,
        "group": 7,
        "developer": [
          {
            "name": "OverBorder Studio",
            "image": "https://project-videogame.s3.amazonaws.com/developers/c0a052335f6e15280a085208215f42b9.png"
          },
          {
            "name": "Rockstar Games",
            "image": "https://project-videogame.s3.amazonaws.com/developers/0f2a0b664fba9c72517eb65f88fc5258.png"
          }
        ],
        "category": [
          "Supervivencia",
          "Disparos",
          "Multijugador",
          "Battle Royale"
        ]
      },
      {
        "gameId": 3,
        "name": "Forza Horizon 4",
        "image": "https://project-videogame.s3.amazonaws.com/games/f8d2ae2d55fd5f624dded9433137f1b8.jpg",
        "description": "Las estaciones dinámicas lo cambian todo en el mejor festival automovilístico del mundo. Ve por cuenta propia o únete a otros equipos para explorar la hermosa e histórica Gran Bretaña en un mundo abierto compartido.",
        "releaseDate": "09 marzo, 2021",
        "restrictionAge": "T",
        "price": 156.14,
        "discount": 0,
        "group": 7,
        "developer": [
          {
            "name": "Bethesda Game Studios",
            "image": "https://project-videogame.s3.amazonaws.com/developers/5abc6935adcd9e5ed065a0c8c3906ea8.png"
          },
          {
            "name": "Nixxes Software",
            "image": "https://project-videogame.s3.amazonaws.com/developers/16870cadac35f005958bc50b9b7cec70.png"
          },
          {
            "name": "Insomniac Games",
            "image": "https://project-videogame.s3.amazonaws.com/developers/48fe81dab732b1d8e928b05a18823568.png"
          }
        ],
        "category": [
          "Carreras",
          "Mundo abierto",
          "Conducción",
          "Multijugador",
          "Simulador de automóviles"
        ]
      }
    ],
    "errors": []
  }
}
```

- Fallido ❌, Ocurre cuando no se pudo obtener ningun juego de los tres grupos.
- Codigo 400 Error

```json
{
  "success": false,
  "data": {
    "message": "No se ha podido obtener las listas de juegos de ningun grupo"
  }
}
```
