# DOCKER

```
docker build . -t fernandvx/obtic-shared-get-single-external-game-microservice
```

```
docker push fernandvx/obtic-shared-get-single-external-game-microservice
```

```
docker run --name shared-get-single-external-game-microservice -p 3000:80 fernandvx/obtic-shared-get-single-external-game-microservice
```

## Documentacion de Servicio

`Descripcion`: Este servicio obtiene la informacion de un solo juego `EXTERNO`, perteneciente a otro grupo

- Tipo de Request: POST
- Requisitos
  - Estar logeado
    - Header de autenticacion
  - Agregar los links y los id de los grupos en las variables de entorno:
    - ID_G1=1
    - ID_G2=7
    - ID_G3=8
    - URL_G1=http://35.223.53.97:3001/games
    - URL_G2=http://api.killjoygt.tk/shared/games
    - URL_G3=http://34.135.218.4/games
- body:

```json
{
  "groupNumber": 2,
  "idGame": 19
}
```

- Ejemplo de peticion: http://localhost:3002/shared/getSingleGame

### Ejemplos de respuesta

- Exitoso ✅ Obtiene toda la informacion de
- Codigo 200 OK

```json
{
  "success": true,
  "data": {
    "name": "Grand Theft Auto IV",
    "image": "https://storage.googleapis.com/sa-g1/covers-juegos/grand-theft-auto-IV.jpg",
    "releaseDate": "1995-06-12T00:00:00.000Z",
    "restrictionAge": "E",
    "price": 9.99,
    "discount": 5,
    "description": "The legendary Grand Theft Auto games utilize a design theme that is recognizable from across the room. Covers feature split section artwork capturing the characters and scenes you'll find inside the game.",
    "category": ["Acción", "Aventura"],
    "developer": [],
    "gameId": 19,
    "group": 1
  }
}
```

- Fallido ❌ | Caso 1, ocurreo cuando el numero de grupo ingresado no coindice con las variables de entorno
- Codigo 404 Error

```json
{
  "success": false,
  "data": {
    "message": "Target group information was not found."
  }
}
```

- Fallido ❌ | Caso 2, ocurreo cuando el numero de juego ingresado no tiene coincidencias
- Codigo 404 Error

```json
{
  "success": false,
  "data": {
    "message": "The game could not be found with idGame: 1998"
  }
}
```

- Fallido ❌ | Caso 3, ocurreo cuando el numero de juego ingresado no tiene coincidencias
- Codigo 404 Error

```json
{
  "success": false,
  "data": {
    "message": "The game could not be found with idGame: 1998"
  }
}
```

- Fallido ❌ | Caso 4, ocurreo cuando no se ingresan los parametros obligatorios.
- Codigo 400 Error

```json
{
  "success": false,
  "data": {
    "message": "Missing data: <groupNumber>, <idGame> must be included."
  }
}
```

- Fallido ❌ | Caso 5, ocurreo cuando el usuario no esta autenticado.
- Codigo 401 Error

```json
{
  "success": false,
  "data": {
    "error": "Error de credenciales"
  }
}
```
