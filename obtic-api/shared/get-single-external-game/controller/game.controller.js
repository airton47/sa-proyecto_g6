const router = require("express").Router();
const response = require("../utils/response");
const axios = require("axios");
const verify = require("../utils/verify");
const jwt = require("jsonwebtoken");
/**
 * Realiza la peticion http a traves de axios
 * @param {*} link
 * @param {*} idGame
 * @returns
 */
function doRequest(link, idGame, token, ip) {
  let body = { games: [idGame] };
  return axios.post(link, body, {
    headers: {
      Authorization: `Bearer ${token}`,
      IP: ip,
    },
  });
}

const getGames = async (req, res) => {
  // Extrayendo y verificando parametros de body
  const { groupNumber, idGame } = req.query;
  const ip = req?.headers?.ip;

  if (!groupNumber || !idGame) {
    return res.status(400).json(
      response(false, {
        message: "Missing data: <groupNumber>, <idGame> must be included.",
      })
    );
  }

  const token = jwt.sign(
    {
      group: 6,
      userId: 1,
      email: "fck@gmail.com",
      name: "...",
      region: "Guatemala",
      ip: ip,
    },
    process.env.COMMON_KEY,
    {
      expiresIn: "2h",
    }
  );

  // Links de los grupos
  const groups = [
    {
      id: process.env.ID_G1,
      link: process.env.URL_G1,
    },
    {
      id: process.env.ID_G2,
      link: process.env.URL_G2,
    },
    {
      id: process.env.ID_G3,
      link: process.env.URL_G3,
    },
  ];

  try {
    // Verificando de groupNumber enviado en body conincide con los de las variables de entorno.
    let targetGroup = groups.find((group) => group.id == groupNumber);
    if ((targetGroup === undefined) | (targetGroup === null)) {
      return res.status(404).json(
        response(false, {
          message: "Target group information was not found.",
        })
      );
    }

    // Haciendo consulta a grupo objetivo
    let result = await doRequest(targetGroup.link, Number(idGame), token, ip);

    // Enviando respueseta
    return result.data?.data[0] !== undefined
      ? res.status(200).json(response(true, result.data.data[0]))
      : res.status(404).json(
          response(false, {
            message: `The game could not be found with idGame: ${idGame}`,
          })
        );
  } catch (error) {
    console.log(error);
    return res.status(500).json(
      response(false, {
        message: "[Error in Request] | " + error.toString(),
      })
    );
  }
};

router.get("/getSingleGame", getGames);

module.exports = router;
