const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const {
  balanceInsert,
  verifyExternalToken,
} = require("../utils/purchase.utils");

const purchase = async (req, res) => {
  const { game, message } = req.body;
  const verifyResponse = verifyExternalToken(req.headers.authorization);
  if (!verifyResponse.success) {
    return res.status(401).json(response(false, verifyResponse.message));
  }
  let payload = verifyResponse.payload;
  if (message.length > 200) {
    return res.status(400).json({
      success: false,
      message: "La razón de devolución no puede ser mayor a 200 caracteres",
    });
  }
  try {
    let queryBalance = `
        select 
          *
        from 
          balance b
        where 				
          id_user = ${payload.userId} 
          and id_game = ${game} 
          and external_platform = ${payload.group}
        order by 
          id_balance 
        desc
        limit 1;
    `;
    let queryLog = `
        select 
        	*
        from 
        	purchase_log pl
        where 				
          id_user = ${payload.userId} 
          and id_game = ${game} 
          and external_platform = ${payload.group}
        order by 
        	id_purchase_log  
        desc
        limit 1
    `;
    let balanceRegister = (await db.query(queryBalance)).rows;
    let logRegister = (await db.query(queryLog)).rows;
    if (balanceRegister.length == 0 || logRegister.length == 0) {
      return res.status(400).json({
        success: false,
        message: "No existe un registro de compra del juego",
      });
    }
    balanceRegister = balanceRegister[0];
    logRegister = logRegister[0];
    if (!logRegister.holding) {
      return res.status(400).json({
        success: false,
        message:
          "El juego ya fue devuelto, por lo cual no se puede volver a procesar otra devolución",
      });
    }
    await db.query(
      balanceInsert(payload.userId, game, balanceRegister.amount, payload.group)
    );
    await db.query(
      `
        update 
        	purchase_log 
        set
        	holding = false,
          devolution_reason = ${message != null ? `'${message}'` : "null"},
          devolution_date = NOW()
        where 	
          id_purchase_log = ${logRegister.id_purchase_log}
      `
    );
    return res
      .status(200)
      .json(response(true, "La devolución se realizo de manera correcta"));
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      success: false,
      message: "Ocurrió un error inesperado en el proceso de devolución",
    });
  }
};

router.post("/devolution", purchase);

module.exports = router;
