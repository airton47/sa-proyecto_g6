# DOCKER

```
docker build . -t fernandvx/obtic-shared-devolution-microservice
```

```
docker push fernandvx/obtic-shared-devolution-microservice
```

```
docker run --name shared-devolution-microservice -p 3000:80 fernandvx/obtic-shared-devolution-microservice
```
