const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const {
  balanceInsert,
  purchaseLogInsert,
  verifyExternalToken,
  getRegionUser,
  getPrice,
  getPriceByProduct,
  getPriceByProductAndRegion,
} = require("../utils/purchase.utils");

const purchase = async (req, res) => {
  const { games } = req.body;
  const verifyResponse = verifyExternalToken(req.headers.authorization);
  if (!verifyResponse.success) {
    return res.status(401).json(response(false, verifyResponse.message));
  }
  let payload = verifyResponse.payload;
  try {
    const regionUser = await getRegionUser(payload.ip);
    let listGames = "(";
    for (let game of games) {
      listGames = `${listGames}${game},`;
    }
    listGames = listGames.substring(0, listGames.length - 1) + ")";
    const query = `
        select 
        	*
        from 
        	product p
        where 	
        	p.id_product in ${listGames}
    `;
    const priceGeneral = await getPriceByProduct(query);
    const priceByRegion = await getPriceByProductAndRegion(query, regionUser);
    for (let game of games) {
      let temporalPrice = getPrice(game, priceByRegion);
      if (temporalPrice.price == -1) {
        temporalPrice = getPrice(game, priceGeneral);
      }
      await db.query(
        balanceInsert(
          payload.userId,
          game,
          Number(temporalPrice.price - temporalPrice.discount) * 0.85,
          payload.group
        )
      );
      await db.query(purchaseLogInsert(payload.userId, game, payload.group));
    }

    return res
      .status(200)
      .json(response(true, "La compra se realizo de manera correcta"));
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json(response(false, [{ message: "Ocurrio un error inesperado" }])); //ERROR DEL SERVIDOR
  }
};

router.post("/shopping", purchase);

module.exports = router;
