# DOCKER

```
docker build . -t fernandvx/obtic-shared-shopping-microservice
```

```
docker push fernandvx/obtic-shared-shopping-microservice
```

```
docker run --name shared-shopping-microservice -p 3000:80 fernandvx/obtic-shared-shopping-microservice
```
