const db = require("../db/database");
const axios = require("axios");
const jwt = require("jsonwebtoken");

const balanceInsert = (user, game, amount, externalGroup) => {
  return `
        INSERT INTO 
            balance
            (
                id_user, 
                id_game,
                id_movement_type,
                is_credit,
                amount,
                external_platform
            )
        VALUES
            (
                ${user},
                ${game},
                5,
                true,
                ${amount},
                ${externalGroup}
            );
    `;
};

const purchaseLogInsert = (user, game, externalGroup) => {
  return `
        INSERT INTO 
            purchase_log
            (
                id_user,
                id_game,
                external_platform
            )
        VALUES
            (
                ${user},
                ${game},
                ${externalGroup}
            )
    `;
};

const generateApiDictionary = () => {
  let dict = {};
  dict[process.env.ID_G1] = process.env.URL_G1;
  dict[process.env.ID_G2] = process.env.URL_G2;
  dict[process.env.ID_G3] = process.env.URL_G3;
  return dict;
};

const verifyExternalToken = (token) => {
  try {
    if (!token) {
      return {
        success: false,
        message: "No se proveyo el token para completaar la interconectividad",
      };
    }
    const decoded = jwt.verify(token.split(" ")[1], process.env.COMMON_KEY);
    return {
      success: true,
      payload: decoded,
    };
  } catch (error) {
    console.log(error);
    return {
      success: false,
      message: "Token invalido",
    };
  }
};

const getRegionUser = async (ip) => {
  const regionInfo = await axios.get(`http://ip-api.com/json/${ip}`);
  let regionName = regionInfo.data.regionName;
  return regionName.replace("Departamento de ", "");
};

const getPrice = (id_product, prices) => {
  for (let price of prices) {
    if (price.id_product === id_product) {
      return price;
    }
  }
  return { price: -1, discount: 0, end_date: "2022-12-31" };
};

const getPriceByProduct = async (query) => {
  const today = new Date(Date.now()).toISOString().split("T")[0];
  query = query.replace("p.*", "p.id_product");
  const queryPrice = `
                  select 
                  	pr.*
                  from
                    (${query}) p
                  inner join
                  	price pr 
                  on
                    p.id_product = pr.id_product
                  where 
                  	pr.active = true and pr.by_region = false
                      and
                          '${today}' >= pr.start_date and '${today}' <= pr.end_date`;
  return (await db.query(queryPrice)).rows;
};

const getPriceByProductAndRegion = async (query, region) => {
  const today = new Date(Date.now()).toISOString().split("T")[0];
  query = query.replace("p.*", "p.id_product");
  const queryPrice = `
                  select 
                  	pri.*, pr.id_region 
                  from
                    (${query}) p
                  inner join
                  	price pri 
                  on
                    pri.id_product = p.id_product
                  inner join
                      price_region pr 
                  on
                      pri.id_price = pr.id_price
                  inner join 	
                      (
                      	select 
                          	id_region
                          from
                          	region r 
                          where 
                          	r.region ilike '%${region}%'
                      ) r
                  on 
                      r.id_region = pr.id_region
                  where 
                    pri.active = true and pri.by_region = true
                      and
                          '${today}' >= pri.start_date and '${today}' <= pri.end_date`;
  return (await db.query(queryPrice)).rows;
};

module.exports = {
  balanceInsert,
  purchaseLogInsert,
  generateApiDictionary,
  verifyExternalToken,
  getRegionUser,
  getPrice,
  getPriceByProduct,
  getPriceByProductAndRegion,
};
