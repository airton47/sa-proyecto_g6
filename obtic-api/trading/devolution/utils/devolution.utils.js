const db = require("../db/database");
const axios = require("axios");

const balanceInsert = (user, group, game, type, credit, amount) => {
  return `
        INSERT INTO 
            balance
            (
                id_user, 
                id_group,
                id_game,
                id_movement_type,
                is_credit,
                amount
            )
        VALUES
            (
                ${user},
                ${group ? group : "null"},
                ${game},
                ${type},
                ${credit ? "true" : "false"},
                ${amount}
            );
    `;
};

const purchaseLogInsert = (user, group, game) => {
  return `
        INSERT INTO 
            purchase_log
            (
                id_user,
                id_group,
                id_game
            )
        VALUES
            (
                ${user},
                ${group},
                ${game}
            )
    `;
};

const generateApiDictionary = () => {
  let dict = {};
  dict[process.env.ID_G1] = process.env.URL_G1;
  dict[process.env.ID_G2] = process.env.URL_G2;
  dict[process.env.ID_G3] = process.env.URL_G3;
  return dict;
};

const getBalanceRegistry = async (userId, gameId, group, type) => {
  const registry = (
    await db.query(
      `   
        select 
          *
        from 
          balance b
        where 				
          id_user = ${userId}
          and id_game = ${gameId}
          ${group ? `and id_group  = ${group}` : ""}
          and id_movement_type = ${type}
        order by 
          id_balance 
        desc
        limit 1
    `
    )
  ).rows;
  return registry[0];
};

const getPurchaseLogRegistry = async (userId, gameId, group) => {
  const registry = (
    await db.query(
      `
      select 
      	*
      from 
      	purchase_log pl
      where 				
      	id_user = ${userId}
      	and id_game = ${gameId}
      	${group ? `and id_group  = ${group}` : "and id_group is null"}
      order by 
      	id_purchase_log  
      desc
      limit 1
    `
    )
  ).rows;
  return registry[0];
};

const deleteLibrary = async (gameId, userId, group) => {
  let query = `
    DELETE FROM
      ${group ? "external_library" : "library"}
    WHERE
      id_product = ${gameId}
      AND id_user = ${userId}
      ${group ? `and "group" = ${group}` : ""}
  `;
  try {
    await db.query(query);
    return true;
  } catch (e) {
    console.log("Ocurrio un error al eliminar el registro de la libreria");
    return false;
  }
};

const devolutionToGroup = async (gameId, group, token, message) => {
  try {
    const apiDictionaryByGroup = generateApiDictionary();
    const responseGroup = await axios.post(
      apiDictionaryByGroup[group],
      {
        game: gameId,
        message: message,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (!responseGroup.data.success) {
      return responseGroup.data;
    }
  } catch (e) {
    return {
      success: false,
      message: `Error al solicitar la devolución al grupo ${group}`,
    };
  }
  return {
    success: true,
  };
};

const verifyPossession = async (userId, productId) => {
  const library = (
    await db.query(
      `
      SELECT 
        *
      FROM
        library
      WHERE
        id_user = ${userId}
      AND
        id_product = ${productId}
    `
    )
  ).rows;
  return library.length > 0;
};

const verifyExternalPossession = async (userId, group, gameId) => {
  const externalLibrary = (
    await db.query(
      `
      SELECT 
        *
      FROM
        external_library
      WHERE
        id_user = ${userId}
      AND
        id_product = ${gameId}
      AND
        "group" = ${group}
    `
    )
  ).rows;
  return externalLibrary.length > 0;
};

const refoundWallet = async (userId, amount) => {
  await db.query(`
    UPDATE 
      "user"
    SET
      wallet = wallet + ${amount}
    WHERE
      id_user = ${userId}
  `);

  await db.query(`
    INSERT INTO
      log_wallet
      (
        amount,
        is_credit,
        id_log_wallet_reason,
        id_user
      )
    VALUES
      (
        ${amount},
        true,
        2,
        ${userId}
      )
  `);
};

module.exports = {
  verifyPossession,
  verifyExternalPossession,
  devolutionToGroup,
  getBalanceRegistry,
  balanceInsert,
  deleteLibrary,
  getPurchaseLogRegistry,
  refoundWallet,
};
