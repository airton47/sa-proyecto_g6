# DOCKER

```
docker build . -t fernandvx/obtic-trading-devolution-microservice
```

```
docker push fernandvx/obtic-trading-devolution-microservice
```

```
docker run --name trading-devolution-microservice -p 3000:80 fernandvx/obtic-trading-devolution-microservice
```
