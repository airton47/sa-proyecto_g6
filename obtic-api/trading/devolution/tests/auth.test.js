require("dotenv").config();
const supertest = require("supertest");
const app = require("..");
//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("create-offer controller", () => {
  it("Should return 400 status code, No se enviaron todos los datos necesarios para la creación", async () => {
    const response = await supertest(app).post("/prices/createOffer").send({});
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 401 status code, authorization", async () => {
    const response = await supertest(app).post("/prices/createOffer").send({
      idProduct: "3",
      price: "500",
      discount: "1",
      byRegion: true,
      regions: true,
    });
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, Para un precio por región debe enviar las regiones en las que estara disponible", async () => {
    const response = await supertest(app)
      .post("/prices/createOffer")
      .send({
        idProduct: "3",
        price: "500",
        discount: "1",
        byRegion: true,
        regions: null,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 500 status code, Catch", async () => {
    const response = await supertest(app)
      .post("/prices/createOffer")
      .send({
        idProduct: "3",
        price: "500",
        discount: "1",
        byRegion: false,
        regions: null,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(500);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status code, Se actualizo la oferta de manera correcta", async () => {
    const response = await supertest(app)
      .post("/prices/createOffer")
      .send({
        idProduct: "31",
        price: "500",
        discount: "0",
        byRegion: false,
        regions: null,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it("Should return 200 status code, Se actualizo la oferta de manera correcta varias regiones", async () => {
    const response = await supertest(app)
      .post("/prices/createOffer")
      .send({
        idProduct: "31",
        price: "500",
        discount: "1",
        byRegion: true,
        regions: [1, 2, 3],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it("Should return 200 status code, sin descuento", async () => {
    const response = await supertest(app)
      .post("/prices/createOffer")
      .send({
        idProduct: "31",
        price: "500",
        discount: 0,
        byRegion: true,
        regions: [1, 2, 3],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
