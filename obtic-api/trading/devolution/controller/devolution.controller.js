const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const {
  verifyPossession,
  verifyExternalPossession,
  devolutionToGroup,
  deleteLibrary,
  balanceInsert,
  getBalanceRegistry,
  getPurchaseLogRegistry,
  refoundWallet,
} = require("../utils/devolution.utils");

const createPrice = async (req, res) => {
  const { productId, group, message, toWallet } = req.body;
  if (!productId) {
    return res.status(400).json(
      response(false, {
        error:
          "Es necesario conocer el id del juego para procesar la devolución",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, false);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    let balancePurchase;
    if (group) {
      if (
        !(await verifyExternalPossession(
          verifyResponse.payload.id_user,
          group,
          productId
        ))
      ) {
        return res.status(400).json(
          response(false, {
            error: "El juego no existe en la biblioteca externa del usuario",
          })
        );
      }
      let responseGroup = await devolutionToGroup(
        productId,
        group,
        verifyResponse.payload.interconectividad,
        message
      );
      if (!responseGroup.success) {
        return res.status(400).json(
          response(false, {
            error: responseGroup.message,
          })
        );
      }
      balancePurchase = await getBalanceRegistry(
        verifyResponse.payload.id_user,
        productId,
        group,
        3
      );
      await db.query(
        await balanceInsert(
          verifyResponse.payload.id_user,
          group,
          productId,
          7,
          true,
          balancePurchase.amount * 0.85
        )
      ); // Insertar la devolución desde el grupo de origen
    } else {
      if (!verifyPossession(verifyResponse.payload.id_user, productId)) {
        return res.status(400).json(
          response(false, {
            error: "El juego no existe en la biblioteca del usuario",
          })
        );
      }
      balancePurchase = await getBalanceRegistry(
        verifyResponse.payload.id_user,
        productId,
        false,
        1
      );
    }
    const deleteResult = await deleteLibrary(
      productId,
      verifyResponse.payload.id_user,
      group ? group : false
    );
    if (!deleteResult) {
      return res.status(400).json(
        response(false, {
          error: "No fue posible eliminar el producto de la biblioteca",
        })
      );
    }
    const logRegistry = await getPurchaseLogRegistry(
      verifyResponse.payload.id_user,
      productId,
      group ? group : false
    );
    await db.query(
      `
        UPDATE 
          purchase_log
        SET
          holding = false,
          devolution_reason = '${message}',
          devolution_date = NOW()
        WHERE
          id_purchase_log = ${logRegistry.id_purchase_log}
      `
    );
    await db.query(
      await balanceInsert(
        verifyResponse.payload.id_user,
        group ? group : false,
        productId,
        group ? 4 : 2,
        false,
        balancePurchase.amount
      )
    );
    if (toWallet) {
      await refoundWallet(
        verifyResponse.payload.id_user,
        balancePurchase.amount
      );
    }

    return res
      .status(200)
      .json(response(true, "La devolución fue realizada de manera exitosa"));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.post("/devolution", createPrice);

module.exports = router;
