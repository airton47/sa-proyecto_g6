require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const PriceController = require("./controller/purchase.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/trading", PriceController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3020;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
