const db = require("../db/database");
const bcrypt = require("bcrypt");

const purchaseProduct = async (payload, cartDetail) => {
  await db.query(
    balanceInsert(
      payload.id_user,
      null,
      cartDetail.id_product,
      1,
      true,
      cartDetail.final_price
    )
  );
  await db.query(
    purchaseLogInsert(payload.id_user, null, cartDetail.id_product)
  );
  await db.query(
    `
    INSERT INTO
        library
        (
            id_user,
            id_product
        )
    VALUES
        (
            ${payload.id_user},
            ${cartDetail.id_product}
        )
    `
  );
};

const externalPurchaseProduct = async (payload, cartDetail) => {
  await db.query(
    balanceInsert(
      payload.id_user,
      cartDetail.group,
      cartDetail.id_product,
      3,
      true,
      cartDetail.final_purchase_price
    )
  );
  await db.query(
    balanceInsert(
      payload.id_user,
      cartDetail.group,
      cartDetail.id_product,
      8,
      false,
      cartDetail.final_purchase_price * 0.85
    )
  );
  await db.query(
    purchaseLogInsert(payload.id_user, cartDetail.group, cartDetail.id_product)
  );
  await db.query(
    `
            INSERT INTO
                external_library
                (
                    id_user,
                    "group",
                    id_product,
                    game_name,
                    game_image
                )
            VALUES
                (
                    ${payload.id_user},
                    ${cartDetail.group},
                    ${cartDetail.id_product},
                    '${cartDetail.game_name}',
                    '${cartDetail.game_image}'
                );
        `
  );
};

const balanceInsert = (user, group, game, type, credit, amount) => {
  return `
        INSERT INTO 
            balance
            (
                id_user, 
                id_group,
                id_game,
                id_movement_type,
                is_credit,
                amount
            )
        VALUES
            (
                ${user},
                ${group},
                ${game},
                ${type},
                ${credit ? "true" : "false"},
                ${amount}
            );
    `;
};

const purchaseLogInsert = (user, group, game) => {
  return `
        INSERT INTO 
            purchase_log
            (
                id_user,
                id_group,
                id_game
            )
        VALUES
            (
                ${user},
                ${group},
                ${game}
            )
    `;
};

const generateApiDictionary = () => {
  let dict = {};
  dict[process.env.ID_G1] = process.env.URL_G1;
  dict[process.env.ID_G2] = process.env.URL_G2;
  dict[process.env.ID_G3] = process.env.URL_G3;
  return dict;
};

const verifyCardOrWallet = async (cardId, cvv, userId, total) => {
  if (!cardId) {
    const walletUser = (
      await db.query(`SELECT wallet FROM "user" WHERE id_user = ${userId}`)
    ).rows[0];
    if ((walletUser?.wallet || 0) * 1 < total) {
      return {
        success: false,
        message: "El cliente no cuenta con suficiente dinero en su monedero",
      };
    }
  } else {
    let card = (
      await db.query(
        `SELECT * FROM credit_card WHERE id_credit_card = ${cardId}`
      )
    ).rows;
    if (card.length == 0) {
      return {
        success: false,
        message: "No existe una tarjeta con el id proporcionado",
      };
    }
    card = card[0];
    if (card.id_user != userId) {
      return {
        success: false,
        message:
          "La tarjeta proporcionada no pertenece al usuario que desea realizar la compra",
      };
    }
    if (!bcrypt.compareSync(cvv, card.cvv)) {
      return {
        success: false,
        message:
          "El codigo cvv proporcionado no coincide con el de la base de datos",
      };
    }
  }
  return {
    success: true,
  };
};

const discountWallet = async (userId, amount) => {
  await db.query(`
    UPDATE 
      "user"
    SET
      wallet = wallet - ${amount}
    WHERE
      id_user = ${userId}
  `);

  await db.query(`
    INSERT INTO
      log_wallet
      (
        amount,
        is_credit,
        id_log_wallet_reason,
        id_user
      )
    VALUES
      (
        ${amount},
        false,
        1,
        ${userId}
      )
  `);
};

module.exports = {
  purchaseProduct,
  externalPurchaseProduct,
  generateApiDictionary,
  verifyCardOrWallet,
  discountWallet,
};
