const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const axios = require("axios");

const {
  purchaseProduct,
  externalPurchaseProduct,
  generateApiDictionary,
  verifyCardOrWallet,
  discountWallet,
} = require("../utils/purchase.utils");

const purchase = async (req, res) => {
  const { cardId, cvv } = req.body;

  const verifyResponse = await verify(req.headers.authorization, false);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    let cart = (
      await db.query(
        `SELECT * FROM shopping_cart WHERE id_user=${verifyResponse.payload.id_user} and active=true`
      )
    ).rows;

    if (cart.length == 0) {
      return res
        .status(400)
        .json(response(false, "No existe un carrito activo para el usuario"));
    }

    cart = cart[0];

    let details = (
      await db.query(
        `SELECT * FROM detail_shopping_cart WHERE id_shopping_cart = ${cart.id_shopping_cart}`
      )
    ).rows;

    let externalDetails = (
      await db.query(
        `SELECT * FROM detail_external_shopping_cart WHERE id_shopping_cart = ${cart.id_shopping_cart}`
      )
    ).rows;
    let verifyCredit = await verifyCardOrWallet(
      cardId,
      cvv,
      verifyResponse.payload.id_user,
      cart.total
    );
    if (!verifyCredit.success) {
      return res.status(400).json(response(false, verifyCredit.message));
    }
    if (details.length == 0 && externalDetails.length == 0) {
      return res
        .status(400)
        .json(response(false, "El carrito se encuentra vacio"));
    }

    for (let detail of details) {
      await purchaseProduct(verifyResponse.payload, detail);
    }
    const apiDictionaryByGroup = generateApiDictionary();
    let externalErrors = [];
    for (let externalDetail of externalDetails) {
      try {
        let responseGroup = await axios.post(
          apiDictionaryByGroup[externalDetail.group],
          {
            games: [externalDetail.id_product],
          },
          {
            headers: {
              Authorization: `Bearer ${verifyResponse.payload.interconectividad}`,
            },
          }
        );
        if (!responseGroup.data.success) {
          externalErrors.push(`Error al intentar pagar el juego ${externalDetail.id_product} al grupo ${externalDetail.group}`);
        } else {
          await externalPurchaseProduct(verifyResponse.payload, externalDetail);
        }
      } catch (e) {
        console.log(e);
        externalErrors.push(`Error al intentar pagar el juego ${externalDetail.id_product} al grupo ${externalDetail.group}`);
      }
    }

    await db.query(
      `UPDATE shopping_cart SET active=false WHERE id_shopping_cart = ${cart.id_shopping_cart}`
    );

    if (cardId == null) {
      discountWallet(verifyResponse.payload.id_user, cart.total);
    }

    if (externalErrors.length > 0) {
      return res
        .status(400)
        .json(response(false, "P-EGR: " + externalErrors.join(', ')));
    }
    return res
      .status(200)
      .json(response(true, "La compra se realizo de manera correcta"));
  } catch (error) {
    console.log(error);
    return res.status(500).json(response(false, "Ocurrio un error inesperado")); //ERROR DEL SERVIDOR
  }
};

router.post("/purchase", purchase);

module.exports = router;
