const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const getCart = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res
      .status(401)
      .json(response(false, { error: "Error de autenticación" }));
  const userId = payload.id_user;

  try {
    const cart = await db.query(
      `SELECT * FROM shopping_cart WHERE id_user = ${userId} AND active = true`
    );
    if (cart.rowCount === 0)
      return res
        .status(404)
        .json(response(false, { error: "No existe carrito activo" }));
    const idShoppingCart = cart.rows[0].id_shopping_cart;
    const detailCart = await db.query(
      `SELECT dsc.id_product, dsc.added_date, dsc.final_price, p.product_name, p.mobile_image, p.is_game FROM detail_shopping_cart dsc INNER JOIN product p ON dsc.id_product = p.id_product WHERE dsc.id_shopping_cart = ${idShoppingCart}`
    );
    const detailCartExternal = await db.query(
      `select dsc.id_product, dsc."group", dsc.added_date, dsc.final_purchase_price as final_price, dsc.game_name as product_name, dsc.game_image as mobile_image, true as is_game FROM detail_external_shopping_cart dsc WHERE dsc.id_shopping_cart = ${idShoppingCart}`
    );

    return res.status(200).json(
      response(true, {
        cart: cart.rows[0],
        normalProducts: detailCart.rows,
        externalProducts: detailCartExternal.rows,
      })
    );
  } catch (error) {
    return res
      .status(500)
      .json(response(false, { error: "Error interno de servidor" }));
  }
};

router.get("/getCart", getCart);

module.exports = router;
