const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const GetList = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res
      .status(401)
      .json(response(false, { error: "Error de autenticación" }));
  const userId = payload.id_user;
  const exists = await db.query(`SELECT * FROM "user" WHERE id_user=${userId}`);
  if (exists.rowCount < 1)
    return res
      .status(404)
      .json(response(false, { error: "Usuario no existente" })); // Usuario no existente
  try {
    const data = await db.query(
      `select * from "user" where active = true and confirmed = true and id_user_type = 2 and (suspension_date is null or now() > suspension_date) and id_user != ${userId}`
    );
    data.rows.forEach((user) => {
      delete user.id_user_type;
      delete user.password;
      delete user.active;
      delete user.confirmed;
      delete user.suspension_date;
    });
    return res.status(200).json(response(true, data.rows));
  } catch (error) {
    return res
      .status(500)
      .json(response(false, { error: "Error interno en el servidor" })); //Error desconocido (bae de datos no conectada)
  }
};

router.get("/getUsers", GetList);

module.exports = router;
