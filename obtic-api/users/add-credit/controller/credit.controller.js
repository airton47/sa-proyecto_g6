const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const bcrypt = require("bcrypt");

const addCredit = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) return res.status(401).json(response(false, { error: 'Error de autenticación' }));
  const userId = payload.id_user;
  
  const { idCard, CVV, credit } = req.body;
  if (!idCard || !CVV || !credit) return res.status(400).json(response(false, { error: 'Faltan parámetros en la petición' }));
  
  try {
    const card = await db.query(`SELECT cvv FROM credit_card WHERE id_credit_card = ${idCard}`);
    if (card.rowCount < 1) return res.status(404).json(response(false, { error: 'Tarjeta no encontrada' }));
    const cvvEncrypted = card.rows[0].cvv;
    const cvvVerification = bcrypt.compareSync(CVV, cvvEncrypted);
    if (!cvvVerification) return res.status(401).json(response(false, { error: 'CVV incorrecto' }));
    await db.query(`UPDATE "user" SET wallet = wallet + ${credit} WHERE id_user = ${userId}`);
    return res.status(200).json(response(true, { idCard, credit }));
  } catch (error) {
    return res.status(500).json(response(false, { error: 'Error interno de servidor' }));
  }
};

router.post("/addCredit", addCredit);

module.exports = router;
