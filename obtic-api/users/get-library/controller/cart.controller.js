const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const getLibrary = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res
      .status(401)
      .json(response(false, { error: "Error de autenticación" }));
  const userId = payload.id_user;

  try {
    const detailCart = await db.query(
      `SELECT dsc.id_product, dsc.added_date, p.product_name, p.mobile_image, p.is_game FROM library dsc INNER JOIN product p ON dsc.id_product = p.id_product WHERE dsc.id_user = ${userId}`
    );
    const detailCartExternal = await db.query(
      `select dsc.id_product, dsc."group", dsc.added_date, dsc.game_name as product_name, dsc.game_image as mobile_image, true as is_game FROM external_library dsc WHERE dsc.id_user = ${userId}`
    );

    return res.status(200).json(
      response(true, {
        normalProducts: detailCart.rows,
        externalProducts: detailCartExternal.rows,
      })
    );
  } catch (error) {
    return res
      .status(500)
      .json(response(false, { error: "Error interno de servidor" }));
  }
};

router.get("/getLibrary", getLibrary);

module.exports = router;
