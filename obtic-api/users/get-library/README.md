# DOCKER

```
docker build . -t fernandvx/obtic-users-get-library-microservice
```

```
docker push fernandvx/obtic-users-get-library-microservice
```

```
docker run --name users-get-library-microservice -p 3000:80 fernandvx/obtic-users-get-library-microservice
```
