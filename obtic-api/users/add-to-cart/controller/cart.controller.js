const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const gameFunction = require("../utils/game.utils");

const addToCart = async (req, res) => {
  const region = req.headers.region;
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) return res.status(401).json(response(false, { error: 'Error de autenticación' }));
  const userId = payload.id_user;
  
  const { idProduct, isGroup, game } = req.body;

  try {
    let cart = await db.query(`SELECT * FROM shopping_cart WHERE id_user = ${userId} AND active = true`);
    if (cart.rowCount === 0) {
      cart = await db.query(`INSERT INTO shopping_cart (id_user, total) VALUES (${userId}, 0) RETURNING *`);
    }
    const idShoppingCart = cart.rows[0].id_shopping_cart;
    const product = await db.query(`SELECT * FROM product WHERE id_product = ${idProduct}`);
    if (product.rowCount === 0) return res.status(404).json(response(false, { error: 'Producto no encontrado' }));

    let regionPrice = await gameFunction.getPriceByProductAndRegion(idProduct, region);
    if (regionPrice.length === 0) {
      regionPrice = await gameFunction.getPriceByProduct(idProduct);
    }
    const internalPrice = regionPrice[0].price - regionPrice[0].discount;
    if (!isGroup) {
      const detailCart = await db.query(`SELECT * FROM detail_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND id_product = ${idProduct}`);
      if (detailCart.rowCount > 0) return res.status(400).json(response(false, { error: 'El producto ya se encuentra en el carrito' }));
      await db.query(`INSERT INTO detail_shopping_cart (id_shopping_cart, id_product, final_price) VALUES (${idShoppingCart}, ${idProduct}, ${internalPrice})`);
      await db.query(`UPDATE shopping_cart SET total = total + ${internalPrice} WHERE id_shopping_cart = ${idShoppingCart}`);
      return res.status(200).json(response(true, { idShoppingCart, idProduct, finalPrice: internalPrice }));
    } else {
      const detailCart = await db.query(`SELECT * FROM detail_external_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND id_product = ${idProduct} AND "group" = ${game.group}`);
      if (detailCart.rowCount > 0) return res.status(400).json(response(false, { error: 'El producto ya se encuentra en el carrito' }));
      await db.query(`INSERT INTO detail_external_shopping_cart (id_shopping_cart, final_purchase_price, "group", id_product, game_name, game_image) VALUES (${idShoppingCart},  ${game.finalPrice}, ${game.group}, ${idProduct}, '${game.name}', '${game.image}')`);
      await db.query(`UPDATE shopping_cart SET total = total + ${game.finalPrice} WHERE id_shopping_cart = ${idShoppingCart}`);
      return res.status(200).json(response(true, { idShoppingCart, idProduct, isGroup, game }));
    }
  } catch (error) {
    return res.status(500).json(response(false, { error: 'Error interno de servidor' }));
  }
};

router.post("/addToCart", addToCart);

module.exports = router;
