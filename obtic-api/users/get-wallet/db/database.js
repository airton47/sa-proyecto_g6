const {Client} = require('pg');

const client = new Client({
  host: process.env.PG_HOST,
  user: process.env.PG_USER,
  password: process.env.PG_PWD,
  port: process.env.PG_PORT,
  database: process.env.PG_DB,
});
client.connect();

module.exports = client;
