const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const getWallet = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) return res.status(401).json(response(false, { error: 'Error de autenticación' }));
  const userId = payload.id_user;

  try {
    const wallet = await db.query(`SELECT wallet FROM "user" WHERE id_user = ${userId}`);
    if (wallet.rowCount < 1) return res.status(404).json(response(false, { error: 'Usuario no encontrado' }));
    
    return res.status(200).json(response(true, { userId, wallet: wallet.rows[0].wallet }));
  } catch (error) {
    return res.status(500).json(response(false, { error: 'Error interno de servidor' }));
  }
};

router.get("/getWallet", getWallet);

module.exports = router;
