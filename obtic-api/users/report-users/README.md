# DOCKER

```
docker build . -t fernandvx/obtic-users-report-users-microservice
```

```
docker push fernandvx/obtic-users-report-users-microservice
```

```
docker run --name users-report-users-microservice -p 3000:80 fernandvx/obtic-users-report-users-microservice
```

## BODY SAMPLE

- Tipo: `POST`
- Ejemplo de peticion:

`http://localhost:3002/users/reportUser`

- Body de request.

```json
{
  "reportedUser": "airton52",
  "reason": "aiyel.leonalfa@gmail.com"
}
```

- Respuesta: ✅ Exitosa

```json
{
  "success": true,
  "data": {
    "report": {
      "reportedUser": "airton52",
      "reason": "aiyel.leonalfa@gmail.com"
    }
  }
}
```

- Respuesta: `❌ Error de autenticación`, ocurre porque se intenta agregar un administrador y no se ha proveido un token de autenticación o bien porque no se cuenta con los permisos necesarios (de administrador) para llevar a cabo la operación.

- Codigo: 401

```json
{
  "success": false,
  "data": "NO proveyo el token de autenticación"
}
```

- Respuesta: `❌ Parámetros faltantes`, ocurre cuando no se ingresan todos los campos obligatorios.

- Codigo: 400

```json
{
  "success": false,
  "data": "Debe ingresar la informacion necesaria: Bad-Request"
}
```
