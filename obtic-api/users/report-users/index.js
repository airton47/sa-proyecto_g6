require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const WishListController = require("./controller/report.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/users", WishListController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3010;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
