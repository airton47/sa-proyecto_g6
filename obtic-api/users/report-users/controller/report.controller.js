const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const reportUser = async (req, res) => {
  const { reportedUser, reason } = req.body;

  // Verificacion de credenciales
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) {
    return res
      .status(401)
      .json(response(false, { error: "Error de credenciales" }));
  }

  // Verificacion de parametros
  const userId = payload.id_user;
  if (!reportedUser || (!reason && reason !== "")) {
    return res
      .status(400)
      .json(response(false, { error: "Parametros invalidos" })); //faltan parametros
  }

  try {
    // Verificar que usuario reportado existe
    const user = await db.query(
      `SELECT id_user FROM "user" WHERE id_user = ${reportedUser};`
    );

    if (user.rowCount === 0) {
      return res
        .status(400)
        .json(response(false, { error: "El usuario a reportar no existe" })); // Usuario no existe
    }

    let query = `INSERT INTO report (
      reporting_user,
      reported_user,
      reason
      ) values (
        ${userId},
        ${reportedUser},
        '${reason}'
      );
    `;

    // Insertando nuevo registro de reporte
    await db.query(query);

    return res.status(200).json(response(true, { report: req.body }));
  } catch (error) {
    return res.status(500).json(response(false, { error })); //Error desconocido (bae de datos no conectada)
  }
};

router.post("/reportUser", reportUser);

module.exports = router;
