require("dotenv").config();
const supertest = require("supertest");
const app = require("..");

//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("get-list controller", () => {
  it("Should return 401 status code, authorization", async () => {
    const response = await supertest(app).post("/wishList/addItem").query({});
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, faltan parametros", async () => {
    const response = await supertest(app)
      .post("/wishList/addItem")
      .query({})
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status, ya existe el favorito", async () => {
    const response = await supertest(app)
      .post("/wishList/addItem")
      .send({ productId: 2 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status, No existe el producto", async () => {
    const response = await supertest(app)
      .post("/wishList/addItem")
      .send({ productId: 51 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status, No existe el producto", async () => {
    const response = await supertest(app)
      .post("/wishList/addItem")
      .send({ productId: 31 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
