const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const removeFromCart = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success) return res.status(401).json(response(false, { error: 'Error de autenticación' }));
  const userId = payload.id_user;
  const { idProduct, group } = req.query;
  
  try {
    const cart = await db.query(`SELECT * FROM shopping_cart WHERE id_user = ${userId} AND active = true`);
    if (cart.rowCount === 0) return res.status(404).json(response(false, { error: 'No existe carrito activo' }));
    const idShoppingCart = cart.rows[0].id_shopping_cart;
    const product = await db.query(`SELECT * FROM product WHERE id_product = ${idProduct}`);
    if (product.rowCount === 0) return res.status(404).json(response(false, { error: 'Producto no encontrado' }));

    if (!group) {
      const detailCart = await db.query(`SELECT * FROM detail_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND id_product = ${idProduct}`);
      if (detailCart.rowCount === 0) return res.status(404).json(response(false, { error: 'El producto no se encuentra en el carrito' }));
      await db.query(`UPDATE shopping_cart SET total = total - ${detailCart.rows[0].final_price} WHERE id_shopping_cart = ${idShoppingCart}`);
      await db.query(`DELETE FROM detail_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND id_product = ${idProduct}`);
      return res.status(200).json(response(true, { idShoppingCart, idProduct }));
    } else {
      const detailCart = await db.query(`SELECT * FROM detail_external_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND id_product = ${idProduct} AND "group" = ${group}`);
      if (detailCart.rowCount === 0) return res.status(404).json(response(false, { error: 'El producto no se encuentra en el carrito' }));
      await db.query(`UPDATE shopping_cart SET total = total - ${detailCart.rows[0].final_purchase_price} WHERE id_shopping_cart = ${idShoppingCart}`);
      await db.query(`DELETE FROM detail_external_shopping_cart WHERE id_shopping_cart = ${idShoppingCart} AND "group" = ${group} AND id_product = ${idProduct}`);
      return res.status(200).json(response(true, { idShoppingCart, idProduct, group }));
    }
  } catch (error) {
    return res.status(500).json(response(false, { error: 'Error interno de servidor' }));
  }
};

router.delete("/removeFromCart", removeFromCart);

module.exports = router;
