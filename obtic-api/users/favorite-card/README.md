# DOCKER

```
docker build . -t fernandvx/obtic-users-favorite-card-microservice
```

```
docker push fernandvx/obtic-users-favorite-card-microservice
```

```
docker run --name users-favorite-card-microservice -p 3000:80 fernandvx/obtic-users-favorite-card-microservice
```
