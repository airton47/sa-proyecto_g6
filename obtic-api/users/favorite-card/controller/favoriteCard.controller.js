const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const favoriteCard = async (req, res) => {
  const { creditCardId } = req.body;
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res.status(401).json(response(false, { error: "EWC-005" }));
  const userId = payload.id_user;
  if (!creditCardId)
    return res.status(400).json(response(false, { error: "EWC-003" })); //faltan parametros

  try {
    const user = await db.query(
      `SELECT * FROM "user" WHERE id_user = ${userId}`
    );
    if (user.rowCount < 1)
      // Usuario no existe
      return res.status(400).json(response(false, { error: "EWC-001" }));
    else {
      await db.query(`UPDATE credit_card
      SET is_favorite = case when id_credit_card=${creditCardId} then true else false end
      WHERE id_user=${userId}`);
    }
    return res.status(200).json(response(true, { userId, creditCardId }));
  } catch (error) {
    return res.status(500).json(response(false, { error: "EWC-004" })); //Error desconocido (bae de datos no conectada)
  }
};

router.post("/favoriteCard", favoriteCard);

module.exports = router;
