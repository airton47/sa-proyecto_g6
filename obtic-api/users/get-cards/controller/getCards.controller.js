const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const CreateCard = async (req, res) => {
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res.status(401).json(response(false, { error: "EWC-005" }));
  const userId = payload.id_user;

  try {
    const user = await db.query(
      `SELECT * FROM "user" WHERE id_user = ${userId}`
    );
    if (user.rowCount < 1)
      return res.status(400).json(response(false, { error: "EWC-001" })); // Usuario no existe

    const { rows } = await db.query(
      `select  * from credit_card WHERE id_user=${userId}`
    );
    return res.status(200).send(response(true, { cards: rows }));
  } catch (error) {
    return res.status(500).json(response(false, { error: "EWC-004" })); //Error desconocido (bae de datos no conectada)
  }
};

router.get("/getCards", CreateCard);

module.exports = router;
