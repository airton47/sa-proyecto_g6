const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const bcrypt = require("bcrypt");

const CreateCard = async (req, res) => {
  const { alias, cvv, cardNumber, expirationDate } = req.body;
  const { success, payload } = await verify(req.headers.authorization, false);
  if (!success)
    return res.status(401).json(response(false, { error: "EWC-005" }));
  const userId = payload.id_user;

  if (!alias & !cvv & !cardNumber & !expirationDate)
    return res.status(400).json(response(false, { error: "EWC-003" })); //faltan parametros

  const salt = bcrypt.genSaltSync(10);
  const hashCVV = bcrypt.hashSync(cvv, salt);

  var hashCard = bcrypt.hashSync(
    cardNumber.toString().substring(0, cardNumber.toString().length - 4),
    salt
  );
  const last_digits = cardNumber
    .toString()
    .substring(cardNumber.toString().length - 4);
  try {
    const user = await db.query(
      `SELECT * FROM "user" WHERE id_user = ${userId}`
    );
    if (user.rowCount < 1)
      // Usuario no existe
      return res.status(400).json(response(false, { error: "EWC-001" }));
    else {
      await db.query(`INSERT INTO credit_card
    (alias, card_number, cvv, is_favorite, id_user, expiration_date, last_digits)
    VALUES('${alias}', '${hashCard}', '${hashCVV}', false, '${userId}', '${expirationDate}', '${last_digits}')`);
    }
    return res.status(200).json(response(true, { userId, alias }));
  } catch (error) {
    return res.status(500).json(response(false, { error: "EWC-004" })); //Error desconocido (bae de datos no conectada)
  }
};

router.post("/createCard", CreateCard);

module.exports = router;
