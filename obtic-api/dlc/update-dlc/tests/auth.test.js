require("dotenv").config();
const supertest = require("supertest");
const app = require("..");
//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("update-dlc controller", () => {
  it("Should return 401 status code, authorization", async () => {
    const response = await supertest(app).put("/dlc").send({
      dlcId: 9,
      name: "DLC Cambio",
      description:
        "Prueba del primer DLC para el juego Call of Duty: Warzone Cambiado",
      shortDescription: "Primer DLC de COD Warzone Cambiado",
      releaseDate: "2022-09-28",
      desktopImage:
        "https://www.xtrafondos.com/wallpapers/call-of-duty-warzone-2021-7336.jpg",
      mobileImage:
        "https://www.thetechgame.com/images/news/article/266600f004.jpg",
      logo: "https://seeklogo.com/images/C/call-of-duty-warzone-logo-D37AA435E3-seeklogo.com.png",
      ageRestriction: "M",
      parentGameId: 4,
    });
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 404 status code, no existe en base de datos", async () => {
    const response = await supertest(app)
      .put("/dlc")
      .send({
        dlcId: 9,
        name: "DLC Cambio",
        description:
          "Prueba del primer DLC para el juego Call of Duty: Warzone Cambiado",
        shortDescription: "Primer DLC de COD Warzone Cambiado",
        releaseDate: "2022-09-28",
        desktopImage:
          "https://www.xtrafondos.com/wallpapers/call-of-duty-warzone-2021-7336.jpg",
        mobileImage:
          "https://www.thetechgame.com/images/news/article/266600f004.jpg",
        logo: "https://seeklogo.com/images/C/call-of-duty-warzone-logo-D37AA435E3-seeklogo.com.png",
        ageRestriction: "M",
        parentGameId: 4,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(404);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status code", async () => {
    const response = await supertest(app)
      .put("/dlc")
      .send({
        dlcId: 11,
        name: "DLC Cambio",
        description:
          "Prueba del primer DLC para el juego Call of Duty: Warzone Cambiado",
        shortDescription: "Primer DLC de COD Warzone Cambiado",
        releaseDate: "2022-09-28",
        desktopImage:
          "https://www.xtrafondos.com/wallpapers/call-of-duty-warzone-2021-7336.jpg",
        mobileImage:
          "https://www.thetechgame.com/images/news/article/266600f004.jpg",
        logo: "https://seeklogo.com/images/C/call-of-duty-warzone-logo-D37AA435E3-seeklogo.com.png",
        ageRestriction: "M",
        parentGameId: 4,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it("Should return 400 status code, faltan parametros", async () => {
    const response = await supertest(app).put("/dlc").send({
      name: "DLC Cambio",
      description:
        "Prueba del primer DLC para el juego Call of Duty: Warzone Cambiado",
      shortDescription: "Primer DLC de COD Warzone Cambiado",
      releaseDate: "2022-09-28",
      desktopImage:
        "https://www.xtrafondos.com/wallpapers/call-of-duty-warzone-2021-7336.jpg",
      mobileImage:
        "https://www.thetechgame.com/images/news/article/266600f004.jpg",
      logo: "https://seeklogo.com/images/C/call-of-duty-warzone-logo-D37AA435E3-seeklogo.com.png",
      ageRestriction: "M",
      parentGameId: 4,
    });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
});
