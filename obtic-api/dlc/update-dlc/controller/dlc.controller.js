const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const UpdateDlc = async (req, res) => {
  const { dlcId, name, description, shortDescription, releaseDate, desktopImage, mobileImage, logo, ageRestriction, parentGameId } = req.body;
  if (!dlcId || !name || !description || !shortDescription || !releaseDate || !desktopImage || !mobileImage || !logo || !ageRestriction || !parentGameId) return res.status(400).json(response(false, {error: 'EDU-002'})); //faltan parametros
  const {success} = await verify(req.headers.authorization, true);
  if (!success) return  res.status(401).json(response(false, {error: 'EDU-004'}));
  try {
    const exists = await db.query(`SELECT id_product FROM product WHERE id_product=${dlcId}`);
    if (exists.rowCount < 1) return res.status(404).json(response(false, {error: 'EDU-001'}));// DLC no existe en la base
    const query = `UPDATE obtic.public.product SET product_name='${name}', description='${description}', short_description='${shortDescription}', release_date='${releaseDate}', desktop_image='${desktopImage}', mobile_image='${mobileImage}', logo='${logo}', age_restriction='${ageRestriction}', parent_game=${parentGameId}
    WHERE id_product=${dlcId};`;
    await db.query(query);
    return res.status(200).json(response(true, {dlcId, name, description, shortDescription, releaseDate, desktopImage, mobileImage, logo, ageRestriction, parentGameId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'EDU-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.put("", UpdateDlc);

module.exports = router;
