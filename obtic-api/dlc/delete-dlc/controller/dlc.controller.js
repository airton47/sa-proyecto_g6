const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const DeleteDlc = async (req, res) => {
  const { dlcId } = req.query;
  
  if (!dlcId) return res.status(400).json(response(false, {error: 'EDD-002'})); //faltan parametros
  const {success} = await verify(req.headers.authorization, true);
  if (!success) return  res.status(401).json(response(false, {error: 'EDD-004'}));
  try {
    const exists = await db.query(`SELECT id_product FROM product WHERE id_product=${dlcId}`);
    if (exists.rowCount < 1) return res.status(404).json(response(false, {error: 'EDD-001'}));// DLC no existe en la base
    const query = `UPDATE product SET active = false WHERE id_product=${dlcId};`;
    await db.query(query);
    return res.status(200).json(response(true, {dlcId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'EDD-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.delete("/deleteDLC", DeleteDlc);

module.exports = router;
