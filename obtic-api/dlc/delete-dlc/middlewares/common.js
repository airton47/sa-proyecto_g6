const morgan = require("morgan");
const cors = require("cors");
const express = require("express");

const createCommonMiddleware = (app) => {
  app.use(cors());
  app.use(morgan("common", {}));
  app.use(express.json());
};

module.exports = createCommonMiddleware;
