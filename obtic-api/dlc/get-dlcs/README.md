# DOCKER

```
docker build . -t fernandvx/obtic-dlc-get-dlc-microservice
```

```
docker push fernandvx/obtic-dlc-get-dlc-microservice
```

```
docker run --name dlc-get-dlc-microservice -p 3000:80 fernandvx/obtic-dlc-get-dlc-microservice
```
