require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const DlcController = require("./controller/dlc.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/dlc", DlcController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3020;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
