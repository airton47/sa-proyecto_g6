require("dotenv").config();
const supertest = require("supertest");
const app = require("..");
//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------
describe("get-dlcs controller", () => {
  it("Should return 400 status code, faltan parametros", async () => {
    const response = await supertest(app).get("/dlc/getDLCs").send({});
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, No existen juegos", async () => {
    const response = await supertest(app)
      .get("/dlc/getDLCs")
      .query({
        id_catalog: "1",
        filter: "1",
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  for (let index = 1; index <= 3; index++) {
    it("Should return 200 status code, Filter=" + index, async () => {
      const response = await supertest(app)
        .get("/dlc/getDLCs")
        .query({
          id_catalog: 0,
          filter: index,
        })
        .set({ authorization: "abc" });
      expect(response.statusCode).toBe(200);
      expect(response.body.success).toBe(true);
    });
  }

  it("Should return 200 status code,Catalog=20", async () => {
    const response = await supertest(app)
      .get("/dlc/getDLCs")
      .query({
        id_catalog: 20,
        filter: 1,
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
