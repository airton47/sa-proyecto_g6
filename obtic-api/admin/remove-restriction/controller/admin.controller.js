const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const RemoveRestriction = async (req, res) => {
  const { productId, regionId } = req.query;
  const {success} = await verify(req.headers.authorization, true);
  if (!success) return  res.status(401).json(response(false, {error: 'RRE-004'}));
  if (isNaN(Number(productId)) || isNaN(Number(regionId))) return res.status(400).json(response(false, {error: 'RRE-002'})); //faltan parametros
  try {
    const list = await db.query(`SELECT * FROM region_restriction WHERE id_product = ${productId} AND id_region = ${regionId}`);
    if (list.rowCount < 1) return res.status(400).json(response(false, {error: 'RRE-001'})); // Restriccion no existe
    await db.query(`DELETE FROM region_restriction WHERE id_product = ${productId} AND id_region = ${regionId}`);
    return res.status(200).json(response(true, {productId, regionId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'RRE-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.delete("/removeRestriction", RemoveRestriction);

module.exports = router;
