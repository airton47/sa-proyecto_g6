# DOCKER

```
docker build . -t fernandvx/obtic-admin-create-admin-microservice
```

```
docker push fernandvx/obtic-admin-create-admin-microservice
```

```
docker run --name admin-create-admin-microservice -p 3000:80 fernandvx/obtic-admin-create-admin-microservice
```

## BODY SAMPLE

- Tipo: `POST`
- Ejemplo de peticion:

`http://localhost:3002/admin/createAdmin`

- Body de request v1: Cuando no se incluye el campo 'profile_image'; todos estos campos son obligatorios (username, email, name, last_name).

```json
{
  "username": "airton47",
  "email": "ayrton.omega01@gmail.com",
  "name": "Airton",
  "lastName": "de Leon"
}
```

- Body de request v2: Cuando si se incluye el campo 'profile_image' (campo opcional).

```json
{
  "username": "airton47",
  "email": "ayrton.omega01@gmail.com",
  "name": "Airton",
  "last_name": "de Leon",
  "profileImage": "url_valida"
}
```

- Respuesta: ✅ Exitosa

```json
{
  "success": true,
  "data": {
    "message": "Nuevo usario administrador ha sido agregado con exito.",
    "admin": {
      "username": "airton52",
      "email": "aiyel.leonalfa@gmail.com",
      "name": "Airton",
      "lastName": "de Leon"
    }
  }
}
```

- Respuesta: `❌ Error por email duplicado`, ocurre porque se intenta agregar un administrador cuyo email ya ha sido usado por otro usuario del tipo administrador.

- Codigo: 400

```json
{
  "success": false,
  "data": {
    "message": "Ya se ha ingresado un usuario administrador con este correo",
    "admin": {
      "username": "airton47",
      "email": "ayrton.omega01@gmail.com",
      "name": "Airton",
      "lastName": "de Leon"
    }
  }
}
```

- Respuesta: `❌ Error de autenticación`, ocurre porque se intenta agregar un administrador y no se ha proveido un token de autenticación o bien porque no se cuenta con los permisos necesarios (de administrador) para llevar a cabo la operación.

- Codigo: 401

```json
{
  "success": false,
  "data": "NO proveyo el token de autenticación"
}
```

- Respuesta: `❌ Parámetros faltantes`, ocurre cuando no se ingresan todos los campos obligatorios.

- Codigo: 400

```json
{
  "success": false,
  "data": "Debe ingresar la informacion necesaria: Bad-Request"
}
```
