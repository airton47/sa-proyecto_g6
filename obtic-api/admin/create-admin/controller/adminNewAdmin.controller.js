const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");
const mail = require("../helpers/mailer");
const bcrypt = require("bcrypt");

const generatePassword = () => {
  let chars =
    "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let passwordLength = 12;
  let password = "";
  for (var i = 0; i < passwordLength; i++) {
    var randomNumber = Math.floor(Math.random() * chars.length);
    password += chars.substring(randomNumber, randomNumber + 1);
  }
  return password;
};

const CreateNewAdmin = async (req, res) => {
  let { username, email, name, lastName, profileImage } = req.body;
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }

  if (!username || !email || !name || !lastName) {
    return res
      .status(400)
      .json(
        response(false, `Debe ingresar la informacion necesaria: Bad-Request`)
      );
  }

  let query = `
  select username 
  from "user" d 
  where email = ('${email}')
  and id_user_type = 1;`;

  try {
    // Se verifica si ya existe un administrador con el mismo correo
    let { rows } = await db.query(query);

    if (rows.length > 0) {
      return res.status(400).json(
        response(false, {
          message:
            "Ya se ha ingresado un usuario administrador con este correo",
          admin: req.body,
        })
      );
    }

    // Se genera la nueva contraseña para muevo admin
    let password = generatePassword();

    // return res.status(200).json(response(true, { ...req.body }));
    // Ahora se encripta la contraseña antes de ingresarla en la base de datos
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);

    // Se inserta el nuevo desarrollador en la base de datos
    query = `
    INSERT INTO "user" (
        username, 
        email, 
        name, 
        last_name, 
        id_user_type,
        confirmed, 
        password 
        ${profileImage ? ", profile_image" : ""}
      ) values (
        '${username}', 
        '${email}', 
        '${name}', 
        '${lastName}', 
        1, 
        true, 
        '${hash}' 
        ${profileImage ? ", '" + profileImage + "'" : ""} 
      );
    `;

    let result = await db.query(query);
    console.log("result: ", result);

    // Se preparan los datos para correo con credenciales para el nuevo admin
    let to = email;
    let txtSubject = "Obtic Games - Credenciales de Acceso";
    let txt = `
    <div id="mail">
    <div>
      <div style="background-color: #ffffff">
        <table
          style="
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            border-spacing: 0;
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
            background-repeat: repeat;
            background-position: top;
          "
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <td style="margin: 0" valign="top">
                <table
                  style="
                    border-collapse: collapse;
                    border-spacing: 0;
                    table-layout: fixed !important;
                    width: 100%;
                  "
                  cellspacing="0"
                  cellpadding="0"
                  bgcolor="#0c171f"
                  align="center"
                >
                  <tbody>
                    <tr>
                      <td align="Center">
                        <img
                          loading="lazy"
                          src="https://i.ibb.co/bXJqn1H/Logo.png"
                          style="border: 0; margin: 32px 0 0; display: block"
                          alt=""
                        />
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px 0;
                          line-height: 40px;
                          color: #ffffff;
                          font-size: 22px;
                        "
                        align="center"
                      >
                        <b>
                          ¡Bienvenido al equipo de Obtic Games!
                        </b
                        >
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px 0;
                          line-height: 20px;
                          color: #ffffff;
                          font-size: 16px;
                        "
                        align="center"
                      >
                        Para ingresar a tu usuario administrador deberás usar la siguiente contraseña:
                      </td>
                    </tr>
                    <tr>
                      <td
                      style="
                          padding: 30px 20px 0;
                          line-height: 20px;
                          color: #ffffff;
                          bgcolor: #637180;
                          font-size: 18px;
                        "
                        align="center"
                      >
                      CONSTRASEÑA: <b>${password}</b>
                      </td>
                    </tr>
                    <tr>
                      <td
                        style="
                          padding: 30px 20px;
                          line-height: 20px;
                          color: #ffffff;
                          font-size: 16px;
                        "
                        align="center"
                      >
                      ¡Bienvenido! <br />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table
                          style="
                            border-collapse: collapse;
                            border-spacing: 0;
                            width: 100%;
                            background-color: #15202d;
                          "
                          cellspacing="0"
                          cellpadding="0"
                          bgcolor="#637180
                          "
                          align="center"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  padding: 3% 0;
                                  font-family: arial, 'helvetica neue', helvetica,
                                    sans-serif;
                                  line-height: 21px;
                                  color: #637180;
                                  font-size: 14px;
                                "
                                align="center"
                              >
                                Este es un correo auto-generado por
                                <a
                                  href="mailto:pruebasusac2022@gmail.com"
                                  style="
                                    text-decoration: none;
                                    text-transform: lowercase;
                                    color: #5b43f4;
                                  "
                                  >pruebasusac2022@gmail.com</a
                                >
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
    `;

    // Ahora se envia el correo con las credenciales para el nuevo admin
    let info = await mail.enviar_mail(txtSubject, to, txt);
    console.log(info);

    return res.status(200).json(
      response(true, {
        message: "Nuevo usario administrador ha sido agregado con exito.",
        admin: req.body,
      })
    );
  } catch (error) {
    return res.status(500).json(response(false, error.toString()));
  }
};

router.post("/createAdmin", CreateNewAdmin);

module.exports = router;
