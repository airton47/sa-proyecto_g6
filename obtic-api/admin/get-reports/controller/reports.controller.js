const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");
const { getUsers } = require("../utils/reports.utils");

const getReports = async (req, res) => {
  const { id_user } = req.query;
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  if (id_user == null) {
    return res.status(400).json(
      response(false, {
        message: "Deben ingresarse todos los parametros obligatorios",
      })
    );
  }
  try {
    let query = `
      select 	
      	r.*, 
      	(u1.name || ' ' || u1.last_name) as reporting_user_name,
      	(u2.name || ' ' || u2.last_name) as reported_user_name,
        u2.username,
        u2.email
      from
      	report r
      inner join	
      	"user" u1
      on
      	r.reporting_user = u1.id_user 
      inner join 
      	"user" u2
      on
      	r.reported_user = u2.id_user
    `;
    if (id_user != 0) {
      query = `${query} WHERE r.reported_user = ${id_user}`;
    }
    let reports = (await db.query(query)).rows;
    let responseReports;
    if (id_user == 0) {
      let users = await getUsers(query);
      for (let user of users) {
        user.reports = [];
        for (let report of reports) {
          if (report.reported_user == user.id_user) {
            user.reports.push(report);
          }
        }
      }
      responseReports = users;
    } else {
      let user = (
        await db.query(`SELECT * FROM "user" WHERE id_user=${id_user}`)
      ).rows;
      user = user[0];
      delete user.password;
      user.reports = reports;
      responseReports = user;
    }
    return res.status(200).send(response(true, responseReports));
  } catch (error) {
    console.log(error);
    return res.status(500).send(response(false, error.toString()));
  }
};

router.get("/getReports", getReports);

module.exports = router;
