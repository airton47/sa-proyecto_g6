const db = require("../db/database");

const getUsers = async (query) => {
  let userQuery = `
        SELECT DISTINCT
            r.reported_user as id_user, 
            r.reported_user_name as name,
            r.username,
            r.email
        FROM
            (${query}) r
    `;
  return (await db.query(userQuery)).rows;
};

module.exports = {
  getUsers,
};
