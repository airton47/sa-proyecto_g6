const response = (status, payload) => {
  return {
    success: status,
    data: payload || {},
  };
};

module.exports = response;
