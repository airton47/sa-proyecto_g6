const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");

const getReport = async (req, res) => {
  let { gameId } = req.body;
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  ////////////////////////////////////////////////////////////
  //---Ingresos que se han obtenido a través de los meses---//
  ////////////////////////////////////////////////////////////
  let query1 = `select
    COALESCE(credito.description_movement_type,debito.description_movement_type) description
    , COALESCE(credito.mes,debito.mes) mes
    , COALESCE(credito.total,0) as ingreso
    , COALESCE(debito.total,0) as devolucion
    , COALESCE(credito.total,0) - COALESCE(debito.total,0) as total
    , case 
      when  credito.id_movement_type=1 then 0 
      when  credito.id_movement_type=3 then (COALESCE(credito.total,0) - COALESCE(debito.total,0))*(1-1/1.15)
      when  credito.id_movement_type=5 then 0--(COALESCE(credito.total,0) - COALESCE(debito.total,0))*(0.1764)
      end regalias
    , case 
      when  credito.id_movement_type=1 then 100 
      when  credito.id_movement_type=3 then 15
      when  credito.id_movement_type=5 then 85
      end porcentaje
  from (
    ---ventas(credito)
    select 
      mt.id_movement_type
      , mt.description_movement_type
      , to_char( movement_date, 'MM-YYYY') mes  
      , sum(amount) total
    from balance inner join movement_type mt on mt.id_movement_type =balance.id_movement_type
      and mt.id_movement_type in (1,3,5)
    group by
      to_char( movement_date , 'MM-YYYY')
      ,mt.description_movement_type
      ,mt.id_movement_type
  )as credito
  full join (
    ---(debito)
    select 
      case 
        when  mt.id_movement_type=2 then 1 
        when  mt.id_movement_type=4 then 3
        when  mt.id_movement_type=6 then 5
        end id_movement_type
      , mt.description_movement_type
      , to_char( movement_date, 'MM-YYYY') mes  
      , sum(amount) total
    from balance inner join movement_type mt on mt.id_movement_type =balance.id_movement_type
      and mt.id_movement_type in (2,4,6)
    group by
      to_char( movement_date , 'MM-YYYY')
      ,mt.description_movement_type
      ,mt.id_movement_type
  )as debito on debito.id_movement_type=credito.id_movement_type`;
  ////////////////////////////////////////////////////////////
  //----Las ventas que ha tenido un juego en específico-----//
  ////////////////////////////////////////////////////////////
  let txtwhere = "";
  if (gameId != undefined && gameId) {
    txtwhere = `and COALESCE(ventas.id_game,devoluciones.id_game)=${gameId}`;
  }
  let query2 = `select
  COALESCE(ventas.id_game,devoluciones.id_game) id_game
  , p.product_name product_name
  , COALESCE(ventas.id_group,devoluciones.id_group) id_group
  , COALESCE(ventas.cantidad,0) ventas
  , COALESCE(ventas.total,0) ingreso
  , COALESCE(devoluciones.cantidad,0) devoluciones
  , COALESCE(devoluciones.total,0) egreso
  , COALESCE(ventas.regalias,devoluciones.regalias) regalias
  , COALESCE(ventas.fecha,devoluciones.fecha) fecha
from (--contador de ventas
  select
    b.id_game
    , COALESCE(b.id_group,-1)id_group
    , count(*) cantidad
    , sum(b.amount) total
    , to_char( movement_date , 'MM-YYYY') fecha
    , sum(case when  b.id_movement_type=3 then (b.amount)*(1-1/1.15)else 0 end)regalias
  from balance b
  where b.id_movement_type in (1,3,5)
  group by 
    b.id_game
    , b.id_group
    , to_char( movement_date , 'MM-YYYY')
)ventas
full join(--contador de devoluciones
  select
    b.id_game
    , COALESCE(b.id_group,-1)id_group
    , count(*) cantidad
    , sum(b.amount) total
    , to_char( movement_date , 'MM-YYYY') fecha
    , 0 regalias
  from balance b
  where b.id_movement_type in (2,4,6)
  group by 
    b.id_game
    , b.id_group
    , to_char( movement_date , 'MM-YYYY')
)devoluciones on devoluciones.id_game=ventas.id_game and devoluciones.id_group=ventas.id_group
left join product p on p.id_product = COALESCE(ventas.id_game,devoluciones.id_game)
where COALESCE(ventas.id_game,devoluciones.id_game)=
    case when COALESCE(ventas.id_group,devoluciones.id_group) = -1 then p.id_product
    else COALESCE(ventas.id_game,devoluciones.id_game) end
  ${txtwhere}`;
  ////////////////////////////////////////////////////////////
  //---------------------tablero----------------------------//
  ////////////////////////////////////////////////////////////
  let query3 = `select  'Vendidos' Descripcion, count(*) as Total from balance b
  where b.id_movement_type in (1,3,5)
  union all
  select  'Devueltos' Descripcion, count(*) as Total from balance b
  where b.id_movement_type in (2,4,6);`;

  let query4 = `select
    case when b.id_movement_type in (1,3,5) then true else false end venta
    ,b.external_platform servicio
    ,b.id_game 
    , b.id_group
    , p.product_name 
    , b.amount 
    , case 
      when  b.id_movement_type=1 then 0 
      when  b.id_movement_type=3 then (b.amount)*(1-1/1.15)
      when  b.id_movement_type=5 then 0--(COALESCE(credito.total,0) - COALESCE(debito.total,0))*(0.1764)
      else 0
      end regalias
    , case 
      when  b.id_movement_type=1 then 100 
      when  b.id_movement_type=3 then 15
      when  b.id_movement_type=5 then 85
      else 100
      end porcentaje
    , b.movement_date 
    from balance b
  left join product p on p.id_product=b.id_game and b.id_group is null 
  where b.id_movement_type in (1,2,3,3,5,6);
  `;
  try {
    let rows1 = await (await db.query(query1)).rows;
    let rows2 = await (await db.query(query2)).rows;
    let rows3 = await (await db.query(query3)).rows;
    let rows4 = await (await db.query(query4)).rows;
    return res.status(200).json(
      response(true, {
        grafica1: {
          titulo: "Ingresos que se han obtenido a través de los meses",
          data: rows1,
        },
        grafica2: {
          titulo: "Las ventas que ha tenido un juego en específico",
          juego: gameId!=undefined && gameId?gameId:"Se muestran todos, enviar body.gameId",
          data: rows2,
        },
        grafica3: { titulo: "Tablero", resumen: rows3, detalle: rows4 },
      })
    );
  } catch (error) {
    return res.status(500).json(response(false, error.toString()));
  }
};

router.get("/getReport", getReport);

module.exports = router;
