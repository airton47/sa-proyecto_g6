require("dotenv").config();
const express = require("express");
const middleware = require("./middlewares/common");
const RestrictionController = require("./controller/restriction.controller");

const createApp = () => {
  const app = express();
  middleware(app);
  app.use("/admin", RestrictionController);
  return app;
};

const app = createApp();
const PORT = process.env.PORT || 3020;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;
