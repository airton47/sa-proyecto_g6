# DOCKER

```
docker build . -t fernandvx/obtic-admin-list-restriction-microservice
```

```
docker push fernandvx/obtic-admin-list-restriction-microservice
```

```
docker run --name admin-list-restriction-microservice -p 3000:80 fernandvx/obtic-admin-list-restriction-microservice
```
