const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const getRestrictions = async (req, res) => {
  const { idProduct } = req.query;
  if (!idProduct) {
    return res.status(400).json(
      response(false, {
        error: "No se enviaron todos los datos necesarios",
      })
    );
  }
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }
  try {
    const query = `
                        select 
                          rer.id_product, rer.id_region, r.region, p.product_name
                        from
                          region_restriction rer
                        inner join
                          region r 
                        on
                          rer.id_region = r.id_region 
                        inner join
                          product p
                        on
                          rer.id_product = p.id_product
                        where 
                          rer.id_product = ${idProduct}`;
    let result = (await db.query(query)).rows;
    return res.status(200).json(response(true, result));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Ocurrio un error inesperado" })); //ERROR DEL SERVIDOR
  }
};

router.get("/getRestrictions", getRestrictions);

module.exports = router;
