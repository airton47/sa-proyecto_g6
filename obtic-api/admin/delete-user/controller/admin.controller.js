const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const DeleteUser = async (req, res) => {
  const { userId } = req.query;
  
  if (!userId) return res.status(400).json(response(false, {error: 'DUE-002'})); //faltan parametros
  const {success} = await verify(req.headers.authorization, true);
  if (!success) return  res.status(401).json(response(false, {error: 'DUE-004'}));
  try {
    const exists = await db.query(`SELECT * FROM "user" WHERE id_user=${userId}`);
    if (exists.rowCount < 1) return res.status(404).json(response(false, {error: 'DUE-001'}));// Usuario no existe en la base
    const query = `UPDATE "user" SET active=false WHERE id_user=${userId};`;
    await db.query(query);
    return res.status(200).json(response(true, {userId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'DUE-003'}));//Error desconocido (bae de datos no conectada)
  }
};

router.delete("/deleteUser", DeleteUser);

module.exports = router;