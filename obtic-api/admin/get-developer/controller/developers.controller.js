const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");

const getDevelopersList = async (req, res) => {
  try {
    const { rows } = await db.query(`select * from developer;`);
    return res.status(200).send(response(true, { developers: rows }));
  } catch (error) {
    return res.status(500).send(response(false, error.toString()));
  }
};

router.get("/getDevelopers", getDevelopersList);

module.exports = router;
