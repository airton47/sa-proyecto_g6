const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");
const mail = require("../helpers/mailer");

const VerifyReport = async (req, res) => {
  const { reportId, resolutionId, resolutionDetail, suspensionDays } = req.body;
  const { success } = await verify(req.headers.authorization, true);
  if (!success)
    return res
      .status(401)
      .json(response(false, { error: "Error de autenticación" }));
  if (
    isNaN(Number(reportId)) ||
    isNaN(Number(resolutionId)) ||
    !resolutionDetail
  )
    return res
      .status(400)
      .json(
        response(false, { error: "Error en los parámetros (bad request)" })
      ); //faltan parametros
  if (resolutionId === 2 && !suspensionDays)
    return res
      .status(400)
      .json(
        response(false, {
          error: "No se puede suspender sin fecha de suspensión",
        })
      ); //faltan parametros
  try {
    const report = await db.query(
      `SELECT * FROM report WHERE id_report = ${reportId}`
    );
    if (report.rowCount < 1)
      return res
        .status(404)
        .json(
          response(false, { error: "Reporte no existe en la base de datos" })
        ); // Reporte no existe
    const reportedUser = report.rows[0].reported_user;
    const type = await db.query(
      `SELECT * FROM resolution_type WHERE id_resolution_type = ${resolutionId}`
    );
    if (type.rowCount < 1)
      return res
        .status(404)
        .json(
          response(false, { error: "Resolución no existe en la base de datos" })
        ); // Resolución no existe

    await db.query(
      `UPDATE report SET attended = true, resolution = ${resolutionId}, resolution_detail = '${resolutionDetail}', resolution_date = NOW() WHERE reported_user = ${reportedUser} and attended = false`
    );
    if (resolutionId === 4)
      return res
        .status(200)
        .json(
          response(true, {
            reportId,
            resolutionId,
            resolutionDetail,
            suspensionDays,
          })
        );

    const user = await db.query(
      `SELECT email FROM "user" WHERE id_user = ${reportedUser}`
    );
    const email = user.rows[0].email;
    const txtSubject = "Informe de reporte Obtic Games";
    let txt;
    switch (resolutionId) {
      case 1: // Baja permanente
        await db.query(
          `UPDATE "user" SET active = false WHERE id_user = ${reportedUser}`
        );
        txt = `
          <div id="mail">
          <div>
            <div style="background-color: #ffffff">
              <table
                style="
                  font-family: Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  border-spacing: 0;
                  padding: 0;
                  margin: 0;
                  width: 100%;
                  height: 100%;
                  background-repeat: repeat;
                  background-position: top;
                "
                width="100%"
                cellspacing="0"
                cellpadding="0"
              >
                <tbody>
                  <tr>
                    <td style="margin: 0" valign="top">
                      <table
                        style="
                          border-collapse: collapse;
                          border-spacing: 0;
                          table-layout: fixed !important;
                          width: 100%;
                        "
                        cellspacing="0"
                        cellpadding="0"
                        bgcolor="#0c171f"
                        align="center"
                      >
                        <tbody>
                          <tr>
                            <td align="Center">
                              <img
                                loading="lazy"
                                src="https://i.ibb.co/bXJqn1H/Logo.png"
                                style="border: 0; margin: 32px 0 0; display: block"
                                alt=""
                              />
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 40px;
                                color: #ffffff;
                                font-size: 22px;
                              "
                              align="center"
                            >
                              <b>
                                Tu cuenta ha sido dada de baja permanentemente</b
                              >
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                              El motivo de la baja es el siguiente:<br>
                              <b>"${resolutionDetail}"</b><br>
                              Raz&oacute;n que fue aprobada por el equipo de Obtic Games.
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                            Sentimos los inconvenientes<br />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <table
                                style="
                                  border-collapse: collapse;
                                  border-spacing: 0;
                                  width: 100%;
                                  background-color: #15202d;
                                "
                                cellspacing="0"
                                cellpadding="0"
                                bgcolor="#637180
                                "
                                align="center"
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style="
                                        padding: 3% 0;
                                        font-family: arial, 'helvetica neue', helvetica,
                                          sans-serif;
                                        line-height: 21px;
                                        color: #637180;
                                        font-size: 14px;
                                      "
                                      align="center"
                                    >
                                      Este es un correo auto-generado por
                                      <a
                                        href="mailto:pruebasusac2022@gmail.com"
                                        style="
                                          text-decoration: none;
                                          text-transform: lowercase;
                                          color: #5b43f4;
                                        "
                                        >pruebasusac2022@gmail.com</a
                                      >
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
          `;
        break;
      case 2: // Baja temporal
        await db.query(
          `UPDATE "user" SET suspension_date = NOW() + interval '${suspensionDays} day' WHERE id_user = ${reportedUser}`
        );
        txt = `
          <div id="mail">
          <div>
            <div style="background-color: #ffffff">
              <table
                style="
                  font-family: Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  border-spacing: 0;
                  padding: 0;
                  margin: 0;
                  width: 100%;
                  height: 100%;
                  background-repeat: repeat;
                  background-position: top;
                "
                width="100%"
                cellspacing="0"
                cellpadding="0"
              >
                <tbody>
                  <tr>
                    <td style="margin: 0" valign="top">
                      <table
                        style="
                          border-collapse: collapse;
                          border-spacing: 0;
                          table-layout: fixed !important;
                          width: 100%;
                        "
                        cellspacing="0"
                        cellpadding="0"
                        bgcolor="#0c171f"
                        align="center"
                      >
                        <tbody>
                          <tr>
                            <td align="Center">
                              <img
                                loading="lazy"
                                src="https://i.ibb.co/bXJqn1H/Logo.png"
                                style="border: 0; margin: 32px 0 0; display: block"
                                alt=""
                              />
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 40px;
                                color: #ffffff;
                                font-size: 22px;
                              "
                              align="center"
                            >
                              <b>
                                Tu cuenta ha sido dada de baja temporalmente (${suspensionDays} días)</b
                              >
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                              El motivo de la baja es el siguiente:<br>
                              <b>"${resolutionDetail}"</b><br>
                              Raz&oacute;n que fue aprobada por el equipo de Obtic Games.
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                            Sentimos los inconvenientes<br />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <table
                                style="
                                  border-collapse: collapse;
                                  border-spacing: 0;
                                  width: 100%;
                                  background-color: #15202d;
                                "
                                cellspacing="0"
                                cellpadding="0"
                                bgcolor="#637180
                                "
                                align="center"
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style="
                                        padding: 3% 0;
                                        font-family: arial, 'helvetica neue', helvetica,
                                          sans-serif;
                                        line-height: 21px;
                                        color: #637180;
                                        font-size: 14px;
                                      "
                                      align="center"
                                    >
                                      Este es un correo auto-generado por
                                      <a
                                        href="mailto:pruebasusac2022@gmail.com"
                                        style="
                                          text-decoration: none;
                                          text-transform: lowercase;
                                          color: #5b43f4;
                                        "
                                        >pruebasusac2022@gmail.com</a
                                      >
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
          `;
        break;
      case 3: // Advertencia
        txt = `
          <div id="mail">
          <div>
            <div style="background-color: #ffffff">
              <table
                style="
                  font-family: Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  border-spacing: 0;
                  padding: 0;
                  margin: 0;
                  width: 100%;
                  height: 100%;
                  background-repeat: repeat;
                  background-position: top;
                "
                width="100%"
                cellspacing="0"
                cellpadding="0"
              >
                <tbody>
                  <tr>
                    <td style="margin: 0" valign="top">
                      <table
                        style="
                          border-collapse: collapse;
                          border-spacing: 0;
                          table-layout: fixed !important;
                          width: 100%;
                        "
                        cellspacing="0"
                        cellpadding="0"
                        bgcolor="#0c171f"
                        align="center"
                      >
                        <tbody>
                          <tr>
                            <td align="Center">
                              <img
                                loading="lazy"
                                src="https://i.ibb.co/bXJqn1H/Logo.png"
                                style="border: 0; margin: 32px 0 0; display: block"
                                alt=""
                              />
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 40px;
                                color: #ffffff;
                                font-size: 22px;
                              "
                              align="center"
                            >
                              <b>
                                Tu cuenta ha sido reportada</b
                              >
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px 0;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                              El motivo del reporte es el siguiente:<br>
                              <b>"${resolutionDetail}"</b><br>
                              Raz&oacute;n que fue aprobada por el equipo de Obtic Games.<br>
                              Si no tienes cuidado tu cuenta podría ser <b>dada de baja permanentemente</b>.
                            </td>
                          </tr>
                          <tr>
                            <td
                              style="
                                padding: 30px 20px;
                                line-height: 20px;
                                color: #ffffff;
                                font-size: 16px;
                              "
                              align="center"
                            >
                            Sentimos los inconvenientes<br />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <table
                                style="
                                  border-collapse: collapse;
                                  border-spacing: 0;
                                  width: 100%;
                                  background-color: #15202d;
                                "
                                cellspacing="0"
                                cellpadding="0"
                                bgcolor="#637180
                                "
                                align="center"
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style="
                                        padding: 3% 0;
                                        font-family: arial, 'helvetica neue', helvetica,
                                          sans-serif;
                                        line-height: 21px;
                                        color: #637180;
                                        font-size: 14px;
                                      "
                                      align="center"
                                    >
                                      Este es un correo auto-generado por
                                      <a
                                        href="mailto:pruebasusac2022@gmail.com"
                                        style="
                                          text-decoration: none;
                                          text-transform: lowercase;
                                          color: #5b43f4;
                                        "
                                        >pruebasusac2022@gmail.com</a
                                      >
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
          `;
        break;
    }
    mail.enviar_mail(txtSubject, email, txt);
    return res
      .status(200)
      .json(
        response(true, {
          reportId,
          resolutionId,
          resolutionDetail,
          suspensionDays,
        })
      );
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(response(false, { error: "Error interno del servidor" })); //Error desconocido (bae de datos no conectada)
  }
};

router.post("/verifyReport", VerifyReport);

module.exports = router;
