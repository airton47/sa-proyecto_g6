const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const getStatistics = async (req, res) => {
  const { success } = await verify(req.headers.authorization, true);
  if (!success) return res.status(401).json(response(false, { error: 'Error de autenticación' }));

  const { filter, id_product } = req.query;
  if (!filter) return res.status(400).json(response(false, { error: 'Parámetro de filtrado no definido' }));

  try {
    switch (filter) {
      case '1':
        const incomes = await db.query(`SELECT COUNT(*) AS incomes FROM balance WHERE is_credit = true`);
        const incomesCount = incomes.rows[0].incomes;
        const outcomes = await db.query(`SELECT COUNT(*) AS outcomes FROM balance WHERE is_credit = false`);
        const outcomesCount = outcomes.rows[0].outcomes;
        return res.status(200).json(response(true, { incomesCount, outcomesCount }));
        case '2':
          const movements = await db.query(`SELECT mt.id_movement_type, mt.description_movement_type as movement_type, mt.is_credit, COUNT(*) as movements FROM balance b  inner join movement_type mt on b.id_movement_type = mt.id_movement_type group by mt.id_movement_type order by mt.id_movement_type`);
          const movementTypes = movements.rows;
          return res.status(200).json(response(true, { movementTypes }));
        case '3':
          const query = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by id_group, external_platform`);
          const fields = [];
          const amounts = [];
          const counts = [];
          query.rows.forEach(element => {
            if (element.external_platform !== null) {
              fields.push(`Desde G${element.external_platform}`);
              amounts.push(Number(element.monto));
              counts.push(Number(element.movimientos));
            } else if (element.id_group !== null && element.external_platform === null) {
              fields.push(`Hacia G${element.id_group}`);
              amounts.push(Number(element.monto));
              counts.push(Number(element.movimientos));
            } else if (element.id_group === null && element.external_platform === null) {
              fields.push('Interno');
              amounts.push(Number(element.monto));
              counts.push(Number(element.movimientos));
            }
          });
          return res.status(200).json(response(true, { fields, amounts, counts }));
        case '4':
          const query2 = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by id_group, external_platform`);
          let entrantesCount = 0;
          let entrantesAmount = 0;
          let salientesCount = 0;
          let salientesAmount = 0;
          let internoCount = 0;
          let internoAmount = 0;
          query2.rows.forEach(element => {
            if (element.external_platform !== null) {
              entrantesAmount+= Number(element.monto);
              entrantesCount += Number(element.movimientos);
            } else if (element.id_group !== null && element.external_platform === null) {
              salientesCount += Number(element.movimientos);
              salientesAmount += Number(element.monto);
            } else if (element.id_group === null && element.external_platform === null) {
              internoAmount += Number(element.monto);
              internoCount += Number(element.movimientos);
            }
          });
          return res.status(200).json(response(true, { 
            fields: ['Entrantes', 'Salientes', 'Internos'],
            amounts: [entrantesAmount, salientesAmount, internoAmount],
            counts: [entrantesCount, salientesCount, internoCount]
          }));
        case '5':
          const query3 = await db.query(`select extract(month from date_trunc('month', movement_date)) as month, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by month`);
          const months = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
          ]
          let amounts2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
          let counts2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

          query3.rows.forEach(element => {
            amounts2[Number(element.month) - 1] = Number(element.monto);
            counts2[Number(element.month) - 1] = Number(element.movimientos);
          });
          return res.status(200).json(response(true, { 
            fields: months,
            amounts: amounts2,
            counts: counts2
          }));
        case '6':
          const queryF = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = false group by id_group, external_platform`);
          const fieldsF = [];
          const amountsF = [];
          const countsF = [];
          queryF.rows.forEach(element => {
            if (element.external_platform !== null) {
              fieldsF.push(`Desde G${element.external_platform}`);
              amountsF.push(Number(element.monto));
              countsF.push(Number(element.movimientos));
            } else if (element.id_group !== null && element.external_platform === null) {
              fieldsF.push(`Hacia G${element.id_group}`);
              amountsF.push(Number(element.monto));
              countsF.push(Number(element.movimientos));
            } else if (element.id_group === null && element.external_platform === null) {
              fieldsF.push('Interno');
              amountsF.push(Number(element.monto));
              countsF.push(Number(element.movimientos));
            }
          });
          return res.status(200).json(response(true, { fields: fieldsF, amounts: amountsF, counts: countsF }));
        case '7':
          const query2F = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = false group by id_group, external_platform`);
          let entrantesCountF = 0;
          let entrantesAmountF = 0;
          let salientesCountF = 0;
          let salientesAmountF = 0;
          let internoCountF = 0;
          let internoAmountF = 0;
          query2F.rows.forEach(element => {
            if (element.external_platform !== null) {
              entrantesAmountF += Number(element.monto);
              entrantesCountF += Number(element.movimientos);
            } else if (element.id_group !== null && element.external_platform === null) {
              salientesCountF += Number(element.movimientos);
              salientesAmountF += Number(element.monto);
            } else if (element.id_group === null && element.external_platform === null) {
              internoAmountF += Number(element.monto);
              internoCountF += Number(element.movimientos);
            }
          });
          return res.status(200).json(response(true, { 
            fields: ['Entrantes', 'Salientes', 'Internos'],
            amounts: [entrantesAmountF, salientesAmountF, internoAmountF],
            counts: [entrantesCountF, salientesCountF, internoCountF]
          }));
        case '8':
          const query3F = await db.query(`select extract(month from date_trunc('month', movement_date)) as month, count(*) as movimientos, sum(amount) as monto from balance where is_credit = false group by month`);
          const monthsF = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
          ]
          let amounts2F = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
          let counts2F = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

          query3F.rows.forEach(element => {
            amounts2F[Number(element.month) - 1] = Number(element.monto);
            counts2F[Number(element.month) - 1] = Number(element.movimientos);
          });
          return res.status(200).json(response(true, { 
            fields: monthsF,
            amounts: amounts2F,
            counts: counts2F
          }));
        case '9':
          const queryD = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by id_group, external_platform`);
          const fieldsD = [];
          const amountsD = [];
          const countsD = [];
          for( const element of queryD.rows) {
            const subQuery = await db.query(`select sum(amount) as outcome from balance where is_credit = false and id_group ${element.id_group !== null ? '=' : 'is'} ${String(element.id_group)} and external_platform ${element.external_platform !== null ? '=' : 'is'} ${String(element.external_platform)} group by id_group, external_platform`)
            const out = subQuery.rowCount > 0 ? subQuery.rows[0].outcome : 0;
            if (element.external_platform !== null) {
              fieldsD.push(`Desde G${element.external_platform}`);
              amountsD.push(Number(element.monto));
              countsD.push(Number(out));
            } else if (element.id_group !== null && element.external_platform === null) {
              fieldsD.push(`Hacia G${element.id_group}`);
              amountsD.push(Number(element.monto));
              countsD.push(Number(out));
            } else if (element.id_group === null && element.external_platform === null) {
              fieldsD.push('Interno');
              amountsD.push(Number(element.monto));
              countsD.push(Number(out));
            }
          }
          return res.status(200).json(response(true, { fields: fieldsD, income: amountsD, outcome: countsD }));
        case '10':
          const query2D = await db.query(`select id_group, external_platform, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by id_group, external_platform`);
          let entrantesCountD = 0;
          let entrantesAmountD = 0;
          let salientesCountD = 0;
          let salientesAmountD = 0;
          let internoCountD = 0;
          let internoAmountD = 0;
          for (const element of query2D.rows) {
            const subQuery = await db.query(`select sum(amount) as outcome from balance where is_credit = false and id_group ${element.id_group !== null ? '=' : 'is'} ${String(element.id_group)} and external_platform ${element.external_platform !== null ? '=' : 'is'} ${String(element.external_platform)} group by id_group, external_platform`)
            const out = subQuery.rowCount > 0 ? subQuery.rows[0].outcome : 0;
            if (element.external_platform !== null) {
              entrantesAmountD+= Number(element.monto);
              entrantesCountD += Number(out);
            } else if (element.id_group !== null && element.external_platform === null) {
              salientesCountD += Number(out);
              salientesAmountD += Number(element.monto);
            } else if (element.id_group === null && element.external_platform === null) {
              internoAmountD += Number(element.monto);
              internoCountD += Number(out);
            }
          }
          return res.status(200).json(response(true, { 
            fields: ['Entrantes', 'Salientes', 'Internos'],
            incomes: [entrantesAmountD, salientesAmountD, internoAmountD],
            outcomes: [entrantesCountD, salientesCountD, internoCountD]
          }));
        case '11':
          const query3D = await db.query(`select extract(month from date_trunc('month', movement_date)) as month, count(*) as movimientos, sum(amount) as monto from balance where is_credit = true group by month`);
          const query3D2 = await db.query(`select extract(month from date_trunc('month', movement_date)) as month, count(*) as movimientos, sum(amount) as monto from balance where is_credit = false group by month`);
          const monthsD = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
          ]
          let amounts2D = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
          let counts2D = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

          query3D.rows.forEach(element => {
            amounts2D[Number(element.month) - 1] = Number(element.monto);
          });

          query3D2.rows.forEach(element => {
            counts2D[Number(element.month) - 1] = Number(element.monto);
          });
          return res.status(200).json(response(true, { 
            fields: monthsD,
            incomes: amounts2D,
            outcomes: counts2D
          }));
        case '12':
          const labels = [];
          const series = [];
          const initialData = await db.query(`select b.id_game, b.id_group, count(*) as movimientos, sum(amount) as monto from balance b  where is_credit = true group by id_game, id_group`);
          for (element of initialData.rows) {
            if (element.id_group !== null) {
              const externalData = await db.query(`select game_name from detail_external_shopping_cart where id_product = ${element.id_game} and "group" = ${element.id_group}`);
              labels.push(externalData.rowCount > 0 ? externalData.rows[0].game_name : '');
              series.push(Number(element.monto));
            } else {
              const internalData = await db.query(`select product_name from product where id_product = ${element.id_game}`);
              labels.push(internalData.rowCount > 0 ? internalData.rows[0].product_name : '');
              series.push(Number(element.monto));
            }
          }

          return res.status(200).json(response(true, {
            labels,
            series
          }));
        case '13':
          const labelsC = [];
          const seriesC = [];
          const initialDataC = await db.query(`select b.id_game, b.id_group, count(*) as movimientos, sum(amount) as monto from balance b  where is_credit = true group by id_game, id_group`);
          for (element of initialDataC.rows) {
            if (element.id_group !== null) {
              const externalData = await db.query(`select game_name from detail_external_shopping_cart where id_product = ${element.id_game} and "group" = ${element.id_group}`);
              labelsC.push(externalData.rowCount > 0 ? externalData.rows[0].game_name : '');
              seriesC.push(Number(element.movimientos));
            } else {
              const internalData = await db.query(`select product_name from product where id_product = ${element.id_game}`);
              labelsC.push(internalData.rowCount > 0 ? internalData.rows[0].product_name : '');
              seriesC.push(Number(element.movimientos));
            }
          }

          return res.status(200).json(response(true, {
            labels: labelsC,
            series: seriesC
          }));
        case '14':
          const labelsT = [];
          const seriesT = [];
          const initialDataT = await db.query(`select b.id_game, b.id_group, count(*) as movimientos, sum(amount) as monto from balance b  where is_credit = false group by id_game, id_group`);
          for (element of initialDataT.rows) {
            if (element.id_group !== null) {
              const externalData = await db.query(`select game_name from detail_external_shopping_cart where id_product = ${element.id_game} and "group" = ${element.id_group}`);
              labelsT.push(externalData.rowCount > 0 ? externalData.rows[0].game_name : '');
              seriesT.push(Number(element.monto));
            } else {
              const internalData = await db.query(`select product_name from product where id_product = ${element.id_game}`);
              labelsT.push(internalData.rowCount > 0 ? internalData.rows[0].product_name : '');
              seriesT.push(Number(element.monto));
            }
          }

          return res.status(200).json(response(true, {
            labels: labelsT,
            series: seriesT
          }));
        case '15':
          const labelsCT = [];
          const seriesCT = [];
          const initialDataCT = await db.query(`select b.id_game, b.id_group, count(*) as movimientos, sum(amount) as monto from balance b  where is_credit = false group by id_game, id_group`);
          for (element of initialDataCT.rows) {
            if (element.id_group !== null) {
              const externalData = await db.query(`select game_name from detail_external_shopping_cart where id_product = ${element.id_game} and "group" = ${element.id_group}`);
              labelsCT.push(externalData.rowCount > 0 ? externalData.rows[0].game_name : '');
              seriesCT.push(Number(element.movimientos));
            } else {
              const internalData = await db.query(`select product_name from product where id_product = ${element.id_game}`);
              labelsCT.push(internalData.rowCount > 0 ? internalData.rows[0].product_name : '');
              seriesCT.push(Number(element.movimientos));
            }
          }

          return res.status(200).json(response(true, {
            labels: labelsCT,
            series: seriesCT
          }));
        case '16':
          if (!id_product) return res.status(400).json(response(false, 'Id del producto no especificado'));
          const initialDataCTPI = await db.query(`select product.product_name, count(*) as movimientos, sum(amount) as monto from balance b inner join product on product.id_product = b.id_game where is_credit = true and id_group is null and b.id_game = ${id_product} group by product.id_product`);
          const initialDataCTPE = await db.query(`select product.product_name, count(*) as movimientos, sum(amount) as monto from balance b inner join product on product.id_product = b.id_game where is_credit = false and id_group is null and b.id_game = ${id_product} group by product.id_product`);
          const labelsCTP = ['Ventas', 'Devoluciones'];
          const seriesCTP = [initialDataCTPI.rowCount > 0 ? Number(initialDataCTPI.rows[0].monto) : 0, initialDataCTPE.rowCount > 0 ? Number(initialDataCTPE.rows[0].monto) : 0];

          return res.status(200).json(response(true, {
            labels: labelsCTP,
            series: seriesCTP,
            game_name: initialDataCTPI.rowCount > 0 ? initialDataCTPI.rows[0].product_name : ''
          }));
        case '17':
          if (!id_product) return res.status(400).json(response(false, 'Id del producto no especificado'));
          const initialDataCTPIC = await db.query(`select product.product_name, count(*) as movimientos, sum(amount) as monto from balance b inner join product on product.id_product = b.id_game where is_credit = true and id_group is null and b.id_game = ${id_product} group by product.id_product`);
          const initialDataCTPEC = await db.query(`select product.product_name, count(*) as movimientos, sum(amount) as monto from balance b inner join product on product.id_product = b.id_game where is_credit = false and id_group is null and b.id_game = ${id_product} group by product.id_product`);
          const labelsCTPC = ['Ingresos', 'Egresos'];
          const seriesCTPC = [initialDataCTPIC.rowCount > 0 ? Number(initialDataCTPIC.rows[0].movimientos) : 0, initialDataCTPEC.rowCount > 0 ? Number(initialDataCTPEC.rows[0].movimientos) : 0];

          return res.status(200).json(response(true, {
            labels: labelsCTPC,
            series: seriesCTPC,
            game_name: initialDataCTPIC.rowCount > 0 ? initialDataCTPIC.rows[0].product_name : ''
          }));
    }
  } catch (error) {
    return res.status(500).json(response(false, { error: 'Error interno de servidor' }));
  }
};

router.get("/getStatistics", getStatistics);

module.exports = router;
