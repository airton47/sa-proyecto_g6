
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Register controller', () => {
  it('Should return 200 status code', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "Romeo",
      lastname: "Marroquin",
      username: "REMS",
      email: `romeomarroquin${(new Date()).toString()}@gmail.com`,
      password: "123456"
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
  
  it('Should return 400 bad request', async () => {
    const response = await supertest(app).post('/auth/register').send({
      region: 1,
      name: "Romeo",
      lastname: "Marroquin",
      username: "REMS",
      email: `romeomarroquin${(new Date()).toString()}@gmail.com`,
    });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
});