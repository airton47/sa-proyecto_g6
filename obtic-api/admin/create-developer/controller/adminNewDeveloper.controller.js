const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");

const CreateNewDeveloper = async (req, res) => {
  let { developerName } = req.body;
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }

  if (!developerName) {
    return res
      .status(400)
      .json(
        response(
          false,
          `Debe ingresar la informacion necesaria: Bad-Request, (developer_name:missing)`
        )
      );
  }

  let query = `
  select developer_name 
  from developer d 
  where LOWER(developer_name) = ('${developerName.toLowerCase()}');`;

  try {
    // Se verifica si ya existe un desarrollador con el mismo nombre
    let { rows } = await db.query(query);

    // return res.status(200).json(response(true,'well done'));
    if (rows.length > 0) {
      return res.status(400).json(
        response(false, {
          message: "Ya se ha ingresado un desarrollador con este nombre.",
          developer: developerName,
        })
      );
    }

    // Se inserta el nuevo de desarrollador en la base de datos
    query = `INSERT INTO developer (developer_name) values ('${developerName}');`;
    let result = await db.query(query);
    console.log("result: ", result);
    return res.status(200).json(
      response(true, {
        message: "Nuevo developer agregado con exito.",
        developer: developerName,
      })
    );
  } catch (error) {
    return res.status(500).json(response(false, error.toString()));
  }
};

router.post("/createDeveloper", CreateNewDeveloper);

module.exports = router;
