
require('dotenv').config();
const supertest = require('supertest');
const app = require('..');

describe('Register controller', () => {
  it('Should return 404 status code', async () => {
    const response = await supertest(app).delete('/dlc').query({
      dlcId: 10,
    });
    expect(response.statusCode).toBe(404);
    expect(response.body.success).toBe(false);
  });

  it('Should return 400 status code', async () => {
    const response = await supertest(app).delete('/dlc').query({});
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
});