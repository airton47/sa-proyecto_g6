const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require("../utils/verify");

const GetUsers = async (req, res) => {
  const { success } = await verify(req.headers.authorization, true);
  if (!success)
    return res.status(401).json(response(false, { error: "GUE-001" }));
  try {
    const users = await db.query(
      `SELECT u.id_user, u.username, u.email, u.name, u.last_name, u.profile_image, u.registration_date, u.active, u.confirmed, u.id_user_type FROM "user" u`
    );
    return res.status(200).json(response(true, users.rows));
  } catch (error) {
    return res.status(500).json(response(false, { error: "GUE-002" })); //Error desconocido (bae de datos no conectada)
  }
};

router.get("/getUsers", GetUsers);

module.exports = router;
