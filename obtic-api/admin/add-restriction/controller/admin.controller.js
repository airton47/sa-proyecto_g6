const router = require("express").Router();
const response = require("../utils/response");
const db = require("../db/database");
const verify = require('../utils/verify');

const AddRestriction = async (req, res) => {
  const { productId, regionId } = req.body;
  const {success} = await verify(req.headers.authorization, true);
  if (!success) return  res.status(401).json(response(false, {error: 'ARE-005'}));
  if (isNaN(Number(productId)) || isNaN(Number(regionId))) return res.status(400).json(response(false, {error: 'ARE-003'})); //faltan parametros
  try {
    const product = await db.query(`SELECT * FROM product WHERE id_product = ${productId}`);
    if (product.rowCount < 1) return res.status(400).json(response(false, {error: 'ARE-001'})); // Producto no existe
    const region = await db.query(`SELECT * FROM region WHERE id_region = ${regionId}`);
    if (region.rowCount < 1) return res.status(400).json(response(false, {error: 'ARE-002'})); // Region no existe
    const repeated = await db.query(`SELECT * FROM region_restriction WHERE id_product = ${productId} AND id_region = ${regionId}`);
    if (repeated.rowCount > 0) return res.status(400).json(response(false, {error: 'ARE-006'})); // Restriccion ya existente
    await db.query(`INSERT INTO region_restriction (id_region, id_product) VALUES (${regionId}, ${productId})`);
    return res.status(200).json(response(true, {regionId, productId}));
  } catch (error) {
    return res.status(500).json(response(false, {error: 'ARE-004'}));//Error desconocido (bae de datos no conectada)
  }
};

router.post("/addRestriction", AddRestriction);

module.exports = router;
