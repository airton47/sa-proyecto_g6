const redis = require("redis");

class CacheService {
  constructor() {
    this.key = "";
    this.redisHost = process.env.REDIS_HOST || "127.0.0.1";
    this.redisPort = process.env.REDIS_PORT || 6379;

    this.redisClient = redis.createClient({
      url: `redis://${this.redisHost}:${this.redisPort}`,
    });
    this.redisClient.connect();
  }

  /**
   *
   * @param {string} id
   */
  async get(id) {
    try {
      const value = await this.redisClient.get(id);
      return value ? JSON.parse(value) : null;
    } catch (error) {
      console.error({
        error,
        msg: `Error obtaining key '${id}' from redis`,
      });
    }
  }

  /**
   *
   * @param {*} id
   * @param {*} data
   */
  async store(id, data) {
    try {
      await this.redisClient.set(id, JSON.stringify(data));
      await this.redisClient.expire(id, 3600);
    } catch (error) {
      console.error({
        errorMessage: error.msg,
        msg: `Error saving key '${id}' from redis`,
        data,
      });
    }
  }

  /**
   *
   * @param {*} id
   */
  async remove(id) {
    try {
      await this.redisClient.del(id);
    } catch (error) {
      console.error({
        errorMessage: error.msg,
        msg: `Error removing key '${id}' from redis`,
      });
    }
  }
}

module.exports = new CacheService();
