require("dotenv").config();
const supertest = require("supertest");
const app = require("..");
//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("Games-Update controller", () => {
  it("Should return 401 status code", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({});
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });
  
  it("Should return 500 status code", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({ idProduct: true, isActive: true })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(500);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, Deben ingresarse todos los parametros obligatorios", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({ idProduct: true, isActive: undefined })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, No se han encontrado coincidencias con id_product proveido.", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({ idProduct: 1, isActive: 1 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, El tipo de datos ingresado es invalido.", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({ idProduct: 7, isActive: 1 })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 200 status code, El estado del producto(juego/DLC) ha sido actualizado", async () => {
    const response = await supertest(app)
      .patch("/games/updateGameStatus")
      .send({ idProduct: 7, isActive: true })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
