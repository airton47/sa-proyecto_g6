# DOCKER

```
docker build . -t fernandvx/hello-world-microservice
```

```
docker push fernandvx/hello-world-microservice
```

```
docker run --name hello-world-microservice -p 3000:80 fernandvx/hello-world-microservice
```

## BODY SAMPLE

- Tipo: `PATCH `

- Ejemplo de peticion:

`http://localhost:3002/games/updateGameStatus`

- Ejemplo de body de request para cambiar el estado de un producto(juego/DLC)

```json
{
  "idProduct": 4,
  "isActive": true
}
```

- Respuesta: ✅ exitosa

```json
{
  "success": true,
  "data": {
    "message": "El estado del producto(juego/DLC) ha sido actualizado"
  }
}
```

- Respuesta: ❌ error por campos obligatorios no ingresados
- Campos:
  - idProduct
  - isActive

```json
{
  "success": false,
  "data": {
    "message": "Deben ingresarse todos los parametros obligatorios"
  }
}
```

- Respuesta: ❌ error por producto(juego) no encontrado

```json
{
  "success": false,
  "data": {
    "message": "No se han encontrado coincidencias con id_product proveido."
  }
}
```

- Respuesta: ❌ error por campos invalidos

```json
{
  "success": false,
  "data": {
    "message": "El tipo de datos ingresado es invalido."
  }
}
```
