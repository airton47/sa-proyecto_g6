const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");

const updateGameStatus = async (req, res) => {
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }

  try {
    // Extrayendo parametros
    let { idProduct, isActive } = req.body;

    // Verificacion de los parametros obligatorios
    if (!idProduct || isActive === undefined) {
      return res.status(400).json(
        response(false, {
          message: "Deben ingresarse todos los parametros obligatorios",
        })
      );
    }

    // Verificar que no se esta ingresando un producto con el mismo nombre/titulo
    let query = `
    select product_name 
    from product 
    where id_product = ${idProduct};
    `;
    let data = await db.query(query);
    if (data.rows.length === 0) {
      return res.status(400).json(
        response(false, {
          message:
            "No se han encontrado coincidencias con id_product proveido.",
        })
      );
    }

    // Verificar que los campos son validos
    if (typeof isActive !== "boolean" || typeof idProduct !== "number") {
      return res.status(400).json(
        response(false, {
          message: "El tipo de datos ingresado es invalido.",
        })
      );
    }

    // return res.status(200).json(response(true, "Well Done"));
    // Ingresar registro del producto en la base de datos
    query = `UPDATE  product 
      SET active = ${isActive}
      WHERE id_product = '${idProduct}';      
    `;
    const { rows } = await db.query(query);
    console.log(rows);
    return res.status(200).send(
      response(true, {
        message: "El estado del producto(juego/DLC) ha sido actualizado",
      })
    );
  } catch (error) {
    return res.status(500).send(response(false, error.toString()));
  }
};

router.patch("/updateGameStatus", updateGameStatus);

module.exports = router;
