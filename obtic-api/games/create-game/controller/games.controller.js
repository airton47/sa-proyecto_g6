const router = require("express").Router();
const response = require("../utils/response");
const verify = require("../utils/verify");
const db = require("../db/database");
const insertComplements = require("../utils/games.utils");

const crateNewGame = async (req, res) => {
  const verifyResponse = await verify(req.headers.authorization, true);
  if (!verifyResponse.success) {
    return res
      .status(verifyResponse.code)
      .json(response(false, verifyResponse.message));
  }

  try {
    // Extrayendo parametros
    let {
      productName,
      description,
      shortDescription,
      releaseDate,
      desktopImage,
      mobileImage,
      logo,
      ageRestriction,
      isGame,
      parentGame,
      developer,
      categories,
      price,
      images,
    } = req.body;

    // Verificacion de los parametros obligatorios
    if (
      !productName ||
      !description ||
      !releaseDate ||
      !ageRestriction ||
      isGame === null ||
      !developer ||
      categories.length === 0
    ) {
      return res.status(400).json(
        response(false, {
          message: "Deben ingresarse todos los parametros obligatorios",
        })
      );
    }

    // Verificar que no se esta ingresando un producto con el mismo nombre/titulo
    let query = `
    select product_name 
    from product 
    where LOWER(product_name) = '${productName.toLowerCase()}' and is_game = ${isGame}
    `;
    let data = await db.query(query);
    if (data.rows.length > 0) {
      return res.status(400).json(
        response(false, {
          message: "Ya se ha registrado un producto con este nombre/titulo",
        })
      );
    }

    // Verificar que el campo parentGame es valido
    if (!isGame) {
      query = `select product_name from product where id_product = ${parentGame};`;
      let products = await db.query(query);
      if (products.rows.length === 0) {
        return res.status(400).json(
          response(false, {
            message: "El campo 'parentGame' es invalido",
          })
        );
      }
    }

    // return res.status(200).json(response(true, "Well Done"));
    // Ingresar registro del producto en la base de datos
    if (isGame) {
      desktopImage = null;
      shortDescription = null;
      parentGame = null;
    } else {
      mobileImage = null;
      logo = null;
      images = [];
    }
    query = `INSERT INTO product (
      product_name,
      description,
      short_description,
      release_date,
      desktop_image,
      mobile_image,
      logo,
      age_restriction,
      is_game,
      parent_game
    ) values (
      '${productName}',
      '${description}',
      ${shortDescription ? `'${shortDescription}'` : shortDescription},
      '${releaseDate}',
      ${desktopImage ? `'${desktopImage}'` : desktopImage},
      ${mobileImage ? `'${mobileImage}'` : mobileImage},
      ${logo ? `'${logo}'` : logo},
      '${ageRestriction}',
      ${isGame},
      ${parentGame}
    ) RETURNING id_product`;
    const { rows } = await db.query(query);
    await insertComplements(
      rows[0].id_product,
      categories,
      developer,
      images,
      price
    );
    return res.status(200).send(
      response(true, {
        message: "El nuevo producto(game/DLC) se ha registrado con exito",
      })
    );
  } catch (error) {
    console.log(error);
    return res.status(500).send(response(false, error.toString()));
  }
};

router.post("/createGame", crateNewGame);

module.exports = router;
