# DOCKER

```
docker build . -t fernandvx/obtic-games-create-game-microservice
```

```
docker push fernandvx/obtic-games-create-game-microservice
```

```
docker run --name games-create-game-microservice -p 3000:80 fernandvx/obtic-games-create-game-microservice
```

## BODY SAMPLE

- Tipo: `POST `

- Ejemplo de peticion:

`http://localhost:3002/games/createGame`

- Body de request v1: Cuando no se incluye el campo 'parentGame'

```json
{
  "productName": "The Legend of Zelda: Breath of the Wild",
  "description": "ENTRA EN UN MUNDO DE AVENTURA. Olvida todo lo que sabes sobre los juegos de The Legend of Zelda. Entra en un mundo de descubrimientos, exploración y aventura en The Legend of Zelda: Breath of the Wild, un nuevo juego de la aclamada serie que rompe con las convenciones. Viaja por prados, bosques y cumbres montañosas para descubrir qué ha sido del asolado reino de Hyrule en esta maravillosa aventura a cielo abierto.",
  "shortDescription": " es un videojuego de acción-aventura de 2017 de la serie The Legend of Zelda, desarrollado por la filial Nintendo",
  "releaseDate": "2017-03-03 00:00:00.000",
  "desktopImage": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "mobileImage": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "logo": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "ageRestriction": "E 10+",
  "isGame": true
}
```

- Body de request v1: Cuando no si se incluye el campo 'parentGame'

```json
{
  "productName": "The Legend of Zelda: Breath of the Wild",
  "description": "ENTRA EN UN MUNDO DE AVENTURA. Olvida todo lo que sabes sobre los juegos de The Legend of Zelda. Entra en un mundo de descubrimientos, exploración y aventura en The Legend of Zelda: Breath of the Wild, un nuevo juego de la aclamada serie que rompe con las convenciones. Viaja por prados, bosques y cumbres montañosas para descubrir qué ha sido del asolado reino de Hyrule en esta maravillosa aventura a cielo abierto.",
  "shortDescription": " es un videojuego de acción-aventura de 2017 de la serie The Legend of Zelda, desarrollado por la filial Nintendo",
  "releaseDate": "2017-03-03 00:00:00.000",
  "desktopImage": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "mobileImage": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "logo": "https://www.max.com.gt/media/catalog/product/cache/40cff66e483d5074b1ae49efef994171/h/a/hacpaaaaa.png",
  "ageRestriction": "E 10+",
  "isGame": true,
  "parentGame": "4"
}
```

- Respuesta: ✅ exitosa

```json
{
  "success": true,
  "data": {
    "message": "El nuevo producto(game/DLC) se ha registrado con exito"
  }
}
```

- Respuesta: ❌ error por campos obligatorios no ingresados
- Campos:
  - productName
  - description
  - shortDescription
  - releaseDate
  - ageRestriction
  - isGame

```json
{
  "success": false,
  "data": {
    "message": "Deben ingresarse todos los parametros obligatorios"
  }
}
```

- Respuesta: ❌ error cuando campo parentGame es invalido, ocurre porque el id_producto ingresado no coincide con ningun registro en la base de datos

```json
{
  "success": false,
  "data": {
    "message": "El campo 'parentGame' es invalido"
  }
}
```

- Respuesta: ❌ error por nombre duplicado, ocurre porque se intenta agregar un nuevo juego cuyo titulo/nombre ya ha sido usado por otro que ya se encuentra registrado

```json
{
  "success": false,
  "data": {
    "message": "Ya se ha registrado un producto con este nombre/titulo"
  }
}
```
