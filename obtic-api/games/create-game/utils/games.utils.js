const db = require("../db/database");

const insertDeveloper = async (idProduct, idDeveloper) => {
  await db.query(`
        INSERT INTO
            developer_product
            (
                id_developer,
                id_product
            )
        VALUES
            (
                ${idDeveloper},
                ${idProduct}
            )
    `);
};

const inserCategories = async (idProduct, categories) => {
  let query = `
        INSERT INTO
            category_game
            (
                id_category,
                id_product
            )
        VALUES`;
  for (let category of categories) {
    query = `${query}
                (${category}, ${idProduct}),`;
  }
  await db.query(query.slice(0, -1));
};

const insertImages = async (idProduct, images) => {
  if (images.length === 0) {
    return;
  }
  let query = `
            INSERT INTO
                product_image(
                    id_product,
                    url_image,
                    is_image
                )
            VALUES`;
  for (let image of images) {
    query = `${query}
                (${idProduct}, '${image}', true),`;
  }
  await db.query(query.slice(0, -1));
};

const insertPrice = async (idProduct, price, byRegion) => {
  let query = `
        INSERT INTO
            price(
                start_date,
                end_date,
                active,
                price,
                discount,
                id_product,
                by_region
            )
        VALUES
            (
                '2022-09-30',
                '2022-12-31',
                true,
                ${price},
                0,
                ${idProduct},
                ${byRegion}
            )`;
  await db.query(query);
};

const insertComplements = async (
  idProduct,
  categories,
  developer,
  images,
  price
) => {
  await inserCategories(idProduct, categories);
  await insertDeveloper(idProduct, developer);
  await insertImages(idProduct, images);
  await insertPrice(idProduct, price, false);
  await insertPrice(idProduct, price, true);
};

module.exports = insertComplements;
