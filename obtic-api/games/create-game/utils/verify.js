const axios = require("axios");

const getVerify = async (token) => {
  const verifyUrl = `${process.env.API_HOST}/auth/verify/`;
  try {
    const response = await axios.post(verifyUrl, { token: token });
    return response.data.data.payload;
  } catch (e) {
    return false;
  }
};

const verify = async (token, admin) => {
  if (!token) {
    return {
      success: false,
      code: 401,
      message: "NO proveyo el token de autenticación",
    };
  }
  const verifyResponse = await getVerify(token.split(" ")[1]);
  if (verifyResponse) {
    if (verifyResponse.id_user_type != 1 && admin) {
      return {
        success: false,
        code: 403,
        message: "No tiene los permisos suficientes para realizar esta acción",
      };
    }
    return {
      success: true,
      payload: verifyResponse,
    };
  } else {
    return {
      success: false,
      code: 401,
      message: "Autenticación errónea",
    };
  }
};

module.exports = verify;
