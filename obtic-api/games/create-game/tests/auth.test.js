require("dotenv").config();
const supertest = require("supertest");
const app = require("..");

//-------saltar obtner validacion (se debe mandar el token con .set())
const axios = require("axios");
jest.mock("axios");
axios.post.mockImplementation(() =>
  Promise.resolve({
    data: {
      data: {
        success: true,
        payload: {
          id_user: 2,
          email: "admin@admin.admin",
          username: "admin",
          id_user_type: 1,
          iat: 1664667741,
          exp: 1664674941,
        },
      },
    },
  })
);
//----------------

describe("Create-Games controller", () => {
  it("Should return 401 status code", async () => {
    const response = await supertest(app).post("/games/createGame").send({});
    expect(response.statusCode).toBe(401);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, Deben ingresarse todos los parametros obligatorios", async () => {
    const response = await supertest(app)
      .post("/games/createGame")
      .send({})
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  let name = "TEST GAME" + new Date().toLocaleString();
  it("Should return 200 status code, El nuevo producto(game) se ha registrado con exito", async () => {
    const response = await supertest(app)
      .post("/games/createGame")
      .send({
        productName: name,
        description: name,
        shortDescription: name,
        releaseDate: "20210101",
        desktopImage: "FFF",
        mobileImage: "FFF",
        logo: "FFF",
        ageRestriction: "AO",
        isGame: true,
        parentGame: null,
        developer: "1",
        categories: "1",
        price: "10",
        images: [],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });

  it("Should return 400 status code, Ya se ha registrado un producto con este nombre/titulo", async () => {
    const response = await supertest(app)
      .post("/games/createGame")
      .send({
        productName: name,
        description: name,
        shortDescription: name,
        releaseDate: "20210101",
        desktopImage: "FFF",
        mobileImage: "FFF",
        logo: "FFF",
        ageRestriction: "AO",
        isGame: true,
        parentGame: null,
        developer: "1",
        categories: "1",
        price: "10",
        images: [],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });

  it("Should return 400 status code, El campo 'parentGame' es invalido", async () => {
    const response = await supertest(app)
      .post("/games/createGame")
      .send({
        productName: name,
        description: name,
        shortDescription: name,
        releaseDate: "20210101",
        desktopImage: "FFF",
        mobileImage: "FFF",
        logo: "FFF",
        ageRestriction: "AO",
        isGame: false,
        parentGame: null,
        developer: "1",
        categories: "1",
        price: "10",
        images: [],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(400);
    expect(response.body.success).toBe(false);
  });
  it("Should return 400 status code, El nuevo producto(DLC) se ha registrado con exito", async () => {
    const response = await supertest(app)
      .post("/games/createGame")
      .send({
        productName: name + "DLC",
        description: name,
        shortDescription: name,
        releaseDate: "20210101",
        desktopImage: "FFF",
        mobileImage: "FFF",
        logo: "FFF",
        ageRestriction: "AO",
        isGame: false,
        parentGame: "5",
        developer: "1",
        categories: "1",
        price: "10",
        images: [],
      })
      .set({ authorization: "abc" });
    expect(response.statusCode).toBe(200);
    expect(response.body.success).toBe(true);
  });
});
